<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Error <?=$error_code;?>
		</h1>
		<!--<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Examples</a></li>
			<li class="active">404 error</li>
		</ol>-->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="error-page">
			<h2 class="headline text-yellow" style="margin-top: -20px;"> <?=$error_code?></h2>

			<div class="error-content">
				<h3><i class="fa fa-warning text-yellow"></i> Oops! <?=$heading?></h3>
				<p>
					<?=$message?><br/>
					Silahkan kembali ke <?=anchor(base_url(), 'dashboard');?>
				</p>

			</div>
			<!-- /.error-content -->
		</div>
		<!-- /.error-page -->
	</section>
	<!-- /.content -->
</div>