
<section class="content-header">
    <h1>
        <?=GetJenisBO($jenis_bo);?>
        <small>Data Entry</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Entry</li>
        <li class="active"><?=GetJenisBO($jenis_bo);?></li>
    </ol>


</section>

<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="form-group form-groups-bordered">

                    <div class="col-sm-4">
                        <?= form_input(array('type' => 'text', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal Berangkat')); ?>
                    </div>


                    <div class="col-sm-4">
                        <?= form_input(array('type' => 'text', 'name' => 'no_body', 'value' => @$no_body, 'class' => 'form-control', 'id' => 'txt_no_body', 'placeholder' => 'No Body')); ?>
                    </div>




                </div>
                <div class="form-group form-groups-bordered">
                    <div class="col-sm-8">
                        <?= form_dropdown(array("selected" => @$id_spj, 'name' => 'id_spj', 'value' => "", 'class' => 'form-control', 'id' => 'dropdown_id_spj', 'placeholder' => 'SPJ'), array_merge(array("" => "Pilih SPJ"), @$listspj)); ?>
                    </div>
                </div>
                <div class="form-group form-groups-bordered">

                    <div class="col-sm-4">
                        <?= form_input(array('type' => 'text', 'name' => 'no_spj', 'value' => @$no_spj, 'class' => 'form-control', 'id' => 'txt_no_spj', 'placeholder' => 'NO SPJ')); ?>
                        <?= form_input(array('type' => 'hidden', 'name' => 'jenis_bo', 'value' => $jenis_bo_int)); ?>
                    </div>


                    <div class="col-sm-4">
                        <?= form_input(array('type' => 'text', 'name' => 'no_random', 'value' => @$no_random, 'class' => 'form-control', 'id' => 'txt_no_random', 'placeholder' => 'Kode SPJ')); ?>
                    </div>

                    <div class="col-sm-2">
                        <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                    </div>


                </div>

            </form>


        </div>
        <div class="box-body" >
            <table class="tablestadt" >
                <tbody>
                    <tr>
                        <td >
                            <b>No.SPJ</b>
                        </td>
                        <td style="width: 25%">
                            <span id="lbl_no_spj"></span>
                        </td>
                        <td>
                            <b>Tanggal</b>
                        </td>
                        <td style="width: 25%">
                            <span id="lbl_tanggal"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>No Body</b>
                        </td>
                        <td>
                            <span id="lbl_no_body"></span>
                        </td>
                        <td>
                            <b>Jam</b>
                        </td>
                        <td>
                            <span id="lbl_jam"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>No Polisi</b>
                        </td>
                        <td>
                            <span id="lbl_nopol"></span>
                        </td>
                        <td>
                            <b>Jenis</b>
                        </td>
                        <td>
                            <span id="lbl_jenis"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Asal Barat</b>
                        </td>
                        <td>
                            <span id="lbl_asal_barat"></span>
                        </td>
                        <td>
                            <b>Tujuan Timur</b>
                        </td>
                        <td>
                            <span id="lbl_tujuan_timur"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Asal Timur</b>
                        </td>
                        <td>
                            <span id="lbl_asal_timur"></span>
                        </td>
                        <td>
                            <b>Tujuan Barat</b>
                        </td>
                        <td>
                            <span id="lbl_tujuan_barat"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Kode SPJ</b>
                        </td>
                        <td>
                            <span id="lbl_kode_spj"></span>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="border-top-style: solid; border-top-width: 1px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Pengemudi 1</b>
                        </td>
                        <td>
                            <span id="lbl_nama_pengemudi_1"></span>
                        </td>
                        <td>
                            <b>NIC</b>
                        </td>
                        <td>
                            <span id="lbl_nic_pengemudi_1"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Pengemudi 2</b>
                        </td>
                        <td>
                            <span id="lbl_nama_pengemudi_2"></span>
                        </td>
                        <td>
                            <b>NIC</b>
                        </td>
                        <td>
                            <span id="lbl_nic_pengemudi_2"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Kernet</b>
                        </td>
                        <td>
                            <span id="lbl_nama_kernet"></span>
                        </td>
                        <td>
                            <b>NIC</b>
                        </td>
                        <td>
                            <span id="lbl_nic_kernet"></span>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
        <div class="box-body">
            <div class=" headerbutton" style="position:relative;margin-bottom:0px">
                <?php echo anchor("", 'Create', 'class="btn btn-success" id="btt_create"'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover table-operasional" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</section>
<style>
    .table-operasional th, .table-operasional td{
        padding-right:10px !important;
    }
</style>
<script type="text/javascript">
    var table;


    function stadtbatal(id_stadt) {
        swal({
            title: "Anda yakin akan membatalkan biaya operasional berikut ini?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya Batalkan!",
            closeOnConfirm: true
        },
                function () {
                    $.ajax({
                        type: 'POST',
                        url: baseurl + 'index.php/<?=$jenis_bo;?>/bo_batal',
                        dataType: 'json',
                        data: {
                            id_stadt: id_stadt
                        },
                        success: function (data) {
                            if (data.st)
                            {
                                messagesuccess(data.msg);
                                table.fnDraw(false);
                            } else
                            {
                                messageerror(data.msg);
                            }

                        },
                        error: function (xhr, status, error) {
                            messageerror(xhr.responseText);
                        }
                    });
                });
    }
    $("#dropdown_id_spj").change(function () {
        UpdateSPJ();
    });

    $("#txt_no_spj").change(function () {
        $("#dropdown_id_spj").val(0);
    });

    function UpdateSPJ()
    {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/spj/get_one_spj',
            dataType: 'json',
            data: {
                id_spj: $("#dropdown_id_spj").val()
            },
            success: function (data) {
                if (data.st)
                {
                    $("#lbl_no_spj").html(data.obj.no_spj);
                    $("#txt_no_spj").val(data.obj.no_spj);
                    $("#lbl_no_body").html(data.obj.no_body);
                    $("#lbl_nopol").html(data.obj.nopol);
                    $("#lbl_asal_barat").html(data.obj.kode_asal_barat + " / " + data.obj.nama_asal_barat);
                    $("#lbl_asal_timur").html(data.obj.kode_asal_timur + " / " + data.obj.nama_asal_timur);
                    $("#lbl_kode_spj").html(data.obj.no_random);
                    $("#lbl_tanggal").html(DefaultDateFormat(data.obj.tanggal));
                    $("#lbl_jam").html(TimeFormat(data.obj.jam));
                    $("#lbl_jenis").html(data.obj.nama_jenis_spj);
                    $("#lbl_tujuan_timur").html(data.obj.kode_tujuan_timur + " / " + data.obj.nama_tujuan_timur);
                    $("#lbl_tujuan_barat").html(data.obj.kode_tujuan_barat + " / " + data.obj.nama_tujuan_barat);
                    $("#lbl_nama_pengemudi_1").html(data.obj.nama_pengemudi_1);
                    $("#lbl_nama_pengemudi_2").html(data.obj.nama_pengemudi_2);
                    $("#lbl_nic_pengemudi_1").html(data.obj.nic_pengemudi_1);
                    $("#lbl_nic_pengemudi_2").html(data.obj.nic_pengemudi_2);
                    $("#lbl_nama_kernet").html(data.obj.nama_kernet);
                    $("#lbl_nic_kernet").html(data.obj.nic_kernet);


                } else
                {
                    $("#lbl_no_spj").html("");
                    $("#lbl_no_body").html("");
                    $("#lbl_nopol").html("");
                    $("#lbl_asal_barat").html("");
                    $("#lbl_asal_timur").html("");
                    $("#lbl_kode_spj").html("");
                    $("#lbl_tanggal").html("");
                    $("#lbl_jam").html("");
                    $("#lbl_jenis").html("");
                    $("#lbl_tujuan_timur").html("");
                    $("#lbl_tujuan_barat").html("");
                    $("#lbl_nama_pengemudi_1").html("");
                    $("#lbl_nama_pengemudi_2").html("");
                    $("#lbl_nic_pengemudi_1").html("");
                    $("#lbl_nic_pengemudi_2").html("");
                    $("#lbl_nama_kernet").html("");
                    $("#lbl_nic_kernet").html("");
                }
                table.fnDraw(true);

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }

    $("form#frm_search").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/spj/get_pencarian_spj',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    clearmessage();
                    $("#dropdown_id_spj").empty();
                    $('#dropdown_id_spj').append($('<option>', {
                        value: "0",
                        text: "Pilih SPJ"
                    }));
                    var keyindex = 0;
                    $.each(data.listspj, function (i, item) {
                        if (i == 0)
                            keyindex = item.id_spj;
                        $('#dropdown_id_spj').append($('<option>', {
                            value: item.id_spj,
                            text: item.no_spj + " => " + item.no_body + " => " + item.no_random + " => " + item.kode_asal_barat + " => " + item.kode_tujuan_timur + " => " + item.kode_asal_timur + " => " + item.kode_tujuan_barat
                        }));
                    });
                    $('#dropdown_id_spj').val(keyindex);
                    UpdateSPJ();
                } else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });


        return false;
    })
    $("#btt_create").click(function () {
        if ($("#lbl_no_spj").html() != "")
        {
            window.location.href = baseurl + "index.php/<?=$jenis_bo;?>/create/" + $("#lbl_no_spj").html();
        } else
        {
            messageerror("Pilih SPJ Terlebih Dahulu");
        }

    })

    $(document).ready(function () {
        $('.datepicker').datepicker({
            autoclose: true,
            format: 'dd M yyyy',
            weekStart: 1,
            language: "id",
            todayHighlight: true,
        });
        <?php if (!CheckEmpty(@$id_spj)) { ?>
                $("#dropdown_id_spj").val(<?= @$id_spj; ?>).change();
        <?php } ?>
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({

            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            searching: false,
            bLengthChange: false,
            ordering: false,
            scrollX: false,
            ajax: {"url": "<?=$jenis_bo;?>/getdatabo", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                { data: "id_bo", width: "20px", title:"#", class: "text-center"}
//                , {data: "tanggal", orderable: false, title: "Tanggal", width:"80px",
//                    mRender: function (data, type, row) {
//                        return  DefaultDateFormat(data);
//                    }}
                , {data: "no_bo", orderable: false, title: "No Biaya Operasional", width:"200px"}
                , {data: "no_spj", orderable: false, title: "No SPJ", width:"150px"}
                , {data: "no_body", orderable: false, title: "No Body", width:"70px"}
                , {data: "nominal", orderable: false, title: "Total Biaya", width:"100px", className:"text-right",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                , {data: "is_batal", orderable: false, title: "Status", width:"150px",
                    mRender: function (data, type, row) {
                        var status = "";
                        if(row['status'] == 1){
                            status = "";
                        }else if(row['status'] == 2){
                            status = "Dihapus";
                        }
                        if(data == 1){
                            status = "Batal";
                        }
                        return status;
                    }}
                ,
                {
                    "data": "id_bo",
                    "orderable": false,
                    "className": "text-left",
                    mRender : function(data, type, row){
                        var tombol = '<a href="javascript:;" class="btn btn-danger btn-xs" onclick="batalkanBo('+data+');return false;"><i class="fa fa-remove"></i> Batal</a>';
                        return tombol;
                    }
                }
            ],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });

    function batalkanBo(id_bo) {
        swal({
                title: "Anda yakin akan membatalkan <?=GetJenisBO($jenis_bo);?> ini?",
//                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya!",
                closeOnConfirm: true
            },
            function () {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/<?=$jenis_bo;?>/batalkan',
                    dataType: 'json',
                    data: {id_bo : id_bo},
                    success: function (resp) {
                        if (resp.st)
                        {
                            messagesuccess(resp.msg);
                            table.fnDraw(false);
                            closemodalboostrap();
                        }
                        else
                        {
                            modaldialogerror(resp.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        modaldialogerror(xhr.responseText);
                    }
                });
            });
    }
</script>
