<?php

/**
 * PHPExcel for Codeigniter
 *
 * @version		1.8
 */
require_once APPPATH . "/third_party/PHPExcel.php";

class Excel extends PHPExcel {

    private $_defCol = 'Error: Please define column set!';
    private $_pass = 'jangandiisi';

    public function __construct() {
        parent::__construct();
    }

    /*
     * function to generate excel file
     * parameter key :
     * sNAMESS => sheet name, sFILENAM => file name, colSet => array of columns, rowSet => result set
     * colSet key :
     * colName => column name, fontSize => font size, bold => bold text(boolean), colWidth=> column width, valueKey => cell value; key on rowSet array, format => cell format
     */

    public function genExcel($parameter, $protect = true, $pass = null) {
        $objPHPExcel = new PHPExcel();
        $sNAMESS = $sFILENAM = "";
        $headInfo = $mainTitle = array();
        $rowNo = 1;

        $cellBg = array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
                'argb' => "#000"
            )
        );

        $cellBorder = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        foreach ($parameter as $key => $value) {
            ${$key} = $value;
        }
        if (!isset($colSet)) {
            echo $this->_defCol;
            die();
        } else {
            $jumlahKolom = count($colSet);
            if ($jumlahKolom == 0) {
                echo $this->_defCol;
                die();
            } else {
                
            }
        }

        $objPHPExcel->setActiveSheetIndex(0);

        if ($protect) {
            if (CheckEmpty($pass)) {
                $pass = $this->_pass;
            }
            $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
            $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
            $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
            $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
            $objPHPExcel->getActiveSheet()->getProtection()->setPassword($pass);
        }
        $objPHPExcel->getActiveSheet()->setTitle($sNAMESS);

        if ($mainTitle) {
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $rowNo, $mainTitle['text']);

            if (isset($mainTitle['style'])) {
                $style = $mainTitle['style'];
                $infoStyle = $objPHPExcel->getActiveSheet()->getStyle('A' . $rowNo);

                if (isset($style['align'])) {
                    $infoStyle->getAlignment()->setHorizontal($this->hAlign($style['align']));
                }

                if (isset($style['fontSize'])) {
                    $infoStyle->getFont()->setSize($style['fontSize']);
                }

                if (isset($style['bold'])) {
                    $infoStyle->getFont()->setBold(true);
                }
            }

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowNo . ':' . $this->getAlphas(count($colSet) - 1) . $rowNo);
            $rowNo++;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowNo . ':' . $this->getAlphas(count($colSet) - 1) . $rowNo);
            $rowNo++;
        }

        if (count($headInfo)) {
            foreach ($headInfo as $key => $value) {
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $rowNo, $value['label']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $rowNo, $value['value']);

                if (isset($value['style'])) {
                    $style = $value['style'];
                    $infoStyle = $objPHPExcel->getActiveSheet()->getStyle('A' . $rowNo);

                    if (isset($style['align'])) {
                        $infoStyle->getAlignment()->setHorizontal($this->hAlign($style['align']));
                    }

                    if (isset($style['fontSize'])) {
                        $infoStyle->getFont()->setSize($style['fontSize']);
                    }

                    if (isset($style['bold'])) {
                        $infoStyle->getFont()->setBold(true);
                    }
                }

                $objPHPExcel->getActiveSheet()->mergeCells('C' . $rowNo . ':' . $this->getAlphas(count($colSet) - 1) . $rowNo);

                $rowNo++;
            }
            $rowNo++;
        }

        $objPHPExcel->getActiveSheet()->getRowDimension($rowNo)->setRowHeight(25);
        $loop = 1;
        $colArray = 0;
        foreach ($colSet as $colvalue) {
            foreach ($colvalue as $keycol => $valuecol) {
                ${$keycol} = $valuecol;
            }

            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getFill()->applyFromArray($cellBg);

            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getFont()->setBold(true)
                    ->setName('Verdana')
                    ->setSize(10)
                    ->getColor()->setRGB('FFFFFF');

            if (isset($colName)) {
                if ($colName != "") {
                    $objPHPExcel->getActiveSheet()->setCellValue($this->getAlphas($colArray) . $rowNo, $colName);
                }
            }
            if (isset($colWidth)) {
                if ($colWidth != "") {
                    $objPHPExcel->getActiveSheet()->getColumnDimension($this->getAlphas($colArray))->setWidth($colWidth);
                }
            }
            if (isset($fontSize)) {
                if ($fontSize != 0) {
                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getFont()->setSize($fontSize);
                }
            }
            if (isset($bold)) {
                if ($bold) {
                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getFont()->setBold(true);
                }
            }
            $classvertical = $this->vAlign();
            if (isset($vAlign)) {
                $classvertical = $this->vAlign($vAlign);
            }
            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getAlignment()->setVertical($classvertical);

            $classhorizontal = $this->hAlign('center');
            if (isset($hAlign)) {
                $classhorizontal = $this->hAlign($hAlign);
            }
            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getAlignment()->setHorizontal($classhorizontal);
            $loop++;
            $colArray++;
        }

        if (isset($rowSet) && count($rowSet)) {
            $nomor = 1;
            $rowloc = $rowNo + 1;
            $start = 0;
            $end = $this->getAlphas(0) . $rowloc;
            foreach ($rowSet as $key => $value) {
                $colArray = 0;
                foreach ($colSet as $colvalue) {
                    foreach ($colvalue as $keycol => $valuecol) {
                        if ($keycol == "valueKey") {
                            if ($valuecol == "nomor") {
                                $valueval = $nomor;
                                if (isset($value[$valuecol])) {
                                    $valueval = $value[$valuecol];
                                }
                            } else {
                                if(empty($value[$valuecol]))
                                {
                                     $valueval = "";
                                }
                                else {
                                     $valueval = $value[$valuecol];
                                }
                               
                            }
                            $objPHPExcel->getActiveSheet()->setCellValue($this->getAlphas($colArray) . $rowloc, $valueval);
                            $objPHPExcel->getActiveSheet()->getColumnDimension($this->getAlphas($colArray))->setAutoSize(true);

                            if ($start === 0) {
                                $start = $this->getAlphas($colArray) . $rowloc;
                            }

                            $end = $this->getAlphas($colArray) . $rowloc;
                        }
                        if ($keycol == "format") {
                            switch ($valuecol) {
                                case 'datetime':
                                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
                                    break;
                                case 'angkakoma':
                                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getNumberFormat()->setFormatCode('_(#,##0.00_);_(\(#,##0.00\);_("-"??_);_(@_)');
                                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getAlignment()->setHorizontal($this->hAlign('right'));
                                    break;
                                case 'nominal':
                                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getNumberFormat()->setFormatCode('_(* #,##0_);_(* (#,##0);_(* "-"_);_(@_)');
                                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getAlignment()->setHorizontal($this->hAlign('right'));
                                    break;
                                case 'jam':
                                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getNumberFormat()->setFormatCode('h:mm');
                                    break;
                            }
                        }
                    }
                    $colArray++;
                }
                $rowloc++;
                $nomor++;
            }
            
            $objPHPExcel->getActiveSheet()->getStyle($start . ':' . $end)->applyFromArray($cellBorder);

            if (isset($footInfo)) {
                foreach ($footInfo as $key => $row) {
                    $colArray = 0;
                    foreach ($row as $val) {
                        if (isset($val['text']))
                            $objPHPExcel->getActiveSheet()->setCellValue($this->getAlphas($colArray) . $rowloc, $val['text']);

                        if (isset($val['format']) && $val['format'] == "nominal") {
                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getNumberFormat()->setFormatCode('_(* #,##0_);_(* (#,##0);_(* "-"_);_(@_)');
                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getAlignment()->setHorizontal($this->hAlign('right'));
                        }

                        if (isset($val['bold']) && (boolean) $val['bold'] == true)
                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getFont()->setBold(true);

                        if (isset($val['align']) && !empty($val['align']))
                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getAlignment()->setHorizontal($this->hAlign($val['align']));

                        if (isset($val['border']) && (boolean) $val['border'] == true)
                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->applyFromArray($cellBorder);

                        if (isset($val['merge'])) {
                            $until = $colArray + $val['merge'];
                            $objPHPExcel->getActiveSheet()->mergeCells($this->getAlphas($colArray) . $rowloc . ':' . $this->getAlphas($until - 1) . $rowloc);

                            if (isset($val['border']) && (boolean) $val['border'] == true)
                                $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc . ':' . $this->getAlphas($until - 1) . $rowloc)->applyFromArray($cellBorder);


                            $colArray = $until;
                        }else {
                            $colArray++;
                        }
                    }

                    $rowloc++;
                }
            }
            
            if ($colSum) {



                $rowNo = $rowloc + 1;


                $objPHPExcel->getActiveSheet()->setCellValue('A' . $rowNo, "Summary");

                if (isset($mainTitle['style'])) {
                    $style = $mainTitle['style'];
                    $infoStyle = $objPHPExcel->getActiveSheet()->getStyle('A' . $rowNo);

                    if (isset($style['align'])) {
                        $infoStyle->getAlignment()->setHorizontal($this->hAlign($style['align']));
                    }

                    if (isset($style['fontSize'])) {
                        $infoStyle->getFont()->setSize($style['fontSize']);
                    }

                    if (isset($style['bold'])) {
                        $infoStyle->getFont()->setBold(true);
                    }
                }

                $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowNo . ':' . $this->getAlphas(count($colSet) - 1) . $rowNo);
                $rowNo++;
                $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowNo . ':' . $this->getAlphas(count($colSet) - 1) . $rowNo);
                $rowNo++;


                $colArray = 0;

                foreach ($colSum as $colvalue) {
                    foreach ($colvalue as $keycol => $valuecol) {
                        ${$keycol} = $valuecol;
                    }

                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getFill()->applyFromArray($cellBg);

                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getFont()->setBold(true)
                            ->setName('Verdana')
                            ->setSize(10)
                            ->getColor()->setRGB('FFFFFF');

                    if (isset($colName)) {
                        if ($colName != "") {
                            $objPHPExcel->getActiveSheet()->setCellValue($this->getAlphas($colArray) . $rowNo, $colName);
                        }
                    }
                    if (isset($colWidth)) {
                        if ($colWidth != "") {
                            $objPHPExcel->getActiveSheet()->getColumnDimension($this->getAlphas($colArray))->setWidth($colWidth);
                        }
                    }
                    if (isset($fontSize)) {
                        if ($fontSize != 0) {
                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getFont()->setSize($fontSize);
                        }
                    }
                    if (isset($bold)) {
                        if ($bold) {
                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getFont()->setBold(true);
                        }
                    }
                    $classvertical = $this->vAlign();
                    if (isset($vAlign)) {
                        $classvertical = $this->vAlign($vAlign);
                    }
                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getAlignment()->setVertical($classvertical);

                    $classhorizontal = $this->hAlign('center');
                    if (isset($hAlign)) {
                        $classhorizontal = $this->hAlign($hAlign);
                    }
                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getAlignment()->setHorizontal($classhorizontal);
                    $loop++;
                    $colArray++;
                }

                if (isset($rowSum)) {
                    $nomor = 1;
                    $rowloc = $rowNo + 1;
                    $start = 0;
                    $end = $this->getAlphas(0) . $rowloc;
                    $summarytotal = array();
                    $issum = false;
                    foreach ($colSum as $key => $colvalue) {
                        $summarytotal[$key] = 0;
                    }

                    foreach ($rowSum as $key => $value) {
                        $colArray = 0;
                        foreach ($colSum as $sumkey => $colvalue) {
                            foreach ($colvalue as $keycol => $valuecol) {

                                if ($keycol == "valueKey") {
                                    if ($valuecol == "nomor") {
                                        $valueval = $nomor;
                                        if (isset($value[$valuecol])) {
                                            $valueval = $value[$valuecol];
                                        }
                                    } else {
                                        $valueval = $value[$valuecol];
                                    }
                                    $objPHPExcel->getActiveSheet()->setCellValue($this->getAlphas($colArray) . $rowloc, $valueval);
                                    $objPHPExcel->getActiveSheet()->getColumnDimension($this->getAlphas($colArray))->setAutoSize(true);

                                    if ($start === 0) {
                                        $start = $this->getAlphas($colArray) . $rowloc;
                                    }

                                    $end = $this->getAlphas($colArray) . $rowloc;
                                }
                                if ($keycol == "format") {
                                    switch ($valuecol) {
                                        case 'datetime':
                                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
                                            break;
                                        case 'angkakoma':
                                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getNumberFormat()->setFormatCode('_(#,##0.00_);_(\(#,##0.00\);_("-"??_);_(@_)');
                                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getAlignment()->setHorizontal($this->hAlign('right'));
                                            $summarytotal[$sumkey] += $valueval;
                                            $issum = true;
                                            break;
                                        case 'nominal':
                                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getNumberFormat()->setFormatCode('_(* #,##0_);_(* (#,##0);_(* "-"_);_(@_)');
                                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getAlignment()->setHorizontal($this->hAlign('right'));
                                            $summarytotal[$sumkey] += $valueval;
                                            $issum = true;
                                            break;
                                        case 'jam':
                                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getNumberFormat()->setFormatCode('h:mm');
                                            break;
                                    }
                                }
                            }
                            $colArray++;
                        }
                        $rowloc++;
                        $nomor++;
                    }
                    $colArray = 0;
                   
                    if ($issum) {
                        foreach ($summarytotal as $key => $colvalue) {
                            $objPHPExcel->getActiveSheet()->setCellValue($this->getAlphas($colArray) . $rowloc, $colvalue);
                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getNumberFormat()->setFormatCode('_(* #,##0_);_(* (#,##0);_(* "-"_);_(@_)');
                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getAlignment()->setHorizontal($this->hAlign('right'));
                            $colArray++;
                        }
                        $objPHPExcel->getActiveSheet()->setCellValue($this->getAlphas(0) . $rowloc, "Total");
                        $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowloc)->getFont()->setBold(true)
                                ->setName('Verdana')
                                ->setSize(10)
                                ->getColor()->setRGB('FFFFFF');
                        $rowloc++;
                    }

                    $objPHPExcel->getActiveSheet()->getStyle($start . ':' . $end)->applyFromArray($cellBorder);

                }
            }
            
        }


        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $sFILENAM . '.xlsx"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function getAlphas($index) {
        $alphas = range("A", "Z");
        if (($index + 1) > 26) {
            $rep = ($index / 26) - 1;
            $mod = $index % 26;
            $column = $alphas[$rep] . $alphas[$mod];
        } else {
            $column = $alphas[$index];
        }

        return $column;
    }

    /*
     * Import Excel File and return the result in assosiative array
     * example use :
     * foreach($allDataInSheet as $import){
     * 		echo $import['A']; /// will return values of Col A
     * 		echo $import['B']; /// will return values of Col B
     * 		echo $import['C']; /// will return values of Col C
     * 		echo $import['D']; /// will return values of Col D
     * }
     *
     * it will be your file name that you are posting with a form or can pass static name $_FILES["file"]["name"];
     */

    public function importExcel() {
        try {
            $objPHPExcel = PHPExcel_IOFactory::load('uploads/' . $_FILES["file"]["name"]);
        } catch (Exception $e) {
            $this->resp->success = FALSE;
            $this->resp->msg = 'Error Uploading file';
            echo json_encode($this->resp);
            exit;
        }

        return $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
    }

    function createColumnsArray($end_column, $first_letters = '') {
        $columns = array();
        $length = strlen($end_column);
        $letters = range('A', 'Z');

        // Iterate over 26 letters.
        foreach ($letters as $letter) {
            // Paste the $first_letters before the next.
            $column = $first_letters . $letter;
            // Add the column to the final array.
            $columns[] = $column;
            // If it was the end column that was added, return the columns.
            if ($column == strtoupper($end_column))
                return $columns;
        }

        // Add the column children.
        foreach ($columns as $column) {
            // Don't itterate if the $end_column was already set in a previous itteration.
            // Stop iterating if you've reached the maximum character length.
            if (!in_array(strtoupper($end_column), $columns) && strlen($column) < $length) {
                $new_columns = $this->createColumnsArray(strtoupper($end_column), $column);
                // Merge the new columns which were created with the final columns array.
                $columns = array_merge($columns, $new_columns);
            }
        }

        return $columns;
    }

    function hAlign($hAlign = null) {
        $classhorizontal = PHPExcel_Style_Alignment::HORIZONTAL_LEFT;
        if ($hAlign) {
            switch ($hAlign) {
                case 'center':
                    $classhorizontal = PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
                    break;
                case 'justify':
                    $classhorizontal = PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY;
                    break;
                case 'right':
                    $classhorizontal = PHPExcel_Style_Alignment::HORIZONTAL_RIGHT;
                    break;
                case 'left':
                    $classhorizontal = PHPExcel_Style_Alignment::HORIZONTAL_LEFT;
                    break;
            }
        }

        return $classhorizontal;
    }

    function vAlign($vAlign = null) {
        $classvertical = PHPExcel_Style_Alignment::VERTICAL_CENTER;
        if ($vAlign) {
            switch ($vAlign) {
                case 'top':
                    $classvertical = PHPExcel_Style_Alignment::VERTICAL_TOP;
                    break;
                case 'middle':
                    $classvertical = PHPExcel_Style_Alignment::VERTICAL_CENTER;
                    break;
                case 'bottom':
                    $classvertical = PHPExcel_Style_Alignment::VERTICAL_BOTTOM;
                    break;
            }
        }

        return $classvertical;
    }

    public function simpleExcel($html, $filename) {
        header("Content-type: application/x-msdownload");
        header('Content-Disposition: attachment; filename="' . $filename . '.xls"');
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $html;
    }

    public function createExcel($data, $filename, $pass = null) {
        $cellBg = array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
                'argb' => "#000"
            )
        );

        $cellBorder = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
//		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
//		$cacheSettings = array( ' memoryCacheSize ' => '8MB');
//		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        if ($pass) {
            $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
            $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
            $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
            $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(false);
            $objPHPExcel->getActiveSheet()->getProtection()->setPassword($this->_pass);
        }

        $objPHPExcel->getActiveSheet()->setTitle($filename);

//		$rowNo = 1;
        foreach ($data as $key => $row) {
            $rowNo = $key + 1;
            $colArray = 0;
            foreach ($row as $col) {
                if (isset($col['text'])) {
                    $objPHPExcel->getActiveSheet()->setCellValue($this->getAlphas($colArray) . $rowNo, $col['text']);
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue($this->getAlphas($colArray) . $rowNo, '');
                }

                if (isset($col['format'])) {
                    switch ($col['format']) {
                        case 'datetime':
                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
                            break;
                        case 'number':
                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getNumberFormat()->setFormatCode('_(#,##0.00_);_(\(#,##0.00\);_("-"??_);_(@_)');
                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getAlignment()->setHorizontal($this->hAlign('right'));
                            break;
                        case 'angka':
//							$objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray).$rowNo)->getNumberFormat()->setFormatCode('#.##');
                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getAlignment()->setHorizontal($this->hAlign('right'));
                            break;
                        case 'time':
                            $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getNumberFormat()->setFormatCode('h:mm');
                            break;
                        case 'tanggal':
                            $arrTgl = explode("-", $col['text'], 3);
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colArray, $rowNo, PHPExcel_Shared_Date::FormattedPHPToExcel($arrTgl[0], $arrTgl[1], $arrTgl[2]));
                            $objPHPExcel->getActiveSheet()
                                    ->getStyleByColumnAndRow($colArray, $rowNo)
                                    ->getNumberFormat()->setFormatCode(
                                    PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14
                            );
                            break;
                    }
                }

                if (isset($col['bold']) && (boolean) $col['bold'] == true)
                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getFont()->setBold(true);

                if (isset($col['fontSize']) && $col['fontSize'] > 0)
                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getFont()->setSize($col['fontSize']);

                if (isset($col['align']) && !empty($col['align'])) {
                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getAlignment()->setHorizontal($this->hAlign($col['align']));
                } else {
                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getAlignment()->setHorizontal($this->hAlign('left'));
                }

                if (isset($col['vAlign'])) {
                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getAlignment()->setVertical($this->vAlign($col['vAlign']));
                } else {
                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getAlignment()->setVertical($this->vAlign('middle'));
                }

                if (isset($col['border']) && (boolean) $col['border'] == true)
                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->applyFromArray($cellBorder);

                if (isset($col['blacked']) && (boolean) $col['blacked'] == true) {
                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getFill()->applyFromArray($cellBg);
                    $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo)->getFont()->getColor()->setRGB('FFFFFF');
                }

                if (isset($col['width'])) {
                    $objPHPExcel->getActiveSheet()->getColumnDimension($this->getAlphas($colArray))->setWidth($col['width']);
                } else {
//					$objPHPExcel->getActiveSheet()->getColumnDimension($this->getAlphas($colArray))->setAutoSize(true);
                }

                if (isset($col['mergeUp'])) {
                    $from = $rowNo - ($col['mergeUp'] - 1);
                    $objPHPExcel->getActiveSheet()->mergeCells($this->getAlphas($colArray) . $from . ':' . $this->getAlphas($colArray) . $rowNo);
                }

                if (isset($col['merge'])) {
                    $until = $colArray + $col['merge'];
                    $objPHPExcel->getActiveSheet()->mergeCells($this->getAlphas($colArray) . $rowNo . ':' . $this->getAlphas($until - 1) . $rowNo);

                    if (isset($col['border']) && (boolean) $col['border'] == true)
                        $objPHPExcel->getActiveSheet()->getStyle($this->getAlphas($colArray) . $rowNo . ':' . $this->getAlphas($until - 1) . $rowNo)->applyFromArray($cellBorder);

                    $colArray = $until;
                }else {
                    $colArray++;
                }
            }

//			$rowNo++;
        }
//		var_dump('<pre>', $objPHPExcel); die();

        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

}

/* end of file */
