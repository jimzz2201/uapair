<?php

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

class Fb
{
    /**
     * @var Auth
     */
    private $auth;

    public function __construct()
    {
        $jsonFile = APPPATH.'/eticket-7ed99-firebase-adminsdk-fvhm1-14404de243.json';
        $serviceAccount = ServiceAccount::fromJsonFile($jsonFile);

        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->create();

        $this->auth = $firebase->getAuth();
    }

    /**
     * Returns an Auth instance.
     *
     * @return Auth
     */
    public function getAuth(): Auth
    {
        return $this->auth;
    }
    
    function getList($defaultMaxResults = 1000, $defaultBatchSize = 1000){
        return $this->auth->listUsers($defaultMaxResults, $defaultBatchSize);
    }
    
    function getInfo($uid){
        return $this->auth->getUser($uid);
    }
    
    function create($userProperties){
        return $this->auth->createUser($userProperties);
    }
    
    function update($uid, $properties){
        return $this->auth->updateUser($uid, $properties);
    }
    
    function updateCustomField($uid, $customFields){
        return $this->auth->setCustomUserAttributes($uid, $customFields);
    }
    
    function updatePassword($uid, $newPassword){
        return $this->auth->changeUserPassword($uid, $newPassword);
    }
    
    function updateEmail($uid, $newEmail){
        return $this->auth->changeUserEmail($uid, $newEmail);
    }
    
    function disable($uid){
        return $this->auth->disableUser($uid);
    }
    
    function enable($uid){
        return $this->auth->enableUser($uid);
    }
    
    function delete($uid){
        return $this->auth->deleteUser($uid);
    }
    
    function verifyEmail($uid, $redirectUrl=null){
        $this->auth->sendEmailVerification($uid, $redirectUrl);
    }
    
    function resetEmail($email, $redirectUrl=null){
        $this->auth->sendPasswordResetEmail($email, $redirectUrl);
    }

	function verifyToken($idTokenString){
		$result =[
			'status' => true
		];
		try {
			$verifiedIdToken = $this->auth->verifyIdToken($idTokenString, true);

			$result['uid'] = $verifiedIdToken->getClaim('sub');
			$result['user'] = (array)$this->auth->getUser($result['uid']);

		} catch (InvalidToken $ex) {
			$result['status'] = false;
			$result['uid'] = null;
			$result['message'] = $ex->getMessage();
		}

		return $result;
	}
}
