<?php
/**
 * Spout for Codeigniter
 */

require_once APPPATH."/third_party/Spout/Autoloader/autoload.php";

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;

class Spout{

	public function export($filename='export', $data=array())
	{
		$writer = WriterFactory::create(Type::XLSX);

		$writer->setTempFolder('assets/temp/');
		$namaFile = $filename.'.xlsx';
		$filePath = 'assets/temp/' . $namaFile;

		if(isset($data['cols']) && count($data['cols'])){
			$writer->setColumnsWidth($data['cols']);
		}else{
			$cols = [];
			foreach($data['header'] as $k => $head){
				if($k !== 'ht'){
					$l = (strlen($head) < 8) ? 8 : strlen($head);
					array_push($cols, $l);
				}
			}
			$writer->setColumnsWidth($cols);
		}

		if(isset($data['merge'])){
			$writer->setColumnsMerge($data['merge']);
		}

		$borderDefault = (new BorderBuilder())
			->setBorderBottom(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
			->setBorderTop(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
			->setBorderRight(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
			->setBorderLeft(Color::BLACK, Border::WIDTH_THIN, Border::STYLE_SOLID)
			->build();

		$styleHour = (new StyleBuilder())
			->setCellFormat(991)
			->setBorder($borderDefault)
			->build();

		$styleShortDate = (new StyleBuilder())
			->setCellFormat(992)
			->setBorder($borderDefault)
			->build();

		$styleLongDate = (new StyleBuilder())
			->setCellFormat(993)
			->setBorder($borderDefault)
			->build();

		$styleNumber = (new StyleBuilder())
			->setCellFormat(994)
			->setHorizontalAlignment('right')
			->setBorder($borderDefault)
			->build();

		$styleNumberWithBracket = (new StyleBuilder())
			->setCellFormat(995)
			->setHorizontalAlignment('right')
			->setBorder($borderDefault)
			->build();

		$styleNumberWithRp = (new StyleBuilder())
			->setCellFormat(996)
			->setHorizontalAlignment('right')
			->setBorder($borderDefault)
			->build();

		$defaultStyle = (new StyleBuilder())
			->setFontName('Arial')
			->setFontSize(8)
			->setShouldWrapText(false)
			->build();

		$writer->setDefaultRowStyle($defaultStyle)
			->openToFile($filePath);

		$styleTitle = (new StyleBuilder())
			->setFontName('Calibri')
			->setFontSize(20)
			->setFontBold()
			->build();
		$writer->addRowWithStyle($data['title'], $styleTitle);

		$styleInfo = (new StyleBuilder())
			->setFontName('Calibri')
			->setFontSize(10)
			->build();

		if(!isset($data['info'])) {
			$data['info'] = [[""]];
		}
		$writer->addRowsWithStyle($data['info'], $styleInfo);

		$styleHeader = (new StyleBuilder())
			->setFontBold()
			->setFontColor(Color::WHITE)
			->setBackgroundColor(Color::BLACK)
			->setHorizontalAlignment('center')
			->build();

		$writer->addRowWithStyle($data['header'], $styleHeader);

		$styleData = (new StyleBuilder())
			->setBorder($borderDefault)
			->build();

		$writer->addRowsWithStyle($data['row'], $styleData);

		$styleFooter = (new StyleBuilder())
			->setFontBold()
			->setBorder($borderDefault)
			->build();

		if(!isset($data['footer'])) {
			$data['footer'] = [[""]];
		}
		$writer->addRowsWithStyle($data['footer'], $styleFooter);

		if(isset($data['summary_title'])) {
			$writer->addRowWithStyle($data['summary_title'], $styleTitle);
		}
		if(isset($data['summary_header'])) {
			$writer->addRowWithStyle($data['summary_header'], $styleHeader);
		}
		if(isset($data['summary_data'])) {
			$writer->addRowsWithStyle($data['summary_data'], $styleData);
		}
		if(isset($data['summary_footer'])) {
			$writer->addRowsWithStyle($data['summary_footer'], $styleFooter);
		}


		$writer->addRowWithStyle([''], $styleHour);
		$writer->addRowWithStyle([''], $styleShortDate);
		$writer->addRowWithStyle([''], $styleLongDate);
		$writer->addRowWithStyle([''], $styleNumber);
		$writer->addRowWithStyle([''], $styleNumberWithBracket);
		$writer->addRowWithStyle([''], $styleNumberWithRp);

		$writer->getCurrentSheet()->setName('penjualan');

		$writer->openToBrowser($namaFile);
		$writer->close();

	}

}

/* end of file */
