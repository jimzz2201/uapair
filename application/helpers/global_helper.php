<?php

define("STR_PAD_BIG", 32);
define("STR_PAD_SMALL", 62);
define("STR_PAD_DETAIL", 20);

function TambahTanggal($date,$increment)
{
    $date = date_create(DefaultTanggalDatabase($date));
    date_add($date, date_interval_create_from_date_string($increment));
    return DefaultTanggalDatabase($date->format('Y-m-d H:i:s'));
    
}

function MaxSize($keyword,$number)
{
    if(strlen($keyword)>$number)
    {
        return substr($keyword, 0,$number)."...";
    }
    else
    {
        return $keyword;
    }
}
function round_out($value, $places = 0) {
    if ($places < 0) {
        $places = 0;
    }
    $mult = pow(10, $places);
    return ($value >= 0 ? ceil($value * $mult) : floor($value * $mult)) / $mult;
}
function DefaultEmptyDropdown($data,$type="obj",$text="")
{
    
    if(CheckEmpty($data))
    {
        $data=array();
    }
    if($type=="obj"||$type=="")
    {
        $data= array("0"=> CheckEmpty($text)? "":"Pilih ".$text)+$data;
    }
   
    return $data;
}
function GetMessageLogin() {

    return 'Your account is expired , you can retry for login again by click <i><b><a href="' . base_url() . '" this</b></i> form above<br/>';
}

function dumperror($obj) {
    echo "<pre>";
    var_dump($obj);
    echo "</pre>";
}

function AutoIncrement($table, $kode, $column, $length = 6) {
    $CI = get_instance();
    $query = "select ifnull(max(right(" . $column . "," . $length . ")),0) as 'sum' from " . $table . " where " . $column . " like '" . $kode . "%' and " . $column . " is not null";

    $lastnumber = intval($CI->db->query($query)->row()->sum);
    return $kode . substr('000000000' . ($lastnumber + 1), -$length);
}

function CheckKey($arr, $key) {
    if (array_key_exists($key, $arr)) {
        if (trim($arr[$key]) != '') {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function array_msort($array, $cols) {
    $colarr = array();
    foreach ($cols as $col => $order) {
        $colarr[$col] = array();
        foreach ($array as $k => $row) {
            $colarr[$col]['_' . $k] = strtolower($row[$col]);
        }
    }
    $eval = 'array_multisort(';
    foreach ($cols as $col => $order) {
        $eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
    }
    $eval = substr($eval, 0, -1) . ');';
    eval($eval);
    $ret = array();
    foreach ($colarr as $col => $arr) {
        foreach ($arr as $k => $v) {
            $k = substr($k, 1);
            if (!isset($ret[$k]))
                $ret[$k] = $array[$k];
            $ret[$k][$col] = $array[$k][$col];
        }
    }
    return $ret;
}

function GetMessageStatus() {
    $CI = get_instance();

    $status = $CI->session->userdata('st');


    if ($status != null && $status != '') {

        return $status;
    } else {

        return 5;
    }
}

Function DefaultDatePicker($val) {
    if (strtotime($val)) {
        return date('d M Y', strtotime($val));
    } else {
        return "";
    }
}

Function DefaultTimePicker($val = "") {
    if (strtotime($val)) {
        return date('H:i', strtotime($val));
    } else {
        return date("H:i");
    }
}

function ClearMessage() {
    $CI = get_instance();
    $CI->session->unset_userdata('message');
    $CI->session->unset_userdata('messagestatus');
    $CI->session->unset_userdata('title');
}

function OpenModalJavaScript($url, $width = 480, $title = "PopUp") {
    echo "<script language='javascript'>var left = (screen.width/2)-(" . $width . "/2);" . "window.open('" . $url . "','" . $title . "', 'width=" . $width . ", height=600, menubar=yes, scrollbars=yes, resizable=yes,left='+left)" . "</script>";
}

function GetMessage() {
    $CI = get_instance();
    if ($CI->session->userdata('msg') != null && $CI->session->userdata('msg') != '') {
        return $CI->session->userdata('msg');
    } else {
        return '';
    }
}

function GetTitle() {
    $CI = get_instance();
    if ($CI->session->userdata('title') != null && $CI->session->userdata('title') != '') {
        return $CI->session->userdata('title');
    } else {
        return GetMessageStatus() == 1 ? "Konfirmasi" : "Error";
    }
}

function CekSessionUser() {
    $CI = get_instance();
    if ($CI->session->userdata('username') != null && $CI->session->userdata('username') != '') {
        return true;
    } else {
        redirect(base_url() . '?url=' . "http://" . $_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI]);
    }
}

function CheckAdmin($akses = '') {
    $CI = get_instance();
    $CI->load->model("user/m_user");
    $user = $CI->m_user->GetOneUser(GetUsername());
    if ($user != null) {
        if ($user->jenis_user == 'S') {
            return true;
        } else {
            redirect(base_url() . 'index.php/user/login');
            return false;
        }
    } else {
        CheckSession();
        return false;
    }
}

function CekModule($kodemenu, $isredirect = true) {
    if ($kodemenu != "") {
        $userid = GetUserId();
        $CI = get_instance();
        $CI->load->model("akses/m_akses");
        if (!$CI->m_akses->CekAkses($userid, $kodemenu)) {
            if ($isredirect) {
                SetMessageSession(2, "Anda tidak memiliki akses untuk menuju link tersebut");
                redirect(base_url() . 'index.php/main/message');
            }
            return false;
        } else {
            return true;
        }
    } else {
        return true;
    }
}
function SetPrint($id,$dokumen,$jenis)
{
    $CI = get_instance();
    $CI->session->set_userdata(array( 'id_'.$jenis=>$id,'dokumen_'.$jenis=>$dokumen));
}
function ClearPrint($jenis)
{
    $CI = get_instance();
    $CI->session->unset_userdata(array( 'id_'.$jenis,'dokumen_'.$jenis));
}
function GetPrint($jenis)
{
    $CI = get_instance();
    return array("id"=>$CI->session->userdata("id_".$jenis),"dokumen"=>$CI->session->userdata("dokumen_".$jenis));
}
function LoadTemplate($model, $template, $javascript = array(), $css = array()) {
    date_default_timezone_set('Asia/Jakarta');
    IF (CekSessionUser()) {
        $CI = get_instance();
        $userid = GetUserId();
        $CI->load->model("user/m_user");
        $CI->load->model("m_menu");
        $user = $CI->m_user->GetOneUser($userid, 'id_user');
        $model['listmenu'] = $CI->m_menu->GetMenu($user->id_group);
        $model['nama_user_login'] = $user->nama_user;
        $CI->load->model("akses/m_akses");
        $group = $CI->m_akses->GetOneAkses($user->id_group);

        $fromcss = array(
            'assets/css/bootstrap.min.css',
            'assets/css/font-awesome.min.css',
            'assets/css/remodal.css',
            'assets/css/owl.carousel.css',
            'assets/css/owl.theme.css',
            'assets/css/owl.transitions.css',
            'assets/css/meanmenu/meanmenu.min.css"',
            'assets/css/animate.css',
            'assets/css/normalize.css',
            'assets/css/scrollbar/jquery.mCustomScrollbar.min.css',
            'assets/css/notika-custom-icon.css',
            'assets/css/wave/waves.min.css',
            'assets/css/main.css',
            'assets/css/style.css',
            'assets/css/sweetalert.css',
            'assets/plugins/select2/select2.css',
            'assets/css/responsive.css',);

        $fromjavascript = array(
            "assets/js/vendor/jquery-1.12.4.min.js",
            "assets/js/bootstrap.min.js",
            "assets/js/wow.min.js",
            "assets/js/owl.carousel.min.js",
            "assets/js/jquery.scrollUp.min.js",
            "assets/js/meanmenu/jquery.meanmenu.js",
            "assets/js/counterup/jquery.counterup.min.js",
            "assets/js/counterup/waypoints.min.js",
            "assets/js/counterup/counterup-active.js",
            "assets/js/scrollbar/jquery.mCustomScrollbar.concat.min.js",
            "assets/js/flot/jquery.flot.js",
            "assets/js/flot/jquery.flot.resize.js",
            "assets/js/flot/curvedLines.js",
            "assets/js/flot/flot-active.js",
            "assets/js/knob/jquery.knob.js",
            "assets/js/knob/jquery.appear.js",
            "assets/js/knob/knob-active.js",
            "assets/js/wave/waves.min.js",
            "assets/js/wave/wave-active.js",
            "assets/js/todo/jquery.todo.js",
            "assets/js/remodal.js",
            "assets/js/plugins.js",
            "assets/js/chat/moment.min.js",
            "assets/js/alasql.js",
            "assets/js/chat/jquery.chat.js",
            "assets/js/sweetalert.js",
            "assets/js/main.js",
            'assets/plugins/select2/select2.js');

        // DEFAULT TEMPLATE
        $view = 'template/template_atas';

        

        $css = array_merge($fromcss, $css);
        $model['css'] = $css;

        $CI->load->view($view, $model);
        $CI->load->view($template, $model);
        $bottom = array();

        $javascript = array_merge($fromjavascript, $javascript);

        $bottom['javascript'] = $javascript;
        $bottom['messagestatus'] = GetMessageStatus();
        $bottom['openmenu'] = @$model['openmenu'];
        $bottom['message'] = GetMessage();

        $CI->load->view('template/template_bawah', $bottom);
        SetMessageSession(5, '');
    }
}

function LoadVendingTemplate($model, $template, $javascript = array(), $css = array()) {

    IF (CekSessionUser()) {
        $CI = get_instance();
        $userid = GetUserId();
        $CI->load->model("user/m_user");
        $CI->load->model("m_menu");
        $user = $CI->m_user->GetOneUser($userid, 'id_user');

        $fromcss = array(
            'assets/css/AdminLTE.min.css',
            'assets/css/jquery-ui.css',
            'assets/css/remodal.css',
            'assets/css/skins/_all-skins.min.css',
            'assets/plugins/iCheck/flat/blue.css',
            'assets/plugins/morris/morris.css',
            'assets/plugins/datepicker/datepicker3.css',
            'assets/plugins/datatables/dataTables.bootstrap.css',
            'assets/plugins/daterangepicker/daterangepicker.css',
            'assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
            'assets/css/sweetalertnew.css');
        $css = array_merge($fromcss, $css);
        $model['css'] = $css;
        $view = 'template/template_atas_vending';
        $CI->load->view($view, $model);
        $CI->load->view($template, $model);
        $bottom = array();
        $fromjavascript = array(
            'assets/js/jquery-ui.min.js',
            'assets/bootstrap/js/bootstrap.min.js',
            'assets/bootstrap/js/bootstrap-alert-box.js',
            'assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
            'assets/js/app.min.js',
            'assets/js/remodal.js',
            'assets/js/moment.min.js',
            'assets/js/sweetalertnew.js');
        $javascript = array_merge($fromjavascript, $javascript);

        $bottom['javascript'] = $javascript;
        $bottom['messagestatus'] = GetMessageStatus();
        $bottom['openmenu'] = @$model['openmenu'];
        $bottom['message'] = GetMessage();

        $CI->load->view('template/template_bawah_vending', $bottom);
        SetMessageSession(5, '');
    }
}

function CheckEmpty($val) {
    if ($val == '' || $val == '0' || $val == false || $val == null) {
        return true;
    }
    return false;
}

function GetUserId() {
    $CI = get_instance();
    $id = $CI->session->userdata('member_id');
    if ($id != null)
        return $id;
    else
        return 0;
}

function GetGroupId() {
    $CI = get_instance();
    $id = $CI->session->userdata('group_id');
    if ($id != null)
        return $id;
    else
        return 0;
}

function GetMasterUserId() {
    $CI = get_instance();
    $id = $CI->session->userdata('master_id');
    if ($id != null)
        return $id;
    else
        return 0;
}

function SetUserId($Id) {
    $CI = get_instance();
    $CI->session->set_userdata("member_id", $Id);
}

function SetGroupId($Id) {
    $CI = get_instance();
    $CI->session->set_userdata("group_id", $Id);
}

function SetShift($Id) {
    $CI = get_instance();
    $CI->session->set_userdata("shift_id", $Id);
}

function GetShift() {
    $CI = get_instance();
    $id = $CI->session->userdata('shift_id');
    if ($id != null)
        return $id;
    else
        return 0;
}

function SetShiftName($Id) {
    $CI = get_instance();
    $CI->session->set_userdata("shift_name", $Id);
}

function GetShiftName() {
    $CI = get_instance();
    $name = $CI->session->userdata('shift_name');
    if ($name != null)
        return $name;
    else
        return '';
}

function SetMasterUserId($Id) {
    $CI = get_instance();
    $CI->session->set_userdata("master_id", $Id);
}

function SetUsername($username) {
    $CI = get_instance();
    $CI->session->set_userdata("username", $username);
}

function SetPangkalan($id_pangkalan) {
    $CI = get_instance();
    $CI->session->set_userdata("id_pangkalan", $id_pangkalan);
}

function SetTicketingLayout($is_ticketing) {
    $CI = get_instance();
    $CI->session->set_userdata("is_ticketing", $is_ticketing);
}

function GetUsername() {
    $CI = get_instance();
    $username = $CI->session->userdata('username');
    if ($username != null)
        return $username;
    else
        return '';
}

function SetIdSpj($id_spj) {
    $CI = get_instance();
    $CI->session->set_userdata("id_spj", $id_spj);
}

function UnsetIdSpj() {
    $CI = get_instance();
    $CI->session->unset_userdata("id_spj");
}

function GetIdSpj() {
    $CI = get_instance();
    $id_spj = $CI->session->userdata('id_spj');
    if ($id_spj != null)
        return $id_spj;
    else
        return '0';
}

function SetVendingSession() {
    $CI = get_instance();
    $CI->session->set_userdata("vending", 1);
}

function UnsetVendingSession() {
    $CI = get_instance();
    $CI->session->set_userdata("vending", 0);
}

function GetVendingSession() {
    $CI = get_instance();
    return $CI->session->userdata('vending');
}

function GetIdPangkalan() {
    $CI = get_instance();
    $id_pangkalan = $CI->session->userdata('id_pangkalan');
    if ($id_pangkalan != null)
        return $id_pangkalan;
    else
        return NULL;
}

function GetTicketingLayout() {
    $CI = get_instance();
    return $CI->session->userdata('is_ticketing');
}

function RandomPassword($numlength = 8) {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < $numlength; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function SetMessageSession($st, $message) {
    $CI = get_instance();

    $CI->session->set_userdata(array("st" => $st, 'msg' => $message));
}

function ClearSessionData() {
    $CI = get_instance();
    $CI->session->unset_userdata("st");
    $CI->session->unset_userdata("msg");
}

function GetDateNow() {
    return date("Y-m-d H:i:s");
}

function DefaultCurrency($number) {
    if (CheckEmpty($number)) {
        $number = '0';
    }
  
    if(($number* 10 )%10 > 0)
    {
        return number_format(DefaultCurrencyDatabase($number), 2, '.', ','); 
    }
    else
    {
       return number_format(DefaultCurrencyDatabase($number), 0, '', ',');  
    }
    
   
}

function DefaultCurrencyAkuntansi($number) {
    if (CheckEmpty($number)) {
        return '-';
    }
    if ($number < 0)
        return "(" . DefaultCurrency(abs($number)) . ")";
    else
        return number_format(DefaultCurrencyDatabase($number), 0, '', ',');
}

function GetValueRadioButton($rad, $arrayname) {
    $yangdibandingin = $rad;
    if ($arrayname !== "") {
        $yangdibandingin = isset($rad[$arrayname]) ? $rad[$arrayname] : null;
    }
    if (isset($yangdibandingin)) {
        if ($yangdibandingin === "on" || $yangdibandingin) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function ForeignKeyFromDb($val, $detault = null) {
    if ($val != null && $val != '' && $val != '0') {
        return $val;
    } else {
        return $detault;
    }
}

function CheckArray($arr, $key) {
    if (array_key_exists($key, $arr)) {
        if (isset($arr[$key])) {
            if ($arr[$key] != null && $arr[$key] != false && $arr[$key] != '' && is_array($arr[$key])) {
                return $arr[$key];
            } else
                return array();
        }
        else {
            return array();
        }
    } else {
        return array();
    }
}

function DefaultCurrencyDatabase($number = NULL,$jumlah=null) {
    if ($number === NULL)
        return NULL;
    if($jumlah)
    {
        return number_format(str_replace(',', '', $number),0,'.','') ;
    }
    else
        return str_replace(',', '', $number);
}

function Multi_array_search($array, $field, $value) {
    $results = array();

    if (is_array($array)) {
        foreach ($array as $arraysatuan) {
            if ($arraysatuan[$field] == $value) {  //chek the filed against teh value, you can do addional check s(i.e. if field has been set)
                $results[] = $arraysatuan;
            }
        }
    }

    return $results;
}

function SearchArray($array, $search) {

    // Create the result array
    $result = array();

    // Iterate over each array element
    foreach ($array as $key => $value) {

        // Iterate over each search condition
        foreach ($search as $k => $v) {

            // If the array element does not meet the search condition then continue to the next element
            if (!isset($value[$k]) || $value[$k] != $v) {
                continue 2;
            }
        }

        // Add the array element's key to the result array
        $result[] = $key;
    }

    // Return the result array
    return $result;
}

function Multi_array_searchindex($array, $field, $value) {
    $index = -1;

    if (is_array($array)) {
        foreach ($array as $key => $arraysatuan) {
            if ($arraysatuan[$field] == $value) {  //chek the filed against teh value, you can do addional check s(i.e. if field has been set)
                $index = $key;
                return $index;
            }
        }
    }

    return $index;
}

function Datatable($arraytemp) {

    $row = 0;
    foreach ($arraytemp as $arraysatuan) {
        $arraysatuan->DT_RowId = "row" . $row;
        $row++;
    }
    return $arraytemp;
}

function GetErrorDb($tempstring = "") {
    $CI = get_instance();
    $message = "";
    if ($CI->db->error()['message'] != "") {
        if ($CI->db->db_debug == true) {
            $message = preg_replace('/(<\/?p>)+/', ' ', $CI->db->error()['message']);
        } else {
            if ($tempstring != "") {
                $message = $tempstring;
            } else {
                $message = "Terjadi Sesuatu yang salah dengan system";
            }
        }
    }
    if ($message != "") {
        throw new Exception("Database error occured with message : {$message}");
    }
}

function SetValue($val, $return = "0") {
    if ($val == null) {
        return $return;
    } else {
        return $val;
    }
}

function DefaultTanggal($val) {
    if (!$val) {
        return '';
    }
    return date('d M Y', strtotime($val));
}

function GetDateNowWithTime() {
    return date("d M Y h:i:s");
}

function DefaultTanggalDempet($val) {
    return date('d/m/y', strtotime($val));
}

function DefaultTanggalSemestinya($val) {
    return date('m/d/Y', strtotime($val));
}

function DefaultTanggalDatabase($val) {
    return date('Y-m-d', strtotime($val));
}

function DefaultTanggalWithday($val) {
    return date('l , d-M-Y', strtotime($val));
}

function GetConfig() {
    $ci = get_instance();
    return $ci->config;
}

function round_up($value, $places = 0) {
    if ($places < 0) {
        $places = 0;
    }
    $mult = pow(10, $places);
    return ceil($value * $mult) / $mult;
}

function CheckSession() {
    if (GetUserId() == 0) {
        redirect(base_url() . 'index.php/user/login');
    }
}

// generate data list from table, type option are 'json' or 'obj'
function GetTableData($table, $id, $col, $where = array(), $type = 'json', $order = array(), $where_in = array(), $join = array()) {
    $CI = get_instance();
    $list = array();
    if (count($where)) {
        $CI->db->where($where);
    }
    if (count($where_in)) {
        $CI->db->where_in($where_in[0], $where_in[1]);
    }
    $CI->db->select($table . ".*");
    if ($order) {
        $CI->db->order_by($order[0], $order[1]);
    }
    if ($join) {
        foreach ($join as $row) {
            $CI->db->join($row["table"], $row["condition"]);
        }
    }

    $data = $CI->db->get($table, 1000)->result_array();

    if ($data) {
        foreach ($data as $row) {
            if (is_array($col)) {
                $text = array();
                foreach ($col as $item) {
                    array_push($text, $row[$item]);
                }
                $text = implode(" - ", $text);
            } else {
                $text = $row[$col];
            }
            if ($type == "json") {
                $list[] = array('id' => (int) $row[$id],
                    'text' => $text,
                );
            } elseif ($type == "obj") {
                $list[$row[$id]] = $text;
            }
        }
    }

    return $list;
}

function GetRowData($table, $where = array(), $where_in = array(), $group_by = array(), $order = array(), $time = false) {
    $CI = get_instance();

    if (count($where)) {
        $CI->db->where($where);
    }
    if (count($where_in)) {
        $CI->db->where_in($where_in[0], $where_in[1]);
    }
    $CI->db->select($table . ".*");
    if ($group_by) {
        $CI->db->group_by($group_by);
    }
    if ($order) {
        $CI->db->order_by($order[0], $order[1]);
    }

    $data = $CI->db->get($table, 1)->row_array();
    if ($time == false) {
        unset($data['updated_by'], $data['updated_date'], $data['created_by'], $data['created_date']);
    }
    return $data;
}

function GetScalarData($table, $key, $match, $col, $alias = null) {
    $result = '';
    $CI = get_instance();
    if ($alias) {
        $CI->db->select($col . ' AS ' . $alias);
    } else {
        $CI->db->select($col);
        $alias = $col;
    }
    $CI->db->where($key, $match);
    $data = $CI->db->get($table)->row_array();
    if ($data) {
        $result = $data[$alias];
    }

    return $result;
}

function GetScalarDataByCond($table, $where = array(), $col, $alias = null) {
    $result = '';
    $CI = get_instance();
    if ($alias) {
        $CI->db->select($col . ' AS ' . $alias);
    } else {
        $CI->db->select($col);
        $alias = $col;
    }
    if ((is_array($where) && count($where)) || !empty($where)) {
        $CI->db->where($where);
    } else {
        $CI->db->where('1 <> 1');
    }

    $data = $CI->db->get($table)->row_array();
    if ($data) {
        $result = $data[$alias];
    }

    return $result;
}

function CountData($table, $where = null, $join = array(), $group_by = null, $where_in_col = null, $where_in = array()) {
    $result = 0;
    $CI = get_instance();
    $CI->db->select("COUNT(*) AS jml");
    $CI->db->from($table);
    if (count($join)) {
        foreach ($join as $item) {
            $CI->db->join($item[0], $item[1]);
        }
    }

    if ($where)
        $CI->db->where($where);

    if ($group_by)
        $CI->db->group_by($group_by);

    if ($where_in && $where_in_col)
        $CI->db->where_in($where_in_col, $where_in);

    $data = $CI->db->get()->row_array();
    if ($data) {
        $result = (int) $data['jml'];
    }

    return $result;
}

function GetJenisTiket($id = null) {
    $jenis = array(
        1 => "PNP",
        2 => "BDB",
        3 => "TNI"
    );

    if ($id) {
        $jenis = $jenis[$id];
    }

    return $jenis;
}

function GetJenisPembayaran($id = null) {
    $jenis = array(
        'TRANSFER' => 'Transfer',
        'CASH' => 'Cash',
    );

    if ($id && array_key_exists($id, $jenis)) {
        $jenis = $jenis[$id];
    }

    return $jenis;
}

function GetJenisPembayaran2($id = null) {
    $jenis = array(
        'TRANSFER' => 'Transfer',
        'CASH' => 'Cash',
//        'CASH & TRANSFER' => 'Cash & Transfer',
    );

    if ($id && array_key_exists($id, $jenis)) {
        $jenis = $jenis[$id];
    }

    return $jenis;
}

function GetStatusKeberangkatan($id = null) {
    $status = array(
        0 => "Planned",
        1 => "Done",
        2 => "Canceled"
    );

    if ($id) {
        $status = $status[$id];
    }

    return $status;
}

function GetMonth($id = null) {
    $month = array();
    $month[1] = "Januari";
    $month[2] = "Februari";
    $month[3] = "Maret";
    $month[4] = "April";
    $month[5] = "Mei";
    $month[6] = "Juni";
    $month[7] = "Juli";
    $month[8] = "Agustus";
    $month[9] = "September";
    $month[10] = "Oktober";
    $month[11] = "November";
    $month[12] = "Desember";

    if ($id) {
        $month = $month[$id];
    }

    return $month;
}

function GetMonthShort($id = null) {
    $month = array();
    $month[1] = "Jan";
    $month[2] = "Feb";
    $month[3] = "Mar";
    $month[4] = "Apr";
    $month[5] = "Mei";
    $month[6] = "Jun";
    $month[7] = "Jul";
    $month[8] = "Agu";
    $month[9] = "Sept";
    $month[10] = "Okt";
    $month[11] = "Nov";
    $month[12] = "Des";

    if ($id) {
        $month = $month[$id];
    }

    return $month;
}

function GetDay($id = null) {
    $day = array();
    $day[0] = "Minggu";
    $day[1] = "Senin";
    $day[2] = "Selasa";
    $day[3] = "Rabu";
    $day[4] = "Kamis";
    $day[5] = "Jumat";
    $day[6] = "Sabtu";

    if ($id != null) {
        $day = $day[$id];
    }

    return $day;
}

function GetJenisBO($id = null) {
    $jenis = array(
        'bopb' => "BOPB",
        'bopt' => "BOPT",
        'titipan' => "Titipan"
    );

    if ($id) {
        $jenis = $jenis[$id];
    }

    return $jenis;
}

function GetJenisBiaya($id = null) {
//    $jenis = array(
//        '1' => "Barat",
//        '2' => "Timur",
//        '3' => "Tambahan Barat",
//        '4' => "Tambahan Timur",
//        '5' => "Lain-lain 1",
//        '6' => "Lain-lain 2",
//        '7' => "BOPB",
//        '8' => "BOPT",
//        '9' => "Titipan",
//    );
    $jenis = GetTableData('tic_jenis_biaya', 'id_jenis_biaya', 'nama_jenis_biaya', array('status' => 1, 'id_jenis_biaya !=' => 9), 'obj');

    if ($id) {
        $jenis = $jenis[$id];
    }

    return $jenis;
}

function GetYaTidak($id = null) {
    $opsi = array(
        1 => 'Ya',
        0 => 'Tidak',
    );

    if ($id !== null) {
        $opsi = $opsi[$id];
    }

    return $opsi;
}

function GetTipeKomisi($id = null) {
    $opsi = array(
        0 => 'Nominal',
        1 => 'Persen',
    );

    if ($id) {
        $opsi = $opsi[$id];
    }

    return $opsi;
}

function GetPerms($class, $action) {
    $CI = & get_instance();
    if (null === $CI->config->item('acl')) {
        $CI->config->load('acl');
    }
    $rules = $CI->config->item('acl');
    $group_id = $CI->session->userdata('group_id');
    $perms = TRUE;

    if (array_key_exists($class, $rules)) {
        $perms = FALSE;
        if (isset($rules[$class][$action][$group_id])) {
            $perms = $rules[$class][$action][$group_id];
        }
    }

    return $perms;
}

function checkPerms($class, $action, $redirect = null) {
    return TRUE;
    if (Getperms($class, $action) == FALSE) {
        $resp = array(
            'message' => "Anda tidak memiliki hak untuk mengakses halaman ini!",
            'heading' => 'An Error Was Encountered',
            'error_code' => 403,
        );
        if (!empty($redirect)) {
            redirect($redirect);
        }
        set_status_header($resp['error_code']);
        ob_start();
        LoadTemplate($resp, 'errors/html/error_page');
        $buffer = ob_get_contents();
        ob_end_clean();
        echo $buffer;
        exit(1);
    } else {
        return TRUE;
    }
}

function getAlphas($index, $row = null) {
    $index += -1;
    $alphas = range("A", "Z");
    if (($index + 1) > 26) {
        $rep = ($index / 26) - 1;
        $mod = $index % 26;
        $column = $alphas[$rep] . $alphas[$mod];
    } else {
        $column = $alphas[$index];
    }

    if ($row) {
        $column .= $row;
    }

    return $column;
}

function getJenisAliranKas($id = null) {
    $jenis = [
        1 => 'Pemasukan',
        2 => 'Pengeluaran',
    ];
    if ($id) {
        $jenis = $jenis[$id];
    }

    return $jenis;
}

function getJenisBiayaKas($id = null) {
    $jenis = [
        1 => 'Setoran',
        2 => 'Biaya Terminal',
        3 => 'Administrasi Kantor',
        4 => 'Biaya lain-lain',
        5 => 'Setoran Resume',
    ];
    if ($id) {
        $jenis = $jenis[$id];
    }

    return $jenis;
}

function bolehLihatReservasi() {
    $isAllowed = true;
    if (GetTicketingLayout()) {
        $isAllowed = false;
        $tipe_bus_user = explode(",", GetScalarData('tic_user', 'id_user', GetUserId(), 'id_tipe_bus'));
        if (count($tipe_bus_user) && in_array(7, $tipe_bus_user)) {
            $isAllowed = true;
        }
    }

    return $isAllowed;
}

function availableTipeBus($type = 'obj') {
    $tipe_bus_user = GetScalarData('tic_user', 'id_user', GetUserId(), 'id_tipe_bus');
    if (!CheckEmpty($tipe_bus_user) && $tipe_bus_user != "0") {
        $list_tipe_bus = GetTableData('tic_tipe_bus', 'id_tipe_bus', array('nama_tipe'), array('status' => 1), $type, array('id_tipe_bus', 'ASC'), array("id_tipe_bus", explode(",", $tipe_bus_user)));
    } else {
        $list_tipe_bus = GetTableData('tic_tipe_bus', 'id_tipe_bus', array('nama_tipe'), array('status' => 1), $type);
    }

    return $list_tipe_bus;
}

function DefaultTanggalIndo($val, $day = false) {
    $tanggal = date('j', strtotime($val)) . ' ' . GetMonth(date('n', strtotime($val))) . ' ' . date('Y', strtotime($val));
    if ($day) {
        $tanggal = GetDay(date('w', strtotime($val))) . ', ' . $tanggal;
    }
    return $tanggal;
}

function DefaultTanggalIndoShort($val, $day = false) {
    $tanggal = date('j', strtotime($val)) . ' ' . GetMonthShort(date('n', strtotime($val))) . ' ' . date('y', strtotime($val));
    if ($day) {
        $tanggal = GetDay(date('w', strtotime($val))) . ', ' . $tanggal;
    }
    return $tanggal;
}

function DefaultCurrencyIndo($number) {
    if (CheckEmpty($number)) {
        $number = '0';
    }
    return number_format(DefaultCurrencyDatabase($number), 0, ',', '.');
}

function GetKey() {
    $setting = GetRowData('tic_setting', ['id_setting' => 1]);
    $mode = $setting['online_payment_mode'];
    $col = ($mode == "sandbox") ? "_sandbox" : "";
    $client_key = $setting['midtrans' . $col . '_client_key'];
    $server_key = $setting['midtrans' . $col . '_server_key'];
    return [
        'client_key' => $client_key,
        'server_key' => $server_key,
        'is_production' => $mode == "production" ? true : false
    ];
}
