<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emailer extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->auth();
		$this->load->model('api/m_member', 'member_model');
		/*$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'user@gmail.com',
			'smtp_pass' => '',
			'mailtype'  => 'html',
			'charset' => 'utf-8',
			'wordwrap' => TRUE

		);
		$this->load->library('email', $config);*/
		$this->load->library('email');
	}

	public function deliver(){
		date_default_timezone_set("Asia/Jakarta");

		$time = date('Y-m-d H:i:s', strtotime('-10 minutes'));
		$this->db->select("no_ref, email, attempt");
		$this->db->from("tic_email_job");
		$this->db->where('executed', 0, false);
		$this->db->where('in_time >=', (int)strtotime($time), false);
		$this->db->where('attempt <', 3, false);

		$list = $this->db->get()->result_array();
		if(count($list)){

			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;

			$this->email->initialize($config);

			foreach($list as $row){
				$no_ref = $row['no_ref'];
				$email = $row['email'];
				$attempt = $row['attempt'];

				$emailBody = $this->emailContent($no_ref);
//				var_dump($emailBody);
				if($emailBody['status']){
					$this->email
						->from('noreply@sinarjayagroup.co.id', 'Sinar Jaya Group')
						->to($email)
						->subject('Bukti Reservasi Online Sinar Jaya #'.$no_ref)
						->message($emailBody['content'])
						->set_mailtype('html');

					if( ! $this->email->send()){
						$update = [
							'attempt' => $attempt + 1
						];

						echo $no_ref.': failed'."\n";
					}else{
						$update = [
							'executed' => 1,
							'executed_on' => date('Y-m-d H:i:s'),
							'attempt' => $attempt + 1
						];

						echo $no_ref.': success'."\n";
					}
					$this->db->update('tic_email_job', $update, ['no_ref' => $no_ref]);
				}else{
					/*$update = [
						'attempt' => $attempt
					];*/
					echo $no_ref.': payment not settled'."\n";
				}

//				$this->db->update('tic_email_job', $update, ['no_ref' => $no_ref]);
			}
		}else{
			echo "no job await"."\n";
		}
    }

	function emailContent($no_ref){
		$result = ['status' => false];
		$row = $this->member_model->getTransaction($no_ref, true);
		$transaksi = [];
		if(count($row)){
			if(!CheckEmpty($row['id_transaksi'])) {
				if((int)$row['id_berangkat'] > 0){
					$berangkat = GetRowData('tic_berangkat', ['id_berangkat'=> $row['id_berangkat']]);

					$id_generate = $berangkat['id_generate'];
					$routeNaik = $this->member_model->getRouteNaik($id_generate);
					$routeTurun = $this->member_model->getRouteTurun($id_generate);
					$route = implode(" - ", $routeNaik)." - ".implode(" - ", $routeTurun);

					$awal_berangkat = $berangkat['tanggal'] .' '.$berangkat['jam'];
					$turun = GetRowData('tic_point_turun',['id_generate'=>$id_generate, 'id_pangkalan_turun'=>$row['id_pangkalan_tujuan']]);
					$durasi = $turun['jam'];
					$query = $this->db->query("SELECT ADDTIME(STR_TO_DATE(CONCAT('".$awal_berangkat."'), '%Y-%m-%d %H:%i:%s'), '".$durasi."') AS estimasi")->row_array();
					$estimasi = $query['estimasi'];
					$jam_sampai = DefaultTanggalIndo($estimasi, true) . ' ' . date("H:i", strtotime($estimasi));

					$asal = GetRowData('tic_pangkalan', ['id_pangkalan'=>$row['id_pangkalan_asal']]);
					$tujuan = GetRowData('tic_pangkalan', ['id_pangkalan'=>$row['id_pangkalan_tujuan']]);
					$pangkalan_asal = [
						'nama_pangkalan' => $row['asal'],
						'nama_kota' => $this->member_model->getKota($row['id_pangkalan_asal']),
						'alamat' => $asal['alamat'] ? $asal['alamat'] : "Di Sini",
						'lat' => $asal['lat'] ? $asal['lat'] : -6.28160508,
						'lng' => $asal['lng'] ? $asal['lng'] : 107.09840308,
					];

					$pangkalan_tujuan = [
						'nama_pangkalan' => $row['tujuan'],
						'nama_kota' => $this->member_model->getKota($row['id_pangkalan_tujuan']),
						'alamat' => $tujuan['alamat'] ? $tujuan['alamat'] : "Di Sana",
						'lat' => $tujuan['lat'] ? $tujuan['lat'] : -6.28160508,
						'lng' => $tujuan['lng'] ? $tujuan['lng'] : 107.09840308,
					];

					$transaksi = [
						'id_transaksi' => $row['id_transaksi'],
						'id_berangkat' => $row['id_berangkat'],
						'no_ref' => $row['no_ref'],
						'jurusan' => $row['asal'] . ' - ' . $row['tujuan'],
						'jml_kursi' => $row['jml_kursi'],
						'jadwal_berangkat' => DefaultTanggalIndo($row['tanggal_berangkat'], true) . ' ' . date("H:i", strtotime($row['jam_berangkat'])),
						'tgl_berangkat' => DefaultTanggalIndoShort($row['tanggal_berangkat'], true),
						'jam_berangkat' => date("H:i", strtotime($row['jam_berangkat'])),
						'estimasi_durasi' => $this->timeToHM($durasi),
						'estimasi_sampai' => $jam_sampai,
						'tgl_estimasi' => $estimasi ? DefaultTanggalIndoShort($estimasi, true) : null,
						'jam_estimasi' => $estimasi ? date("H:i", strtotime($estimasi)) : null,
						'kelas' => $row['nama_kelas'],
						'total' => DefaultCurrencyIndo($row['total']),
						'no_kursi' => ($row['no_kursi']) ? explode(",", $row['no_kursi']) : null,
						'tgl_transaksi' => $row['transaction_date'],
						'tgl_pembelian' => DefaultTanggalIndo($row['created_date'], true) . ' ' . date("H:i", strtotime($row['created_date'])),
						'expire' => DefaultTanggalIndo($row['expire_date'], true) . ' ' . date("H:i", strtotime($row['expire_date'])),
						'route' => $route,
						'asal' => $pangkalan_asal,
						'tujuan' => $pangkalan_tujuan,
						'pemesan' => [
							'name' => $row['name'],
							'email' => $row['email'],
							'phone' => $row['phone'],
							'no_id' => $row['no_id'],
						]
					];
					$transaksi['penumpang'] = ($row['pnp']) ? explode("|", $row['pnp']) : null;
					$transaksi['no_barcode'] = ($row['barcode']) ? explode("|", $row['barcode']) : null;
				}
			}
		}
		if($transaksi){
			$html = $this->load->view('api/email_template', $transaksi, true);
			$result['status'] = true;
			$result['content'] = $html;
		}

		return $result;
	}

	function timeToHM($durasi=null){
		if(CheckEmpty($durasi)){
			return null;
		}else{
			$t = explode(":", $durasi);
			$lama = '';
			if((int)$t[0] > 0){
				$lama .= (int)$t[0].' jam ';
			}
			if((int)$t[1] > 0){
				$lama .= (int)$t[1].' menit';
			}

			return $lama;
		}
	}

	public function waktu(){
		echo time();
	}

	private function auth(){
		if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])){
			$user = $_SERVER['PHP_AUTH_USER'];
			$pass = $_SERVER['PHP_AUTH_PW'];

			if($user == "SJOnline" && $pass == "s1n4rJaya"){
				return true;
			}else{
				header('HTTP/1.0 401 Unauthorized');
			}
		}else{
			header('HTTP/1.0 401 Unauthorized');
		}
		exit;
	}

}
