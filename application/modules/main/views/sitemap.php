<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Sitemap </h2>
                                <p>Sitemap</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="box box-default form-element-list">
        
        <?php if(!CheckEmpty(@$row)){?>
            <a href="<?php echo base_url().'index.php/main/sitemap'?>"> Back to Header</a>
        <?php }?>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-hover" id="mytable">
                                <thead>
                                    <tr>
                                        <th>
                                            Kode Form
                                        </th>
                                        <th>
                                            Form Name
                                        </th>
                                        <th>
                                            Urut
                                        </th>
                                        <th>
                                            Action
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($listheader as $key => $header) { ?>
                                        <tr>
                                            <td><a href="<?php echo base_url() . 'index.php/main/sitemap/' . $header->id_form ?>"><?php echo $header->kode_form; ?></a></td>
                                            <td><a href="<?php echo base_url() . 'index.php/main/sitemap/' . $header->id_form ?>"><?php echo $header->form_name; ?></a></td>
                                            <td style="width:100px;"><?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'style' => 'padding:2px 10px;height:auto;', 'value' => $header->urut, 'class' => 'form-control input-sm', 'placeholder' => 'Urut')); ?></td>
                                            <td style="width:200px;text-align:center;">
                                                <?php if ($key != 0) { ?>
                                                <button type="button" style="padding:3px 10px;" onclick="up(<?php echo $header->id_form ?>)" class="btn btn-default btn-icon-notika waves-effect"><i class="notika-icon notika-up-arrow"></i> Top</button>
                                                <?php } ?>
                                                <?php if ($key != count($listheader) - 1) { ?>
                                                    <button type="button" style="padding:3px 10px;"  onclick="down(<?php echo $header->id_form ?>)" class="btn btn-default btn-icon-notika waves-effect"><i class="notika-icon notika-down-arrow"></i> Down</button>
                                                <?php } ?>
                                            </td>
                                        </tr>

                                    <?php }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    function up(id)
    {
        $.ajax({
            type: 'GET',
            url: baseurl + 'index.php/main/up/'+id,
            dataType: 'json',
            success: function (data) {
                if (data.st)
                {
                   window.location.reload();
                } else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }

        });
    }
    function down(id)
    {
        $.ajax({
            type: 'GET',
            url: baseurl + 'index.php/main/down/'+id,
            dataType: 'json',
            success: function (data) {
                if (data.st)
                { window.location.reload();
                  
                } else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }

        });
    }

</script>