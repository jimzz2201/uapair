<section class="content-header">
    <h1>
        Account
        <small>Setting Password</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Setting Password</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div id="notification" ></div>
    <div class="box box-default">
        <div class="box-body">
            <div class="row">
                <form id="frmupdateprofile" class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="dropdown_id_country" class="col-sm-2 control-label">Last Password</label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="lastpassword"  value="">
                           </div>
                            <div class="col-sm-6">
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="dropdown_id_country" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="password"  value="">
                                <input type="hidden" class="form-control" name="user_id"  value="<?php echo GetUserId()?>">
                            </div>
                            <div class="col-sm-6">
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="dropdown_id_country" class="col-sm-2 control-label">Confirm Password</label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="confirm"  value="">
                            </div>
                            <div class="col-sm-6">
                            </div>

                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" >Save</button>
                        </div>
                    </div>

                </form>
            </div>



            </section>


            <script>



                $("#frmupdateprofile").submit(function () {
                    $.ajax({
                        type: 'POST',
                        url: baseurl + 'index.php/main/change_password_action',
                        dataType: 'json',
                        data: $(this).serialize(),
                        success: function (data) {
                            if (data.st)
                            {
                                window.location.href = baseurl + "index.php/main/message";
                            }
                            else
                            {
                                messageerror(data.msg);
                            }
                        },
                        error: function (xhr, status, error) {
                            messageerror(xhr.responseText);
                        }
                    });

                    return false;


                })

            </script>



