<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>DIG1 BackOffice</title>
        <script>
            var baseurl="<?php echo base_url()?>";
        </script>
         <link rel="icon" href="<?php echo base_url() ?>favicon.ico" type="image/x-icon">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/iCheck/square/blue.css">
        <script src="<?php echo base_url() ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
         <script src="<?php echo base_url() ?>assets/js/helper.js"></script>
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
             <div class="login-logo">
                <a href="<?php echo base_url() ?>"><b><img src="<?php echo base_url().'assets/images/logo.png'?>" /></b></a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Send Activation Code</p>

                <form id="frmlogin">
                    <div id="notification" ></div>
                    <div class="form-group has-feedback">
                        <input type="text" id="txt_email" name="email" class="form-control" placeholder="Email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" id="txt_activation_code" name="activationcode" class="form-control" placeholder="Activation Code">
                         <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <a href="<?php echo base_url()?>index.php/main/activate_sent">Sent Again the activation code</a>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            
                            <input type="submit" class="btn btn-primary btn-block btn-flat" value="Continue" id="button-sendemailactivation" class="button">
                            
                        </div>
                        <!-- /.col -->
                    </div>
                </form>


            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery 2.2.3 -->
       
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script>
            

            $("form#frmlogin").submit(function () {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/main/activate_process',
                    dataType: 'json',
                    data: $(this).serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            window.location.href=data.url;
                            
                        }
                        else
                        {
                            $("#txt_activation_code").val("");
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });

                return false;
            });
        </script>
    </body>
</html>
