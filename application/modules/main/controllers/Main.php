<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    public function message() {
        LoadTemplate(array(), "main/message_view", array());
    }

    public function sentemail($id) {
        $this->load->model("m_email");
        $this->m_email->SendEmailActivitation($id);
    }
    public function sitemap($id=null) {
        $this->Reordering($id);
        if (CheckEmpty($id)) {
            $this->db->where(array("kode_parent" => null));
        } else {
            $this->db->where(array("kode_parent" => $id));
        }
        $this->db->from("uap_form");
        $this->db->order_by("urut");
        $listheader = $this->db->get()->result();
        $this->db->from("uap_form");
        $this->db->where(array("id_form" => $id));
        $row = $this->db->get()->row();
        LoadTemplate(array("listheader" => $listheader,"row"=>$row), 'main/sitemap');
    }
    public function up($id)
    {
        $this->db->from("uap_form");
        $this->db->where(array("id_form"=>$id));
        $form=$this->db->get()->row();
        
        if($form!=null)
        {
            $this->Reordering($form->kode_parent);
            $urut=$form->urut-1;
            $this->db->from("uap_form");
            $this->db->where(array("kode_parent" => $form->kode_parent,"urut"=>$urut));
            $row=$this->db->get()->row();
            if($row!=null)
            {
                $this->db->update("uap_form",array("urut"=>$urut+1),array("id_form"=>$row->id_form));
            }
            $this->db->update("uap_form",array("urut"=>$urut),array("id_form"=>$id));
        }
        echo json_encode(array("st"=>true));
    }
    public function down($id)
    {
        $this->db->from("uap_form");
        $this->db->where(array("id_form"=>$id));
        $form=$this->db->get()->row();
        
        if($form!=null)
        {
            $this->Reordering($form->kode_parent);
            $urut=$form->urut+1;
            $this->db->from("uap_form");
            $this->db->where(array("kode_parent" => $form->kode_parent,"urut"=>$urut));
            $row=$this->db->get()->row();
            if($row!=null)
            {
                $this->db->update("uap_form",array("urut"=>$urut-1),array("id_form"=>$row->id_form));
            }
            $this->db->update("uap_form",array("urut"=>$urut),array("id_form"=>$id));
        }
        echo json_encode(array("st"=>true));
    }
    
    public function Reordering($kodeparent)
    {
        $this->db->where(array("kode_parent" => $kodeparent));
        $this->db->order_by("urut","asc");
        $this->db->from("uap_form");
        $listmenu = $this->db->get()->result();
        foreach($listmenu as $key=>$item)
        {
            $this->db->update("uap_form",array("urut"=>$key),array("id_form"=>$item->id_form));
        }
    }

    public function LGIActivation() {

        $this->load->model("register/m_register");
        $this->m_register->LGIActivate();
    }

    public function new_password_action() {
        $model = $this->input->post();

        $message = '';
        $data = array();
        $data['st'] = false;

        $this->form_validation->set_rules('password', "Password", 'required');
        $this->form_validation->set_rules('confirm', "Confirm Password", 'required|matches[password]');
        $this->form_validation->set_rules('pin', "Pin", 'required|exact_length[6]|numeric');
        $this->form_validation->set_rules('confirm_pin', "Confirm", 'required|matches[pin]');
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            if ($model['user_id'] != GetUserId()) {
                $message.='This action cannot be completed because you need to refresh your account<br/>';
            } else {
                $this->load->model('user/m_user');
                $this->m_user->set_new_password($model['password'], $model['pin']);
                $this->session->set_userdata("ischangepassword", false);
            }
            $data['st'] = $message == '';
            $data['msg'] = $message;
        }
        echo json_encode($data);
        exit;
    }

    public function newpassword() {

        $session = $this->session->userdata('ischangepassword');

        if (!($session != null && $session == true)) {
            redirect(base_url() . 'index.php/profile');
        }
        LoadTemplate(array(), 'main/vaccount_new_password');
    }

    public function change_password() {


        LoadTemplate(array(), 'main/vaccount_change_password');
    }

    public function change_password_action() {
        $model = $this->input->post();
        $message = '';
        $data['st'] = false;
        $this->load->model("user/m_user");
        $this->form_validation->set_rules('password', "Password", 'required');
        $this->form_validation->set_rules('confirm', "Confirm", 'required|matches[password]');
        $this->form_validation->set_rules('lastpassword', "Last Password", 'required');

        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = validation_errors() . $message;
        } else {
            if ($model['user_id'] != GetUserId()) {
                $message.='This action cannot be completed because you need to refresh your account<br/>';
            } else {
                $message = $this->m_user->profile_change_password($model);
            }

            $data['st'] = $message == '';
            $data['msg'] = $message;
            if ($data['st']) {
                SetMessageSession(1, "Password Has been Updated");
            }
        }
        echo json_encode($data);
        exit;
    }

    public function activationdirectly() {
        $email = $this->input->get("email");
        $activation = $this->input->get("activation");
        $this->load->model("user/m_user");


        $message = $this->m_user->CheckActivation($email);
        if ($message == '') {
            $message = $this->m_user->CheckActivation($email);
            $user = $this->m_user->GetOneUser($email, "mlmtbl_user_master.email");

            if ($user->activation_key != $activation) {
                $message.='Your Activation Code is wrong<br/>';
            }
            if ($user->activation_key != $activation) {
                $message.='Your Activation Code is wrong<br/>';
            } else {
                $user = $this->m_user->GetOneUser($email, "mlmtbl_user_master.email");
                $this->m_user->activate($email,"email");

                if (CheckEmpty($user->password)) {
                    SetUserId($user->id_user);
                    SetUsername($user->username);
                    $this->session->set_userdata('ischangepassword', true);
                    redirect(base_url() . 'index.php/main/newpassword');
                } else {
                    SetMessageSession(1, 'Your Account has been acivated');
                    SetUserId($user->id_user);
                    SetUsername($user->username);
                    redirect(base_url() . 'index.php/main/message');
                }
            }
        }


        echo $message;
    }

    public function TestPassword() {
        $this->load->model("m_email");
        $this->m_email->SendForgotPassword(1);
    }

    public function checktokenautoload() {

        $email = $this->input->get('email');
        $code = $this->input->get('auth');
        $this->load->model('user/m_user');
        if ($this->m_user->AuthUserWithCodemd5($email, $code)) {
            $user = $this->m_user->GetOneUser($email, "email");

            if ($user->status == 2) {
                SetMessageSession(0, "Your account has been disabled by admin, Please contact admin for activate your account");
                redirect('/user/forgot');
            } else {
                SetUserId($user->id_user);
                SetUsername($user->username);
                $this->session->set_userdata('ischangepassword', true);
                redirect('/main/newpassword');
            }
        } else {
            SetMessageSession(0, 'Your Recover Code is expired , you can retry for recover your password again by click form above<br/>');

            redirect('/main/forgot');
        }
    }

    public function checktoken() {
        $model = array();
        $model['title'] = 'Edit Profile';
        $this->load->view("main/vaccount_recover");
    }

    public function sent_forgot_password() {
        $model = $this->input->post();
        $this->form_validation->set_rules('email', "Email", 'required');
        $message = '';
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            $this->load->model('user/m_user');
            $user = $this->m_user->GetOneUser($model['email'], 'email');
            if ($user != null) {
                $this->load->model("m_email");
                $this->m_user->forgot_password($user->id_user);
                $this->m_email->SendForgotPassword($user->id_user);
            } else {
                $message.='The Email is not registered in our system';
            }
            $data['st'] = $message == '';
            $data['msg'] = $message;
        }
        echo json_encode($data);
    }

    public function sent_activation() {
        $model = $this->input->post();
        $this->form_validation->set_rules('email', "Email", 'required');
        $message = '';
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            $this->load->model('user/m_user');
            $user = $this->m_user->GetOneUser($model['email'], 'email');
            if ($user != null) {
                if ($user->status == 0) {
                    $this->load->model("m_email");
                    $this->m_email->SendEmailActivitation($user->id_user);
                } else {
                    $message.='Your account has been activated before';
                }
            } else {
                $message.='The Email is not registered in our system';
            }
            $data['st'] = $message == '';
            $data['msg'] = $message != '' ? $message : "We have sent email step for activation your account";
        }
        echo json_encode($data);
    }

    public function forgot() {
        $this->load->view("logon/forgot_password");
    }

    public function activate_sent() {
        $model = array();
        $model['title'] = 'Edit Profile';
        $this->load->view("main/vaccount_activate_sent");
    }

    public function activate() {
        $model = array();
        $model['title'] = 'Edit Profile';
        $this->load->view("main/vaccount_activate");
    }

    public function activate_process() {
        $message = '';
        $email = $this->input->post("email");
        $activationcode = $this->input->post("activationcode");
        $data['st'] = FALSE;
        $this->form_validation->set_rules('email', "Email", 'required');
        $this->form_validation->set_rules('activationcode', "Activation Code", 'required');
        $url = '';
        $this->load->model("user/m_user");
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            $message = $this->m_user->CheckActivation($email);
            if ($message == '') {
                $user = $this->m_user->GetOneUser($email, "mlmtbl_user_master.email");

                if ($user->activation_key != $activationcode) {
                    $message.='Your Activation Code is wrong<br/>';
                } else {
                    $url.=base_url() . 'index.php/main/activationdirectly?email=' . strtolower($email) . '&&activation=' . $activationcode;
                }
            }
            $data['st'] = $message == '';
            $data['url'] = $url;
            $data['msg'] = $message != '' ? $message : "We have sent email step for activation your account";
        }
        echo json_encode($data);
    }

    public function changepasswordaction() {
        $this->form_validation->set_rules('email', "Email", 'required');
    }

    public function forgot_process() {
        $message = '';
        $email = $this->input->post("email");
        $activationcode = $this->input->post("activationcode");
        $data['st'] = FALSE;
        $this->form_validation->set_rules('email', "Email", 'required');
        $this->form_validation->set_rules('activationcode', "Recover Code", 'required');
        $url = '';
        $this->load->model("user/m_user");
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            $row = $this->m_user->GetOneUser($email, 'email');

            if ($row != null) {

                if ($row->forgot_password == '') {
                    $message.='Your Recover Code is expired , you can retry for recover your password again by click form above<br/>';
                }
                if ($message == '') {
                    if ($row->forgot_password != $activationcode) {
                        $message.='Your Recover Code is wrong<br/>';
                    } else {
                        $url.=base_url() . 'index.php/main/checktokenautoload?email=' . strtolower($email) . '&&auth=' . md5($activationcode);
                    }
                }
            } else {
                $message.='We Cannot found your profil from our database<br/>';
            }
            $data['st'] = $message == '';
            $data['url'] = $url;
            $data['msg'] = $message != '' ? $message : "We have sent email step for activation your account";
        }
        echo json_encode($data);
    }

}
