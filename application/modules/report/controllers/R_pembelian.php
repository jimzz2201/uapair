<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_pembelian extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_pembelian');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K053";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier']=$this->m_supplier->GetDropDownSupplier();
        LoadTemplate($model, "report/v_report_pembelian_index", $javascript, $css);
    }
    
    public function detail() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K054";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier']=$this->m_supplier->GetDropDownSupplier();
        $this->load->model("barang/m_barang");
        $model['list_barang'] = $this->m_barang->GetDropDownBarang();
        LoadTemplate($model, "report/v_report_pembelian_detail", $javascript, $css);        
    }
    
    public function per_group() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K055";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier']=$this->m_supplier->GetDropDownSupplier();
        $this->load->model("barang/m_barang");
        $model['list_barang'] = $this->m_barang->GetDropDownBarang();
        $this->load->model("jenis/m_jenis");
        $model['list_jenis'] = $this->m_jenis->GetDropDownJenis();
        $this->load->model("merek/m_merek");
        $model['list_merek'] = $this->m_merek->GetDropDownMerek();        
        LoadTemplate($model, "report/v_report_pembelian_per_group", $javascript, $css);        
    }
    
    public function pajak_masukan() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K056";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = "assets/js/icheck/icheck.min.js";
        $javascript[] = "assets/js/icheck/icheck-active.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);               
        LoadTemplate($model, "report/v_report_pembelian_pajak_masukan", $javascript, $css);        
    }
    
    public function getdatapembelian() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw"=>1,"recordsTotal"=>"2","recordsFiltered"=>"2","data"=>[]));
    }
        
    public function getdatapembeliandetail() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw"=>1,"recordsTotal"=>"2","recordsFiltered"=>"2","data"=>[]));
    }
    
    public function getdatapembelianpergroup() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw"=>1,"recordsTotal"=>"2","recordsFiltered"=>"2","data"=>[]));
    }
    
    public function getdatapembelianpajakmasukan() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw"=>1,"recordsTotal"=>"2","recordsFiltered"=>"2","data"=>[]));
    }
}

/* End of file Barang.php */