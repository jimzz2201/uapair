<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_piutang extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_akuntansi');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K053";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);  
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $this->load->model("pegawai/m_pegawai");
        $model['list_pegawai']=$this->m_pegawai->GetDropDownPegawai();
        LoadTemplate($model, "report/v_report_piutang_index", $javascript, $css);
    }
    
    public function pembayaran() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K054";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier']=$this->m_supplier->GetDropDownSupplier();  
        $jenis_pembayaran = array();
        $jenis_pembayaran[1] = "All";
        $jenis_pembayaran[2] = "Cash";
        $jenis_pembayaran[3] = "Transfer";
        $jenis_pembayaran[4] = "Cek & Giro";
        $jenis_pembayaran[5] = "Transfer==Giro";
        $model['list_jenis_pembayaran'] = $jenis_pembayaran;
        LoadTemplate($model, "report/v_report_piutang_pembayaran", $javascript, $css);        
    }
    
    public function sisa() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K054";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier']=$this->m_supplier->GetDropDownSupplier();
        LoadTemplate($model, "report/v_report_piutang_sisa", $javascript, $css);        
    }
    
    public function detail() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K054";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier']=$this->m_supplier->GetDropDownSupplier();
        $jenis_pembayaran = array();
        $jenis_pembayaran[1] = "All";
        $jenis_pembayaran[2] = "Cash";
        $jenis_pembayaran[3] = "Transfer";
        $jenis_pembayaran[4] = "Cek & Giro";
        $jenis_pembayaran[5] = "Transfer==Giro";
        $model['list_jenis_pembayaran'] = $jenis_pembayaran;
        LoadTemplate($model, "report/v_report_piutang_detail", $javascript, $css);        
    }  
        
    public function getdatapiutang() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw"=>1,"recordsTotal"=>"2","recordsFiltered"=>"2","data"=>[]));
    }
        
    public function getdatapiutangpembayaran() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw"=>1,"recordsTotal"=>"2","recordsFiltered"=>"2","data"=>[]));
    }
    
    public function getdatapiutangsisa() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw"=>1,"recordsTotal"=>"2","recordsFiltered"=>"2","data"=>[]));
    }
    
    public function getdatapiutangdetail() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw"=>1,"recordsTotal"=>"2","recordsFiltered"=>"2","data"=>[]));
    }       
}

/* End of file Barang.php */