<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_akuntansi extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_akuntansi');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K053";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);  
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $this->load->model("pegawai/m_pegawai");
        $model['list_pegawai']=$this->m_pegawai->GetDropDownPegawai();
        LoadTemplate($model, "report/v_report_akuntansi_index", $javascript, $css);
    }
    
    public function omzet_customer() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K054";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $this->load->model("customer/m_customer");
        $model['list_customer']=$this->m_customer->GetDropDownCustomer();        
        LoadTemplate($model, "report/v_report_akuntansi_omzet_customer", $javascript, $css);        
    }
    
    public function biaya() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K054";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $this->load->model("biaya_master/m_biaya_master");
        $model['list_group']=$this->m_biaya_master->GetDropDownGroup();
        $this->load->model("biaya_master/m_biaya_master");
        $model['list_master']=$this->m_biaya_master->GetDropDownBiayaMaster();
        LoadTemplate($model, "report/v_report_akuntansi_biaya", $javascript, $css);        
    }
    
    public function biaya_bank() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K054";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $this->load->model("biaya_master/m_biaya_master");
        $model['list_group']=$this->m_biaya_master->GetDropDownGroup();
        $this->load->model("biaya_master/m_biaya_master");
        $model['list_master']=$this->m_biaya_master->GetDropDownBiayaMaster();
        LoadTemplate($model, "report/v_report_akuntansi_biaya_bank", $javascript, $css);        
    }  
        
    public function getdataakuntansi() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw"=>1,"recordsTotal"=>"2","recordsFiltered"=>"2","data"=>[]));
    }
        
    public function getdataakuntansiomzetcustomer() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw"=>1,"recordsTotal"=>"2","recordsFiltered"=>"2","data"=>[]));
    }
    
    public function getdataakuntansibiaya() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw"=>1,"recordsTotal"=>"2","recordsFiltered"=>"2","data"=>[]));
    }
    
    public function getdataakuntansibiayabank() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw"=>1,"recordsTotal"=>"2","recordsFiltered"=>"2","data"=>[]));
    }       
}

/* End of file Barang.php */