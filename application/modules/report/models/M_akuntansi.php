<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_akuntansi extends CI_Model {

    public $table = 'uap_pembelian';
    public $id = 'id_pembelian_po_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

}
