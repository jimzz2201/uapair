<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_pembelian extends CI_Model {

    public $table = 'uap_pembelian';
    public $id = 'id_pembelian_po_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function GetDetailPo($id) {
        $this->db->from("uap_pembelian_po_detail");
        $this->db->join("uap_satuan", "uap_pembelian_po_detail.id_satuan=uap_satuan.id_satuan", "left");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_pembelian_po_detail.id_barang", "left");
        $this->db->select("uap_pembelian_po_detail.*,sku_barang as sku,nama_satuan,harga_beli_akhir");
        $this->db->where(array("id_pembelian_po_master" => $id, "uap_pembelian_po_detail.status !=" => 5));
        $listdetail = $this->db->get()->result();
        return $listdetail;
    }
    function GetDetail($id) {
        $this->db->from("uap_pembelian_detail");
        $this->db->join("uap_satuan", "uap_pembelian_detail.id_satuan=uap_satuan.id_satuan", "left");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_pembelian_detail.id_barang", "left");
        $this->db->select("uap_pembelian_detail.*,sku_barang as sku,nama_satuan,harga_beli_akhir");
        $this->db->where(array("id_pembelian_master" => $id, "uap_pembelian_detail.status !=" => 5));
        $listdetail = $this->db->get()->result();
        return $listdetail;
    }
    function GetOnePembelianPo($id, $isfull = false) {
        $this->db->from("uap_pembelian_po_master");
        $this->db->where(array("id_pembelian_po_master" => $id));
        $row = $this->db->get()->row();
        if ($isfull) {

            $row->listdetail = $this->GetDetailPo($id);
        }

        return $row;
    }
    function GetOnePembelian($id, $isfull = false) {
        $this->db->from("uap_pembelian_master");
        $this->db->where(array("id_pembelian_master" => $id));
        $row = $this->db->get()->row();
        if ($isfull) {

            $row->listdetail = $this->GetDetail($id);
            $this->db->from("uap_pembelian_link");
            $this->db->join("uap_pembelian_po_master","uap_pembelian_po_master.id_pembelian_po_master=uap_pembelian_link.id_pembelian_po_master");
            $this->db->where(array("uap_pembelian_link.id_pembelian_master"=>$id));
            $this->db->select("uap_pembelian_po_master.id_pembelian_po_master,nomor_po_master,tgl_po");
            $row->listheader=$this->db->get()->result();
        }

        return $row;
    }
    function PembelianPOManipulate($model) {
        try {
            $message = "";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            $pembelianpo = array();
            $this->load->model("cabang/m_cabang");
            $this->load->model("barang/m_barang");
            $cabang = $this->m_cabang->GetOneCabang($model['id_cabang']);
            $pembelianpo['kepada'] = $model['kepada'];
            $pembelianpo['no_fax'] = $model['no_fax'];
            $pembelianpo['pembuat'] = $model['pembuat'];

            $pembelianpo['keterangan'] = $model['keterangan'];
            $pembelianpo['id_supplier'] = ForeignKeyFromDb($model['id_supplier']);
            $pembelianpo['id_gudang'] = ForeignKeyFromDb($model['id_gudang']);
            $pembelianpo['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
            $pembelianpo['tgl_po'] = DefaultTanggalDatabase($model['tgl_po']);
            $pembelianpo['nominal_po'] = 0;
            $id_pembelian = 0;
            if (CheckEmpty(@$model['id_pembelian_po_master'])) {
                $pembelianpo['created_date'] = GetDateNow();
                $pembelianpo['nomor_po_master'] = AutoIncrement('uap_pembelian_po_master', $cabang->kode_cabang . '/PO/' . date("y") . '/', 'nomor_po_master', 5);
                $pembelianpo['created_by'] = ForeignKeyFromDb($userid);
                $pembelianpo['status'] = '1';
                $this->db->insert("uap_pembelian_po_master", $pembelianpo);
                $id_pembelian = $this->db->insert_id();
            } else {
                $pembelianpo['updated_date'] = GetDateNow();
                $pembelianpo['updated_by'] = ForeignKeyFromDb($userid);
                $id_pembelian = $model['id_pembelian_po_master'];
                $pembeliandb = $this->GetOnePembelianPo($id_pembelian);
                $pembelianpo['nomor_po_master'] = $pembeliandb->nomor_po_master;
                $this->db->update("uap_pembelian_po_master", $pembelianpo, array("id_pembelian_po_master" => $id_pembelian));
            }
            $arraydetailpembelian = array();
            foreach ($model['dataset'] as $key => $det) {
                $detailpembelian = array();
                $detailpembelian['id_barang'] = $det['id_barang'];
                $detailpembelian['price_barang'] = DefaultCurrencyDatabase($det['price_barang']);
                $detailpembelian['nama_barang'] = $det['nama_barang'];
                $detailpembelian['qty'] = DefaultCurrencyDatabase($det['qty']);
                $detailpembelian['disc_1'] = DefaultCurrencyDatabase($det['disc_1']);
                $detailpembelian['disc_2'] = DefaultCurrencyDatabase($det['disc_2']);
                $detailpembelian['subtotal'] = DefaultCurrencyDatabase($det['subtotal']);

                if (CheckEmpty($det['id_pembelian_po_detail'])) {
                    $barang = $this->m_barang->GetOneBarang($det['id_barang']);
                    $detailpembelian['id_satuan'] = ForeignKeyFromDb(@$barang->id_satuan);
                    $detailpembelian['created_date'] = GetDateNow();
                    $detailpembelian['created_by'] = $userid;
                    $detailpembelian['status'] = 1;
                    $detailpembelian['id_pembelian_po_master'] = $id_pembelian;
                    $this->db->insert("uap_pembelian_po_detail", $detailpembelian);
                    $arraydetailpembelian[] = $this->db->insert_id();
                } else {

                    $detailpembelian['updated_date'] = GetDateNow();
                    $detailpembelian['updated_by'] = $userid;
                    if (!CheckEmpty(@$det['id_satuan']))
                        $detailpembelian['id_satuan'] = ForeignKeyFromDb($det['id_satuan']);
                    $this->db->update("uap_pembelian_po_detail", $detailpembelian, array("id_pembelian_po_detail" => $det['id_pembelian_po_detail']));
                    $arraydetailpembelian[] = $det['id_pembelian_po_detail'];
                }
            }


            if (count($arraydetailpembelian) > 0) {
                $this->db->where_not_in("id_pembelian_po_detail", $arraydetailpembelian);
                $this->db->where(array("id_pembelian_po_master" => $id_pembelian));
                $this->db->update("uap_pembelian_po_detail", array("status" => 5, 'updated_date' => GetDateNow(), 'updated_by' => $userid));
            } else {
                $this->db->update("uap_pembelian_po_detail", array("status" => 5, 'updated_date' => GetDateNow()), array("id_pembelian_po_master" => $pembelianpo));
            }




            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data Pembelian PO Sudah di" . (CheckEmpty(@$model['id_pembelian_po_master']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_pembelian, $pembelianpo['nomor_po_master'], 'pembelianpo');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function PembelianManipulate($model) {
        try {
            
            
            $message = "";
            $listheader=CheckArray($model, 'listheader');
            $listpopembelian=array();
            foreach($listheader as $headersatuan)
            {
                array_push($listpopembelian,array("id_pembelian_po_master"=>$headersatuan['id_pembelian_po_master']) );
            }
            
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            $pembelian = array();
            $this->load->model("cabang/m_cabang");
            $this->load->model("supplier/m_supplier");
            $this->load->model("barang/m_barang");
            $cabang = $this->m_cabang->GetOneCabang($model['id_cabang']);
           
            $pembelian['keterangan'] = $model['keterangan'];
            $pembelian['id_supplier'] = ForeignKeyFromDb($model['id_supplier']);
            $pembelian['id_gudang'] = ForeignKeyFromDb($model['id_gudang']);
            $pembelian['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
            $pembelian['tanggal'] = DefaultTanggalDatabase($model['tanggal']);
            $pembelian['jth_tempo'] = DefaultTanggalDatabase($model['jth_tempo']);
            $pembelian['type_ppn'] = $model['type_ppn'];
            $pembelian['type_pembayaran'] = $model['type_pembayaran'];
            $pembelian['type_pembulatan'] = $model['type_pembulatan'];
            $pembelian['disc_pembulatan'] = DefaultCurrencyDatabase($model['disc_pembulatan']);
            $pembelian['nominal_ppn'] = DefaultCurrencyDatabase($model['nominal_ppn']);
            $pembelian['grandtotal'] = DefaultCurrencyDatabase($model['grandtotal']);
            $pembelian['jumlah_bayar'] = DefaultCurrencyDatabase($model['jumlah_bayar'])>$pembelian['grandtotal']?$pembelian['grandtotal']:DefaultCurrencyDatabase($model['jumlah_bayar']);
            $pembelian['total'] = DefaultCurrencyDatabase($model['total']);
            $pembelian['ppn'] = DefaultCurrencyDatabase($model['ppn']);
            $supplier=$this->m_supplier->GetOneSupplier($pembelian['id_supplier']);
            if (CheckEmpty(@$model['id_pembelian_master'])) {
                $pembelian['created_date'] = GetDateNow();
                $pembelian['nomor_master'] = AutoIncrement('uap_pembelian_master', $cabang->kode_cabang . '/PR/' . date("y") . '/', 'nomor_master', 5);
                $pembelian['created_by'] = ForeignKeyFromDb($userid);
                $pembelian['status'] = '1';
                $this->db->insert("uap_pembelian_master", $pembelian);
                $id_pembelian = $this->db->insert_id();
            } else {
                $pembelian['updated_date'] = GetDateNow();
                $pembelian['updated_by'] = ForeignKeyFromDb($userid);
                $id_pembelian = $model['id_pembelian_master'];
                $pembeliandb = $this->GetOnepembelian($id_pembelian);
                $pembelian['nomor_master'] = $pembeliandb->nomor_master;
                $this->db->update("uap_pembelian_master", $pembelian, array("id_pembelian_master" => $id_pembelian));
            }
            
            $this->db->delete("uap_pembelian_link",array("id_pembelian_master"=>$id_pembelian));
            foreach($listpopembelian as $linksatuan)
            {
                $linksatuan['id_pembelian_master']=$id_pembelian;
                $this->db->insert("uap_pembelian_link",$linksatuan);
            }
            $arraydetailpembelian = array();
            $totalpembelianwithoutppn=0;
            $totalpembelianwithppn=0;
            foreach ($model['dataset'] as $key => $det) {
                $detailpembelian = array();
                $detailpembelian['id_barang'] = $det['id_barang'];
                $detailpembelian['price_barang'] = DefaultCurrencyDatabase($det['price_barang']);
                $detailpembelian['nama_barang'] = $det['nama_barang'];
                $detailpembelian['qty'] = DefaultCurrencyDatabase($det['qty']);
                $detailpembelian['disc_1'] = DefaultCurrencyDatabase($det['disc_1']);
                $detailpembelian['disc_2'] = DefaultCurrencyDatabase($det['disc_2']);
                $detailpembelian['subtotal'] = DefaultCurrencyDatabase($det['subtotal']);
                if($pembelian['type_ppn']=="1")
                {
                    $detailpembelian['ppn_satuan'] = $detailpembelian['subtotal']*((float)$pembelian['ppn']/(float)$pembelian['ppn']+(float)100);
                    $totalpembelianwithoutppn+=$detailpembelian['subtotal']-$detailpembelian['ppn_satuan']; 
                }
                else  if($pembelian['type_ppn']=="2")
                {
                    $detailpembelian['ppn_satuan'] = $detailpembelian['subtotal']*((float)$pembelian['ppn']/(float)100);
                    $totalpembelianwithoutppn+=$detailpembelian['subtotal']; 
                }
                else
                {
                    $detailpembelian['ppn_satuan'] = 0;
                    $totalpembelianwithoutppn+=$detailpembelian['subtotal']; 
                }
                
                $totalpembelianwithppn+=$detailpembelian['subtotal'];
                if (CheckEmpty(@$det['id_pembelian_detail'])) {
                    $barang = $this->m_barang->GetOneBarang($det['id_barang']);
                    $detailpembelian['id_satuan'] = ForeignKeyFromDb(@$barang->id_satuan);
                    $detailpembelian['created_date'] = GetDateNow();
                    $detailpembelian['created_by'] = $userid;
                    $detailpembelian['status'] = 1;
                    $detailpembelian['id_pembelian_master'] = $id_pembelian;
                    $this->db->insert("uap_pembelian_detail", $detailpembelian);
                    $arraydetailpembelian[] = $this->db->insert_id();
                } else {

                    $detailpembelian['updated_date'] = GetDateNow();
                    $detailpembelian['updated_by'] = $userid;
                    if (!CheckEmpty(@$det['id_satuan']))
                        $detailpembelian['id_satuan'] = ForeignKeyFromDb($det['id_satuan']);
                    $this->db->update("uap_pembelian_detail", $detailpembelian, array("id_pembelian_detail" => $det['id_pembelian_detail']));
                    $arraydetailpembelian[] = $det['id_pembelian_detail'];
                }
            }


            if (count($arraydetailpembelian) > 0) {
                $this->db->where_not_in("id_pembelian_detail", $arraydetailpembelian);
                $this->db->where(array("id_pembelian_master" => $id_pembelian));
                $this->db->update("uap_pembelian_detail", array("status" => 5, 'updated_date' => GetDateNow(), 'updated_by' => $userid));
            } else {
                $this->db->update("uap_pembelian_detail", array("status" => 5, 'updated_date' => GetDateNow()), array("id_pembelian_master" => $pembelian));
            }
            
            $this->db->from("uap_pembayaran_utang_detail");
            $this->db->join("uap_pembayaran_utang_master","uap_pembayaran_utang_master.pembayaran_utang_id=uap_pembayaran_utang_detail.pembayaran_utang_id");
            $this->db->where(array("keterangan"=>"Pembayaran Di Muka","id_pembelian_master"=>$id_pembelian));
            
            $pembayaran_utang_master=$this->db->get()->row();
            if($pembayaran_utang_master!=null && $pembelian['jumlah_bayar']==0)
            {
                $this->db->delete("uap_pembayaran_utang_detail",array("pembayaran_utang_detail_id"=>$pembayaran_utang_master->pembayaran_utang_detail_id));
                $this->db->delete("uap_pembayaran_utang_master",array("pembayaran_utang_id"=>$pembayaran_utang_master->pembayaran_utang_id));
            }
            else if ($pembayaran_utang_master!=null&&$pembayaran_utang_master->jumlah_bayar!=$pembelian['jumlah_bayar'])
            {
                $updatedet=array();
                $updatedet['updated_date']= GetDateNow();
                $updatedet['updated_by']= $userid;
                $updatedet['jumlah_bayar']=$pembelian['jumlah_bayar'];
                $updatemas=array();
                $updatemas['updated_date']= GetDateNow();
                $updatemas['updated_by']= $userid;
                $updatemas['total_bayar']=$pembelian['jumlah_bayar'];
                $this->db->update("uap_pembayaran_utang_detail",$updatedet,array("pembayaran_utang_detail_id"=>$pembayaran_utang_master->pembayaran_utang_detail_id));
                $this->db->update("uap_pembayaran_utang_master",$updatemas,array("pembayaran_utang_id"=>$pembayaran_utang_master->pembayaran_utang_id));
            }
            else if($pembayaran_utang_master==null&&$pembelian['jumlah_bayar']>0)
            {
                $dokumenpembayaran=AutoIncrement('uap_pembayaran_utang_master', $cabang->kode_cabang . '/BB/' . date("y") . '/', 'dokumen', 5);
                $pembayaranmaster=array();
                $pembayaranmaster['dokumen']=$dokumenpembayaran;
                $pembayaranmaster['tanggal_transaksi']=$pembelian['tanggal'];
                $pembayaranmaster['id_supplier']=$pembelian['id_supplier'];
                $pembayaranmaster['jenis_pembayaran']="cash";
                $pembayaranmaster['total_bayar']=$pembelian['jumlah_bayar']>$pembelian['grandtotal']?$pembelian['grandtotal']:$pembelian['jumlah_bayar'];
                $pembayaranmaster['biaya']=0;
                $pembayaranmaster['potongan']=0;
                $pembayaranmaster['status']=1;
                $pembayaranmaster['keterangan']="Pembayaran Di Muka";
                $pembayaranmaster['id_cabang']=ForeignKeyFromDb($model['id_cabang']);
                $pembayaranmaster['created_date']= GetDateNow();
                $pembayaranmaster['created_by']=$userid;
                $this->db->insert("uap_pembayaran_utang_master",$pembayaranmaster);
                $pembayaran_utang_id=$this->db->insert_id();
                $pembayarandetail=array();
                $pembayarandetail['id_pembelian_master']=$id_pembelian;
                $pembayarandetail['pembayaran_utang_id']=$pembayaran_utang_id;
                $pembayarandetail['jumlah_bayar']=$pembelian['jumlah_bayar'];
                $pembayarandetail['created_date']= GetDateNow();
                $pembayarandetail['created_by']=$userid;
                $this->db->insert("uap_pembayaran_utang_detail",$pembayarandetail);
            }
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account"=>"24","dokumen"=>$pembelian['nomor_master'] ));
            $akun_pembelian=$this->db->get()->row();
            
            if(CheckEmpty($akun_pembelian))
            {   
                $akunpembelianinsert=array();
                $akunpembelianinsert['id_gl_account']="24";
                $akunpembelianinsert['tanggal_buku']=$pembelian['tanggal'];
                $akunpembelianinsert['id_cabang'] = $pembelian['id_cabang'];
                $akunpembelianinsert['keterangan']="Pembelian Ke " .@$supplier->nama_supplier.' '.$pembelian['keterangan'];
                $akunpembelianinsert['dokumen']=$pembelian['nomor_master'] ;
                $akunpembelianinsert['subject_name']=@$supplier->kode_supplier;
                $akunpembelianinsert['id_subject']=$pembelian['id_supplier'];
                $akunpembelianinsert['debet']=$totalpembelianwithoutppn;
                $akunpembelianinsert['kredit']=0;
                $akunpembelianinsert['created_date']= GetDateNow();
                $akunpembelianinsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunpembelianinsert);
            } 
            else
            {
                $akunpembelianupdate['tanggal_buku']=$pembelian['tanggal'];
                $akunpembelianupdate['id_cabang'] = $pembelian['id_cabang'];
                $akunpembelianupdate['keterangan']="Pembelian Ke " .@$supplier->nama_supplier.' '.$pembelian['keterangan'];
                $akunpembelianupdate['dokumen']=$pembelian['nomor_master'] ;
                $akunpembelianupdate['subject_name']=@$supplier->kode_supplier;
                $akunpembelianupdate['id_subject']=$pembelian['id_supplier'];
                $akunpembelianupdate['debet']=$totalpembelianwithoutppn;
                $akunpembelianupdate['kredit']=0;
                $akunpembelianupdate['updated_date']= GetDateNow();
                $akunpembelianupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunpembelianupdate,array("id_buku_besar"=>$akun_pembelian->id_buku_besar));
            }
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account"=>"33","dokumen"=>$pembelian['nomor_master'] ));
            $akun_ppn=$this->db->get()->row();
            
            if(CheckEmpty($akun_ppn)&&$pembelian['nominal_ppn']>0)
            {
                $akunppninsert=array();
                $akunppninsert['id_gl_account']="33";
                $akunppninsert['tanggal_buku']=$pembelian['tanggal'];
                $akunppninsert['id_cabang'] = $pembelian['id_cabang'];
                $akunppninsert['keterangan']="PPN pembelian Ke " .@$supplier->nama_supplier.' '.$pembelian['keterangan'];
                $akunppninsert['dokumen']=$pembelian['nomor_master'] ;
                $akunppninsert['subject_name']=@$supplier->kode_supplier;
                $akunppninsert['id_subject']=$pembelian['id_supplier'];
                $akunppninsert['debet']=$pembelian['nominal_ppn'];
                $akunppninsert['kredit']=0;
                $akunppninsert['created_date']= GetDateNow();
                $akunppninsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunppninsert);
            } 
            else if(!CheckEmpty($akun_ppn))
            {
                $akunppnupdate['tanggal_buku']=$pembelian['tanggal'];
                $akunppnupdate['id_cabang'] = $pembelian['id_cabang'];
                $akunppnupdate['keterangan']="PPN pembelian Ke " .@$supplier->nama_supplier.' '.$pembelian['keterangan'];
                $akunppnupdate['dokumen']=$pembelian['nomor_master'] ;
                $akunppnupdate['subject_name']=@$supplier->kode_supplier;
                $akunppnupdate['id_subject']=$pembelian['id_supplier'];
                $akunppnupdate['debet']=$pembelian['nominal_ppn'];
                $akunppnupdate['kredit']=0;
                $akunppnupdate['updated_date']= GetDateNow();
                $akunppnupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunppnupdate,array("id_buku_besar"=>$akun_ppn->id_buku_besar));
            }
            
            
            
            
            
            
            
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account"=>"11","dokumen"=>$pembelian['nomor_master'] ));
            $akun_utang=$this->db->get()->row();
            
            if(CheckEmpty($akun_utang)&&$pembelian['jumlah_bayar']<$pembelian['grandtotal'])
            {
                $akunutanginsert=array();
                $akunutanginsert['id_gl_account']="11";
                $akunutanginsert['tanggal_buku']=$pembelian['tanggal'];
                $akunutanginsert['id_cabang'] = $pembelian['id_cabang'];
                $akunutanginsert['keterangan']="Utang pembelian Ke " .@$supplier->nama_supplier.' '.$pembelian['keterangan'];
                $akunutanginsert['dokumen']=$pembelian['nomor_master'] ;
                $akunutanginsert['subject_name']=@$supplier->kode_supplier;
                $akunutanginsert['id_subject']=$pembelian['id_supplier'];
                $akunutanginsert['debet']=0;
                $akunutanginsert['kredit']=$pembelian['grandtotal']-$pembelian['jumlah_bayar'];
                $akunutanginsert['created_date']= GetDateNow();
                $akunutanginsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunutanginsert);
            } 
            else if(!CheckEmpty($akun_utang))
            {
                $akunutangupdate['tanggal_buku']=$pembelian['tanggal'];
                $akunutangupdate['id_cabang'] = $pembelian['id_cabang'];
                $akunutangupdate['keterangan']="Utang pembelian Ke " .@$supplier->nama_supplier.' '.$pembelian['keterangan'];
                $akunutangupdate['dokumen']=$pembelian['nomor_master'] ;
                $akunutangupdate['subject_name']=@$supplier->kode_supplier;
                $akunutangupdate['id_subject']=$pembelian['id_supplier'];
                $akunutangupdate['debet']=0;
                $akunutangupdate['kredit']=$pembelian['grandtotal']-$pembelian['jumlah_bayar'];
                $akunutangupdate['updated_date']= GetDateNow();
                $akunutangupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunutangupdate,array("id_buku_besar"=>$akun_utang->id_buku_besar));
            }
            
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account"=>"25","dokumen"=>$pembelian['nomor_master'] ));
            $akun_potongan=$this->db->get()->row();
            
            if(CheckEmpty($akun_potongan)&&$pembelian['disc_pembulatan']>0)
            {
                $akunpotonganinsert=array();
                $akunpotonganinsert['id_gl_account']="25";
                $akunpotonganinsert['tanggal_buku']=$pembelian['tanggal'];
                $akunpotonganinsert['id_cabang'] = $pembelian['id_cabang'];
                $akunpotonganinsert['keterangan']="Potongan pembelian Ke " .@$supplier->nama_supplier.' '.$pembelian['keterangan'];
                $akunpotonganinsert['dokumen']=$pembelian['nomor_master'] ;
                $akunpotonganinsert['subject_name']=@$supplier->kode_supplier;
                $akunpotonganinsert['id_subject']=$pembelian['id_supplier'];
                $akunpotonganinsert['debet']=0;
                $akunpotonganinsert['kredit']=$pembelian['disc_pembulatan'];
                $akunpotonganinsert['created_date']= GetDateNow();
                $akunpotonganinsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunpotonganinsert);
            } 
            else if(!CheckEmpty($akun_potongan))
            {
                $akunpotonganupdate['tanggal_buku']=$pembelian['tanggal'];
                $akunpotonganupdate['id_cabang'] = $pembelian['id_cabang'];
                $akunpotonganupdate['keterangan']="Potongan pembelian Ke " .@$supplier->nama_supplier.' '.$pembelian['keterangan'];
                $akunpotonganupdate['dokumen']=$pembelian['nomor_master'] ;
                $akunpotonganupdate['subject_name']=@$supplier->kode_supplier;
                $akunpotonganupdate['id_subject']=$pembelian['id_supplier'];
                $akunpotonganupdate['debet']=0;
                $akunpotonganupdate['kredit']=$pembelian['disc_pembulatan'];
                $akunpotonganupdate['updated_date']= GetDateNow();
                $akunpotonganupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunpotonganupdate,array("id_buku_besar"=>$akun_potongan->id_buku_besar));
            }
            
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account"=>"3","dokumen"=>$pembelian['nomor_master'] ));
            $akun_kas=$this->db->get()->row();
            
            if(CheckEmpty($akun_kas)&&$pembelian['jumlah_bayar']>0)
            {
                $akunkasinsert=array();
                $akunkasinsert['id_gl_account']="3";
                $akunkasinsert['tanggal_buku']=$pembelian['tanggal'];
                $akunkasinsert['id_cabang'] = $pembelian['id_cabang'];
                $akunkasinsert['keterangan']="Pengeluaran kas untuk pembelian Ke " .@$supplier->nama_supplier.' '.$pembelian['keterangan'];
                $akunkasinsert['dokumen']=$pembelian['nomor_master'] ;
                $akunkasinsert['subject_name']=@$supplier->kode_supplier;
                $akunkasinsert['id_subject']=$pembelian['id_supplier'];
                $akunkasinsert['debet']=0;
                $akunkasinsert['kredit']=$pembelian['jumlah_bayar'];
                $akunkasinsert['created_date']= GetDateNow();
                $akunkasinsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunkasinsert);
            } 
            else if(!CheckEmpty($akun_kas))
            {
                $akunkasupdate['tanggal_buku']=$pembelian['tanggal'];
                $akunkasupdate['id_cabang'] = $pembelian['id_cabang'];
                $akunkasupdate['keterangan']="Pengeluaran kas untuk Ke " .@$supplier->nama_supplier.' '.$pembelian['keterangan'];
                $akunkasupdate['dokumen']=$pembelian['nomor_master'] ;
                $akunkasupdate['subject_name']=@$supplier->kode_supplier;
                $akunkasupdate['id_subject']=$pembelian['id_supplier'];
                $akunkasupdate['debet']=0;
                $akunkasupdate['kredit']=$pembelian['jumlah_bayar'];
                $akunkasupdate['updated_date']= GetDateNow();
                $akunkasupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunkasupdate,array("id_buku_besar"=>$akun_kas->id_buku_besar));
            }
            
            
           

            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                die();
                SetMessageSession(true, "Data Pembelian PO Sudah di" . (CheckEmpty(@$model['id_pembelian_master']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_pembelian, $pembelian['nomor_master'], 'pembelian');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    
    
  

    // datatables
    function GetDatapembelianPO($params) {
        $this->load->library('datatables');
        $this->datatables->select('id_pembelian_po_master,nama_supplier,tgl_po,replace(nomor_po_master,"/","-") as nomor_po_master,nominal_po');
        $this->datatables->join("uap_supplier", "uap_supplier.id_supplier=uap_pembelian_po_master.id_supplier", "left");
        $this->datatables->from('uap_pembelian_po_master');
        //add this line for join
        //$this->datatables->join('table2', 'uap_pembelian.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        $extra = array();
        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['uap_pembelian_po_master.id_cabang'] = $params['id_cabang'];
        }
        if (isset($params['id_supplier']) && !CheckEmpty(@$params['id_supplier'])) {
            $params['uap_pembelian_po_master.id_supplier'] = $params['id_supplier'];
        }
        unset($params['id_supplier']);
        unset($params['id_cabang']);
        unset($params['status']);
      
        
        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "tgl_po BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tgl_po'] = DefaultTanggalDatabase($params['start_date']) ;
            unset($params['start_date']);
        }
          
        if (count($params)) {
              $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }

        if ($isedit) {
            $straction .= anchor("pembelian/editpembelianpo/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batalpo($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_pembelian_po_master,nomor_po_master');
        return $this->datatables->generate();
    }

    function GetDatapembelian($params) {
        $this->load->library('datatables');
        $this->datatables->select('id_pembelian_master,nama_supplier,tanggal,replace(nomor_master,"/","-") as nomor_master,grandtotal,jth_tempo');
        $this->datatables->join("uap_supplier", "uap_supplier.id_supplier=uap_pembelian_master.id_supplier", "left");
        $this->datatables->from('uap_pembelian_master');
        //add this line for join
        //$this->datatables->join('table2', 'uap_pembelian.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        $extra = array();
        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['uap_pembelian_master.id_cabang'] = $params['id_cabang'];
        }
        if (isset($params['id_supplier']) && !CheckEmpty(@$params['id_supplier'])) {
            $params['uap_pembelian_master.id_supplier'] = $params['id_supplier'];
        }
        unset($params['id_supplier']);
        unset($params['id_cabang']);
        unset($params['status']);
      
        
        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "tanggal BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tanggal'] = DefaultTanggalDatabase($params['start_date']) ;
            unset($params['start_date']);
        }
          
        if (count($params)) {
              $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }

        if ($isedit) {
            $straction .= anchor("pembelian/editpembelian/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batal($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_pembelian_master,nomor_master');
        return $this->datatables->generate();
    }


}
