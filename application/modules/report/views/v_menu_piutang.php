<?php    
    if(CekModule("K075",false))
    {
        echo anchor(site_url('report/r_piutang'), '<i class="fa fa-file"></i> Semua Piutang ', 'class="btn btn-success"');
    }
    if(CekModule("K076",false))
    {
        echo anchor(site_url('report/r_piutang/pembayaran'), '<i class="fa fa-file"></i> Pembayaran ', 'class="btn btn-info"');
    }
    if(CekModule("K077",false))
    {
        echo anchor(site_url('report/r_piutang/sisa'), '<i class="fa fa-file"></i> Invoice Sisa ', 'class="btn btn-danger"');
    }
    if(CekModule("K078",false))
    {
        echo anchor(site_url('report/r_piutang/detail'), '<i class="fa fa-file"></i> Piutang Detail ', 'class="btn btn-warning"');
    }