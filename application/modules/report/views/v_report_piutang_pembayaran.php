
<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Piutang</h2>
                                <p>Pembayaran | Report</p>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-3'>
                        <div class='breadcomb-report'>
                            <?php include APPPATH . 'modules/report/views/v_menu_piutang.php' ?> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification"></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Tanggal', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker(date('Y-m-d')), array('required' => 'required')); ?>
                        </div>
                        <?= form_label('s / d', "txt_tanggal_akhir", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker(date('Y-m-d'))); ?>
                        </div>
                        <?= form_label('Cabang', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_cabang', 'value' => @$id_cabang, 'class' => 'form-control', 'id' => 'dd_id_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown(@$list_cabang, "", "Cabang")); ?>
                        </div>
                    </div>
                </div>   
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Customer', "dd_id_customer", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_customer', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "", "Customer")); ?>
                        </div>
                        <?= form_label('Jenis', "dd_id_jenis_pembayaran", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_jenis_pembayaran', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_jenis_pembayaran', 'placeholder' => 'Jenis Pembayaran'), DefaultEmptyDropdown(@$list_jenis_pembayaran, "", "Jenis")); ?>
                        </div>
                        <div class="col-sm-3">
                            <button id="btt_Search" type="submit" class="btn-small btn-block btn-primary pull-right">Search</button>
                        </div>
                    </div>
                </div>
            </form>
            <form id="frm_extra" method="post" target="_blank" style="display: none;">
                <input type="hidden" name="search" id="extra" value=""/>
            </form>
            <br/>
            <div class="row">
                <div class="col-md-12">
                    <div id="report-view" style="overflow-x: auto;"></div>
                </div>
            </div>
        </div>
    </div>

</section>

<div class="">
    <div class="box box-default form-element-list">


        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</div>
<script type="text/javascript">
    var table;
      $("form#frm_search").submit(function(){
        table.fnDraw(false);
        return false;
    })
    function deletepembelian(id_pembelian) {
        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            $("#dd_action_" + id_pembelian).val(0);
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/pembelian/pembelian_delete',
                    dataType: 'json',
                    data: {
                        id_pembelian: id_pembelian
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }

                });
            }
        });


    }
    function RefreshGrid() {
        $(".selectaction").change(function () {
            var valselect = $(this).val();
            var id = $(this).attr("id").replace("dd_action_", "");
            if (valselect != "0")
            {
                if (valselect == "Lihat Stock")
                {

                } else if (valselect == "Lihat Ingridient")
                {

                } else if (valselect == "Tambah Ingridient")
                {

                } else if (valselect == "Edit")
                {
                    window.location.href = baseurl + "index.php/pembelian/edit_pembelian/" + id;
                } else if (valselect == "Hapus")
                {
                    deletepembelian(id);
                } else
                {
                    $(this).val(0);
                }
            }
        })

    }

    $(document).ready(function () {
         $(".datepicker").datepicker();
        $(".select2").select2();
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": "getdatapiutangpembayaran", "type": "POST", "data": function (d) {
                return $.extend({}, d, {
                    "extra_search": $("form#frm_search").serialize()
                });
            }},
            columns: [
                {
                    data: "id_pembelian_master",
                    title: "Kode",
                    width:"50px",
                    orderable: false
                }
                , {data: "tanggal", orderable: false, title: "Tanggal","width":"120px",
                    mRender: function (data, type, row) {
                        return DefaultDateFormat(data == undefined ? 0 : data);
                    }}
                , {data: "nama_supplier", orderable: false, title: "Supplier","width":"300px"}
                , {data: "nomor_master", orderable: false, title: "NO Doc","width":"150px"}
                , {data: "grandtotal", orderable: false, title: "Jumlah","width":"150px",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                , {data: "jth_tempo", orderable: false, title: "Jatuh Tempo","width":"120px",
                    mRender: function (data, type, row) {
                        return DefaultDateFormat(data == undefined ? 0 : data);
                    }}

                ,

                {
                    "data": "action",
                    "orderable": false,
                    "width":"100px",
                    "className": "text-center nopadding"
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

            },
            initComplete: function () {
                RefreshGrid();
            }
        });

    });
</script>
