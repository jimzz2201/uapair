<?php    
    if(CekModule("K058",false))
    {
        echo anchor(site_url('report/r_penjualan'), '<i class="fa fa-file"></i> Rekap ', 'class="btn btn-success"');
    }
    if(CekModule("K059",false))
    {
        echo anchor(site_url('report/r_penjualan/detail'), '<i class="fa fa-file"></i> Detail ', 'class="btn btn-info"');
    }
    if(CekModule("K060",false))
    {
        echo anchor(site_url('report/r_penjualan/sales_summary'), '<i class="fa fa-file"></i> Sales Summary ', 'class="btn btn-danger"');
    }
    if(CekModule("K061",false))
    {
        echo anchor(site_url('report/r_penjualan/sales_detail'), '<i class="fa fa-file"></i> Sales Detail ', 'class="btn btn-warning"');
    }
    if(CekModule("K062",false))
    {
        echo anchor(site_url('report/r_penjualan/per_group'), '<i class="fa fa-file"></i> Per Group ', 'class="btn btn-primary"');
    }
    if(CekModule("K063",false))
    {
        echo anchor(site_url('report/r_penjualan/per_invoice'), '<i class="fa fa-file"></i> Per Infoice ', 'class="btn btn-success"');
    }
    