<?php    
    if(CekModule("K070",false))
    {
        echo anchor(site_url('report/r_utang'), '<i class="fa fa-file"></i> Semua Utang ', 'class="btn btn-success"');
    }
    if(CekModule("K071",false))
    {
        echo anchor(site_url('report/r_utang/pembayaran'), '<i class="fa fa-file"></i> Pembayaran ', 'class="btn btn-info"');
    }
    if(CekModule("K072",false))
    {
        echo anchor(site_url('report/r_utang/sisa'), '<i class="fa fa-file"></i> Doc Sisa ', 'class="btn btn-danger"');
    }
    if(CekModule("K073",false))
    {
        echo anchor(site_url('report/r_utang/detail'), '<i class="fa fa-file"></i> Utang Detail ', 'class="btn btn-warning"');
    }