<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Aliran_kas extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_aliran_kas');
    }

    public function editaliran_kas($id, $nomorinvoice) {
        $row = (object) array();

        $module = "K039";
        $header = "K036";
        CekModule($module);
        $aliran_kas = $this->m_aliran_kas->GetOneAliran_kas($id, true);
        
        if ($aliran_kas == null || $aliran_kas->dokumen_aliran_kas != str_replace("-", "/", $nomorinvoice)) {
            SetMessageSession(2, "Anda tidak memiliki akses untuk menuju link tersebut");
            redirect(base_url() . 'index.php/main/message');
        } else {
            $row = $aliran_kas;
        }
        
        $row->button = 'Edit';
        $row->listjenistransaksi=array('1' => 'Kas Ke Bank' , '2' => 'Bank Ke Kas' , '3' => 'Pengeluaran Sehari hari' , '4' => 'Pemasukan Kas' , '5' => 'Pemasukan Bank',  '6' => 'Pengeluaran Bank');
     
        $this->load->model("cabang/m_cabang");
        $this->load->model("bank/m_bank");
        $this->load->model("biaya_master/m_biaya_master");
        $biayamaster=$this->m_biaya_master->GetOneBiaya_master($row->id_biaya_master);
        if($biayamaster)
        {
            $row->id_biaya_group=$biayamaster->id_biaya_group;
        }
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->list_biaya_master = $this->m_biaya_master->GetDropDownBiayaMaster();
        $row->list_biaya_group = $this->m_biaya_master->GetDropDownGroup();
        $row->list_bank = $this->m_bank->GetDropDownBank();
        $row->listheader = [];
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
       
        $row->title = 'Edit Aliran_kas';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';

        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'aliran_kas/v_aliran_kas_manipulate', $javascript, $css);
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K039";
        $header = "K036";
        $title = "Aliran_kas";
        CekModule($module);
        LoadTemplate(array("title" => $title, "form" => $header, "formsubmenu" => $module), "aliran_kas/v_aliran_kas_index", $javascript);
    }

    public function getdataaliran_kas() {
        header('Content-Type: application/json');
        echo $this->m_aliran_kas->GetDataaliran_kas();
    }

    public function create() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K038";
        $header = "K036";
        CekModule($module);
        $this->load->model("cabang/m_cabang");
        $this->load->model("bank/m_bank");
        $this->load->model("biaya_master/m_biaya_master");
        $row->form = $header;
        $row->listjenistransaksi=array('1' => 'Kas Ke Bank' , '2' => 'Bank Ke Kas' , '3' => 'Pengeluaran Sehari hari' , '4' => 'Pemasukan Kas' , '5' => 'Pemasukan Bank',  '6' => 'Pengeluaran Bank');
        $row->listdetail = [];
        $row->list_biaya_master =$this->m_biaya_master->GetDropDownBiayaMaster();
        $row->list_biaya_group =$this->m_biaya_master->GetDropDownGroup();
        $row->list_bank =$this->m_bank->GetDropDownBank();
        $row->listheader = [];
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->tanggal_transaksi = GetDateNow();
        $row->title = 'Aliran_kas';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';

        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'aliran_kas/v_aliran_kas_manipulate', $javascript, $css);
    }

    public function create_aliran_kas() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K004";
        $header = "K001";
        CekModule($module);
        $this->load->model("satuan/m_satuan");
        $this->load->model("jenis/m_jenis");
        $this->load->model("merek/m_merek");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->list_merek = $this->m_merek->GetDropDownMerek();
        $row->list_jenis = $this->m_jenis->GetDropDownJenis();
        $row->list_satuan = $this->m_satuan->GetDropDownSatuan();
        $row->title = 'Create Barang';
        $row = json_decode(json_encode($row), true);

        $javascript = array();
        LoadTemplate($row, 'aliran_kas/v_aliran_kas_manipulate', $javascript);
    }

    public function aliran_kas_manipulate() {
        $message = '';
        $model = $this->input->post();

       
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');
        $this->form_validation->set_rules('tanggal_transaksi', 'Tanggal', 'trim|required');
        $this->form_validation->set_rules('type_transaksi', 'Jenis Transaksi', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
        $this->form_validation->set_rules('nominal', 'Nominal', 'trim|required');
        
        if (!CheckEmpty(@$model['type_transaksi'])) {
            if($model['type_transaksi']=="1"||$model['type_transaksi']=="2"||$model['type_transaksi']=="5"||$model['type_transaksi']=="6")
            {
                 $this->form_validation->set_rules('id_bank', 'Bank', 'trim|required');
            }
            if($model['type_transaksi']>="3")
            {
                 $this->form_validation->set_rules('id_biaya_master', 'Biaya', 'trim|required'); 
            }
            
        }
        
        
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_aliran_kas->Aliran_kasManipulate($model);
            echo json_encode($status);
        }
    }

    public function get_one_aliran_kas() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_aliran_kas', 'nama aliran_kas', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_aliran_kas->GetOneBarang($model["id_aliran_kas"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data aliran_kas tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function aliran_kas_delete() {
        $message = '';
        $this->form_validation->set_rules('id_aliran_kas', 'Barang', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_aliran_kas->BarangDelete($model['id_aliran_kas']);
        }

        echo json_encode($result);
    }

}

/* End of file Barang.php */