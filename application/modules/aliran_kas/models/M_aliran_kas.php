<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_aliran_kas extends CI_Model {

    public $table = 'uap_aliran_kas';
    public $id = 'id_aliran_kas_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function GetDetailPo($id) {
        $this->db->from("uap_aliran_kas_detail");
        $this->db->join("uap_satuan", "uap_aliran_kas_detail.id_satuan=uap_satuan.id_satuan", "left");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_aliran_kas_detail.id_barang", "left");
        $this->db->select("uap_aliran_kas_detail.*,sku_barang as sku,nama_satuan,harga_beli_akhir");
        $this->db->where(array("id_aliran_kas_master" => $id, "uap_aliran_kas_detail.status !=" => 5));
        $listdetail = $this->db->get()->result();
        return $listdetail;
    }
    function GetDetail($id) {
        $this->db->from("uap_aliran_kas_detail");
        $this->db->join("uap_satuan", "uap_aliran_kas_detail.id_satuan=uap_satuan.id_satuan", "left");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_aliran_kas_detail.id_barang", "left");
        $this->db->select("uap_aliran_kas_detail.*,sku_barang as sku,nama_satuan,harga_beli_akhir");
        $this->db->where(array("id_aliran_kas_master" => $id, "uap_aliran_kas_detail.status !=" => 5));
        $listdetail = $this->db->get()->result();
        return $listdetail;
    }
    function GetOneAliran_kasPo($id, $isfull = false) {
        $this->db->from("uap_aliran_kas_master");
        $this->db->where(array("id_aliran_kas_master" => $id));
        $row = $this->db->get()->row();
        if ($isfull) {

            $row->listdetail = $this->GetDetailPo($id);
        }

        return $row;
    }
    function GetOneAliran_kas($id, $isfull = false) {
        $this->db->from("uap_aliran_kas");
        $this->db->where(array("id_aliran_kas" => $id));
        $row = $this->db->get()->row();
       

        return $row;
    }
    function Aliran_kasManipulate($model) {
       
        try {
            $message = "";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            $aliran_kas = array();
            $this->load->model("bank/m_bank");
            $this->load->model("cabang/m_cabang");
            $this->load->model("biaya_master/m_biaya_master");
            $cabang = $this->m_cabang->GetOneCabang($model['id_cabang']);
            if($model['type_transaksi']=="1")
            {
                $kode="KB";
            }
            else if($model['type_transaksi']=="2")
            {
                $kode="BK";
            }
            else if($model['type_transaksi']=="3")
            {
                $kode="PS";
            }
            else if($model['type_transaksi']=="4")
            {
                $kode="PK";
            }
            else if($model['type_transaksi']=="5")
            {
                $kode="PB";
            }
            else if($model['type_transaksi']=="2")
            {
                $kode="BP";
            }
            
            
            
            
            
            $aliran_kas=array();
            $aliran_kas['id_cabang']= ForeignKeyFromDb($model['id_cabang']);
            $aliran_kas['tanggal_transaksi']= DefaultTanggalDatabase($model['tanggal_transaksi']);
            $aliran_kas['id_bank']= ForeignKeyFromDb(@$model['id_bank']);
            $aliran_kas['type_transaksi']= $model['type_transaksi'];
            $aliran_kas['id_biaya_master']= ForeignKeyFromDb(@$model['id_biaya_master']);
            $aliran_kas['keterangan']= $model['keterangan'];
            $aliran_kas['nominal']= DefaultCurrencyDatabase($model['nominal']) ;
            if (CheckEmpty(@$model['id_aliran_kas_master'])) {
                $aliran_kas['created_date'] = GetDateNow();
                $aliran_kas['dokumen_aliran_kas'] = AutoIncrement('uap_aliran_kas', $cabang->kode_cabang . '/'.$kode.'/' . date("y") . '/', 'dokumen_aliran_kas', 5);
                $aliran_kas['created_by'] = ForeignKeyFromDb($userid);
                $aliran_kas['status'] = '0';
                $this->db->insert("uap_aliran_kas", $aliran_kas);
                $id_aliran_kas = $this->db->insert_id();
            } else {
                $aliran_kas['updated_date'] = GetDateNow();
                $aliran_kas['updated_by'] = ForeignKeyFromDb($userid);
                $id_aliran_kas = $model['id_aliran_kas_master'];
                $aliran_kasdb = $this->GetOneAliran_kas($id_aliran_kas);
                $aliran_kas['dokumen_aliran_kas'] = $aliran_kasdb->dokumen_aliran_kas;
                $this->db->update("uap_aliran_kas", $aliran_kas, array("id_aliran_kas" => $id_aliran_kas));
            }
            
           
            $biayamaster=null;
            $bank=null;
            if($model['type_transaksi']>=3)
            {
                 $biayamaster=$this->m_biaya_master->GetOneBiaya_master($model['id_biaya_master']);
            }
            if($model['type_transaksi']=="1"||$model['type_transaksi']=="2"||$model['type_transaksi']=="5"||$model['type_transaksi']=="6")
            {
                 $bank=$this->m_bank->GetOneBank($model['id_bank']);
            }
            $gldebet=0;
            $glkredit=0;
            $subjectdebet=0;
            $subjectkredit=0;
            $kodedebet='';
            $kodekredit='';
            if($model['type_transaksi']=="1")
            {
                $gldebet="4";
                $subjectdebet=$bank!=null?$bank->id_bank:null;
                $kodedebet=$bank!=null?$bank->kode_bank:'';
                $glkredit="3";
                $subjectkredit=null;
                $kodekredit='';
            }
            else if($model['type_transaksi']=="2")
            {
                $gldebet="3";
                $subjectdebet=null;
                $kodedebet='';
                $glkredit="4";
                $subjectkredit=$bank!=null?$bank->id_bank:null;
                $kodekredit=$bank!=null?$bank->kode_bank:'';
            }
            else if($model['type_transaksi']=="3")
            {
                $gldebet="28";
                $subjectdebet=$biayamaster!=null?$biayamaster->id_biaya_master:null;
                $kodedebet=$biayamaster!=null?$biayamaster->kode_biaya_master:'';
                $glkredit="3";
                $subjectkredit=null;
                $kodekredit='';
            }
            else if($model['type_transaksi']=="4")
            {
                $gldebet="3";
                $subjectdebet=null;
                $kodedebet='';
                $glkredit="28";
                $subjectkredit=$biayamaster!=null?$biayamaster->id_biaya_master:null;
                $kodekredit=$biayamaster!=null?$biayamaster->kode_biaya_master:null;
            }
            else if($model['type_transaksi']=="5")
            {
                $gldebet="4";
                $subjectdebet=$bank!=null?$bank->id_bank:null;
                $kodedebet=$bank!=null?$bank->kode_bank:'';
                $glkredit="28";
                $subjectkredit=$biayamaster!=null?$biayamaster->id_biaya_master:null;
                $kodekredit=$biayamaster!=null?$biayamaster->kode_biaya_master:null;
            }
            else if($model['type_transaksi']=="6")
            {
                $gldebet="3";
                $subjectdebet=$biayamaster!=null?$biayamaster->id_biaya_master:null;
                $kodedebet=$biayamaster!=null?$biayamaster->kode_biaya_master:'';
                $glkredit="4";
                $subjectkredit=$bank!=null?$bank->id_bank:null;
                $kodekredit=$bank!=null?$bank->kode_bank:'';
            }
            
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array( "dokumen" => $aliran_kas['dokumen_aliran_kas'],"debet !="=>0));
            $akun_aliran_kas_debet = $this->db->get()->row();


            if(CheckEmpty($akun_aliran_kas_debet))
            {   
                $akundebet_kasinsert=array();
                $akundebet_kasinsert['id_gl_account']=$gldebet;
                $akundebet_kasinsert['tanggal_buku']=$aliran_kas['tanggal_transaksi'];
                $akundebet_kasinsert['id_cabang'] = ForeignKeyFromDb($aliran_kas['id_cabang']) ;
                $akundebet_kasinsert['keterangan']="Aliran kas untuk " .$aliran_kas['keterangan'];
                $akundebet_kasinsert['dokumen']=$aliran_kas['dokumen_aliran_kas'] ;
                $akundebet_kasinsert['subject_name']=$kodedebet;
                $akundebet_kasinsert['id_subject']=$subjectdebet;
                $akundebet_kasinsert['debet']= $aliran_kas['nominal'];
                $akundebet_kasinsert['kredit']=0;
                $akundebet_kasinsert['created_date']= GetDateNow();
                $akundebet_kasinsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akundebet_kasinsert);
               
            } 
            else 
            {
                $akunaliran_kasupdate['id_gl_account']=$gldebet;
                $akunaliran_kasupdate['tanggal_buku']=$aliran_kas['tanggal_transaksi'];
                $akunaliran_kasupdate['id_cabang'] = ForeignKeyFromDb($aliran_kas['id_cabang']) ;
                $akunaliran_kasupdate['keterangan']="Aliran kas untuk " .$aliran_kas['keterangan'];
                $akunaliran_kasupdate['dokumen']=$aliran_kas['dokumen_aliran_kas'] ;
                $akunaliran_kasupdate['subject_name']=$kodedebet;
                $akunaliran_kasupdate['id_subject']=$subjectdebet;
                $akunaliran_kasupdate['debet']=$aliran_kas['nominal'];
                $akunaliran_kasupdate['kredit']=0;;
                $akunaliran_kasupdate['updated_date']= GetDateNow();
                $akunaliran_kasupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunaliran_kasupdate,array("id_buku_besar"=>$akun_aliran_kas_debet->id_buku_besar));
            }
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("dokumen"=>$aliran_kas['dokumen_aliran_kas'] ,"kredit !="=>0));
            $akun_aliran_kas_kredit=$this->db->get()->row();
            
            if(CheckEmpty($akun_aliran_kas_kredit))
            {
                $akunaliran_kasinsert=array();
                $akunaliran_kasinsert['id_gl_account']=$glkredit;
                $akunaliran_kasinsert['tanggal_buku']=$aliran_kas['tanggal_transaksi'];
                $akunaliran_kasinsert['id_cabang'] = $aliran_kas['id_cabang'];
                $akunaliran_kasinsert['keterangan']="Aliran kas untuk " .$aliran_kas['keterangan'];
                $akunaliran_kasinsert['dokumen']=$aliran_kas['dokumen_aliran_kas'] ;
                $akunaliran_kasinsert['subject_name']=$kodekredit;
                $akunaliran_kasinsert['id_subject']=$subjectkredit;
                $akunaliran_kasinsert['debet']=0;
                $akunaliran_kasinsert['kredit']=$aliran_kas['nominal'];
                $akunaliran_kasinsert['created_date']= GetDateNow();
                $akunaliran_kasinsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunaliran_kasinsert);
            } 
            else 
            {
                $akunaliran_kasupdate['id_gl_account']=$glkredit;
                $akunaliran_kasupdate['tanggal_buku']=$aliran_kas['tanggal_transaksi'];
                $akunaliran_kasupdate['id_cabang'] = $aliran_kas['id_cabang'];
                $akunaliran_kasupdate['keterangan']="Aliran kas untuk " .$aliran_kas['keterangan'];
                $akunaliran_kasupdate['dokumen']=$aliran_kas['dokumen_aliran_kas'] ;
                $akunaliran_kasupdate['subject_name']=$kodekredit;
                $akunaliran_kasupdate['id_subject']=$subjectkredit;
                $akunaliran_kasupdate['debet']=0;
                $akunaliran_kasupdate['kredit']=$aliran_kas['nominal'];
                $akunaliran_kasupdate['updated_date']= GetDateNow();
                $akunaliran_kasupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunaliran_kasupdate,array("id_buku_besar"=>$akun_aliran_kas_kredit->id_buku_besar));
            }
            

            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data Aliran_kas Sudah di" . (CheckEmpty(@$model['id_aliran_kas']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_aliran_kas, $aliran_kas['dokumen_aliran_kas'], 'aliran_kas');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    
    
    
    
    
  

    function GetDataaliran_kas() {
        $this->load->library('datatables');
        $this->datatables->select('nama_jenis_aliran_kas,id_aliran_kas,tanggal_transaksi,nama_cabang,nominal,keterangan,replace(dokumen_aliran_kas,"/","-") as dokumen_aliran_kas');
        $this->datatables->join('uap_jenis_aliran_kas', 'uap_jenis_aliran_kas.id_jenis_aliran_kas = uap_aliran_kas.type_transaksi');
        $this->datatables->join('uap_cabang', 'uap_cabang.id_cabang = uap_aliran_kas.id_cabang');
        $this->datatables->from('uap_aliran_kas');
        //add this line for join
        //$this->datatables->join('table2', 'uap_aliran_kas.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';


        if ($isedit) {
            $straction .= anchor("aliran_kas/editaliran_kas/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batal($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_aliran_kas,dokumen_aliran_kas');
        return $this->datatables->generate();
    }


}
