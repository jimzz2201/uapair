<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Aliran Kas </h2>
                                <p><?= $button ?> Aliran Kas  | Aliran Kas </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_aliran_kas" class="form-horizontal form-groups-bordered validate" method="post">
                                <?php if(!CheckEmpty(@$dokumen_aliran_kas)) { ?>
                                <div class="form-group">
                                    <?= form_label('Dokumen', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off',  'value' => @$dokumen_aliran_kas, 'class' => 'form-control', 'placeholder' => 'Dokumen','disabled'=>'disabled')); ?>
                                    </div>
                                </div>
                                <?php } ?>
                                <input type="hidden" name="id_aliran_kas" value="<?php echo @$id_aliran_kas; ?>" /> 
                                <div class="form-group">

                                    <?= form_label('Cabang', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_cabang"), DefaultEmptyDropdown(@$list_cabang, "", "Cabang"), @$id_cabang, array('class' => 'form-control select2', 'id' => 'dd_id_cabang')); ?>
                                    </div>
                                    <?= form_label('Jenis&nbsp;Transaksi', "txt_customer", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("name" => "type_transaksi"), DefaultEmptyDropdown(@$listjenistransaksi, "", "Jenis Transaksi"), @$type_transaksi, array('class' => 'form-control', 'id' => 'dd_type_transaksi')); ?>
                                    </div>    



                                </div>
                                <div class="form-group">
                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal_transaksi', 'value' => DefaultDatePicker(@$tanggal_transaksi), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                    <?= form_label('Bank', "dd_id_bank", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("name" => "id_bank"), DefaultEmptyDropdown(@$list_bank, "", "Bank"), @$id_bank, array('class' => 'form-control', 'id' => 'dd_id_bank')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Group', "dd_id_group", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_biaya_group"), DefaultEmptyDropdown(@$list_biaya_group, "", "Group"), @$id_biaya_group, array('class' => 'form-control', 'id' => 'dd_id_biaya_group')); ?>
                                    </div>
                                    <?= form_label('Biaya', "dd_id_biaya_master", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("name" => "id_biaya_master"), DefaultEmptyDropdown(@$list_biaya_master, "", "Biaya"), @$id_biaya_master, array('class' => 'form-control', 'id' => 'dd_id_biaya_master')); ?>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>

                                </div>  
                                <div class="form-group">
                                    <?= form_label('Nominal', "txt_price_list", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text','name'=>'nominal', 'value' => DefaultCurrency(@$nominal), 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'class' => 'form-control', 'id' => 'txt_nominal', 'placeholder' => 'Nominal')); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                    </div>
                                </div>
                                <hr/>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>


    var table;

    function RefreshPanelGrid()
    {
        if ($("#dd_id_cabang").val() == "0" || $("#dd_type_transaksi").val() == "0")
        {
            $("#frm_aliran_kas button").attr("disabled", "disabled");
            $("#txt_tanggal").attr("disabled", "disabled");
            $("#dd_id_bank").attr("disabled", "disabled");
            $("#dd_id_biaya_group").attr("disabled", "disabled");
            $("#dd_id_biaya_master").attr("disabled", "disabled");
            $("#txt_nominal").attr("disabled", "disabled");
            $("#txt_keterangan").attr("disabled", "disabled");
        } 
        else
        {
            $("#frm_aliran_kas button").removeAttr("disabled");
            $("#txt_tanggal").removeAttr("disabled");
            $("#txt_nominal").removeAttr("disabled");
            $("#txt_keterangan").removeAttr("disabled");
            
            if($("#dd_type_transaksi").val()=="1"||$("#dd_type_transaksi").val()=="2"||$("#dd_type_transaksi").val()=="5"||$("#dd_type_transaksi").val()=="6")
            {
                $("#dd_id_bank").removeAttr("disabled");
            }
            else
            {
                 $("#dd_id_bank").attr("disabled", "disabled");
            }
            
            if($("#dd_type_transaksi").val()=="3"||$("#dd_type_transaksi").val()=="4"||$("#dd_type_transaksi").val()=="5"||$("#dd_type_transaksi").val()=="6")
            {
                $("#dd_id_biaya_group").removeAttr("disabled");
                $("#dd_id_biaya_master").removeAttr("disabled");
            }
            else
            {
                $("#dd_id_biaya_group").attr("disabled", "disabled");
                $("#dd_id_biaya_master").attr("disabled", "disabled");
            }
            
            
            

        }
    }
    $("#dd_id_biaya_group").change(function () {
        $("#dd_id_biaya_master").empty();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/biaya_master/getdropdownbiayamaster',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                LoadBar.show();
                $('#dd_id_biaya_master').append($('<option>', {
                    value: 0,
                    text: "Pilih Biaya"
                }));
                $.each(data.listbiayamaster, function (key, value) {
                    $('#dd_id_biaya_master').append($('<option>', {
                        value: key,
                        text: value
                    }));
                });

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
    })
    $(document).ready(function () {
        $("#txt_tanggal").datepicker();

        $("#dd_id_cabang , #dd_type_transaksi ").change(function () {
            RefreshPanelGrid();
        })

        RefreshPanelGrid();

    })
    $("#frm_aliran_kas").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data aliran kas berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/aliran_kas/aliran_kas_manipulate',
                    dataType: 'json',
                    data: $("#frm_aliran_kas").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/aliran_kas";
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;


    })

</script>