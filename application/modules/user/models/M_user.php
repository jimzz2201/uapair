<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_user extends CI_Model {

    public $table = 'uap_user';
    public $id = 'id_user';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function forgot_password($id_user) {
        $this->db->update("uap_user", array("forgot_password" => RandomPassword()), array("id_user" => $id_user));
    }

    function clear_forgot_password($id_user) {
        $this->db->update("", array("forgot_password" => ''), array("id_user" => $id_user));
    }

    function AuthUserWithCodemd5($email, $code) {
        $user = $this->GetOneUser($email, 'email');
        if ($user != null) {
            if (md5($user->forgot_password) == $code) {
                return true;
            } else
                return false;
        } else {
            return false;
        }
    }

    function profile_change_password($model) {
        $user = $this->GetOneUser($model['user_id'], 'id_user');
        $message = '';
        if ($user != null) {
            if ($model['password'] == $model['confirm']) {
                if ($user->password_user <> sha1($model['lastpassword'])) {
                    $message .= 'Your last password didn\'t match with our password in the system<br/>';
                } else {
                    $this->db->update("uap_user", array("password_user" => sha1($model['password'])), array("id_user" => $model['user_id']));
                }
            } else {
                $message .= 'Your password and confirm pin didn\'t match<br/>';
            }
        } else {
            $message .= 'We cannot find the user in database<br/>';
        }
        return $message;
    }

    function login($username, $password, $type = 'username') {

        $message = '';
        $wherearray = array();
        $wherearray['lower(' . $type . ')'] = strtolower($username);
        $wherearray['password_user'] = sha1($password);
        $this->db->from("uap_user");
        $this->db->where($wherearray);
        $row = $this->db->get()->row();
        if ($row == null) {
            $message .= "Username or password is wrong<br/>";
        } else {
            if ($row->status == 0) {
                $message .= 'your account not activated yet ';
            }
            else {
                SetUserId($row->id_user);
                SetGroupId($row->id_group);
                SetUsername($row->username);
                $this->load->model("akses/m_akses");
                $group = $this->m_akses->GetOneAkses($row->id_group);
                $this->insert_log(['id_user' => $row->id_user, 'username' => $row->username]);
            }
        }

        
       

        return $message;
    }

    public function set_new_password($password, $pin = '') {
        $this->db->update("", array("password" => sha1($password), 'pin_user' => sha1($pin), 'forgot_password' => ''), array("id_user" => GetUserId()));
        SetMessageSession(1, 'Password has updated');
    }

    function GetOneUser($value, $key = 'username') {
        $this->db->from("uap_user");
        $where = array();
        if ($key == 'username')
            $where["lower(" . $key . ")"] = strtolower($value);
        else
            $where[$key] = $value;

        $this->db->where($where);
        $user = $this->db->get()->row();

        return $user;
    }

    function GetDropDownUser() {
      

        $listkota = GetTableData('uap_user', 'id_user', 'username', array('uap_user.status' => 1),"obj");

        return $listkota;
    }

    function profile_edit($model, $userid = 0) {
        if (CheckEmpty($userid)) {
            $userid = GetUserId();
        }

        if (array_key_exists('profilpic', $_FILES)) {
            if (CheckKey($_FILES['profilpic'], 'name')) {

                $config['upload_path'] = './assets/images/profile/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '1000000';
                $filename = date('Ymd') . '_' . $_FILES['profilpic']['name'];
                $config['file_name'] = $filename;
                $this->load->library('upload', $config);


                if (!$this->upload->do_upload('profilpic')) {
                    $filename = '';
                }
                unset($model['profilpic']);
                if (!CheckEmpty($filename)) {
                    $model['profilpic'] = $filename;
                }
            }
        }
        $model['update_date'] = GetDateNow();
        $model['update_by'] = GetUsername();

        $model['date_birth'] = DefaultTanggalDatabase($model['date_birth']);
        $this->db->update("", $model, array("id_user" => $userid));
    }

    // datatables
    function GetDatauser() {
        $this->load->library('datatables');
        $this->datatables->select('id_user,kode_user,nama_user,username,uap_group.nama_group,uap_pangkalan.nama_pangkalan,uap_user.status');
        $this->datatables->from('uap_user');
        //add this line for join
        $this->datatables->join('uap_pangkalan', 'uap_user.id_pangkalan = uap_pangkalan.id_pangkalan', 'left');
        $this->datatables->join('uap_group', 'uap_user.id_group = uap_group.id_group', 'left');
        $this->datatables->where(array('uap_user.status' => 1));
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor(site_url('user/edit_user/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deleteuser($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_user');
        return $this->datatables->generate();
    }

    // get all
    function GetUserDetail($keyword, $type = 'id_user') {
        $this->db->where($type, $keyword);
        $user = $this->db->get($this->table)->row();
        return $user;
    }

    function UserManipulate($model) {
        try {
            $model['id_group'] = ForeignKeyFromDb($model['id_group']);
            $model['id_jenis_spj'] = ForeignKeyFromDb($model['id_jenis_spj']);

            $model['id_pangkalan'] = ForeignKeyFromDb($model['id_pangkalan']);
            if (!isset($model['jumlah_hari_uapketing']) || !$model['jumlah_hari_uapketing']) {
                $model['jumlah_hari_uapketing'] = NULL;
            }

            if (CheckEmpty($model['id_user'])) {
                $model['created_date'] = GetDateNow();
                $model['kode_user'] = AutoIncrement("uap_user", "P", "kode_user", 3);
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);

                SetMessageSession(1, 'User successfull added into database');
                return array("st" => true, "msg" => "User successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_user" => $model['id_user']));

                SetMessageSession(1, 'User has been updated');
                return array("st" => true, "msg" => "User has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function UserDelete($id_user) {
//        try {
//            $this->db->delete($this->table, array('id_user' => $id_user));
//        } catch (Exception $ex) {
//            $model['updated_date'] = GetDateNow();
//            $model['status'] = 2;
//            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
//            $this->db->update($this->table, $model, array('id_user' => $id_user));
//        }

        $model['updated_date'] = GetDateNow();
        $model['status'] = 0;
        $model['updated_by'] = ForeignKeyFromDb(GetUserId());
        $this->db->update($this->table, $model, array('id_user' => $id_user));
        return array("st" => true, "msg" => "User has been deleted from database");
    }

    function insert_log($model) {
        $model['last_login_date'] = date('Y-m-d');
        $model['last_login_time'] = date('H:i:s');
        $model['last_login_ip'] = $this->input->ip_address();

        $this->db->insert('uap_user_log', $model);
    }

}
