<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/select2/select2.css">
<section class="content-header">
    <h1>
        User <?= @$button ?>
        <small>User</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>User</li>
        <li class="active">User <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
							<form id="frm_user" class="form-horizontal form-groups-bordered validate" method="post">
								<div class="col-md-6">
									<input type="hidden" name="id_user" value="<?php echo @$id_user; ?>" />
									<div class="form-group">
										<?= form_label('Kode User', "txt_kode_user", array("class" => 'col-sm-4 control-label')); ?>
										<div class="col-sm-8">
											<?= form_input(array('type' => 'text', 'value' => @$kode_user, 'class' => 'form-control', 'id' => 'txt_kode_user', 'placeholder' => 'Kode User', 'disabled'=>true)); ?>
										</div>
									</div>
									<div class="form-group">
										<?= form_label('Nama User', "txt_nama_user", array("class" => 'col-sm-4 control-label')); ?>
										<div class="col-sm-8">
											<?= form_input(array('type' => 'text', 'name' => 'nama_user', 'value' => @$nama_user, 'class' => 'form-control', 'id' => 'txt_nama_user', 'placeholder' => 'Nama User')); ?>
										</div>
									</div>
									<div class="form-group">
										<?= form_label('Username', "txt_username", array("class" => 'col-sm-4 control-label')); ?>
										<div class="col-sm-8">
											<?php $merge=array();
											if(!CheckEmpty(@$username)){
												$merge['disabled']=true;
											}else{
												$merge['name']='username';
											}
											?>

											<?= form_input(array_merge($merge,array('type' => 'text', 'value' => @$username, 'class' => 'form-control', 'id' => 'txt_username', 'placeholder' => 'Username'))); ?>
										</div>
									</div>
									<div class="form-group">
										<?= form_label('Group', "txt_id_group", array("class" => 'col-sm-4 control-label')); ?>
										<div class="col-sm-8">
											<?= form_dropdown(array('type' => 'text', 'name' => 'id_group', 'class' => 'form-control', 'id' => 'txt_id_group', 'placeholder' => 'Group'), $list_group, @$id_group); ?>
										</div>
									</div>
									<div class="form-group">
										<?= form_label('Password User', "txt_password_user", array("class" => 'col-sm-4 control-label')); ?>
										<div class="col-sm-8">
											<?= form_input(array('type' => 'password', 'name' => 'password_user', 'value' => @$password_user, 'class' => 'form-control', 'id' => 'txt_password_user', 'placeholder' => 'Password User')); ?>
										</div>
									</div>
									<div class="form-group">
										<?= form_label('Is Public', "txt_is_public", array("class" => 'col-sm-4 control-label')); ?>
										<div class="col-sm-8">
											<?= form_dropdown(array("selected" => @$is_public, "name" => "is_public"), array('1' => 'Yes', '0' => 'No'), @$is_public, array('class' => 'form-control', 'id' => 'is_public')); ?>
										</div>
									</div>
									<div class="form-group">
										<?= form_label('IP Address', "txt_ip_address", array("class" => 'col-sm-4 control-label')); ?>
										<div class="col-sm-8">
											<?= form_input(array('type' => 'text', 'name' => 'ip_address', 'value' => @$ip_address, 'class' => 'form-control', 'id' => 'txt_ip_address', 'placeholder' => 'IP Address')); ?>
										</div>
									</div>
									<div class="form-group">
										<?= form_label('Jenis Spj', "txt_id_jenis_spj", array("class" => 'col-sm-4 control-label')); ?>
										<div class="col-sm-8">
											<?= form_dropdown(array('type' => 'text', 'name' => 'id_jenis_spj','class' => 'form-control', 'id' => 'txt_id_jenis_spj', 'placeholder' => 'Jenis Spj'), $list_jenis_spj, @$id_jenis_spj); ?>
										</div>
									</div>
									<div class="form-group">
										<?= form_label('Pangkalan', "txt_id_pangkalan", array("class" => 'col-sm-4 control-label')); ?>
										<div class="col-sm-8">
											<?= form_dropdown(array('type' => 'text', 'name' => 'id_pangkalan','class' => 'form-control', 'id' => 'txt_id_pangkalan', 'placeholder' => 'Pangkalan'), $list_pangkalan, @$id_pangkalan); ?>
										</div>
									</div>
									<div class="form-group">
										<?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
										<div class="col-sm-8">
											<?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
										</div>
									</div>
									<div class="form-group">
										<?= form_label('Lihat Spj Check', "txt_lihat_spj_check", array("class" => 'col-sm-4 control-label')); ?>
										<div class="col-sm-8">
											<?= form_dropdown(array("selected" => @$lihat_spj_check, "name" => "lihat_spj_check"), array('1' => 'Yes', '0' => 'No'), @$lihat_spj_check, array('class' => 'form-control', 'id' => 'lihat_spj_check')); ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<?= form_label('Vending', "txt_is_vending", array("class" => 'col-sm-4 control-label')); ?>
										<div class="col-sm-8">
											<?= form_dropdown(array("selected" => @$is_vending, "name" => "is_vending"), GetYaTidak(), @$is_vending, array('class' => 'form-control', 'id' => 'txt_is_vending')); ?>
										</div>
									</div>

									<div class="form-group">
										<?= form_label('Komisi', "dropdown_komisi_is_persen", array("class" => 'col-sm-4 control-label')); ?>
										<div class="col-sm-4">
											<?= form_dropdown(array('type' => 'text', 'name' => 'komisi_is_persen', 'class' => 'form-control', 'id' => 'dropdown_komisi_is_persen', 'placeholder' => 'Komisi'), GetTipeKomisi(), @$komisi_is_persen); ?>
										</div>
										<div class="col-sm-4">
											<div class="input-group">
												<div class="input-group-addon" id="opsi-nominal" style="display: none;">Rp.</div>
												<?= form_input(array('type' => 'text', 'name' => 'komisi', 'value' => @$komisi, 'class' => 'form-control', 'id' => 'txt_komisi', 'placeholder' => 'Besar Komisi', 'maxlength' => 6)); ?>
												<div class="input-group-addon" id="opsi-persen" style="display: none;">&percnt;</div>
											</div>
										</div>
									</div>

									<div class="form-group">
										<?= form_label('Akses semua Pangkalan', "dd_allow_all_pangkalan", array("class" => 'col-sm-6 control-label')); ?>
										<div class="col-sm-6">
											<?= form_dropdown(array("name" => "allow_all_pangkalan"), GetYaTidak(), @$allow_all_pangkalan, array('class' => 'form-control', 'id' => 'dd_allow_all_pangkalan')); ?>
										</div>
									</div>

									<div class="form-group">
										<?= form_label('Jumlah Hari Penjualan<br/> [Ticketing]', "txt_jumlah_hari_ticketing", array("class" => 'col-sm-6 control-label')); ?>
										<div class="col-sm-6">
											<?= form_input(array("type"=>"text", "name" => "jumlah_hari_ticketing", 'value' => @$jumlah_hari_ticketing, 'class' => 'form-control', 'id' => 'txt_jumlah_hari_ticketing', "onkeypress" => "return isNumberKey(event);")); ?>
										</div>
									</div>

									<div class="form-group">
										<?= form_label('Tipe Bus', "dropdown_id_tipe_bus", array("class" => 'col-sm-4 control-label')); ?>
										<div class="col-sm-8">
											<?php //echo form_dropdown(array('type' => 'text', 'name' => 'id_tipe_bus','class' => 'form-control', 'id' => 'dropdown_id_tipe_bus', 'placeholder' => 'Tipe Bus'), array(0=>'Semua') + $list_tipe_bus, @$id_tipe_bus); ?>
											<?php
//											$checked = (CheckEmpty(@$id_tipe_bus) || @$id_tipe_bus == 0) ? "checked" : "";
//											echo '<label style="font-weight: normal;">'.form_input(array('type' => 'checkbox', 'name' => 'tipe_bus[]'), 0, $checked) .' Semua</label><br/>';
											foreach($list_tipe_bus as $k=>$v){
												$checked = in_array($k, explode(",", @$id_tipe_bus)) ? "checked" : "";
												echo '<label style="font-weight: normal;">'.form_input(array('type' => 'checkbox', 'name' => 'id_tipe_bus[]'), $k, $checked) .' '.$v.'</label><br/>';
											}
											?>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<a href="<?php echo base_url().'index.php/user' ?>" class="btn btn-default"  >Cancel</a>
										<button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
									</div>
								</div>

</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?php echo base_url() ?>assets/plugins/select2/select2.js"></script>
<script>
$(document).ready(function(){
	<?php if($this->input->post('id_pangkalan')): ?>
	$("#txt_id_pangkalan").select2().val("<?=@$id_pangkalan;?>").trigger('change');
	<?php else: ?>
	$("#txt_id_pangkalan").select2();
	<?php endif; ?>

	$("#dropdown_komisi_is_persen").change(function () {
		var opsi_value = $(this).val();
		if (opsi_value == "0") {
			$("#opsi-persen").hide();
			$("#opsi-nominal").show();
		} else {
			$("#opsi-nominal").hide();
			$("#opsi-persen").show();
		}
	}).change();
});
$("#frm_user").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/user/user_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href=baseurl + 'index.php/user';
                }
                else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    });
</script>