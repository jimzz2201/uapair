<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_barang extends CI_Model {

    public $table = 'uap_barang';
    public $id = 'id_barang';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }
    function GetDetailHistoryBarang($id)
    {
        $this->db->from("uap_barang_history");
        $this->db->where(array("id_barang_history"=>$id));
        return $this->db->get()->row();
    }
    // datatables
    function GetDatabarang() {
        $this->load->library('datatables');
        $this->datatables->select('id_barang,sku_barang,nama_barang,nama_jenis,nama_satuan,barcode,min_stock,harga_beli_average,harga_beli_akhir,harga_jual,image_url,uap_barang.status,kategory_item');
        $this->datatables->from('uap_barang');
        $this->datatables->join("uap_jenis", "uap_jenis.id_jenis=uap_barang.id_jenis", "left");
        $this->datatables->join("uap_satuan", "uap_satuan.id_satuan=uap_barang.id_satuan", "left");
        //add this line for join
        //$this->datatables->join('table2', 'uap_barang.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';


        if ($isedit) {
            $where = array('Lihat Stock' => 'Lihat Stock', 'Lihat Ingridient' => 'Lihat Ingridient', 'Tambah Ingridient' => 'Tambah Ingridient', 'Edit' => "Edit", 'Hapus' => 'Hapus');
        } else {
            $where = array('Lihat Stock' => 'Lihat Stock', 'Lihat Ingridient' => 'Lihat Ingridient', 'Tambah Ingridient' => 'Tambah Ingridient');
        }
        $straction .= form_dropdown(array("selected" => @$kategory_item, "name" => "kategory_item"), DefaultEmptyDropdown($where, "", "Action"), "", array('class' => 'form-control selectaction', 'id' => 'dd_action_$1'));

        $this->datatables->add_column('action', $straction, 'id_barang');
        return $this->datatables->generate();
    }

    function GetHistoryPrice($id_barang, $id_gudang) {
        $this->db->from("uap_barang_history");
        $this->db->where(array("id_barang" => $id_barang, "id_gudang" => $id_gudang, "stock_sisa >" => 0));

        $listhistory = $this->db->get()->result();

        return $listhistory;
    }

    function GetStockByTanggal($id_barang, $id_gudang, $tanggal) {
        $this->db->from("uap_barang_jurnal");
        $this->db->where(array("id_barang" => $id_barang, "id_gudang" => $id_gudang, "tanggal_jurnal <" => DefaultTanggalDatabase($tanggal)));
        $this->db->select("sum(debet) as debet,sum(kredit) as kredit,harga_average");
        $this->db->order_by("tanggal_jurnal desc,created_date desc");
        $row = $this->db->get()->row();
        $barang = $this->GetStockBarang($id_barang, $id_gudang);
        if ($row == null) {
            $row = (object) array("debet" => 0, "kredit" => 0, "harga_average" => $barang->harga_average_awal,"harga_beli_akhir"=>$barang->harga_beli_akhir);
        }
        else
        {
            $row->harga_beli_akhir=$barang->harga_beli_akhir;
        }
        return array("stock" => $row->debet - $row->kredit + $barang->saldo_awal, "harga_average" => $row->harga_average,"harga_beli_akhir"=>$row->harga_beli_akhir,"sku_barang"=>$barang->sku_barang);
    }

    function SyncronizeHargaAverage($id_barang, $id_gudang, $tanggal) {
        $this->db->from("uap_barang_jurnal");
        $this->db->where(array("tanggal_jurnal >=" => DefaultTanggalDatabase($tanggal), 'id_barang' => $id_barang, 'id_gudang' => $id_gudang));
        $this->db->order_by("tanggal_jurnal asc, created_date asc");
        $listhistory = $this->db->get()->result();
        $stocktanggal = $this->GetStockByTanggal($id_barang, $id_gudang, $tanggal);
        $harga_averageawal= $stocktanggal['harga_average'];
        $hargabeliakhir=$stocktanggal['harga_beli_akhir'];
        $harga_average = $stocktanggal['harga_average'];
        $stock = $stocktanggal['stock'];
        $userid=GetUserId();
        foreach ($listhistory as $history) {
            if ($history->debet > 0) {
                $harga_average = ($stock * $harga_average + $history->debet * $history->harga) / ($stock + $history->debet);
                $hargabeliakhir=$history->harga;
            }
            $stock = $stock + $history->debet - $history->kredit;
            $history->harga_average = $harga_average;
            $history->updated_date = GetDateNow();
            $history->updated_by = ForeignKeyFromDb($userid);
            $history->harga_beli_akhir = DefaultCurrencyDatabase($hargabeliakhir);
            $this->db->update("uap_barang_jurnal", $history, array("id_jurnal_barang" => $history->id_jurnal_barang));
        }
        $this->db->from("uap_barang_gudang");
        $this->db->where(array("id_barang" => $id_barang, "id_gudang" => $id_gudang));
        $row = $this->db->get()->row();

        if ($row == null) {
            $update = array();
            $update['updated_date'] = GetDateNow();
            $update['updated_by'] = ForeignKeyFromDb($userid);
            $update['stock'] = $stock;
            $update['saldo_awal'] = DefaultCurrencyDatabase(0);
            $update['harga_average'] = $harga_average;
            $update['stock_min'] = DefaultCurrencyDatabase(0);
            $update['harga_beli_terakhir'] = DefaultCurrencyDatabase($hargabeliakhir);
            $update['harga_average_awal'] = DefaultCurrencyDatabase($harga_averageawal);
            $update['id_gudang'] = $id_gudang;
            $update['id_barang'] = $id_barang;
            $update['created_date'] = GetDateNow();
            $update['created_by'] = ForeignKeyFromDb($userid);
            $this->db->insert("uap_barang_gudang", $update);
        }
        else {
             $this->db->update("uap_barang_gudang", array("stock" => $stock,'harga_beli_terakhir'=>$hargabeliakhir, 'harga_average' => $harga_average, "updated_date" => GetDateNow(), "updated_by" => $userid), array("id_barang" => $id_barang, "id_gudang" => $id_gudang));
        }

    }

    function GetOneHistoryItem($id) {
        $this->db->from("uap_barang_history");
        $this->db->where(array("id_barang_history" => $id));
        return $this->db->get()->row();
    }

    function MutasiBarang($model) {
        $this->db->trans_begin();

        $message = "";
        try {
            $userid = GetUserId();
            $quantity = DefaultCurrencyDatabase($model['quantity']);
            $qtyawal = $quantity;
            $transaction = array();
            $transaction["id_barang"] = $model["id_barang"];
            $transaction["id_gudang"] = $model["id_gudang"];
            $transaction["dokumen"] = AutoIncrement("uap_barang_transaction", $model['jenis'] == "1" ? "MM" : "MK", "dokumen", 5);
            $transaction["module"] = "Mutasi " . $model['jenis'] == "1" ? "Masuk" : "Keluar";
            $transaction["quantity"] = $qtyawal;
            $transaction["description"] = "Mutasi " . ($model['jenis'] == "1" ? "Masuk" : "Keluar");
            $transaction['created_date'] = GetDateNow();
            $transaction['created_by'] = ForeignKeyFromDb($userid);
            $this->db->insert("uap_barang_transaction", $transaction);
            $transaction_id = $this->db->insert_id();
            $mutasi = array();
            $mutasi['id_barang'] = $model['id_barang'];
            $mutasi['id_gudang'] = $model['id_gudang'];
            $mutasi['dokumen'] = $transaction['dokumen'];
            $mutasi['tanggal'] = DefaultTanggalDatabase($model['tanggal_jurnal']);
            $mutasi['keterangan'] = $model['keterangan'];
            $mutasi['jenis'] = $model['jenis'];
            $mutasi['type_stock'] = $model['type_stock'];
            $mutasi['quantity'] = DefaultCurrencyDatabase($model['quantity']);
            $mutasi['created_date'] = GetDateNow();
            $mutasi['created_by'] = ForeignKeyFromDb($userid);
            $this->db->insert("uap_barang_mutasi", $mutasi);
            if ($model['type_stock'] == "0") {
                $this->db->from("uap_barang_history");
                $this->db->where(array("id_gudang" => $model['id_gudang'], "id_barang" => $model["id_barang"], "stock_sisa >" => 0));
                $this->db->order_by("tanggal", "asc");
                $listhistoryfromdb = $this->db->get()->result();

                if ($model['jenis'] == 2) {
                    foreach ($listhistoryfromdb as $histdb) {

                        $qtydatabase = 0;
                        if ($quantity > $histdb->stock_sisa) {
                            $qtydatabase = $histdb->stock_sisa;
                            $quantity -= $histdb->stock_sisa;
                            $histdb->stock_sisa = 0;
                        } else {
                            $histdb->stock_sisa = $histdb->stock_sisa - $quantity;
                            $qtydatabase = $quantity;
                            $quantity = 0;
                        }

                        $histdb->updated_by = $userid;
                        $histdb->updated_date = GetDateNow();
                        $this->db->update("uap_barang_history", $histdb, array("id_barang_history" => $histdb->id_barang_history));
                        $jurnal = array();
                        $jurnal['id_barang'] = $model['id_barang'];
                        $jurnal['reference_id'] = $transaction_id;
                        $jurnal['tanggal_jurnal'] = DefaultTanggalDatabase($model['tanggal_jurnal']);
                        $jurnal['id_transaction'] = $transaction_id;
                        $jurnal['dokumen'] = $transaction["dokumen"];
                        $jurnal['description'] = $model["keterangan"];
                        $jurnal['kredit'] = $qtydatabase;
                        $jurnal['debet'] = 0;
                        $jurnal['harga_average'] = 0;
                        $jurnal['harga_beli_akhir'] = 0;
                        $jurnal['harga'] = $histdb->harga_ref;
                        $jurnal['id_gudang'] = $model['id_gudang'];
                        $jurnal['id_barang_history'] = $histdb->id_barang_history;
                        $jurnal['created_date'] = GetDateNow();
                        $jurnal['created_by'] = $userid;
                        $this->db->insert("uap_barang_jurnal", $jurnal);
                        if ($quantity == 0) {
                            break;
                        }
                    }
                } else {
                    $harga_beli_akhir = $this->GetStockBarang($model['id_barang'], $model['id_gudang'])->harga_beli_akhir;
                    
                    $history = array();
                    $history['id_barang'] = $model['id_barang'];
                    $history['id_gudang'] = $model['id_gudang'];
                    $history['id_transaction'] = $transaction_id;
                    $history['tanggal'] = DefaultTanggalDatabase($model['tanggal_jurnal']);
                    $history['dokumen'] = $transaction["dokumen"];
                    $history['stock_tersedia'] = $quantity;
                    $history['stock_sisa'] = $quantity;
                    $history['harga_ref'] = $harga_beli_akhir;
                    $history['created_date'] = GetDateNow();
                    $history['created_by'] = ForeignKeyFromDb($userid);
                    $this->db->insert("uap_barang_history", $history);
                    $id_barang_history = $this->db->insert_id();
                    $jurnal = array();
                    $jurnal['id_barang'] = $model['id_barang'];
                    $jurnal['reference_id'] = $transaction_id;
                    $jurnal['tanggal_jurnal'] = DefaultTanggalDatabase($model['tanggal_jurnal']);
                    $jurnal['id_transaction'] = $transaction_id;
                    $jurnal['dokumen'] = $transaction["dokumen"];
                    $jurnal['description'] = $model["keterangan"];
                    $jurnal['kredit'] = 0;
                    $jurnal['debet'] = $quantity;
                    $jurnal['harga_average'] = 0;
                    $jurnal['harga_beli_akhir'] = 0;
                    $jurnal['harga'] = $harga_beli_akhir;
                    $jurnal['id_gudang'] = $model['id_gudang'];
                    $jurnal['id_barang_history'] = $id_barang_history;
                    $jurnal['created_date'] = GetDateNow();
                    $jurnal['created_by'] = $userid;
                    $this->db->insert("uap_barang_jurnal", $jurnal);
                }
            } else {
                $isupdate = false;
                if ($model['jenis'] == 2) {
                    foreach ($model['id_history'] as $key => $history) {
                        $objhist = $this->GetOneHistoryItem($history);
                        if ($objhist->stock_sisa < DefaultCurrencyDatabase($model['qty'][$key])) {
                            $message .= "Stock tidak tersedia<br/>";
                        } else {
                            if (DefaultCurrencyDatabase($model['qty'][$key]) > 0) {
                                $isupdate = true;
                                $jurnal = array();
                                $jurnal['id_barang'] = $model['id_barang'];
                                $jurnal['reference_id'] = $transaction_id;
                                $jurnal['tanggal_jurnal'] = DefaultTanggalDatabase($model['tanggal_jurnal']);
                                $jurnal['id_transaction'] = $transaction_id;
                                $jurnal['dokumen'] = $transaction["dokumen"];
                                $jurnal['description'] = $model["keterangan"];
                                $jurnal['kredit'] = DefaultCurrencyDatabase($model['qty'][$key]);
                                $jurnal['debet'] = 0;
                                $jurnal['harga_average'] = 0;
                                $jurnal['harga_beli_akhir'] = 0;
                                $jurnal['harga'] = DefaultCurrencyDatabase($model['price'][$key]);
                                $jurnal['id_gudang'] = $model['id_gudang'];
                                $jurnal['id_barang_history'] = $history;
                                $jurnal['created_date'] = GetDateNow();
                                $jurnal['created_by'] = $userid;
                                $this->db->insert("uap_barang_jurnal", $jurnal);
                                $hist = array();
                                $hist['stock_sisa -='] = $jurnal['kredit'];
                                $hist['updated_date'] = GetDateNow();
                                $hist['updated_by'] = ForeignKeyFromDb($userid);
                                $this->db->update("uap_barang_history", $hist, array("id_history" => $history));
                            }
                        }
                    }
                } else {
                    foreach ($model['id_history'] as $key => $history) {
                        $objhist = $this->GetOneHistoryItem($history);
                        if (DefaultCurrencyDatabase($model['qty'][$key]) > 0) {
                            $isupdate = true;
                            $hist = array();
                            $hist['stock_sisa'] = DefaultCurrencyDatabase($model['qty'][$key]);
                            $hist['stock_tersedia'] = DefaultCurrencyDatabase($model['qty'][$key]);
                            $hist['created_date'] = GetDateNow();
                            $hist['created_by'] = ForeignKeyFromDb($userid);
                            $hist['tanggal'] = DefaultTanggalDatabase($model['tanggal'][$key]);
                            $hist['id_barang'] = $model['id_barang'];
                            $hist['id_gudang'] = $model['id_gudang'];
                            $hist['id_transaction'] = $transaction_id;
                            $hist['dokumen'] = $transaction["dokumen"];
                            $hist['harga_ref'] = DefaultCurrencyDatabase($model['price'][$key]);
                            $hist['created_date'] = GetDateNow();
                            $hist['created_by'] = $userid;
                            $this->db->insert("uap_barang_history", $hist);
                            $jurnal = array();
                            $jurnal['id_barang'] = $model['id_barang'];
                            $jurnal['reference_id'] = $transaction_id;
                            $jurnal['tanggal_jurnal'] = DefaultTanggalDatabase($model['tanggal_jurnal']);
                            $jurnal['id_transaction'] = $transaction_id;
                            $jurnal['dokumen'] = $transaction["dokumen"];
                            $jurnal['description'] = $model["keterangan"];
                            $jurnal['kredit'] = 0;
                            $jurnal['debet'] = DefaultCurrencyDatabase($model['qty'][$key]);
                            $jurnal['harga_average'] = 0;
                            $jurnal['harga_beli_akhir'] = 0;
                            $jurnal['harga'] = DefaultCurrencyDatabase($model['price'][$key]);
                            $jurnal['id_gudang'] = $model['id_gudang'];
                            $jurnal['id_barang_history'] = $history;
                            $jurnal['created_date'] = GetDateNow();
                            $jurnal['created_by'] = $userid;
                            $this->db->insert("uap_barang_jurnal", $jurnal);
                        }
                    }
                }
                if (!$isupdate) {
                    $message .= "Masukkan quantity mutasi<br/>";
                }
            }




            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->SyncronizeHargaAverage($model['id_barang'], $model['id_gudang'], DefaultCurrencyDatabase($model['tanggal_jurnal']));
                $this->db->trans_commit();
                $arrayreturn["msg"] = "Stock dan Saldo Awal barang telah diupdate";
                $arrayreturn["st"] = true;
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetStockBarang($id_barang, $id_gudang) {
        $this->db->from("uap_barang_gudang");
        $this->db->select("uap_barang_gudang.*,sku_barang");
        $this->db->join("uap_barang","uap_barang.id_barang=uap_barang_gudang.id_barang","left");
        $this->db->where(array("uap_barang_gudang.id_barang" => $id_barang, "uap_barang_gudang.id_gudang" => $id_gudang));
        $row = $this->db->get()->row();
        $barang = $this->GetOneBarang($id_barang);
        if ($row) {
            if (CheckEmpty(@$row->harga_beli_akhir)) {
                $row->harga_beli_akhir = $barang->harga_beli_akhir;
            }
            return $row;
        } else {

            if ($barang) {
                $harga_average_awal = $barang->harga_beli_akhir;
            } else {
                $harga_average_awal = 0;
            }
            return (object) array("stock" => 0, "stock_min" => 0, "saldo_awal" => 0,'sku_barang'=>@$barang->sku_barang, 'harga_average_awal' => $harga_average_awal, "harga_beli_akhir" => $harga_average_awal);
        }
    }

    function KoreksiSaldoBarang($id_barang, $id_gudang, $saldo_awal, $harga_average_awal, $stock_min) {
        try {
            $this->db->trans_begin();
            $this->db->from("uap_barang_jurnal");
            $this->db->where(array("id_barang" => $id_barang, "id_gudang" => $id_gudang));
            $this->db->order_by("tanggal_jurnal");
            $listtransaksi = $this->db->get()->result();
            $stock = DefaultCurrencyDatabase($saldo_awal);

            $this->db->from("uap_barang_gudang");
            $this->db->where(array("id_barang" => $id_barang, "id_gudang" => $id_gudang));
            $stock_awal = $this->db->get()->row();
            $harga_average = DefaultCurrencyDatabase($harga_average_awal);

            foreach ($listtransaksi as $transaksi) {
                $stock = $stock + $transaksi->debet - $transaksi->kredit;
                if ($stock_awal != null && $saldo_awal != $stock_awal->saldo_awal) {
                    if ($transaksi->debet > 0) {
                        $harga_average = (($harga_average * $stock) + ($transaksi->harga * $transaksi->debet)) / ($transaksi->debet + $stock);
                    }
                    $this->db->update("uap_barang_jurnal", array("harga_average" => $harga_average), array("id_jurnal_barang" => $transaksi->id_jurnal_barang));
                }
            }
            $update = array();
            $update['updated_date'] = GetDateNow();
            $update['updated_by'] = ForeignKeyFromDb(GetUserId());
            $update['stock'] = $stock;
            $update['saldo_awal'] = DefaultCurrencyDatabase($saldo_awal);
            $update['harga_average'] = $harga_average;
            $update['stock_min'] = DefaultCurrencyDatabase($stock_min);
            $update['harga_average_awal'] = DefaultCurrencyDatabase($harga_average_awal);
            $message = "";
            $transaction = array();
            $transaction["id_barang"] = $id_barang;
            $transaction["id_gudang"] = $id_gudang;
            $transaction["dokumen"] = AutoIncrement("uap_barang_transaction", "SL", "dokumen", 5);
            $transaction["module"] = "UpdateStockAwal";
            $transaction["quantity"] = $update['saldo_awal'];
            $transaction["description"] = "Update Stock awal , stock menjadi " . $update['stock'];
            $transaction['created_date'] = GetDateNow();
            $transaction['created_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->insert("uap_barang_transaction", $transaction);
            $transaction_id = $this->db->insert_id();


            if ($stock_awal) {
                $this->db->update("uap_barang_gudang", $update, array("id_barang" => $id_barang, "id_gudang" => $id_gudang));
                $this->db->select("uap_barang_history.*");
                $this->db->from("uap_barang_history");
                $this->db->join("uap_barang_transaction", "uap_barang_transaction.id_transaction=uap_barang_history.id_transaction");
                $this->db->where(array("uap_barang_history.id_barang" => $id_barang, "uap_barang_history.id_gudang" => $id_gudang, "module" => "UpdateStockAwal"));
                $this->db->order_by("id_transaction", "desc");
                $transhis = $this->db->get()->row();


                if ($transhis != null) {
                    $stock_tersedia = $transhis->stock_tersedia;
                    $stock_sisa = $transhis->stock_sisa;

                    if ($saldo_awal > $transhis->stock_tersedia - $transhis->stock_sisa) {
                        $updatetrans = $transhis;
                        $updatetrans->stock_tersedia = DefaultCurrencyDatabase($saldo_awal);
                        $updatetrans->stock_sisa = $updatetrans->stock_tersedia - ($stock_tersedia - $stock_sisa);

                        $updatetrans->id_transaction = $transaction_id;
                        $updatetrans->dokumen = $transaction["dokumen"];
                        $updatetrans->created_date = GetDateNow();
                        $updatetrans->created_by = ForeignKeyFromDb(GetUserId());
                        $this->db->update("uap_barang_history", $updatetrans, array("id_barang_history" => $updatetrans->id_barang_history));
                        if ($updatetrans->stock_sisa < 0 || $updatetrans->stock_sisa > $updatetrans->stock_tersedia) {
                            $message .= "Barang di saldo awal sebelumnya sudah dijual";
                        }
                    } else {
                        $message .= "Barang di saldo awal sebelumnya sudah dijual";
                    }
                } else {
                    $history = array();
                    $history['id_barang'] = $id_barang;
                    $history['id_gudang'] = $id_gudang;
                    $history['id_transaction'] = $transaction_id;
                    $history['dokumen'] = $transaction["dokumen"];
                    $history['stock_tersedia'] = $stock;
                    $history['stock_sisa'] = $stock;
                    $history['harga_ref'] = DefaultCurrencyDatabase($harga_average_awal);
                    $history['created_date'] = GetDateNow();
                    $history['created_by'] = ForeignKeyFromDb(GetUserId());
                    $this->db->insert("uap_barang_history", $history);
                }
            } else {
                $update['id_gudang'] = $id_gudang;
                $update['id_barang'] = $id_barang;
                $update['created_date'] = GetDateNow();
                $update['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert("uap_barang_gudang", $update);

                $history = array();
                $history['id_barang'] = $id_barang;
                $history['id_gudang'] = $id_gudang;
                $history['id_transaction'] = $transaction_id;
                $history['dokumen'] = $transaction["dokumen"];
                $history['stock_tersedia'] = $stock;
                $history['stock_sisa'] = $stock;
                $history['harga_ref'] = DefaultCurrencyDatabase($harga_average_awal);
                $history['created_date'] = GetDateNow();
                $history['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert("uap_barang_history", $history);
            }

            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                $arrayreturn["msg"] = "Stock dan Saldo Awal barang telah diupdate";
                $arrayreturn["st"] = true;
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    // get all
    function GetOneBarang($keyword, $type = 'id_barang') {
        $this->db->where($type, $keyword);
        $barang = $this->db->get($this->table)->row();
        return $barang;
    }

    function BarangManipulate($model) {
        try {
            $model['id_merek'] = ForeignKeyFromDb($model['id_merek']);
            $model['id_jenis'] = ForeignKeyFromDb($model['id_jenis']);
            $model['min_stock'] = DefaultCurrencyDatabase($model['min_stock']);
            $model['harga_jual'] = DefaultCurrencyDatabase($model['harga_jual']);
            $model['id_satuan'] = ForeignKeyFromDb($model['id_satuan']);
            $model['status'] = DefaultCurrencyDatabase($model['status']);
            $model['kategory_item'] = DefaultCurrencyDatabase($model['kategory_item']);

            if (CheckEmpty($model['id_barang'])) {
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                SetMessageSession(1, 'Barang successfull added into database');
                return array("st" => true, "msg" => "Barang successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_barang" => $model['id_barang']));
                SetMessageSession(1, 'Barang has been updated');
                return array("st" => true, "msg" => "Barang has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function KoreksiManipulate($model) {
        try {
            $model['id_merek'] = ForeignKeyFromDb($model['id_merek']);
            $model['id_jenis'] = ForeignKeyFromDb($model['id_jenis']);
            $model['min_stock'] = DefaultCurrencyDatabase($model['min_stock']);
            $model['harga_jual'] = DefaultCurrencyDatabase($model['harga_jual']);
            $model['id_satuan'] = ForeignKeyFromDb($model['id_satuan']);
            $model['status'] = DefaultCurrencyDatabase($model['status']);
            $model['kategory_item'] = DefaultCurrencyDatabase($model['kategory_item']);

            if (CheckEmpty($model['id_barang'])) {
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                SetMessageSession(1, 'Barang successfull added into database');
                return array("st" => true, "msg" => "Barang successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_barang" => $model['id_barang']));
                SetMessageSession(1, 'Barang has been updated');
                return array("st" => true, "msg" => "Barang has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDropDownBarang() {

        $where = [];
        $where['status'] = 1;
        $listreturn = GetTableData("uap_barang", 'id_barang', 'nama_barang', $where, 'obj');
        return $listreturn;
    }

    function BarangDelete($id_barang) {
        try {
            $this->db->delete($this->table, array('id_barang' => $id_barang));
        } catch (Exception $ex) {
            $model['updated_date'] = GetDateNow();
            $model['status'] = 2;
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array('id_barang' => $id_barang));
        }
        return array("st" => true, "msg" => "Barang has been deleted from database");
    }

}
