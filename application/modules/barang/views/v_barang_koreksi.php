<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Barang</h2>
                                <p><?= $button ?> Barang | Data Barang</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_koreksi_barang" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="id_barang" value="<?php echo @$id_barang; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Nama', "txt_nama_barang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_barang"), DefaultEmptyDropdown($list_barang, "", "Barang"), @$id_barang, array('class' => 'form-control', 'id' => 'dd_id_barang')); ?>
                                    </div>
                                    <?= form_label('Gudang', "txt_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("name" => "id_gudang"), DefaultEmptyDropdown($list_gudang, "", "Gudang"), @$id_gudang, array('class' => 'form-control', 'id' => 'dd_id_gudang')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Saldo Awal Sebelumnya', "txt_saldo_awal", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'value' => DefaultCurrency(@$saldo_awal), 'class' => 'form-control', 'id' => 'txt_saldo_awal', 'placeholder' => 'Saldo Awal', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Stock', "txt_stock", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'value' => DefaultCurrency(@$stock), 'class' => 'form-control', 'id' => 'txt_stock', 'placeholder' => 'Stock', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Saldo Awal', "txt_saldo_awal", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'saldo_awal', 'value' => DefaultCurrency(@$saldo_awal), 'class' => 'form-control', 'id' => 'txt_saldo_semestinya', 'placeholder' => 'Saldo Awal', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Min Stock', "txt_stock_min", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'stock_min', 'value' => DefaultCurrency(@$stock_min), 'class' => 'form-control', 'id' => 'txt_stock_min', 'placeholder' => 'Min Stock', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Harga Pertama', "txt_saldo_awal", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'harga_average_awal', 'value' => DefaultCurrency(@$harga_average_awal), 'class' => 'form-control', 'id' => 'txt_harga_average_awal', 'placeholder' => 'Average Awal', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    
                                </div>
                                <div class="form-group buttonarea">
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    
    function RefreshStock() {
        $.ajax({
        type: 'POST',
                url: baseurl + 'index.php/barang/get_stock_barang',
                dataType: 'json',
                data: $("form#frm_koreksi_barang").serialize(),
                success: function (data) {
                        $("#txt_saldo_awal").val(Comma(data.saldo_awal));
                        $("#txt_saldo_semestinya").val(Comma(data.saldo_awal));
                        $("#txt_stock").html(Comma(data.stock));
                        $("#txt_stock_min").val(Comma(data.stock_min));
                        $("#txt_harga_average_awal").val(Comma(data.harga_average_awal));
                },
                error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                }
        });
        

    }
    $(document).ready(function () {

        $("#dd_id_barang").select2();
        $("#dd_id_barang , #dd_id_gudang").change(function () {
            RefreshStock();
        })
    })

    $("#frm_koreksi_barang").submit(function () {
        swal({
            title: "Apakah anda ingin melakukan transaksi berikut?",
            text: "Transaksi ini akan mengubah saldo dan hutang barang!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Ubah data ini!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/barang/koreksi_saldo_action',
                    dataType: 'json',
                    data: $("form#frm_koreksi_barang").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            $("#dd_id_barang").val(0).trigger('change');
                            $("#dd_id_gudang").val(0);
                            $("#txt_saldo_awal").val(0);
                            $("#txt_saldo_semestinya").val(0);
                            $("#txt_stock").html(0);
                            $("#txt_stock_min").val(0);
                            $("#txt_harga_average_awal").val(0);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });

        return false;

    })
</script>