<div class="form-group">
    <?= form_input(array('type' => 'hidden', 'name' => 'id_history[]', 'value' => 0, 'class' => 'form-control')); ?>
    <div class="col-sm-2">
        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal[]', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
    </div>
    <?= form_label('Qty', "txt_saldo_awal", array("class" => 'col-sm-1 control-label')); ?>
    <div class="col-sm-2">
        <?= form_input(array('type' => 'text', "name" => "qty[]", 'value' => DefaultCurrency(0), 'class' => 'form-control', 'placeholder' => 'Quantity', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
    </div>
    <?= form_label('Price', "txt_stock", array("class" => 'col-sm-1 control-label')); ?>
    <div class="col-sm-3">
        <?= form_input(array('type' => 'text', 'name' => "price[]", 'value' => DefaultCurrency(0), 'class' => 'form-control', 'placeholder' => 'Stock', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
         <?= form_input(array('type' => 'hidden', 'name' => "stock[]", 'value' => 0, 'class' => 'form-control', 'placeholder' => 'Stock', )); ?>
    </div>

    <div class="col-sm-1">
        <button type="button" onclick="TambahRow()" style="padding:4px 12px" class="btn btn-success waves-effect" id="btt_modal_ok" >Tambah</button>
    </div>
    <div class="col-sm-1">
        <button type="button" onclick="HapusRow(this)" style="padding:4px 12px" class="btn btn-danger btthapus waves-effect" id="btt_modal_ok" >Hapus</button>
    </div>
</div>
