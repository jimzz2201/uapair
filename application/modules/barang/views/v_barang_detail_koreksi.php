<hr/>
<?php

foreach ($listhistory as $history) { ?>
    <div class="form-group default_area">
        <?= form_input(array('type' => 'hidden', 'name' => 'id_history[]', 'value' => @$history->id_barang_history, 'class' => 'form-control')); ?>
        <?= form_input(array('type' => 'hidden', 'name' => 'tanggal[]', 'value' => DefaultDatePicker($history->tanggal), 'class' => 'form-control')); ?>
        <?= form_label($history->tanggal == "0000-00-00" ? 'Saldo Awal | ' : DefaultDatePicker($history->tanggal) . " | ", "Saldo Awal", array("class" => 'col-sm-2 control-label')); ?>
        <?= form_label('Qty', "txt_saldo_awal", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-2">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', "name" => "stock[]", 'value' => DefaultCurrency(@$history->stock_sisa), 'class' => 'form-control', 'placeholder' => 'Quantity')); ?>
        </div>
        <?= form_label('Price', "txt_stock", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => "price[]", 'value' => DefaultCurrency(@$history->harga_ref), 'class' => 'form-control', 'id' => 'txt_stock', 'placeholder' => 'Stock', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
        </div>
        <?= form_label('Nominal', "txt_saldo_awal", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-2">
            <?= form_input(array('type' => 'text', 'name' => 'qty[]', 'value' => DefaultCurrency(0), 'class' => 'form-control', 'placeholder' => 'Quantity', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
        </div>
    </div>
<?php } ?>

