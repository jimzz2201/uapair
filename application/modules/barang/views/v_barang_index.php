
<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Barang</h2>
                                <p>Barang | Data Master</p>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-3'>
                        <div class='breadcomb-report'>
                            <?php echo anchor(site_url('barang/create_barang'), '<i class="fa fa-plus"></i> Create', 'class="btn btn-success"'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="">
    <div class="box box-default form-element-list">


        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</div>
<script type="text/javascript">
    var table;
    function deletebarang(id_barang) {
        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            $("#dd_action_"+id_barang).val(0);
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/barang/barang_delete',
                    dataType: 'json',
                    data: {
                        id_barang: id_barang
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }

                });
            }
        });


    }
    function RefreshGrid() {
        $(".selectaction").change(function () {
            var valselect = $(this).val();
            var id = $(this).attr("id").replace("dd_action_", "");
            if (valselect != "0")
            {
                 if(valselect=="Lihat Stock")
                 {
                     
                 }
                 else if(valselect=="Lihat Ingridient")
                 {
                     
                 }
                 else if(valselect=="Tambah Ingridient")
                 {
                     
                 }
                 else if(valselect=="Edit")
                 {
                     window.location.href=baseurl+"index.php/barang/edit_barang/"+id;
                 }
                 else if(valselect=="Hapus")
                 {
                     deletebarang(id);
                 }
                 else
                 {
                     $(this).val(0);
                 }
            }
        })

    }

    $(document).ready(function () {
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": "barang/getdatabarang", "type": "POST"},
            columns: [
                {
                    data: "id_barang",
                    title: "Kode",
                    orderable: false
                }
                , {data: "sku_barang", orderable: false, title: "Sku Barang"}
                , {data: "nama_barang", orderable: false, title: "Nama Barang"}
                , {data: "nama_jenis", orderable: false, title: "Group"}
                , {data: "harga_beli_akhir", orderable: false, title: "Harga Beli",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                , {data: "harga_jual", orderable: false, title: "Harga Jual",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                , {data: "status", orderable: false, title: "Status",
                    mRender: function (data, type, row) {
                        return data == 1 ? "Active" : "Not Active";
                    }}
                , {data: "kategory_item", orderable: false, title: "Kategory",
                    mRender: function (data, type, row) {
                        return data == 1 ? "Bahan Baku" : (data == 2 ? "Bahan Jadi" : "Dua Duanya");
                    }}
                ,
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center nopadding"
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

            },
            initComplete: function () {
                RefreshGrid();
            }
        });

    });
</script>
