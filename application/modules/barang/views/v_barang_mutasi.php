<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Barang</h2>
                                <p><?= $button ?> Barang | Data Barang</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_mutasi_barang" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="id_barang" value="<?php echo @$id_barang; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Tanggal', "txt_saldo_awal", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal_jurnal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <?= form_label('Nama', "txt_nama_barang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_barang"), DefaultEmptyDropdown($list_barang, "", "Barang"), @$id_barang, array('class' => 'form-control select2', 'id' => 'dd_id_barang')); ?>
                                    </div>
                                    <?= form_label('Gudang', "txt_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("name" => "id_gudang"), DefaultEmptyDropdown($list_gudang, "", "Gudang"), @$id_gudang, array('class' => 'form-control', 'id' => 'dd_id_gudang')); ?>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_saldo_awal", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'value' => @$keterangan,'name'=>'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Jenis', "txt_saldo_awal", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("selected" => @$jenis, "name" => "jenis"), array('0' => "Pilih Jenis", '1' => 'Masuk', '2' => 'Keluar'), @$status, array('class' => 'form-control', 'id' => 'dd_jenis')); ?>
                                    </div>
                                    <?= form_label('Type', "txt_saldo_awal", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("selected" => @$type_stock, "name" => "type_stock"), array('0' => "Default", '1' => 'Custom'), @$type_stock, array('class' => 'form-control', 'id' => 'dd_type_stock')); ?>
                                    </div>
                                </div>


                                <div class="form-group default_area">
                                    <?= form_label('Quantity', "txt_saldo_awal", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'quantity', 'value' => DefaultCurrency(@$quantity), 'class' => 'form-control', 'id' => 'txt_quantity', 'placeholder' => 'Quantity', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Stock', "txt_stock", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'value' => DefaultCurrency(@$stock), 'class' => 'form-control', 'id' => 'txt_stock', 'placeholder' => 'Stock', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>

                                <div class="form-group custom_area" style="display:none">
                                    <?= form_label('Stock', "txt_stock", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'value' => DefaultCurrency(@$stock), 'class' => 'form-control', 'id' => 'txt_stock', 'placeholder' => 'Stock', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>

                                <div id="divgroup" class="custom_area" style="display:none">

                                </div>
                                <div class="custom_area input_masuk">
                                    <?php include APPPATH . 'modules/barang/views/v_barang_detail_mutasi.php' ?> 
                                </div>
                                <div class="custom_area input_masuk grouptambah">
                                </div>
                                <div class="form-group buttonarea">
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="frmdefaultcustom" style="display:none">
        <?php include APPPATH . 'modules/barang/views/v_barang_detail_mutasi.php' ?> 
    </div>
</section>
<script>
    $(document).ready(function () {
        RefreshChoice();
        $(".input_masuk").find(".btthapus").remove();
        $(".select2").select2();

    })
    function HapusRow(e) {

        $(e).parent().parent().remove();
    }
    function TambahRow() {

        $(".grouptambah").append($(".frmdefaultcustom").html());
        $(".datepicker").datepicker();
    }

    function RefreshChoice() {
        if (!($("#dd_type_stock").val() == "1" && $("#dd_id_barang").val() > 0 && $("#dd_id_gudang").val() > 0))
        {
            $(".default_area").css("display", "block");
            $(".custom_area").css("display", "none");
            if($("#dd_id_barang").val() > 0 && $("#dd_id_gudang").val() > 0){
                RefreshStock();
            }
            else
            {
                 $("#txt_stock").html(0);
            }
            
        } 
        else
        {

            $(".default_area").css("display", "none");
            $(".custom_area").css("display", "block");
            $(".grouptambah").empty();
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/barang/gethistoryprice',
                data: $("form#frm_mutasi_barang").serialize(),
                success: function (data) {
                    $("#divgroup").html(data);
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
            if ($("#dd_jenis").val() == "1")
            {
                $(".input_masuk").css("display", "block");
            } else
            {
                $(".input_masuk").css("display", "none");
            }
        }
    }




    function RefreshStock() {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/barang/get_stock_barang',
            dataType: 'json',
            data: $("form#frm_mutasi_barang").serialize(),
            success: function (data) {
                $("#txt_stock").val(Comma(data.stock));
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });


    }
    $(document).ready(function () {
        $(".datepicker").datepicker();
        $("#dd_id_barang").select2();
        $("#dd_id_barang , #dd_id_gudang , #dd_type_stock ,#dd_jenis").change(function () {
            RefreshChoice();
        })
    })

    $("#frm_mutasi_barang").submit(function () {
        swal({
            title: "Apakah anda ingin melakukan transaksi berikut?",
            text: "Transaksi ini akan mengubah saldo dan hutang barang!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Ubah data ini!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/barang/mutasi_barang_action',
                    dataType: 'json',
                    data: $("form#frm_mutasi_barang").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            $('#frm_mutasi_barang')[0].reset();
                            $("#dd_id_barang").val(0).trigger('change');
                            $("#dd_id_gudang").val(0);
                           
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });

        return false;

    })
</script>