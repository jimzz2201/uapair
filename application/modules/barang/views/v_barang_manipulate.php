<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Barang</h2>
                                <p>Add Barang | Data Barang</p>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-3'>
                        <div class='breadcomb-report'>
                            <?php echo anchor(site_url('/barang'), '<i class="fa fa-arrow-left"></i> Cancel', 'class="btn btn-warning"'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_barang" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="id_barang" value="<?php echo @$id_barang; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Sku', "txt_sku_barang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'value' => @$sku_barang, 'class' => 'form-control', 'id' => 'txt_sku_barang', 'placeholder' => 'Sku Barang')); ?>
                                    </div>
                                    <?= form_label('Barcode', "txt_barcode", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'barcode', 'value' => @$barcode, 'class' => 'form-control', 'id' => 'txt_barcode', 'placeholder' => 'Barcode')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Nama Barang', "txt_nama_barang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_input(array('type' => 'text', 'name' => 'nama_barang', 'value' => @$nama_barang, 'class' => 'form-control', 'id' => 'txt_nama_barang', 'placeholder' => 'Nama Barang')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Group', "txt_id_jenis", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_jenis"), DefaultEmptyDropdown(@$list_jenis, "", "Group"), @$id_jenis, array('class' => 'form-control', 'id' => 'dd_id_jenis')); ?>
                                    </div>
                                    <?= form_label('Merek', "txt_id_merek", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("name" => "id_merek"), DefaultEmptyDropdown(@$list_merek, "", "Merek"), @$id_merek, array('class' => 'form-control', 'id' => 'dd_id_merek')); ?>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <?= form_label('Min Stock', "txt_min_stock", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'min_stock', 'value' => DefaultCurrency(@$min_stock), 'class' => 'form-control', 'id' => 'txt_min_stock', 'placeholder' => 'Min Stock', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Satuan', "txt_id_satuan", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("name" => "id_satuan"), DefaultEmptyDropdown(@$list_satuan, "", "Satuan"), @$id_satuan, array('class' => 'form-control', 'id' => 'dd_id_satuan')); ?>
                                    </div>
                                </div>
                                <?php if (CekModule("K098", false)) { ?>
                                    <div class="form-group">
                                        <?= form_label('Beli Average', "txt_harga_beli_average", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'disabled' => "disabled", 'value' => DefaultCurrency(@$harga_beli_average), 'class' => 'form-control', 'id' => 'txt_harga_beli_average', 'placeholder' => 'Harga Beli Average', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                        <?= form_label('Beli Akhir', "txt_harga_beli_akhir", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_input(array('type' => 'text', 'disabled' => "disabled", 'value' => DefaultCurrency(@$harga_beli_akhir), 'class' => 'form-control', 'id' => 'txt_harga_beli_akhir', 'placeholder' => 'Harga Beli Akhir', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="form-group">
                                    <?= form_label('Harga Jual', "txt_harga_jual", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'harga_jual', 'value' => DefaultCurrency(@$harga_jual), 'class' => 'form-control', 'id' => 'txt_harga_jual', 'placeholder' => 'Harga Jual', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                    </div>
                                    <?= form_label('Kategory', "txt_kategory_item", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("selected" => @$kategory_item, "name" => "kategory_item"), DefaultEmptyDropdown(array('1' => 'Bahan Baku', '2' => 'Bahan Jadi', '3' => "Dua Duanya"), "", "Kategori"), @$kategory_item, array('class' => 'form-control', 'id' => 'dd_kategory_item')); ?>
                                    </div>
                                </div>

                                <div class="form-group buttonarea">
                                    <a href="<?php echo base_url() . 'index.php/barang' ?>" class="btn btn-default"  >Cancel</a>
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><script>
    $("#frm_barang").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/barang/barang_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href = baseurl + 'index.php/barang';
                } else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>