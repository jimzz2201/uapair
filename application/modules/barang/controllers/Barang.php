<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Barang extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_barang');
    }

    public function gethistoryprice() {
        $model = $this->input->post();
        if (!CheckKey($model, 'id_gudang')) {
            $model["id_gudang"] = 0;
        }
        if (!CheckKey($model, 'id_barang')) {
            $model["id_barang"] = 0;
        }
        $listhistory = $this->m_barang->GetHistoryPrice($model["id_barang"], $model["id_gudang"]);
       
        $this->load->view('barang/v_barang_detail_koreksi', array("listhistory" => $listhistory));
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K002";
        $header = "K001";
        $title = "Barang";
        CekModule($module);
        LoadTemplate(array("title" => $title, "form" => $header, "formsubmenu" => $module), "barang/v_barang_index", $javascript);
    }

    public function get_stock_barang() {
        $model = $this->input->post();
        echo json_encode($this->m_barang->GetStockBarang(@$model["id_barang"], @$model['id_gudang']));
    }

    public function getdatabarang() {
        header('Content-Type: application/json');
        echo $this->m_barang->GetDatabarang();
    }

    public function create_barang() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K004";
        $header = "K001";
        CekModule($module);
        $this->load->model("satuan/m_satuan");
        $this->load->model("jenis/m_jenis");
        $this->load->model("merek/m_merek");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->list_merek = $this->m_merek->GetDropDownMerek();
        $row->list_jenis = $this->m_jenis->GetDropDownJenis();
        $row->list_satuan = $this->m_satuan->GetDropDownSatuan();
        $row->title = 'Create Barang';
        $row = json_decode(json_encode($row), true);

        $javascript = array();
        LoadTemplate($row, 'barang/v_barang_manipulate', $javascript);
    }

    public function mutasi_barang_action() {
        $model = $this->input->post();
        
        $this->form_validation->set_rules('id_barang', 'nama barang', 'trim|required');
        $this->form_validation->set_rules('id_gudang', 'gudang', 'trim|required');
        $this->form_validation->set_rules('tanggal_jurnal', 'tanggal jurnal', 'trim|required');
        $this->form_validation->set_rules('jenis', 'jenis aliran', 'trim|required');
        $message = "";
        if (array_column($model, 'id_history')) {
            foreach ($model['id_history'] as $key => $history) {
                if ($model['qty'][$key] > $model['stock'][$key] && $history != "0" && $model['jenis'] == "2") {
                    $message .= "Quantity tidak boleh lebih besar dari stock yang tersedia";
                }
            }
        }

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {

            $data = $this->m_barang->MutasiBarang($model);
            echo json_encode($data);
        }
    }

    public function barang_mutasi() {
        $row = (object) array();
        $row->button = 'Mutasi Stock';
        $module = "K005";
        $header = "K001";
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        CekModule($module);
        $row->form = $header;
        $this->m_barang->SyncronizeHargaAverage(18,1,'2018-11-02');
        $row->formsubmenu = $module;
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->title = 'Mutasi barang';
        $row->tanggal = GetDateNow();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row = json_decode(json_encode($row), true);
        
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'barang/v_barang_mutasi', $javascript, $css);
    }

    public function barang_manipulate() {
        $message = '';

        $this->form_validation->set_rules('nama_barang', 'nama barang', 'trim|required');
        $this->form_validation->set_rules('id_jenis', 'id jenis', 'trim|required');
        $this->form_validation->set_rules('id_satuan', 'id satuan', 'trim|required');
        $this->form_validation->set_rules('kategory_item', 'kategory item', 'trim|required');
        $model = $this->input->post();
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_barang->BarangManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_barang($id = 0) {
        $row = $this->m_barang->GetOneBarang($id);

        if ($row) {
            $row->button = 'Update';
            $module = "K004";
            $header = "K001";
            CekModule($module);
            $row->form = $header;
            $row->formsubmenu = $module;
            $row->title = 'Update Barang';
            $this->load->model("satuan/m_satuan");
            $this->load->model("jenis/m_jenis");
            $this->load->model("merek/m_merek");
            $row->list_merek = $this->m_merek->GetDropDownMerek();
            $row->list_jenis = $this->m_jenis->GetDropDownJenis();
            $row->list_satuan = $this->m_satuan->GetDropDownSatuan();
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            LoadTemplate($row, 'barang/v_barang_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Barang cannot be found in database");
            redirect(site_url('barang'));
        }
    }

    public function koreksi_saldo_action() {
        $model = $this->input->post();

        $this->form_validation->set_rules('id_barang', 'nama barang', 'trim|required');
        $this->form_validation->set_rules('id_gudang', 'gudang', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            if (!CheckKey($model, 'saldo_awal')) {
                $barang = $this->m_barang->GetOnebarang($model["id_barang"]);
                $model['saldo_awal'] = $barang->saldo_awal;
            }
            $data = $this->m_barang->KoreksiSaldoBarang($model["id_barang"], $model["id_gudang"], $model['saldo_awal'], $model['harga_average_awal'], $model['stock_min']);
            echo json_encode($data);
        }
    }

    public function get_one_barang() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_barang', 'nama barang', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_barang->GetOneBarang($model["id_barang"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data barang tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function koreksi_saldo_barang() {
        $row = (object) array();
        $row->button = 'Koreksi Saldo';
        $module = "K006";
        $header = "K001";
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        CekModule($module);
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->title = 'Koreksi Saldo barang';
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        LoadTemplate($row, 'barang/v_barang_koreksi', $javascript);
    }

    public function barang_delete() {
        $message = '';
        $this->form_validation->set_rules('id_barang', 'Barang', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_barang->BarangDelete($model['id_barang']);
        }

        echo json_encode($result);
    }

}

/* End of file Barang.php */