
<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Dashboard</h2>
                                <p>Dashboard | Welcome</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main content -->
<section class="content" style="min-height:600px">
    <div class="row">
        <div class="col-sm-12">
            <div class="well text-center">
                <h4 style="margin-top:0px"><strong><?php echo date('l, jS F Y', strtotime('today')); ?></strong></h4>
                <p>Selamat datang <strong><?php echo @$nama_user?></strong></p>
            </div>
        </div>
    </div>
    

</section>
<!-- /.content -->
<script>
    $(document).ready(function(){
        <?php
        foreach($content as $item){
        ?>
        updateValue("<?=$item['tipe']?>", "<?=$item['ajax']?>");
        <?php
        }
        ?>
    });

    function updateValue(tipe, ajax) {
        $("#counter-"+tipe).html('<i class="fa fa-refresh fa-spin"></i>');
        $.post(
            baseurl + 'index.php/'+ajax,
            {},
            function(value){
                $("#counter-"+tipe).html(value);
            }
        );
    }

    function activateGraph(gId) {
        if($("#graphbox-"+gId).hasClass("collapsed-box") && $("#graphbox-"+gId).hasClass("closed")){
            $("#graphbox-"+gId).append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i><p style="color: black;">Mengambil Data...</p></div>');
            $.post(
                baseurl + 'index.php/dashboard/get'+gId+'_data',
                {},
                function(resp){
                    if(resp.st){
                        buildGraph(gId+'-chart', gId, resp.data, resp.title);
                        $("#graphbox-"+gId+" .overlay").remove();
                        $("#graphbox-"+gId).removeClass("closed");
                    }
                },"json"
            );
        }
    }
</script>