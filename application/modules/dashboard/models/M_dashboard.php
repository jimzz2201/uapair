<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_dashboard extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getCountWhere($table, $where){
        $jml = 0;
        $this->db->select("COUNT(*) AS jml ");
        $this->db->from($table);
        if(is_array($where)){
            $this->db->where($where[0], $where[1]);
        }else{
            $this->db->where($where);
        }

        $data = $this->db->get()->row_array();
        if($data){
            $jml = $data['jml'];
        }
        return $jml;
    }

    function getCountLike($table, $col, $match, $pos){
        $jml = 0;
        if(CheckEmpty($pos)){
            $post = "both";
        }
        $this->db->select("COUNT(*) AS jml ");
        $this->db->from($table);
        $this->db->like($col, $match, $pos);

        $data = $this->db->get()->row_array();
        if($data){
            $jml = $data['jml'];
        }
        return $jml;
    }

    function getSumLike($table, $colSum, $col, $match, $pos){
        $jml = 0;
        if(CheckEmpty($pos)){
            $post = "both";
        }
        $this->db->select("SUM(".$colSum.") AS jml ");
        $this->db->from($table);
        $this->db->like($col, $match, $pos);

        $data = $this->db->get()->row_array();
        if($data){
            $jml = $data['jml'];
        }
        return (int)$jml;
    }

}