<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_dashboard');
      
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $userid = GetUserId();
        $this->load->model("user/m_user");
        $this->load->library('user_agent');
        $model = $this->m_user->GetOneUser($userid, 'id_user');
        $model = json_decode(json_encode($model), true);
        $model['title'] = "Dashboard";
        $model['openmenu'] = "dashboardmenu";
        $model['is_mobile'] = $this->agent->is_mobile();

        $js = $css = array();
        $js[] = "assets/js/modules/dashboard/dashboard.js";
        $model['content'] = $this->getContent();
        $model['graph'] = array();
        if (CekModule('K068', 0)) {
            array_push($model['graph'], ['id' => 'penjualan', 'title' => 'Penjualan']);
        }
        LoadTemplate($model, "dashboard/dashboard_index", $js, $css);
    }

    function getContent(){
        $content = array();

        //$total_penjualan = $this->m_dashboard->getSumLike('tic_pemesanan', 'harga_tiket', 'created_date', date('Y-m-d'),'after');
        //$total_komisi = $this->m_dashboard->getSumLike('tic_pemesanan', 'komisi', 'created_date', date('Y-m-d'),'after');
        //$total_netto = $total_penjualan - $total_komisi;
        if(CekModule('K067', 0)) {
//            $content['jml_keberangkatan'] = $this->prepareContent('Jadwal Keberangkatan Hari ini', 'bus', 'keberangkatan', 'aqua', $this->m_dashboard->getCountWhere('tic_berangkat', "tanggal = '" . date('Y-m-d') . "' AND status < 2"));
            $content['jml_keberangkatan'] = [
                'html' => $this->prepareContent('berangkat', 'Keberangkatan&nbsp;Hari&nbsp;ini', 'bus', 'keberangkatan', 'aqua'),
                'ajax' => 'dashboard/counter/berangkat',
                'tipe' => 'berangkat'
            ];
        }
        if(CekModule('K066', 0)) {
//            $content['jml_spj'] = $this->prepareContent('SPJ untuk Hari ini', 'file-text', 'spj', 'green', $this->m_dashboard->getCountWhere('tic_spj', array('tanggal', date('Y-m-d'))));
            $content['jml_spj'] = [
                'html' => $this->prepareContent('spj', 'SPJ untuk Hari ini', 'file-text', 'spj', 'green'),
                'ajax' => 'dashboard/counter/spj',
                'tipe' => 'spj'
            ];
        }
        if(CekModule('K065', 0)) {
//            $content['jml_tiket'] = $this->prepareContent('Tiket Terjual Hari ini', 'ticket', 'keberangkatan/pemesanan', 'yellow', $this->m_dashboard->getCountLike('tic_pemesanan', 'created_date', date('Y-m-d'), 'after'));
            $content['jml_tiket'] = [
                'html' => $this->prepareContent('tiket', 'Tiket Terjual Hari ini', 'ticket', 'keberangkatan/pemesanan', 'yellow'),
                'ajax' => 'dashboard/counter/tiket',
                'tipe' => 'tiket'
            ];
        }
        if(CekModule('K064', 0)){
//            $content['jml_penjualan'] = $this->prepareContent('Total Penjualan Hari ini','money','report/penjualan','red',$total_penjualan);
            $content['jml_penjualan'] = [
                'html' => $this->prepareContent('penjualan', 'Penjualan Hari ini','money','report/penjualan','red'),
                'ajax' => 'dashboard/counter/penjualan',
                'tipe' => 'penjualan'
            ];
        }

        return $content;
    }

    public function counter($tipe){
        $count = 0;
        switch($tipe){
            case 'berangkat' : $count = (CekModule('K067', 0)) ? $this->m_dashboard->getCountWhere('tic_berangkat', "tanggal = '" . date('Y-m-d') . "' AND status < 2") : 0;
                break;
            case 'spj' : $count = (CekModule('K066', 0)) ? $this->m_dashboard->getCountWhere('tic_spj', array('tanggal', date('Y-m-d'))) : 0;
                break;
            case 'tiket' : $count = (CekModule('K065', 0)) ? $this->m_dashboard->getCountLike('tic_pemesanan', 'created_date', date('Y-m-d'), 'after') : 0;
                break;
            case 'penjualan' : $count = (CekModule('K064', 0)) ? DefaultCurrency($this->m_dashboard->getSumLike('tic_pemesanan', 'harga_tiket', 'created_date', date('Y-m-d'),'after')) : 0;
                break;
            default : $count = 0;
        }

        echo $count;
    }

    function prepareContent($tipe, $title,$icon,$url,$color, $val="--"){
        $contentHtml = '<div class="col-lg-3 col-xs-6">';
        $contentHtml .= '<div class="small-box bg-'.$color.'">';
                $contentHtml .= '<div class="inner">';
                    $contentHtml .= '<h3 id="counter-'.$tipe.'">'.$val.'</h3>';
                    $contentHtml .= '<p>'.$title.'</p>';
                $contentHtml .= '</div>';
        $contentHtml .= '<div class="icon">';
        $contentHtml .= '<i class="fa fa-'.$icon.'"></i>';
        $contentHtml .= '</div>';
        $contentHtml .= '<div style="position: absolute; top: 0px; right: 3px; opacity: 0.5; cursor: pointer" onclick="updateValue(\''.$tipe.'\',\'dashboard/counter/'.$tipe.'\')"><i class="fa fa-refresh"></i></div>';
        $contentHtml .= anchor(site_url($url),'Lebih Lengkap <i class="fa fa-arrow-circle-right"></i>', array('class'=>'small-box-footer'));
        $contentHtml .= '</div>';
        $contentHtml .= '</div>';

        return $contentHtml;
    }

    function getGraphPenjualan($json=true){
        $result = array();
        $tahun = date('Y');
        $bulan = date('n');
        for($x = 1; $x <= 12; $x++){
            if($x > $bulan){
                array_push($result, array(
                    'y' => $tahun.'-'.str_pad($x,2,'0',STR_PAD_LEFT),
                    'penjualan' => (int)0,
                ));
            }else{
                array_push($result, array(
                    'y' => $tahun.'-'.str_pad($x,2,'0',STR_PAD_LEFT),
                    'penjualan' => (int)$this->m_chart->getStatPeriode($tahun, $x),
                ));
            }
        }


//        12 bulan terakhir, tapi lama
//        for($m = 1; $m <= 12 ; $m++){
//            $b = ($m + $bulan) - 12;
//            $t = $tahun;
//            if($b <= 0){
//                $b += 12;
//                $t = $tahun - 1;
//            }
//
//            array_push($result, array(
//                'y' => $t.'-'.str_pad($b,2,'0',STR_PAD_LEFT),
//                'penjualan' => (int)$this->m_chart->getStatPeriode($t, $b),
//            ));
//        }

        $data = array(
            'title' => 'Penjualan',
            'data' => $json ? json_encode($result) : $result,
        );

        return $data;
    }

    public function getpenjualan_data(){
        $data = $this->getGraphPenjualan(false);
        if($data['data']){
            $data['st'] = true;
        }else{
            $data['st'] = false;
        }

        echo json_encode($data);
    }

}
