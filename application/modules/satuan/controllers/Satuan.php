<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Satuan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_satuan');
    }

    public function index()
    {
        $javascript = array();
        $module="K082";
        $header="K081";
        $title="Master Satuan";
        CekModule($module);
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        LoadTemplate(array("title"=>$title,"form"=>$header,"formsubmenu"=>$module), "satuan/v_satuan_index", $javascript);
        
    } 
    
    public function getdatasatuan() {
        header('Content-Type: application/json');
        echo $this->m_satuan->GetDatasatuan();
    }
    
    public function create_satuan() 
    {
        $row=(object)array();
        $row->button='Add';
        $row=  json_decode(json_encode($row),true);
        $javascript = array();
        $this->load->view('satuan/v_satuan_manipulate', $row);       
    }
    
    public function satuan_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('nama_satuan', 'nama satuan', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_satuan->SatuanManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_satuan($id=0) 
    {
        $row = $this->m_satuan->GetOneSatuan($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();        $this->load->view('satuan/v_satuan_manipulate', $row);
        } else {
            SetMessageSession(0, "Satuan cannot be found in database");
            redirect(site_url('satuan'));
        }
    }
    
    public function satuan_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_satuan', 'Satuan', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_satuan->SatuanDelete($model['id_satuan']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Satuan.php */