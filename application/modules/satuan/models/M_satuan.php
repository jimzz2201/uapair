<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_satuan extends CI_Model {

    public $table = 'uap_satuan';
    public $id = 'id_satuan';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDatasatuan() {
        $this->load->library('datatables');
        $this->datatables->select('id_satuan,kode_satuan,nama_satuan');
        $this->datatables->from('uap_satuan');
        //add this line for join
        //$this->datatables->join('table2', 'uap_satuan.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editsatuan($1);return false;"));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletesatuan($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_satuan');
        return $this->datatables->generate();
    }

    // get all
    function GetOneSatuan($keyword, $type = 'id_satuan') {
        $this->db->where($type, $keyword);
        $satuan = $this->db->get($this->table)->row();
        return $satuan;
    }

    function SatuanManipulate($model) {
        try {

            if (CheckEmpty($model['id_satuan'])) {
                $model['kode_satuan']= AutoIncrement($this->table, "S", "kode_satuan",3);
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                return array("st" => true, "msg" => "Satuan successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_satuan" => $model['id_satuan']));
                return array("st" => true, "msg" => "Satuan has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    function GetDropDownSatuan() {

        $where = [];
        $listreturn = GetTableData("uap_satuan", 'id_satuan', 'nama_satuan', $where, 'obj');
        return $listreturn;
    }
    function SatuanDelete($id_satuan) {
        try {
            $this->db->delete($this->table, array('id_satuan' => $id_satuan));
        } catch (Exception $ex) {
            $model['updated_date'] = GetDateNow();
            $model['status'] = 2;
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array('id_satuan' => $id_satuan));
        }
        return array("st" => true, "msg" => "Satuan has been deleted from database");
    }

}
