<div class="modal-header">
    Satuan <?php echo @$button ?>
</div>
<div class="modal-body">
    <form id="frm_satuan" class="form-horizontal form-groups-bordered validate" method="post">
        <input type="hidden" name="id_satuan" value="<?php echo @$id_satuan; ?>" /> 
        <div class="form-group">
            <?= form_label('Kode Satuan', "txt_kode_satuan", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text','disabled'=>'disabled', 'name' => 'kode_satuan', 'value' => @$kode_satuan, 'class' => 'form-control', 'id' => 'txt_kode_satuan', 'placeholder' => 'Kode Satuan')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Nama Satuan', "txt_nama_satuan", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text', 'name' => 'nama_satuan', 'value' => @$nama_satuan, 'class' => 'form-control', 'id' => 'txt_nama_satuan', 'placeholder' => 'Nama Satuan')); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
            <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
        </div>

    </form>
</div>
<script>
    $("#frm_satuan").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/satuan/satuan_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>