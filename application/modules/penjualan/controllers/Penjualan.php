<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penjualan extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_penjualan');
    }
    public function editpenjualan($id,$nomorinvoice)
    {
        $row = (object) array();
        
        $module = "K024";
        $header = "K021";
        CekModule($module);
        $penjualan=$this->m_penjualan->GetOnePenjualan($id,true);
        if($penjualan==null||$penjualan->nomor_master!= str_replace("-", "/", $nomorinvoice))
        {
            SetMessageSession(2, "Anda tidak memiliki akses untuk menuju link tersebut");
            redirect(base_url() . 'index.php/main/message');
        }
        else {
            $row=$penjualan;
        }
        $row->button = 'Edit';
        $this->load->model("customer/m_customer");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("pegawai/m_pegawai");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->list_customer = $this->m_customer->GetDropDownCustomer();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $row->list_pegawai = $this->m_pegawai->GetDropDownPegawai();
        $jenis_pembayaran=array();
        $jenis_pembayaran[1]="Tunai";
        $jenis_pembayaran[2]="Kredit";
        $row->list_type_pembayaran  =$jenis_pembayaran;
        $list_type_ppn=array();
        $list_type_ppn[1]="Include";
        $list_type_ppn[2]="Exclude";
        $row->list_type_ppn  =$list_type_ppn;
        
        $list_type_pembulatan=array();
        $list_type_pembulatan[1]="Sebelum PPN";
        $list_type_pembulatan[2]="Sesudah PPN";
        $row->list_type_pembulatan  =$list_type_pembulatan;
        $row->title = 'Edit Penjualan';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';
        
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'penjualan/v_penjualan_manipulate', $javascript, $css);
    }
    
    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
         $module = "K025";
        $header = "K021";
        $title = "Penjualan";
        CekModule($module);
        LoadTemplate(array("title" => $title, "form" => $header, "formsubmenu" => $module), "penjualan/v_penjualan_index", $javascript);
    }

    public function getdatapenjualan() {
        header('Content-Type: application/json');
        echo $this->m_penjualan->GetDatapenjualan();
    }
    public function create() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K024";
        $header = "K021";
        CekModule($module);
        $this->load->model("customer/m_customer");
        $this->load->model("pegawai/m_pegawai");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->listdetail=[];
        $row->listheader=[];
        $row->list_customer = $this->m_customer->GetDropDownCustomer();
        $row->list_pegawai = $this->m_pegawai->GetDropDownPegawai();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $jenis_pembayaran=array();
        $jenis_pembayaran[1]="Tunai";
        $jenis_pembayaran[2]="Kredit";
        $row->list_type_pembayaran  =$jenis_pembayaran;
        $list_type_ppn=array();
        $list_type_ppn[1]="Include";
        $list_type_ppn[2]="Exclude";
        $row->list_type_ppn  =$list_type_ppn;
        $list_type_pembulatan=array();
        $list_type_pembulatan[1]="Sebelum PPN";
        $list_type_pembulatan[2]="Sesudah PPN";
        $row->list_type_pembulatan  =$list_type_pembulatan;
        $row->tanggal = GetDateNow();
        $row->jth_tempo = date('Y-m-d', strtotime(GetDateNow() . "+1 months") );
        $row->title = 'Penjualan';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';
        
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'penjualan/v_penjualan_manipulate', $javascript, $css);
    }
    
    

    public function create_penjualan() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K004";
        $header = "K001";
        CekModule($module);
        $this->load->model("satuan/m_satuan");
        $this->load->model("jenis/m_jenis");
        $this->load->model("merek/m_merek");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->list_merek = $this->m_merek->GetDropDownMerek();
        $row->list_jenis = $this->m_jenis->GetDropDownJenis();
        $row->list_satuan = $this->m_satuan->GetDropDownSatuan();
        $row->title = 'Create Barang';
        $row = json_decode(json_encode($row), true);

        $javascript = array();
        LoadTemplate($row, 'penjualan/v_penjualan_manipulate', $javascript);
    }

   
    public function penjualan_manipulate() {
        $message = '';
        $model = $this->input->post();
       
        if(CheckEmpty($model['dataset'])||count($model['dataset'])==0)
        {
            $message.="Detail Penjualan is required<br/>";
        }
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');
        $this->form_validation->set_rules('id_gudang', 'Gudang', 'trim|required');
        $this->form_validation->set_rules('id_customer', 'Customer', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        $this->form_validation->set_rules('jth_tempo', 'Jatuh Tempo', 'trim|required');
        $this->form_validation->set_rules('type_pembayaran', 'Pembayaran', 'trim|required');
        if(!CheckEmpty(@$model['type_pembayaran'])&&$model['type_pembayaran']=="1")
        {
            $this->form_validation->set_rules('jumlah_bayar', 'Jumlah Bayar', 'trim|required');
        }
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_penjualan->PenjualanManipulate($model);
            echo json_encode($status);
        }
    }
    

    public function get_one_penjualan() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_penjualan', 'nama penjualan', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_penjualan->GetOneBarang($model["id_penjualan"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data penjualan tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function penjualan_delete() {
        $message = '';
        $this->form_validation->set_rules('id_penjualan', 'Barang', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_penjualan->BarangDelete($model['id_penjualan']);
        }

        echo json_encode($result);
    }

}

/* End of file Barang.php */