<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_penjualan extends CI_Model {

    public $table = 'uap_penjualan';
    public $id = 'id_penjualan_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function GetDetailPo($id) {
        $this->db->from("uap_penjualan_detail");
        $this->db->join("uap_satuan", "uap_penjualan_detail.id_satuan=uap_satuan.id_satuan", "left");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_penjualan_detail.id_barang", "left");
        $this->db->select("uap_penjualan_detail.*,sku_barang as sku,nama_satuan,harga_beli_akhir");
        $this->db->where(array("id_penjualan_master" => $id, "uap_penjualan_detail.status !=" => 5));
        $listdetail = $this->db->get()->result();
        return $listdetail;
    }
    function GetDetail($id) {
        $this->db->from("uap_penjualan_detail");
        $this->db->join("uap_satuan", "uap_penjualan_detail.id_satuan=uap_satuan.id_satuan", "left");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_penjualan_detail.id_barang", "left");
        $this->db->select("uap_penjualan_detail.*,sku_barang as sku,nama_satuan,harga_beli_akhir");
        $this->db->where(array("id_penjualan_master" => $id, "uap_penjualan_detail.status !=" => 5));
        $listdetail = $this->db->get()->result();
        return $listdetail;
    }
    function GetOnePenjualanPo($id, $isfull = false) {
        $this->db->from("uap_penjualan_master");
        $this->db->where(array("id_penjualan_master" => $id));
        $row = $this->db->get()->row();
        if ($isfull) {

            $row->listdetail = $this->GetDetailPo($id);
        }

        return $row;
    }
    function GetOnePenjualan($id, $isfull = false) {
        $this->db->from("uap_penjualan_master");
        $this->db->where(array("id_penjualan_master" => $id));
        $row = $this->db->get()->row();
        if ($isfull) {

            $row->listdetail = $this->GetDetail($id);
            $row->listheader=[];
        }

        return $row;
    }
    function PenjualanManipulate($model) {
       
       
        try {
            $message = "";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            $penjualan = array();
            $this->load->model("cabang/m_cabang");
            $this->load->model("barang/m_barang");
            $this->load->model("customer/m_customer");
            $cabang = $this->m_cabang->GetOneCabang($model['id_cabang']);
            
            $penjualan['type_ppn'] = $model['type_ppn'];
            $penjualan['ppn'] = $model['ppn'];
            $penjualan['type_pembayaran'] = $model['type_pembayaran'];
            $penjualan['jth_tempo'] = DefaultTanggalDatabase($model['jth_tempo']);
            $penjualan['keterangan'] = $model['keterangan'];
            $penjualan['id_sales_order_master'] = ForeignKeyFromDb($model['id_sales_order_master']);
            $penjualan['id_customer'] = ForeignKeyFromDb($model['id_customer']);
            $penjualan['id_pegawai'] = ForeignKeyFromDb($model['id_pegawai']);
            $penjualan['id_gudang'] = ForeignKeyFromDb($model['id_gudang']);
            $penjualan['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
            $penjualan['tanggal'] = DefaultTanggalDatabase($model['tanggal']);
            $penjualan['nominal_ppn'] = DefaultCurrencyDatabase($model['nominal_ppn']);
            $penjualan['grandtotal'] = DefaultCurrencyDatabase($model['grandtotal']);
            $penjualan['jumlah_bayar'] = DefaultCurrencyDatabase($model['jumlah_bayar']) > $penjualan['grandtotal'] ? $penjualan['grandtotal'] : DefaultCurrencyDatabase($model['jumlah_bayar']);
            $penjualan['total'] = DefaultCurrencyDatabase($model['total']);
            $penjualan['disc_pembulatan'] = DefaultCurrencyDatabase($model['disc_pembulatan']);
            $penjualan['ppn'] = DefaultCurrencyDatabase($model['ppn']);
            $id_penjualan = 0;
            $customer=$this->m_customer->GetOneCustomer($penjualan['id_customer']);
            if (CheckEmpty(@$model['id_penjualan_master'])) {
                $penjualan['created_date'] = GetDateNow();
                $penjualan['nomor_master'] = AutoIncrement('uap_penjualan_master', $cabang->kode_cabang . '/FK/' . date("y") . '/', 'nomor_master', 5);
                $penjualan['created_by'] = ForeignKeyFromDb($userid);
                $penjualan['status'] = '0';
                $this->db->insert("uap_penjualan_master", $penjualan);
                $id_penjualan = $this->db->insert_id();
            } else {
                $penjualan['updated_date'] = GetDateNow();
                $penjualan['updated_by'] = ForeignKeyFromDb($userid);
                $id_penjualan = $model['id_penjualan_master'];
                $penjualandb = $this->GetOnePenjualanPo($id_penjualan);
                $penjualan['nomor_master'] = $penjualandb->nomor_master;
                $this->db->update("uap_penjualan_master", $penjualan, array("id_penjualan_master" => $id_penjualan));
            }
            $arraydetailpenjualan = array();
            $totalpenjualanwithoutppn=0;
            $totalpenjualanwithppn=0;
            foreach ($model['dataset'] as $key => $det) {
                $detailpenjualan = array();
                $detailpenjualan['id_barang'] = $det['id_barang'];
                $detailpenjualan['price_barang'] = DefaultCurrencyDatabase($det['price_barang']);
                $detailpenjualan['nama_barang'] = $det['nama_barang'];
                $detailpenjualan['qty'] = DefaultCurrencyDatabase($det['qty']);
                $detailpenjualan['disc_1'] = DefaultCurrencyDatabase($det['disc_1']);
                $detailpenjualan['disc_2'] = DefaultCurrencyDatabase($det['disc_2']);
                $detailpenjualan['subtotal'] = DefaultCurrencyDatabase($det['subtotal']);
                if($penjualan['type_ppn']=="1")
                {
                    $detailpenjualan['ppn_satuan'] = $detailpenjualan['subtotal']*((float)$penjualan['ppn']/((float)$penjualan['ppn']+(float)100));
                    $totalpenjualanwithoutppn+=$detailpenjualan['subtotal']-$detailpenjualan['ppn_satuan']; 
                }
                else  if($penjualan['type_ppn']=="2")
                {
                    $detailpenjualan['ppn_satuan'] = $detailpenjualan['subtotal']*((float)$penjualan['ppn']/(float)100);
                    $totalpenjualanwithoutppn+=$detailpenjualan['subtotal']; 
                }
                else
                {
                    $detailpenjualan['ppn_satuan'] = 0;
                    $totalpenjualanwithoutppn+=$detailpenjualan['subtotal']; 
                }
                $totalpenjualanwithppn+=$detailpenjualan['subtotal'];
                if (CheckEmpty($det['id_penjualan_detail'])) {
                    $barang = $this->m_barang->GetOneBarang($det['id_barang']);
                    $detailpenjualan['id_satuan'] = ForeignKeyFromDb(@$barang->id_satuan);
                    $detailpenjualan['created_date'] = GetDateNow();
                    $detailpenjualan['created_by'] = $userid;
                    $detailpenjualan['status'] = 1;
                    $detailpenjualan['id_penjualan_master'] = $id_penjualan;
                    $this->db->insert("uap_penjualan_detail", $detailpenjualan);
                    $arraydetailpenjualan[] = $this->db->insert_id();
                } else {

                    $detailpenjualan['updated_date'] = GetDateNow();
                    $detailpenjualan['updated_by'] = $userid;
                    if (!CheckEmpty(@$det['id_satuan']))
                        $detailpenjualan['id_satuan'] = ForeignKeyFromDb($det['id_satuan']);
                    $this->db->update("uap_penjualan_detail", $detailpenjualan, array("id_penjualan_detail" => $det['id_penjualan_detail']));
                    $arraydetailpenjualan[] = $det['id_penjualan_detail'];
                }
            }


            if (count($arraydetailpenjualan) > 0) {
                $this->db->where_not_in("id_penjualan_detail", $arraydetailpenjualan);
                $this->db->where(array("id_penjualan_master" => $id_penjualan));
                $this->db->update("uap_penjualan_detail", array("status" => 5, 'updated_date' => GetDateNow(), 'updated_by' => $userid));
            } else {
                $this->db->update("uap_penjualan_detail", array("status" => 5, 'updated_date' => GetDateNow()), array("id_penjualan_master" => $penjualan));
            }
            
            $this->db->from("uap_pembayaran_piutang_detail");
            $this->db->join("uap_pembayaran_piutang_master","uap_pembayaran_piutang_master.pembayaran_piutang_id=uap_pembayaran_piutang_detail.pembayaran_piutang_id");
            $this->db->where(array("keterangan"=>"Pembayaran Di Muka","id_penjualan_master"=>$id_penjualan));
            
            $pembayaran_piutang_master=$this->db->get()->row();
            if($pembayaran_piutang_master!=null && $penjualan['jumlah_bayar']==0)
            {
                $this->db->delete("uap_pembayaran_piutang_detail",array("pembayaran_piutang_detail_id"=>$pembayaran_piutang_master->pembayaran_piutang_detail_id));
                $this->db->delete("uap_pembayaran_piutang_master",array("pembayaran_piutang_id"=>$pembayaran_piutang_master->pembayaran_piutang_id));
            }
            else if ($pembayaran_piutang_master!=null&&$pembayaran_piutang_master->jumlah_bayar!=$penjualan['jumlah_bayar'])
            {
                $updatedet=array();
                $updatedet['updated_date']= GetDateNow();
                $updatedet['updated_by']= $userid;
                $updatedet['jumlah_bayar']=$penjualan['jumlah_bayar'];
                $updatemas=array();
                $updatemas['updated_date']= GetDateNow();
                $updatemas['updated_by']= $userid;
                $updatemas['total_bayar']=$penjualan['jumlah_bayar'];
                $this->db->update("uap_pembayaran_piutang_detail",$updatedet,array("pembayaran_piutang_detail_id"=>$pembayaran_piutang_master->pembayaran_piutang_detail_id));
                $this->db->update("uap_pembayaran_piutang_master",$updatemas,array("pembayaran_piutang_id"=>$pembayaran_piutang_master->pembayaran_piutang_id));
            }
            else if($pembayaran_piutang_master==null&&$penjualan['jumlah_bayar']>0)
            {
                $dokumenpembayaran=AutoIncrement('uap_pembayaran_piutang_master', $cabang->kode_cabang . '/BB/' . date("y") . '/', 'dokumen', 5);
                $pembayaranmaster=array();
                $pembayaranmaster['dokumen']=$dokumenpembayaran;
                $pembayaranmaster['tanggal_transaksi']=$penjualan['tanggal'];
                $pembayaranmaster['id_customer']=$penjualan['id_customer'];
                $pembayaranmaster['jenis_pembayaran']="cash";
                $pembayaranmaster['total_bayar']=$penjualan['jumlah_bayar']>$penjualan['grandtotal']?$penjualan['grandtotal']:$penjualan['jumlah_bayar'];
                $pembayaranmaster['biaya']=0;
                $pembayaranmaster['potongan']=0;
                $pembayaranmaster['status']=1;
                $pembayaranmaster['keterangan']="Pembayaran Di Muka";
                $pembayaranmaster['id_cabang']=ForeignKeyFromDb($model['id_cabang']);
                $pembayaranmaster['created_date']= GetDateNow();
                $pembayaranmaster['created_by']=$userid;
                $this->db->insert("uap_pembayaran_piutang_master",$pembayaranmaster);
                $pembayaran_piutang_id=$this->db->insert_id();
                $pembayarandetail=array();
                $pembayarandetail['id_penjualan_master']=$id_penjualan;
                $pembayarandetail['pembayaran_piutang_id']=$pembayaran_piutang_id;
                $pembayarandetail['jumlah_bayar']=$penjualan['jumlah_bayar'];
                $pembayarandetail['created_date']= GetDateNow();
                $pembayarandetail['created_by']=$userid;
                $this->db->insert("uap_pembayaran_piutang_detail",$pembayarandetail);
            }
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account"=>"15","dokumen"=>$penjualan['nomor_master'] ));
            $akun_penjualan=$this->db->get()->row();
          
                
            if(CheckEmpty($akun_penjualan))
            {   
                $akunpenjualaninsert=array();
                $akunpenjualaninsert['id_gl_account']="15";
                $akunpenjualaninsert['tanggal_buku']=$penjualan['tanggal'];
                $akunpenjualaninsert['id_cabang'] = $penjualan['id_cabang'];
                $akunpenjualaninsert['keterangan']="Penjualan Ke " .@$customer->nama_customer.' '.$penjualan['keterangan'];
                $akunpenjualaninsert['dokumen']=$penjualan['nomor_master'] ;
                $akunpenjualaninsert['subject_name']=@$customer->kode_customer;
                $akunpenjualaninsert['id_subject']=$penjualan['id_customer'];
                $akunpenjualaninsert['debet']= 0;
                $akunpenjualaninsert['kredit']=DefaultCurrencyDatabase($model['type_pembulatan'] == "1"?$totalpenjualanwithppn * 100 / (100+$penjualan['ppn']): $totalpenjualanwithoutppn,0);
                $akunpenjualaninsert['created_date']= GetDateNow();
                $akunpenjualaninsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunpenjualaninsert);
            } 
            else
            {
                $akunpenjualanupdate['tanggal_buku']=$penjualan['tanggal'];
                $akunpenjualanupdate['id_cabang'] = $penjualan['id_cabang'];
                $akunpenjualanupdate['keterangan']="Penjualan Ke " .@$customer->nama_customer.' '.$penjualan['keterangan'];
                $akunpenjualanupdate['dokumen']=$penjualan['nomor_master'] ;
                $akunpenjualanupdate['subject_name']=@$customer->kode_customer;
                $akunpenjualanupdate['id_subject']=$penjualan['id_customer'];
                $akunpenjualanupdate['debet']=0;
                $akunpenjualanupdate['kredit']=DefaultCurrencyDatabase($model['type_pembulatan'] == "1"?$totalpenjualanwithppn * 100 / (100+$penjualan['ppn']): $totalpenjualanwithoutppn,0);
                $akunpenjualanupdate['updated_date']= GetDateNow();
                $akunpenjualanupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunpenjualanupdate,array("id_buku_besar"=>$akun_penjualan->id_buku_besar));
            }
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account"=>"20","dokumen"=>$penjualan['nomor_master'] ));
            $akun_ppn=$this->db->get()->row();
            
            if(CheckEmpty($akun_ppn)&&$penjualan['nominal_ppn']>0)
            {
                $akunppninsert=array();
                $akunppninsert['id_gl_account']="20";
                $akunppninsert['tanggal_buku']=$penjualan['tanggal'];
                $akunppninsert['id_cabang'] = $penjualan['id_cabang'];
                $akunppninsert['keterangan']="PPN penjualan Ke " .@$customer->nama_customer.' '.$penjualan['keterangan'];
                $akunppninsert['dokumen']=$penjualan['nomor_master'] ;
                $akunppninsert['subject_name']=@$customer->kode_customer;
                $akunppninsert['id_subject']=$penjualan['id_customer'];
                $akunppninsert['debet']=0;
                $akunppninsert['kredit']=$penjualan['nominal_ppn'];
                $akunppninsert['created_date']= GetDateNow();
                $akunppninsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunppninsert);
            } 
            else if(!CheckEmpty($akun_ppn))
            {
                $akunppnupdate['tanggal_buku']=$penjualan['tanggal'];
                $akunppnupdate['id_cabang'] = $penjualan['id_cabang'];
                $akunppnupdate['keterangan']="PPN penjualan Ke " .@$customer->nama_customer.' '.$penjualan['keterangan'];
                $akunppnupdate['dokumen']=$penjualan['nomor_master'] ;
                $akunppnupdate['subject_name']=@$customer->kode_customer;
                $akunppnupdate['id_subject']=$penjualan['id_customer'];
                $akunppnupdate['debet']=0;
                $akunppnupdate['kredit']=$penjualan['nominal_ppn'];
                $akunppnupdate['updated_date']= GetDateNow();
                $akunppnupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunppnupdate,array("id_buku_besar"=>$akun_ppn->id_buku_besar));
            }
            
            
            
            
            
            
            
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account"=>"7","dokumen"=>$penjualan['nomor_master'] ));
            $akun_piutang=$this->db->get()->row();
            
            if(CheckEmpty($akun_piutang)&&$penjualan['jumlah_bayar']<$penjualan['grandtotal'])
            {
                $akunpiutanginsert=array();
                $akunpiutanginsert['id_gl_account']="7";
                $akunpiutanginsert['tanggal_buku']=$penjualan['tanggal'];
                $akunpiutanginsert['id_cabang'] = $penjualan['id_cabang'];
                $akunpiutanginsert['keterangan']="Piutang penjualan dari " .@$customer->nama_customer.' '.$penjualan['keterangan'];
                $akunpiutanginsert['dokumen']=$penjualan['nomor_master'] ;
                $akunpiutanginsert['subject_name']=@$customer->kode_customer;
                $akunpiutanginsert['id_subject']=$penjualan['id_customer'];
                $akunpiutanginsert['debet']=$penjualan['grandtotal']-$penjualan['jumlah_bayar'];
                $akunpiutanginsert['kredit']=0;
                $akunpiutanginsert['created_date']= GetDateNow();
                $akunpiutanginsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunpiutanginsert);
            } 
            else if(!CheckEmpty($akun_piutang))
            {
                $akunpiutangupdate['tanggal_buku']=$penjualan['tanggal'];
                $akunpiutangupdate['id_cabang'] = $penjualan['id_cabang'];
                $akunpiutangupdate['keterangan']="Piutang penjualan Ke " .@$customer->nama_customer.' '.$penjualan['keterangan'];
                $akunpiutangupdate['dokumen']=$penjualan['nomor_master'] ;
                $akunpiutangupdate['subject_name']=@$customer->kode_customer;
                $akunpiutangupdate['id_subject']=$penjualan['id_customer'];
                $akunpiutangupdate['debet']=$penjualan['grandtotal']-$penjualan['jumlah_bayar'];
                $akunpiutangupdate['kredit']=0;
                $akunpiutangupdate['updated_date']= GetDateNow();
                $akunpiutangupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunpiutangupdate,array("id_buku_besar"=>$akun_piutang->id_buku_besar));
            }
            
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account"=>"16","dokumen"=>$penjualan['nomor_master'] ));
            $akun_potongan=$this->db->get()->row();
            
            if(CheckEmpty($akun_potongan)&&$penjualan['disc_pembulatan']>0)
            {
                $akunpotonganinsert=array();
                $akunpotonganinsert['id_gl_account']="16";
                $akunpotonganinsert['tanggal_buku']=$penjualan['tanggal'];
                $akunpotonganinsert['id_cabang'] = $penjualan['id_cabang'];
                $akunpotonganinsert['keterangan']="Potongan penjualan Ke " .@$customer->nama_customer.' '.$penjualan['keterangan'];
                $akunpotonganinsert['dokumen']=$penjualan['nomor_master'] ;
                $akunpotonganinsert['subject_name']=@$customer->kode_customer;
                $akunpotonganinsert['id_subject']=$penjualan['id_customer'];
                $akunpotonganinsert['debet']=$penjualan['disc_pembulatan'];
                $akunpotonganinsert['kredit']=0;
                $akunpotonganinsert['created_date']= GetDateNow();
                $akunpotonganinsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunpotonganinsert);
            } 
            else if(!CheckEmpty($akun_potongan))
            {
                $akunpotonganupdate['tanggal_buku']=$penjualan['tanggal'];
                $akunpotonganupdate['id_cabang'] = $penjualan['id_cabang'];
                $akunpotonganupdate['keterangan']="Potongan penjualan Ke " .@$customer->nama_customer.' '.$penjualan['keterangan'];
                $akunpotonganupdate['dokumen']=$penjualan['nomor_master'] ;
                $akunpotonganupdate['subject_name']=@$customer->kode_customer;
                $akunpotonganupdate['id_subject']=$penjualan['id_customer'];
                $akunpotonganupdate['debet']=$penjualan['disc_pembulatan'];
                $akunpotonganupdate['kredit']=0;
                $akunpotonganupdate['updated_date']= GetDateNow();
                $akunpotonganupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunpotonganupdate,array("id_buku_besar"=>$akun_potongan->id_buku_besar));
            }
            
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account"=>"3","dokumen"=>$penjualan['nomor_master'] ));
            $akun_kas=$this->db->get()->row();
            
            if(CheckEmpty($akun_kas)&&$penjualan['jumlah_bayar']>0)
            {
                $akunkasinsert=array();
                $akunkasinsert['id_gl_account']="3";
                $akunkasinsert['tanggal_buku']=$penjualan['tanggal'];
                $akunkasinsert['id_cabang'] = $penjualan['id_cabang'];
                $akunkasinsert['keterangan']="Pengeluaran kas untuk penjualan Ke " .@$customer->nama_customer.' '.$penjualan['keterangan'];
                $akunkasinsert['dokumen']=$penjualan['nomor_master'] ;
                $akunkasinsert['subject_name']=@$customer->kode_customer;
                $akunkasinsert['id_subject']=$penjualan['id_customer'];
                $akunkasinsert['debet']=$penjualan['jumlah_bayar'];
                $akunkasinsert['kredit']=0;
                $akunkasinsert['created_date']= GetDateNow();
                $akunkasinsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunkasinsert);
            } 
            else if(!CheckEmpty($akun_kas))
            {
                $akunkasupdate['tanggal_buku']=$penjualan['tanggal'];
                $akunkasupdate['id_cabang'] = $penjualan['id_cabang'];
                $akunkasupdate['keterangan']="Pengeluaran kas untuk Ke " .@$customer->nama_customer.' '.$penjualan['keterangan'];
                $akunkasupdate['dokumen']=$penjualan['nomor_master'] ;
                $akunkasupdate['subject_name']=@$customer->kode_customer;
                $akunkasupdate['id_subject']=$penjualan['id_customer'];
                $akunkasupdate['debet']=$penjualan['jumlah_bayar'];
                $akunkasupdate['kredit']=0;
                $akunkasupdate['updated_date']= GetDateNow();
                $akunkasupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunkasupdate,array("id_buku_besar"=>$akun_kas->id_buku_besar));
            }
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("dokumen"=>$penjualan['nomor_master']));
            $this->db->select("sum(debet)-sum(kredit) as selisih");
            $row = $this->db->get()->row();
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "38", "dokumen" => $penjualan['nomor_master']));
            $akun_penyeimbang = $this->db->get()->row();

            if (CheckEmpty($akun_penyeimbang) && $row && $row->selisih>0) {
                $akun_penyeimbanginsert = array();
                $akun_penyeimbanginsert['id_gl_account'] = "38";
                $akun_penyeimbanginsert['tanggal_buku'] = $penjualan['tanggal'];
                $akun_penyeimbanginsert['id_cabang'] = $penjualan['id_cabang'];
                $akun_penyeimbanginsert['keterangan'] = "Penyeimbang untuk penjualan Ke " . @$customer->nama_customer . ' ' . $penjualan['keterangan'];
                $akun_penyeimbanginsert['dokumen'] = $penjualan['nomor_master'];
                $akun_penyeimbanginsert['subject_name'] = @$customer->kode_customer;
                $akun_penyeimbanginsert['id_subject'] = $penjualan['id_customer'];
                $akun_penyeimbanginsert['debet'] = DefaultCurrencyDatabase($row->selisih) > 0  ? 0 : abs(DefaultCurrencyDatabase($row->selisih)) ;
                $akun_penyeimbanginsert['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih)) ;
                $akun_penyeimbanginsert['created_date'] = GetDateNow();
                $akun_penyeimbanginsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akun_penyeimbanginsert);
            } else if (!CheckEmpty($akun_penyeimbang)) {
                $akun_penyeimbangupdate['tanggal_buku'] = $penjualan['tanggal'];
                $akun_penyeimbangupdate['id_cabang'] = $penjualan['id_cabang'];
                $akun_penyeimbangupdate['keterangan'] = "Penyeimbang untuk Ke " . @$customer->nama_customer . ' ' . $penjualan['keterangan'];
                $akun_penyeimbangupdate['dokumen'] = $penjualan['nomor_master'];
                $akun_penyeimbangupdate['subject_name'] = @$customer->kode_customer;
                $akun_penyeimbangupdate['id_subject'] = $penjualan['id_customer'];
                $akun_penyeimbangupdate['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih)) ;
                $akun_penyeimbangupdate['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih)) ;
                $akun_penyeimbangupdate['updated_date'] = GetDateNow();
                $akun_penyeimbangupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akun_penyeimbangupdate, array("id_buku_besar" => $akun_penyeimbang->id_buku_besar));
            }




            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data Penjualan Sudah di" . (CheckEmpty(@$model['id_penjualan_master']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_penjualan, $penjualan['nomor_master'], 'penjualan');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

  

    // datatables
    function GetDatapenjualanPO() {
        $this->load->library('datatables');
        $this->datatables->select('id_penjualan_master,nama_customer,tgl_po,replace(nomor_master,"/","-") as nomor_master,nominal_po');
        $this->datatables->join("uap_customer", "uap_customer.id_customer=uap_penjualan_master.id_customer", "left");
        $this->datatables->from('uap_penjualan_master');
        //add this line for join
        //$this->datatables->join('table2', 'uap_penjualan.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';


        if ($isedit) {
            $straction .= anchor("penjualan/editpenjualan/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batalpo($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_penjualan_master,nomor_master');
        return $this->datatables->generate();
    }

    function GetDatapenjualan() {
        $this->load->library('datatables');
        $this->datatables->select('id_penjualan_master,nama_customer,tanggal,replace(nomor_master,"/","-") as nomor_master,grandtotal,jth_tempo');
        $this->datatables->join("uap_customer", "uap_customer.id_customer=uap_penjualan_master.id_customer", "left");
        $this->datatables->from('uap_penjualan_master');
        //add this line for join
        //$this->datatables->join('table2', 'uap_penjualan.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';


        if ($isedit) {
            $straction .= anchor("penjualan/editpenjualan/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batal($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_penjualan_master,nomor_master');
        return $this->datatables->generate();
    }

   

}
