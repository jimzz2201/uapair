<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Sinar Jaya Ticketing System Forgot Password</title>
        <script>
            var baseurl="<?php echo base_url()?>";
        </script>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/iCheck/square/blue.css">
        <script src="<?php echo base_url() ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
         <script src="<?php echo base_url() ?>assets/js/helper.js"></script>
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="<?php echo base_url() ?>"><b><img src="<?php echo base_url().'assets/images/logo.png'?>" /></b></a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Fill The email and we will sent you step how to you recover your password and pin</p>

                <form id="frmrecoverpassword">
                    <div id="notification" ></div>
                    <div class="form-group has-feedback">
                        <input type="text" name="email" class="form-control" placeholder="Email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    
                    <div class="row">
                       <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>


                <!-- /.social-auth-links -->


            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery 2.2.3 -->
       
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url() ?>assets/plugins/iCheck/icheck.min.js"></script>
        <script>
           

            $("#frmrecoverpassword").submit(function () {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/main/sent_forgot_password',
                    dataType: 'json',
                    data: $(this).serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            window.location.href=baseurl+"index.php/forgot_activation"
                        }
                        else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });

                return false;
            });
        </script>
    </body>
</html>
