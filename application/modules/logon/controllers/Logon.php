<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logon extends CI_Controller {

    public function index() {

        if (CheckEmpty(GetUserId())) {
            $model = array();

            $this->load->view("logon/logon_index", $model);
        } else {
            redirect('dashboard');

            $userid = GetUserId();
            $this->load->model("user/m_user");
            $model = $this->m_user->GetOneUser($userid, 'id_user');
            $model = json_decode(json_encode($model), true);
            $model['title'] = "Dashboard";
            $model['openmenu'] = "dashboardmenu";
            LoadTemplate($model, "dashboard/dashboard_index", array());
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url());
    }

    public function login_validation() {
        $model = $this->input->post();
        $message = '';
        $data = array();
        $data['st'] = false;

        $this->form_validation->set_rules('username', "Username", 'required');
        $this->form_validation->set_rules('password', "Password", 'required');
        $this->load->model("user/m_user");

        if ($this->form_validation->run() === FALSE || $message != '') {

            $data['msg'] = validation_errors() . $message;
        } else {
            $message = $this->m_user->login($model['username'], $model['password'], 'username');

            $data['st'] = $message == '';
            $data['msg'] = $message;
        }
        echo json_encode($data);
        exit;
    }

}
