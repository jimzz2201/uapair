<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_sales_order extends CI_Model {

    public $table = 'uap_sales_order';
    public $id = 'id_sales_order_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }
    
    
    function BatalSalesOrder($id_sales_order_master){
        $message="";
        $this->db->from("uap_penjualan_master");
        $this->db->where(array("status !="=>2,"id_sales_order_master"=>$id_sales_order_master));
        $penjualan=$this->db->get()->row();
        if($penjualan!=null)
        {
            $message.="Penjualan dengan invoice : ".$penjualan->nomor_master ." belum dibatalkan dalam database<br/>";
        }
        $this->db->from("uap_surat_jalan_master");
        $this->db->where(array("status !="=>2,"id_sales_order_master"=>$id_sales_order_master));
        $suratjalan=$this->db->get()->row();
        if($suratjalan!=null)
        {
            $message.="Surat Jalan  dengan nomor : ".$suratjalan->nomor_master ." belum dibatalkan dalam database<br/>";
        }
        $salesorder=$this->GetOneSales_Order($id_sales_order_master);
        if($salesorder!=null)
        {
            if($salesorder->status=="2")
            {
                 $message.="Sales Order sudah dibatalkan sebelumnya<br/>";
            }
        }
        else {
             $message.="Sales Order tidak terdaftar dalam database<br/>";
        }
        
        if($message=="")
        {
            $this->db->update("uap_sales_order_master",array("status"=>2),array("id_sales_order_master"=>$id_sales_order_master,"keterangan"=>@$salesorder->keterangan.' | Batal'.$message));
        }
        
        
        return array("st"=>$message=="","msg"=>$message==""?"Data berhasil dibatalkan":$message);
        
        
    }
    function ApproveSalesOrder($id_sales_order_master){
        $message="";
        
        if($message=="")
        {
            $this->db->update("uap_sales_order_master",array("status"=>1,"approved_by"=> GetUserId(),"approved_date"=> GetDateNow(),"approved_comment"=>""),array("id_sales_order_master"=>$id_sales_order_master));
        }
        return array("st"=>$message=="","msg"=>$message==""?"Data berhasil diapprove":$message);
    }
    function RejectSalesOrder($id_sales_order_master,$message_approved){
        $message="";
        
        if($message=="")
        {
            $this->db->update("uap_sales_order_master",array("status"=>5,"approved_by"=> GetUserId(),"approved_date"=> GetDateNow(),"approved_comment"=>$message_approved),array("id_sales_order_master"=>$id_sales_order_master));
        }
        return array("st"=>$message=="","msg"=>$message==""?"Data berhasil direject":$message);
    }
    function GetDetailPo($id) {
        $this->db->from("uap_sales_order_detail");
        $this->db->join("uap_satuan", "uap_sales_order_detail.id_satuan=uap_satuan.id_satuan", "left");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_sales_order_detail.id_barang", "left");
        $this->db->select("uap_sales_order_detail.*,sku_barang as sku,nama_satuan,harga_beli_akhir");
        $this->db->where(array("id_sales_order_master" => $id, "uap_sales_order_detail.status !=" => 5));
        $listdetail = $this->db->get()->result();
        return $listdetail;
    }

    function GetDetail($id,$is_stock=false,$id_gudang=0) {
        $this->db->from("uap_sales_order_detail");
        $this->db->join("uap_satuan", "uap_sales_order_detail.id_satuan=uap_satuan.id_satuan", "left");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_sales_order_detail.id_barang", "left");
       
        $this->db->where(array("id_sales_order_master" => $id, "uap_sales_order_detail.status !=" => 5));
        if($is_stock&&!CheckEmpty($id_gudang))
        {
            $this->db->join("uap_barang_gudang","uap_barang_gudang.id_barang=uap_barang.id_barang","left");
            $this->db->select("uap_sales_order_detail.*,sku_barang as sku,nama_satuan,uap_barang_gudang.harga_beli_terakhir as harga_beli_akhir,uap_barang_gudang.stock");
        }
        else
        {
             $this->db->select("uap_sales_order_detail.*,sku_barang as sku,nama_satuan,harga_beli_akhir");
        }
        $listdetail = $this->db->get()->result();
        
        
        if($is_stock)
        {
             foreach ($listdetail as $detail) {
                $detail->qtypenerimaan = $this->m_surat_jalan->GetJumlahPengiriman($detail->id_sales_order_detail);
            }
        }
        return $listdetail;
    }
    function GetOneSales_OrderPo($id, $isfull = false) {
        $this->db->from("uap_sales_order_master");
        $this->db->where(array("id_sales_order_master" => $id));
        $row = $this->db->get()->row();
        if ($isfull) {

            $row->listdetail = $this->GetDetailPo($id);
        }

        return $row;
    }
    
    function GetNoSalesOrder($id)
    {
        $this->db->from("uap_sales_order_master");
        $this->db->join("uap_penjualan_master","uap_sales_order_master.id_sales_order_master=uap_penjualan_master.id_Sales_order_master","left");
        $this->db->where("uap_sales_order_master.id_sales_order_master",$id);
        $this->db->select("uap_penjualan_master.tanggal as tanggal_penjualan,uap_penjualan_master.nomor_master as nomor_penjualan,uap_sales_order_master.tanggal as tanggal_so,uap_sales_order_master.nomor_master as nomor_so");
        return $this->db->get()->row();
        
    }
    function GetOneSales_Order($id, $isfull = false,$is_stock=false) {
        $this->load->model("surat_jalan/m_surat_jalan");
        $this->db->from("uap_sales_order_master");
        $this->db->where(array("id_sales_order_master" => $id));
        if($isfull)
        {
            $this->db->join("uap_customer", "uap_customer.id_customer=uap_sales_order_master.id_customer", "left");
            $this->db->join("uap_gudang", "uap_gudang.id_gudang=uap_sales_order_master.id_gudang", "left");
            $this->db->join("uap_cabang", "uap_cabang.id_cabang=uap_sales_order_master.id_cabang", "left");
            $this->db->join("uap_pegawai", "uap_sales_order_master.id_pegawai=uap_pegawai.id_pegawai", "left");
            $this->db->select("uap_sales_order_master.*,uap_customer.nama_customer,uap_customer.alamat,uap_customer.contact_person,uap_customer.no_telp,uap_cabang.nama_cabang,uap_gudang.nama_gudang,uap_pegawai.nama_pegawai");
        }
        $row = $this->db->get()->row();
        if ($isfull) {
           
            $row->listdetail = $this->GetDetail($id,$is_stock,$row->id_gudang);
            if($is_stock)
            {
                foreach($row->listdetail as $detailpnm)
                {
                    $detailpnm->qty_dikirim=$this->m_surat_jalan->GetJumlahPengiriman($detailpnm->id_sales_order_detail);
                }
            }
            $row->listheader=[];
        }

        return $row;
    }
    function SalesOrderManipulate($model) {
       
        try {
            $message = "";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            $sales_order = array();
            $this->load->model("cabang/m_cabang");
            $this->load->model("barang/m_barang");
            $cabang = $this->m_cabang->GetOneCabang($model['id_cabang']);
            
            $sales_order['type_ppn'] = $model['type_ppn'];
            $sales_order['ppn'] = $model['ppn'];
            $sales_order['type_pembayaran'] = $model['type_pembayaran'];
            $sales_order['jth_tempo'] = DefaultTanggalDatabase($model['jth_tempo']);
            $sales_order['keterangan'] = $model['keterangan'];
            $sales_order['id_customer'] = ForeignKeyFromDb($model['id_customer']);
            $sales_order['id_pegawai'] = ForeignKeyFromDb($model['id_pegawai']);
            $sales_order['id_gudang'] = ForeignKeyFromDb($model['id_gudang']);
            $sales_order['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
            $sales_order['tanggal'] = DefaultTanggalDatabase($model['tanggal']);
            $sales_order['nominal_ppn'] = DefaultCurrencyDatabase($model['nominal_ppn']);
            $sales_order['grandtotal'] = DefaultCurrencyDatabase($model['grandtotal']);
            $sales_order['jumlah_bayar'] = DefaultCurrencyDatabase($model['jumlah_bayar']) > $sales_order['grandtotal'] ? $sales_order['grandtotal'] : DefaultCurrencyDatabase($model['jumlah_bayar']);
            $sales_order['total'] = DefaultCurrencyDatabase($model['total']);
            $sales_order['disc_pembulatan'] = DefaultCurrencyDatabase($model['disc_pembulatan']);
            $sales_order['ppn'] = DefaultCurrencyDatabase($model['ppn']);
            $id_sales_order = 0;
            if (CheckEmpty(@$model['id_sales_order_master'])) {
                $sales_order['created_date'] = GetDateNow();
                $sales_order['nomor_master'] = AutoIncrement('uap_sales_order_master', $cabang->kode_cabang . '/SO/' . date("y") . '/', 'nomor_master', 5);
                $sales_order['created_by'] = ForeignKeyFromDb($userid);
                $sales_order['status'] = '0';
                $this->db->insert("uap_sales_order_master", $sales_order);
                $id_sales_order = $this->db->insert_id();
            } else {
                $salesorderdb=$this->GetOneSales_Order($model['id_sales_order_master']);
                if($salesorderdb!=null&&$salesorderdb->status=="5")
                {
                     $sales_order['status'] = '0';
                }
               
                $sales_order['updated_date'] = GetDateNow();
                $sales_order['updated_by'] = ForeignKeyFromDb($userid);
                $id_sales_order = $model['id_sales_order_master'];
                $sales_orderdb = $this->GetOneSales_OrderPo($id_sales_order);
                $sales_order['nomor_master'] = $sales_orderdb->nomor_master;
                $this->db->update("uap_sales_order_master", $sales_order, array("id_sales_order_master" => $id_sales_order));
            }
            $arraydetailsales_order = array();
            foreach ($model['dataset'] as $key => $det) {
                $detailsales_order = array();
                $detailsales_order['id_barang'] = $det['id_barang'];
                $detailsales_order['price_barang'] = DefaultCurrencyDatabase($det['price_barang']);
                $detailsales_order['nama_barang'] = $det['nama_barang'];
                $detailsales_order['qty'] = DefaultCurrencyDatabase($det['qty']);
                $detailsales_order['disc_1'] = DefaultCurrencyDatabase($det['disc_1']);
                $detailsales_order['disc_2'] = DefaultCurrencyDatabase($det['disc_2']);
                $detailsales_order['subtotal'] = DefaultCurrencyDatabase($det['subtotal']);
                if($sales_order['type_ppn']=="1")
                {
                    $detailsales_order['ppn_satuan'] = $detailsales_order['subtotal']*((float)$sales_order['ppn']/((float)$sales_order['ppn']+(float)100));
                }
                else  if($sales_order['type_ppn']=="2")
                {
                    $detailsales_order['ppn_satuan'] = $detailsales_order['subtotal']*((float)$sales_order['ppn']/(float)100);
                }
                else
                {
                    $detailsales_order['ppn_satuan'] = 0;
                }
                if (CheckEmpty($det['id_sales_order_detail'])) {
                    $barang = $this->m_barang->GetOneBarang($det['id_barang']);
                    $detailsales_order['id_satuan'] = ForeignKeyFromDb(@$barang->id_satuan);
                    $detailsales_order['created_date'] = GetDateNow();
                    $detailsales_order['created_by'] = $userid;
                    $detailsales_order['status'] = 1;
                    $detailsales_order['id_sales_order_master'] = $id_sales_order;
                    $this->db->insert("uap_sales_order_detail", $detailsales_order);
                    $arraydetailsales_order[] = $this->db->insert_id();
                } else {

                    $detailsales_order['updated_date'] = GetDateNow();
                    $detailsales_order['updated_by'] = $userid;
                    if (!CheckEmpty(@$det['id_satuan']))
                        $detailsales_order['id_satuan'] = ForeignKeyFromDb($det['id_satuan']);
                    $this->db->update("uap_sales_order_detail", $detailsales_order, array("id_sales_order_detail" => $det['id_sales_order_detail']));
                    $arraydetailsales_order[] = $det['id_sales_order_detail'];
                }
            }


            if (count($arraydetailsales_order) > 0) {
                $this->db->where_not_in("id_sales_order_detail", $arraydetailsales_order);
                $this->db->where(array("id_sales_order_master" => $id_sales_order));
                $this->db->update("uap_sales_order_detail", array("status" => 5, 'updated_date' => GetDateNow(), 'updated_by' => $userid));
            } else {
                $this->db->update("uap_sales_order_detail", array("status" => 5, 'updated_date' => GetDateNow()), array("id_sales_order_master" => $sales_order));
            }




            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data Sales_Order PO Sudah di" . (CheckEmpty(@$model['id_sales_order_master']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_sales_order, $sales_order['nomor_master'], 'sales_order');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

  

    // datatables
    function GetDatasales_orderForPenjualan($params) {
        $isedit = CekModule("K108",false);
        $isdelete = CekModule("K109",false);
        $this->load->library('datatables');
        $this->datatables->select('id_sales_order_master,uap_vw_history_sales_order_surat_jalan.tgl_pengiriman,no_invoice,nama_customer,tanggal,nomor_master,replace(nomor_master,"/","-") as nomor_link,grandtotal,status,no_surat_jalan,nama_customer,nama_cabang,nama_gudang');
        $this->datatables->from('uap_vw_history_sales_order_surat_jalan');
        $extra = array();
        //add this line for join
        //
        //$this->datatables->join('table2', 'uap_sales_order.field = table2.field');
        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['uap_vw_history_sales_order_surat_jalan.id_cabang'] = $params['id_cabang'];
        }
        if (isset($params['id_customer']) && !CheckEmpty(@$params['id_customer'])) {
            $params['uap_vw_history_sales_order_surat_jalan.id_customer'] = $params['id_customer'];
        }
        if (isset($params['id_gudang']) && !CheckEmpty(@$params['id_gudang'])) {
            $params['uap_vw_history_sales_order_surat_jalan.id_gudang'] = $params['id_gudang'];
        }
        if(isset($params['status']) &&!CheckEmpty($params['status']))
        {
            $params['uap_vw_history_sales_order_surat_jalan.status']=$params['status'];
        }
       
        unset($params['id_customer']);
        unset($params['id_gudang']);
        unset($params['id_cabang']);
        unset($params['status']);


        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "uap_vw_history_sales_order_surat_jalan.tanggal BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tanggal_penerimaan'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }
       

        if (count($params)) {
            $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        $stredit = '';
        $strdelete = '';
        $straction = anchor("", 'view', array('class' => 'btn btn-default btn-xs', "onclick" => "viewsalesorder($1);return false;"));
         
        if ($isedit) {
            $stredit .= anchor("sales_order/editsales_order/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $strdelete .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batal($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_sales_order_master,nomor_master');
        $this->datatables->add_column('action_edit', $stredit, 'id_sales_order_master,nomor_link');
        $this->datatables->add_column('action_delete', $strdelete, 'id_sales_order_master,nomor_master');
        return $this->datatables->generate();
    }
    function GetDatasales_order($params) {
        $isedit = CekModule("K108",false);
        $isdelete = CekModule("K109",false);
        $this->load->library('datatables');
        $this->datatables->select('id_sales_order_master,nama_customer,tanggal,nomor_master,replace(nomor_master,"/","-") as nomor_link,grandtotal,uap_sales_order_master.status');
        $this->datatables->join("uap_customer", "uap_customer.id_customer=uap_sales_order_master.id_customer", "left");
        $this->datatables->from('uap_sales_order_master');
        $extra = array();
        //add this line for join
        //
        //$this->datatables->join('table2', 'uap_sales_order.field = table2.field');
        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['uap_sales_order_master.id_cabang'] = $params['id_cabang'];
        }
        if (isset($params['id_customer']) && !CheckEmpty(@$params['id_customer'])) {
            $params['uap_sales_order_master.id_customer'] = $params['id_customer'];
        }
        if (isset($params['id_gudang']) && !CheckEmpty(@$params['id_gudang'])) {
            $params['uap_sales_order_master.id_gudang'] = $params['id_gudang'];
        }
        if(!CheckEmpty($params['status']))
        {
            $params['uap_sales_order_master.status']=$params['status'];
        }
       
        unset($params['id_customer']);
        unset($params['id_gudang']);
        unset($params['id_cabang']);
        unset($params['status']);


        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "uap_sales_order_master.tanggal BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tanggal_penerimaan'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }
       

        if (count($params)) {
            $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        $stredit = '';
        $strdelete = '';
        $straction = anchor("", 'view', array('class' => 'btn btn-default btn-xs', "onclick" => "viewsalesorder($1);return false;"));
         
        if ($isedit) {
            $stredit .= anchor("sales_order/editsales_order/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $strdelete .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batal($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_sales_order_master,nomor_master');
        $this->datatables->add_column('action_edit', $stredit, 'id_sales_order_master,nomor_link');
        $this->datatables->add_column('action_delete', $strdelete, 'id_sales_order_master,nomor_master');
        return $this->datatables->generate();
    }
}
