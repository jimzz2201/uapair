<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sales_order extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_sales_order');
    }

    public function editsales_order($id, $nomorinvoice) {
        $row = (object) array();

        $module = "K099";
        $header = "K021";
        CekModule($module);
        $sales_order = $this->m_sales_order->GetOneSales_Order($id, true);
        if ($sales_order == null || $sales_order->nomor_master != str_replace("-", "/", $nomorinvoice)) {
            SetMessageSession(2, "Anda tidak memiliki akses untuk menuju link tersebut");
            redirect(base_url() . 'index.php/main/message');
        } else {
            $row = $sales_order;
        }
        $row->button = 'Edit';
        $this->load->model("customer/m_customer");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("pegawai/m_pegawai");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->list_customer = $this->m_customer->GetDropDownCustomer();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $row->list_pegawai = $this->m_pegawai->GetDropDownPegawai();
        $jenis_pembayaran = array();
        $jenis_pembayaran[1] = "Tunai";
        $jenis_pembayaran[2] = "Kredit";
        $row->list_type_pembayaran = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row->list_type_ppn = $list_type_ppn;

        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row->list_type_pembulatan = $list_type_pembulatan;
        $row->title = 'Edit Sales Order';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';

        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'sales_order/v_sales_order_manipulate', $javascript, $css);
    }

    public function detailsales_order($id) {
        $model = $this->m_sales_order->GetOneSales_Order($id, true);

        $this->load->view("v_sales_order_search_detail", $model);
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $module = "K100";
        $header = "K021";
        $title = "Sales Order";
        CekModule($module);
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_status']=array("2"=>"Batal","1"=>"Non Batal");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $model['list_gudang']=$this->m_gudang->GetDropDownGudang();
        $this->load->model("customer/m_customer");
        $model['list_customer']=$this->m_customer->GetDropDownCustomer();
        LoadTemplate($model, "sales_order/v_sales_order_index", $javascript,$css);
    }
    
    public function sales_order_batal(){
        $message = '';
        $this->form_validation->set_rules('id_sales_order_master', 'Sales Order', 'required');
        $this->form_validation->set_rules('message', 'Message', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_sales_order->BatalSalesOrder($model['id_sales_order_master'],$model['message']);
        }

        echo json_encode($result);
    }
    
    public function sales_order_approve(){
        $message = '';
        $this->form_validation->set_rules('id_sales_order_master', 'Sales Order', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_sales_order->ApproveSalesOrder($model['id_sales_order_master']);
        }

        echo json_encode($result);
    }
    public function sales_order_reject(){
        $message = '';
        $this->form_validation->set_rules('id_sales_order_master', 'Sales Order', 'required');
        $this->form_validation->set_rules('message', 'Keterangan Reject', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_sales_order->RejectSalesOrder($model['id_sales_order_master'],$model['message']);
        }

        echo json_encode($result);
    }
    
    
    public function viewsalesorder()
    {
        $id=$this->input->post("id_sales_order_master");
        $salesorder = $this->m_sales_order->GetOneSales_Order($id, true);
        $this->load->view("sales_order/v_sales_order_view",array("row"=>$salesorder));
    }

    public function getdatasales_order() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_sales_order->GetDatasales_order($params);
    }
    public function getdatasales_order_searching() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_sales_order->GetDatasales_orderForPenjualan($params);
    }
    public function searchsales_order()
    {
        $model=$this->input->post();
        $model['tanggal'] = GetDateNow();
        $model['tanggal_awal']=TambahTanggal($model['tanggal'],"-7 Days");
        $this->load->view("v_sales_order_search",$model);
    }
    public function create() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K099";
        $header = "K021";
        CekModule($module);
        $this->load->model("customer/m_customer");
        $this->load->model("pegawai/m_pegawai");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->listdetail = [];
        $row->listheader = [];
        $row->list_customer = $this->m_customer->GetDropDownCustomer();
        $row->list_pegawai = $this->m_pegawai->GetDropDownPegawai();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $jenis_pembayaran = array();
        $jenis_pembayaran[1] = "Tunai";
        $jenis_pembayaran[2] = "Kredit";
        $row->list_type_pembayaran = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row->list_type_ppn = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row->list_type_pembulatan = $list_type_pembulatan;
        $row->tanggal = GetDateNow();
        $row->jth_tempo = date('Y-m-d', strtotime(GetDateNow() . "+1 months"));
        $row->title = 'Sales Order';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';

        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'sales_order/v_sales_order_manipulate', $javascript, $css);
    }

    public function create_sales_order() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K004";
        $header = "K001";
        CekModule($module);
        $this->load->model("satuan/m_satuan");
        $this->load->model("jenis/m_jenis");
        $this->load->model("merek/m_merek");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->list_merek = $this->m_merek->GetDropDownMerek();
        $row->list_jenis = $this->m_jenis->GetDropDownJenis();
        $row->list_satuan = $this->m_satuan->GetDropDownSatuan();
        $row->title = 'Create Barang';
        $row = json_decode(json_encode($row), true);

        $javascript = array();
        LoadTemplate($row, 'sales_order/v_sales_order_manipulate', $javascript);
    }

    public function sales_order_manipulate() {
        $message = '';
        $model = $this->input->post();

        if (CheckEmpty($model['dataset']) || count($model['dataset']) == 0) {
            $message .= "Detail Sales Order is required<br/>";
        }
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');
        $this->form_validation->set_rules('id_gudang', 'Gudang', 'trim|required');
        $this->form_validation->set_rules('id_customer', 'Customer', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        $this->form_validation->set_rules('jth_tempo', 'Jatuh Tempo', 'trim|required');
        $this->form_validation->set_rules('type_pembayaran', 'Pembayaran', 'trim|required');
        if (!CheckEmpty(@$model['type_pembayaran']) && $model['type_pembayaran'] == "1") {
            $this->form_validation->set_rules('jumlah_bayar', 'Jumlah Bayar', 'trim|required');
        }
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_sales_order->SalesOrderManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_sales_order($id = 0) {
        $row = $this->m_sales_order->GetOneBarang($id);

        if ($row) {
            $row->button = 'Update';
            $module = "K004";
            $header = "K001";
            CekModule($module);
            $row->form = $header;
            $row->formsubmenu = $module;
            $row->title = 'Update Barang';
            $this->load->model("satuan/m_satuan");
            $this->load->model("jenis/m_jenis");
            $this->load->model("merek/m_merek");
            $row->list_merek = $this->m_merek->GetDropDownMerek();
            $row->list_jenis = $this->m_jenis->GetDropDownJenis();
            $row->list_satuan = $this->m_satuan->GetDropDownSatuan();
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            LoadTemplate($row, 'sales_order/v_sales_order_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Barang cannot be found in database");
            redirect(site_url('sales_order'));
        }
    }

    public function get_one_sales_order() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_sales_order', 'nama sales_order', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_sales_order->GetOneBarang($model["id_sales_order"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data sales_order tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function sales_order_delete() {
        $message = '';
        $this->form_validation->set_rules('id_sales_order', 'Barang', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_sales_order->BarangDelete($model['id_sales_order']);
        }

        echo json_encode($result);
    }

}

/* End of file Barang.php */