<div class="modal-header">
    Pencarian Sales Order
</div>
<div class="modal-body" style="min-width:100px;">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="portlet-body form">
                    <form id="frmhide" >
                        <input name="id_customer" type="hidden" value="<?php echo @$id_customer ?>">
                        <input name="id_cabang" type="hidden" value="<?php echo @$id_cabang ?>">
                        <input name="id_gudang" type="hidden" value="<?php echo @$id_gudang ?>">
                        <div class="form-group">
                            <?= form_label('Tanggal', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>

                            <div class="col-sm-2">
                                <?= form_input(array("selected" => "","autocomplete"=>"off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal_searching'), DefaultDatePicker(@$tanggal_awal), array('required' => 'required')); ?>
                            </div>
                            <?= form_label('s / d', "txt_tanggal_akhir", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-2">
                                <?= form_input(array("selected" => "","autocomplete"=>"off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir_searching'), DefaultDatePicker(@$tanggal)); ?>
                            </div>
                            <div class="col-sm-2">
                                <button id="btt_Search" type="submit" class="btn-small btn-block btn-primary pull-right">Search</button>
                            </div>

                        </div>

                    </form>
                    <table class="table table-striped table-bordered table-hover" id="mytablesearch" style="width:100%">

                    </table>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="sales_orderrtlet-body form">

                    <table class="table table-striped table-bordered table-hover" id="mytablesearch" style="width:100%">

                    </table>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="detailsales_order">

            </div>
        </div>
    </div>
</div>
<script>
    var tablesearch;
    $("form#frmhide").submit(function(){
        tablesearch.fnDraw(false);
        return false;
    })
    $(document).ready(function () {
        setTimeout(function () {
            InitialGridSearch();
        }, 500);
        $(".datepicker").datepicker();
    })

    function selectsales_order(id_sales_order)
    {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/sales_order/detailsales_order/' + id_sales_order,
            success: function (data) {
                $("#detailsales_order").html(data);

            },
            error: function (xhr, status, error) {
                messageerror(xhr.ressales_ordernseText);
            }
        });
    }
    function InitialGridSearch()
    {
        tablesearch = $("#mytablesearch").dataTable({

            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": baseurl+"index.php/sales_order/getdatasales_order_searching", "type": "POST", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frmhide").serialize()
                    });
                }},
            columns: [
                {data: "tanggal", orderable: false, title: "Tanggal"}
                , {data: "nama_customer", orderable: false, title: "Customer"}
                , {data: "nomor_master", orderable: false, title: "NO PO"}
                , {data: "nama_cabang", orderable: false, title: "Cabang"}
                , {data: "nama_gudang", orderable: false, title: "Gudang"}
                , {data: "grandtotal", orderable: false, title: "Jumlah",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                , {data: "no_surat_jalan", orderable: false, title: "NO Surat Jalan"}
                , {data: "no_invoice", orderable: false, title: "NO Invoice"}
                ,{
                    "data": "id_sales_order_master",
                    "orderable": false,
                    "className": "text-center nopadding",
                    mRender: function (data, type, row) {
                        return "<a href=\"javascript:;\" class=\"btn  btn-success btn-xs\" onclick=\"selectsales_order(" + data + ");return false;\">Pilih</a>";
                    }
                }
            ],
            order: [[0, 'desc']],
            initComplete: function () {

            }

        });
    }



</script>