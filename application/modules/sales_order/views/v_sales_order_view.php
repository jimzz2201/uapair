<div style="text-align: left" >
    <h3>Sales Order : <?php echo @$row->nomor_master ?><?php
        if (@$row->status == "2") {
            echo "  <span style='color:red;font-weight:bold'>( BATAL )</span>";
        }
        ?></h3>
    <p>View Sales Order | Sales Order</p>
    <h2>
    <?php 
        switch (@$row->status) {
        case "0":
            echo "Pending";
            break;
        case "1":
            echo "Approved";
            break;
        case "5":
            echo "<span style='color:red;font-weight:bold'>"."( ".DefaultDatePicker($row->approved_date)." ) "."Rejected : ".$row->approved_comment. "</span>";
            break;
      
        default:
            echo "";
    }
    ?>
    </h2>
</div>

<section class="content" >
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">

                        <div class="form-group" >
                            <div class="row " style="margin-bottom:20px;">
                                <div class="col-sm-12 kolomheaderpo">
                                    <hr/>
                                    <?php
                                    echo "<b>Nomor : " . DefaultDatePicker(@$row->tanggal) . " == " . @$row->nomor_master . "</b><br/>";
                                    ?>
                                    <hr/>
                                </div>

                            </div>
                            

                        </div>
                        <form id="frm_pembelian" class="form-horizontal form-groups-bordered validate" method="post">
                            <input type="hidden" id="txt_id_penerimaan_pembelian" name="id_penerimaan" value="<?php echo @$id_penerimaan; ?>" /> 
                            <input type="hidden" id="txt_id_pembelian_master" name="id_pembelian_master" value="<?php echo @$id_pembelian_master; ?>" /> 
                            <div class="form-group">
                                <?= form_label('Customer', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'value' => @$row->nama_customer, 'class' => 'form-control', 'placeholder' => 'Customer', 'disabled' => 'disabled')); ?>
                                </div>    
                                <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                <div class="col-sm-5">
                                    <?= form_input(array('type' => 'text', 'value' => DefaultDatePicker(@$row->tanggal), 'class' => 'form-control ', 'placeholder' => 'Tanggal', 'disabled' => 'disabled')); ?>
                                </div>


                            </div>


                            <div class="form-group">

                                <?= form_label('Cabang', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'value' => @$row->nama_cabang, 'class' => 'form-control', 'placeholder' => 'Cabang', 'disabled' => 'disabled')); ?>
                                </div>

                                <?= form_label('Gudang', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                <div class="col-sm-5">
                                    <?= form_input(array('type' => 'text', 'value' => @$row->nama_gudang, 'class' => 'form-control', 'placeholder' => 'Gudang', 'disabled' => 'disabled')); ?>
                                </div>

                            </div>
                            <div class="form-group">
                                <?= form_label('Sales', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'value' => @$row->nama_pegawai, 'class' => 'form-control', 'placeholder' => 'Sales', 'disabled' => 'disabled')); ?>
                                </div>    
                                <?= form_label('PPN', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                <div class="col-sm-5">
                                    <?= form_input(array('type' => 'text', 'value' => @$row->ppn > 0 ? ( (@$row->type_ppn == "1" ? "Include ( " : "Exclude ( ") . DefaultCurrency(@$row->ppn) . " % )") : "Tanpa PPN", 'class' => 'form-control', 'placeholder' => 'Gudang', 'disabled' => 'disabled')); ?>
                                </div>


                            </div>
                            <div class="form-group">
                                <?= form_label('Jatuh Tempo', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'value' => DefaultDatePicker(@$row->jth_tempo), 'class' => 'form-control', 'placeholder' => 'Jatuh Tempo', 'disabled' => 'disabled')); ?>
                                </div>    
                                <?= form_label('Pembayaran', "txt_supplier", array("class" => 'col-sm-1 control-label')); ?>
                                <div class="col-sm-5">
                                    <?= form_input(array('type' => 'text', 'value' => @$row->type_pembayaran == "1" ? "Tunai" : "Kredit", 'class' => 'form-control', 'placeholder' => 'Jatuh Tempo', 'disabled' => 'disabled')); ?>
                                </div> 


                            </div>

                            <div class="form-group">
                                <?= form_label('Keterangan', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?= form_textarea(array('type' => 'text', 'value' => @$row->keterangan, 'class' => 'form-control', 'placeholder' => 'Keterangan', 'disabled' => 'disabled')); ?>
                                </div>    



                            </div>

                            <div class="box box-default">


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet-body form">
                                            <table class="table table-striped table-bordered table-hover" id="mytable" style="width:100%;">
                                                <thead><tr>
                                                        <th>SKU</th>
                                                        <th>Nama Barang</th>
                                                        <th>Qty</th>
                                                        <th>Price</th>
                                                        <th>Disc 1</th>
                                                        <th>Disc 2</th>
                                                        <th>Subtotal</th>
                                                    </tr></thead>
                                                <tbody>
                                                    <?php
                                                    foreach (@$row->listdetail as $detail) {
                                                        echo "<tr>";
                                                        echo "<td>" . $detail->sku . "</td>";
                                                        echo "<td>" . $detail->nama_barang . "</td>";
                                                        echo "<td>" . DefaultCurrency($detail->qty) . "</td>";
                                                        echo "<td>" . DefaultCurrency($detail->price_barang) . "</td>";
                                                        echo "<td>" . DefaultCurrency($detail->disc_1) . "</td>";
                                                        echo "<td>" . DefaultCurrency($detail->disc_2) . "</td>";
                                                        echo "<td>" . DefaultCurrency($detail->subtotal) . "</td>";
                                                        echo "</tr>";
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <hr/>
                            <div class="form-group">
                                <?= form_label('Total', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$row->total), 'class' => 'form-control', 'placeholder' => 'Jenis Potongan', 'disabled' => 'disabled')); ?>
                                </div>    
                                <?= form_label('PPN', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                <div class="col-sm-5">
                                    <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$row->nominal_ppn), 'class' => 'form-control', 'placeholder' => 'Gudang', 'disabled' => 'disabled')); ?>
                                </div>


                            </div>


                            <?php if (!CheckEmpty(@$row->disc_pembulatan)) { ?>
                                <div class="form-group">
                                    <?= form_label('Jenis Pembulatan', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'value' => @$row->type_pembulatan == "1" ? "Sebelum PPN " : "Sesudah PPN", 'class' => 'form-control', 'placeholder' => 'Jenis Potongan', 'disabled' => 'disabled')); ?>
                                    </div>    
                                    <?= form_label('Pembulatan', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$row->disc_pembulatan), 'class' => 'form-control', 'placeholder' => 'Pembulatan', 'disabled' => 'disabled')); ?>
                                    </div>


                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <?= form_label('Grandtotal', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$row->grandtotal), 'class' => 'form-control', 'placeholder' => 'Jenis Potongan', 'disabled' => 'disabled')); ?>
                                </div>    
                                <?= form_label('Jumlah&nbsp;Bayar', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                <div class="col-sm-5">
                                    <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$row->jumlah_bayar), 'class' => 'form-control', 'placeholder' => 'Jumlah Bayar', 'disabled' => 'disabled')); ?>
                                </div>


                            </div>

                        </form>
                        <div class="form-group" style="text-align:left">
                            <div class="col-sm-4">
                                <?php
                                if (@$row->status == "0") {
                                    echo '<a href="javascript:;" class="btn btn-success waves-effect" onclick="ApproveSalesOrder('.@$row->id_sales_order_master.');return false;"><i class="fa fa-plus"></i> Approve</a>&nbsp;&nbsp;&nbsp;';
                                    echo '<a href="javascript:;" class="btn btn-danger waves-effect" onclick="RejectSalesOrder('.@$row->id_sales_order_master.');return false;"><i class="fa fa-plus"></i> Reject</a>';
                                }
                                ?>
                            </div>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</section>