<input  type="hidden" name="id_sales_order_master" id="txt_id_sales_order_master_obj" value="<?php echo $id_sales_order_master ?>"   />
<input  type="hidden" name="nomor_master" id="txt_nomor_master_obj" value="<?php echo $nomor_master ?>"  />
<input  type="hidden" name="id_customer" id="txt_id_customer_obj" value="<?php echo $id_customer ?>"  />
<input  type="hidden" name="id_cabang" id="txt_id_cabang_obj" value="<?php echo $id_cabang ?>"  />
<input  type="hidden" name="id_gudang" id="txt_id_gudang_obj" value="<?php echo $id_gudang ?>"  />
<input  type="hidden" name="tanggal" id="txt_tanggal_obj" value="<?php echo $tanggal ?>"  />
<input  type="hidden" name="id_pegawai" id="txt_id_pegawai_obj" value="<?php echo $id_pegawai ?>"  />
<input  type="hidden" name="type_ppn" id="txt_type_ppn_obj" value="<?php echo $type_ppn ?>"  />
<input  type="hidden" name="ppn" id="txt_ppn_obj" value="<?php echo $ppn ?>"  />
<input  type="hidden" name="disc_pembulatan" id="txt_disc_pembulatan_obj" value="<?php echo $disc_pembulatan ?>"  />
<input  type="hidden" name="jumlah_bayar" id="txt_jumlah_bayar_obj" value="<?php echo $jumlah_bayar ?>"  />

<input  type="hidden" name="type_pembayaran" id="txt_type_pembayaran_obj" value="<?php echo $type_pembayaran ?>"  />
<table class="table table-striped table-bordered table-hover dataTable no-footer">
    <thead>
        <tr>
            <th><input type="checkbox" checked="" class="i-checks" id="cb_all" /></th>
            <th>SKU Barang</th>
            <th>Nama Barang</th>
            <th>Price Barang</th>
            <th width="100px">Qty</th>
            <th width="100px">Quantity Order</th>
            <th>Satuan</th>
        </tr>
    </thead>
    <?php foreach (@$listdetail as $key => $detail) {
        ?>
        <tr>
            <td align="center">
                <input  checked="" type="checkbox" id="cb_det_<?php echo $key ?>" class="i-checks checkdetail"  />
                <?= form_input(array('type' => 'hidden', "id" => "txt_harga_beli_akhir_" . $key, 'value' => DefaultCurrency($detail->harga_beli_akhir))); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_harga_beli_" . $key, 'value' => DefaultCurrency($detail->price_barang))); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_disc1_" . $key, 'value' => DefaultCurrency($detail->disc_1))); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_disc2_" . $key, 'value' => DefaultCurrency($detail->disc_2))); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_nama_barang_" . $key, 'value' => $detail->nama_barang)); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_sku_" . $key, 'value' => $detail->sku)); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_id_barang_" . $key, 'value' => $detail->id_barang)); ?>
            </td>
            <td><?= $detail->sku ?></td>
            <td><?= $detail->nama_barang ?></td>
            <td><?= DefaultCurrency($detail->price_barang) ?></td>
            <td style="width:100px"><?= DefaultCurrency($detail->qty) ?></td>
            <td style="width:100px"><?= form_input(array('type' => 'text', 'name' => 'qty[]', 'value' => DefaultCurrency($detail->qty), 'class' => 'form-control qty', 'id' => 'txt_qty_' . $key, 'placeholder' => 'Qty', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'style' => "padding:2px 12px")); ?></td>
            <td><?= $detail->nama_satuan ?></td>
        </tr>

    <?php } ?>
</table>

<br/>
<button type="button" class="btn btn-primary btn-block" id="btt_input_detail" >Input Detail</button>
<script>
    $(document).ready(function () {

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    })
    $("#btt_clear_pembelian").click(function () {
        listheader = [];
        dataset = [];
        RefreshGrid();
        $(".kolomheaderpo").empty();
    })
    $("#btt_input_detail").click(function () {
        var isclosed = false;

        var res = alasql('SELECT * FROM ? where id_sales_order_master=\'' + $("#txt_id_sales_order_master").val() + '\'', [listheader]);
        if (res.length > 0)
        {
            modaldialogerror("Data PO Sudah ada dalam list pembelian");
        } else
        {
            $.each($(".qty"), function (key, value) {
                var qtyid = value.id;
                var id = qtyid.replace("txt_qty_", "");
                if (value.value > 0)
                {
                    isclosed = true;
                    var pricelist = 0;
                    if ($("#txt_harga_beli_" + id).val() == "0")
                    {
                        pricelist = Number($("#txt_harga_beli_akhir_" + id).val().replace(/[^0-9\.]+/g, ""));
                    } else
                    {
                        pricelist = Number($("#txt_harga_beli_" + id).val().replace(/[^0-9\.]+/g, ""));
                    }

                    var disc1 = Number($("#txt_disc1_" + id).val().replace(/[^0-9\.]+/g, ""));
                    var disc2 = Number($("#txt_disc2_" + id).val().replace(/[^0-9\.]+/g, ""));
                    var qty = Number($("#txt_qty_" + id).val().replace(/[^0-9\.]+/g, ""));
                    var subtotal = pricelist * ((100 - disc1) / 100) * ((100 - disc2) / 100) * qty;
                    var detail = {};
                    detail.id_penjualan_detail = 0;
                    detail.id_barang = $("#txt_id_barang_" + id).val();
                    detail.price_barang = pricelist;
                    detail.nama_barang = $("#txt_nama_barang_" + id).val();
                    detail.sku = $("#txt_sku_" + id).val();
                    detail.qty = qty;
                    detail.disc_1 = disc1;
                    detail.disc_2 = disc2;
                    detail.subtotal = subtotal;
                    dataset.push(detail);
                    RefreshGrid();
                }
            });
            if (isclosed)
            {
                var header = {};
                header.id_sales_order_master = $("#txt_id_sales_order_master_obj").val();
                header.nomor_master = $("#txt_nomor_master").val();
                header.tgl = $("#txt_tgl").val();
                $("#txt_id_sales_order_master").val($("#txt_id_sales_order_master_obj").val());
                if ($("#txt_id_customer_obj").val() != "")
                {
                    $("#dd_id_customer").val($("#txt_id_customer_obj").val());
                    $("#dd_id_customer").trigger("change");
                }
                if ($("#txt_id_pegawai_obj").val() != "")
                {
                    $("#dd_id_pegawai").val($("#txt_id_pegawai_obj").val());
                    $("#dd_id_pegawai").trigger("change");
                }
                if ($("#txt_id_cabang_obj").val() != "")
                {
                    $("#dd_id_cabang").val($("#txt_id_cabang_obj").val());
                    $("#dd_id_cabang").trigger("change");
                }
                if ($("#txt_id_gudang_obj").val() != "")
                {
                    $("#dd_id_gudang").val($("#txt_id_gudang_obj").val());
                    $("#dd_id_gudang").trigger("change");
                }
                if ($("#txt_type_pembayaran_obj").val() != "")
                {
                    $("#dd_type_pembayaran").val($("#txt_type_pembayaran_obj").val());
                    $("#dd_type_pembayaran").trigger("change");
                }
                if ($("#txt_ppn_obj").val() != "")
                {
                    $("#txt_ppn").val($("#txt_ppn_obj").val());
                    $("#txt_ppn").trigger("change");
                }
                if ($("#txt_type_ppn_obj").val() != "")
                {
                    $("#dd_type_ppn").val($("#txt_type_ppn_obj").val());
                    $("#dd_type_ppn").trigger("change");
                }
                if ($("#txt_jumlah_bayar_obj").val() != "")
                {
                    $("#txt_jumlah_bayar").val(Comma($("#txt_jumlah_bayar_obj").val()));
                    $("#txt_jumlah_bayar").trigger("change");
                }
                if ($("#txt_disc_pembulatan_obj").val() != "")
                {
                    $("#txt_disc_pembulatan").val(Comma($("#txt_disc_pembulatan_obj").val()));
                    $("#txt_disc_pembulatan").trigger("change");
                }
                $(".kolomheaderpo").append("Nomor PO : " + $("#txt_tanggal_obj").val() + " == " + $("#txt_nomor_master_obj").val() + "<br/>");
                
                listheader.push(header);
                closemodalboostrap();
            } 
            else
            {
                modaldialogerror("Quantity detail pembelian harus lebih besar dari 1");
            }
        }

    })
    $("#cb_all").on("ifChanged", function (e) {
        if (e.target.checked)
        {
            $('.checkdetail').iCheck('check');
        } else
        {
            $('.checkdetail').iCheck('uncheck');
        }
    })
    function ChangeDet(checkid) {
        var id = checkid.replace("cb_det_", "");
        if ($("#" + checkid).is(":checked"))
        {
            $("#txt_qty_" + id).removeAttr("readonly", "readonly");
            $("#txt_qty_" + id).val(Comma($("#txt_hidden_qty_" + id).val()));
        } else
        {
            $("#txt_qty_" + id).attr("readonly", "readonly");
            $("#txt_qty_" + id).val(0);
        }
    }
    $(".checkdetail").on("ifChanged", function (e) {
        ChangeDet(e.target.id);
        if (!e.target.checked)
        {
            $('#cb_all').prop('checked', false).iCheck('update');
        }
    })
</script>