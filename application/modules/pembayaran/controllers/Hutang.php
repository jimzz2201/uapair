<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hutang extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("retur/m_retur_beli");
       
    }
    
    public  function index(){
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K027";
        $header = "K026";
        $title = "Pembayaran Hutang";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $model['list_gudang']=$this->m_gudang->GetDropDownGudang();
        $model['list_status']=array("2"=>"Batal","1"=>"Non Batal");
        $this->load->model("supplier/m_supplier");
        $model['list_supplier']=$this->m_supplier->GetDropDownSupplier();
        LoadTemplate($model, "pembayaran/v_pembayaran_hutang_index", $javascript, $css);
    }
     public function create() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K042";
        $header = "K016";
        CekModule($module);
        $this->load->model("supplier/m_supplier");
        $this->load->model("barang/m_barang");
        $this->load->model("bank/m_bank");
        $this->load->model("cabang/m_cabang");
        $row->form = $header;
        $row->formsubmenu = $module;
        $jenis_pembayaran = array();
        $jenis_pembayaran["Cash"] = "Cash";
        $jenis_pembayaran["Transfer"] = "Transfer";
        $jenis_pembayaran["Debit Card"] = "Debit Card";
        $jenis_pembayaran["Credit Card"] = "Credit Card";
        $jenis_pembayaran["Cek & Giro"] = "Cek & Giro";
        $row->list_type_pembayaran = $jenis_pembayaran;
        $row->list_supplier = $this->m_supplier->GetDropDownSupplier();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->tanggal = GetDateNow();
        $row->listbank=$this->m_bank->GetDropDownBank();
        $row->listdetail=[];
        $row->title = 'Create Pembayaran Hutang';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';

        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'pembayaran/v_pembayaran_hutang_manipulate', $javascript, $css);
    }
    
     
}

/* End of file Barang.php */