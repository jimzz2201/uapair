<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Retur Pembelian</h2>
                                <p><?= $button ?> Retur Pembelian | Retur Pembelian</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                            <div class="form-group" >
                                
                            </div>
                            <form id="frm_pembelian" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" id="txt_id_penerimaan" name="id_penerimaan" value="<?php echo @$id_penerimaan; ?>" /> 
                                 <input type="hidden" name="withdocument" value="yes" />
                                <input type="hidden" id="txt_id_pembelian_master" name="id_pembelian_master" value="<?php echo @$id_pembelian_master; ?>" /> 
                                <input type="hidden" id="txt_id_retur_pembelian_master" name="id_retur_pembelian_master" value="<?php echo @$id_retur_pembelian_master; ?>" /> 
                               <input type="hidden" id="txt_id_cabang" name="id_cabang" value="<?php echo @$id_cabang; ?>" /> 
                                <input type="hidden" id="txt_id_gudang" name="id_gudang" value="<?php echo @$id_gudang; ?>" /> 
                                <div class="form-group">
                                   
                                    <?= form_label('Type Pembayaran', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "jenis_pembayaran"), DefaultEmptyDropdown(@$list_type_pembayaran, "", "Pembayaran"), @$jenis_pembayaran, array('class' => 'form-control', 'id' => 'dd_jenis_pembayaran')); ?>
                                    </div>
                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Supplier', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        $listsupplier = DefaultEmptyDropdown(@$list_supplier, "", "Supplier");
                                        if (!CheckEmpty(@$id_supplier)) {
                                            $textsupplier = $listsupplier[$id_supplier];
                                            $listsupplier = [];
                                            $listsupplier[@$id_supplier] = $textsupplier;
                                        }
                                        ?>
                                        <?= form_dropdown(array("name"=>"id_supplier"), $listsupplier, @$id_supplier, array('class' => 'form-control', 'id' => 'dd_id_supplier')); ?>
                                    </div>    
                                    <?= form_label('Hutang', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text',"disabled"=>"disabled", 'autocomplete' => 'off', 'value' => DefaultCurrency(@$hutang), 'class' => 'form-control', 'id' => 'txt_hutang', 'placeholder' => 'Hutang')); ?>
                                    </div>
                                    

                                </div>
                                <div class="form-group">
                                    <?php
                                    $listcabang = DefaultEmptyDropdown(@$list_cabang, "", "Cabang");
                                    if (!CheckEmpty(@$id_cabang)) {
                                        $textcabang = $listcabang[@$id_cabang];
                                        $listcabang = [];
                                        $listcabang[@$id_cabang] = $textcabang;
                                    }
                                    ?>
                                    <?= form_label('Nama Cabang', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array(), $listcabang, @$id_cabang, array('class' => 'form-control', 'id' => 'dd_id_cabang')); ?>
                                    </div>
                                    <?php
                                    $listbank = DefaultEmptyDropdown(@$listbank, "", "Bank");
                                    ?>
                                    <?= form_label('Nama Bank', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array(), @$listbank, @$id_bank, array("disabled"=>"disabled",'class' => 'form-control', 'id' => 'dd_id_bank')); ?>
                                    </div>
                                </div>
                               <hr/>
                               
                               <div class="form-group">
                                    
                                   
                                    <?= form_label('Nomor Cek', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text',"disabled"=>"disabled", 'autocomplete' => 'off', 'value' => @$no_jenis_pembayaran, 'class' => 'form-control', 'id' => 'txt_no_jenis_pembayaran','name'=>'no_jenis_pembayaran', 'placeholder' => 'No Cek')); ?>
                                    </div>
                                    <?= form_label('Jatuh&nbsp;Tempo', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                         <?= form_input(array('type' => 'text',"disabled"=>"disabled", 'autocomplete' => 'off', 'value' => DefaultDatePicker(@$jatuh_tempo), 'class' => 'form-control datepicker', 'id' => 'txt_jatuh_tempo', 'placeholder' => 'Jatuh Tempo')); ?>
                                    </div>
                                </div>
                               <div class="form-group">
                                    
                                   
                                    <?= form_label('Debit Card', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text',"disabled"=>"disabled", 'autocomplete' => 'off', 'value' => @$debit_card, 'class' => 'form-control', 'id' => 'txt_debit_card','name'=>'debit_card', 'placeholder' => 'Debit Card')); ?>
                                    </div>
                                    <?= form_label('Biaya Lain', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                         <?= form_input(array('type' => 'text',"name"=>"biaya",'autocomplete' => 'off', 'value' => DefaultCurrency(@$biaya_lain),'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'class' => 'form-control', 'id' => 'txt_biaya_lain', 'placeholder' => 'Biaya Lain')); ?>
                                    </div>
                                </div>
                               <div class="form-group">
                                    
                                   
                                    <?= form_label('Credit Card', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text',"disabled"=>"disabled", 'autocomplete' => 'off', 'value' => @$credit_card, 'class' => 'form-control', 'id' => 'txt_credit_card','name'=>'credit_card', 'placeholder' => 'Kredit Card')); ?>
                                    </div>
                                    <?= form_label('Potongan', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                         <?= form_input(array('type' => 'text',"name"=>"potongan", 'autocomplete' => 'off', 'value' => DefaultCurrency(@$potongan),'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'class' => 'form-control', 'id' => 'txt_potongan', 'placeholder' => 'Potongan')); ?>
                                    </div>
                                </div>
                               
                               <hr/>
                               <div class="form-group">
                                <?= form_label('Dokumen', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array(), DefaultEmptyDropdown(@$listdokumen,"","Dokumen"), @$id_pembelian, array('class' => 'form-control', 'id' => 'dd_dokumen')); ?>
                                    </div>
                                    <?= form_label('Sisa', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-4">
                                         <?= form_input(array('type' => 'text',"disabled"=>"disabled","name"=>"potongan", 'autocomplete' => 'off', 'value' => DefaultCurrency(@$potongan),'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'class' => 'form-control', 'id' => 'txt_potongan', 'placeholder' => 'Potongan')); ?>
                                    </div>
                                   <div class="col-sm-1">
                                       <button type="button" class="btn btn-primary btn-block btn-oke" id="btt_save_detail" >Save</button>
                                   </div>
                                </div>
                                <div class="box box-default">


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet-body form">
                                                <table class="table table-striped table-bordered table-hover" id="mytable" style="width:100%;">
                                                    <thead>
                                                        <tr>
                                                            <th>Kode</th>
                                                            <th>SKU</th>
                                                            <th>Nama Barang</th>
                                                            <th>Diterima</th>
                                                            <th>DiRetur</th>
                                                            <th>Price</th>
                                                            <th>Qty Retur</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="bodytable">
                                                     <?php include APPPATH . 'modules/retur/views/v_retur_beli_content.php' ?> 
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <hr/>
                               
                                <div class="form-group">
                                    <?= form_label('Total', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'total', 'value' => DefaultCurrency(@$total), 'class' => 'form-control', 'id' => 'txt_total', 'placeholder' => 'Total', "readonly" => "readonly")); ?>
                                    </div>
                                    <?= form_label('Jumlah&nbsp;Bayar', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                       <?= form_input(array('type' => 'text',  'name' => 'jumlah_bayar', 'value' => DefaultCurrency(@$jumlah_bayar), 'class' => 'form-control', 'id' => 'txt_jumlah_bayar', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);','placeholder' => 'Jumlah&nbsp;Bayar')); ?>
                                    </div>
                                   


                                </div>
                                <div class="form-group" style="margin-top:50px">
                                    <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>


    var table;
    var dataset = <?php echo json_encode(@$listdetail) ?>;
    var listheader = <?php echo json_encode(@$listheader) ?>;

    
    $(document).ready(function () {
        $("#dd_jenis_pembayaran,#dd_id_supplier").select2();
        $("#dd_jenis_pembayaran").on("select2:select", function (e) {
            CekSectionPart($(this).val());
        });
        $("#dd_jenis_pembayaran").select2();
        $("#dd_dokumen").select2();
        $(".datepicker").datepicker();
        $("#dd_id_supplier").change(function () {
            if ($("#dd_id_supplier").val() != "0")
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/supplier/get_one_supplier',
                    dataType: 'json',
                    data: $("form#frm_pembelian").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            $("#txt_kode_supplier").val(data.obj.kode_supplier);
                            $("#txt_kode_supplier").val(data.obj.kode_supplier);
                            $("#txt_alamat").html(data.obj.alamat);
                            $("#txt_hutang").val(Comma(data.obj.hutang));
                            $("#txt_saldo_awal").val(Comma(data.obj.saldo_awal));
                            $("#txt_saldo_semestinya").val(Comma(data.obj.saldo_awal));
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            } else
            {
                $("#txt_kode_supplier").val("");
                $("#txt_alamat").html("");
                $("#txt_saldo_awal").val("0");
                $("#txt_hutang").val("0");
                $("#txt_saldo_semestinya").val("0");
            }
        })

    })
    function GetSupplierAttribute()
    {
        
    }
    function CekSectionPart(jenis_bayar)
    {
        if(jenis_bayar=="Cash"||jenis_bayar=="0")
        {
            $("#dd_id_bank").attr("disabled","disabled");
        }
        else
        {
            $("#dd_id_bank").removeAttr("disabled");
        }
        
        if(jenis_bayar!="Debit Card")
        { 
            $("#txt_debit_card").attr("disabled","disabled");
        }
        else
        {
            $("#txt_debit_card").removeAttr("disabled");
        }
        if(jenis_bayar!="Credit Card")
        {
             $("#txt_credit_card").attr("disabled","disabled");
        }
        else
        {
            $("#txt_credit_card").removeAttr("disabled");
        }
        if(jenis_bayar!="Cek & Giro")
        {
             $("#txt_jatuh_tempo").attr("disabled","disabled");
        }
        else
        {
             $("#txt_jatuh_tempo").removeAttr("disabled");
        }
    }
    function HitungSemua(){
        
        
    }
    $("#dd_type_pembulatan,#dd_type_ppn,#txt_ppn").change(function () {
        HitungSemua();
    })
    function RefreshPanelGrid()
    {
        if ($(".returcolumn").length > 0)
        {
            $("#frm_pembelian button[type=submit]").removeAttr("disabled");
        } else
        {
            $("#frm_pembelian button[type=submit]").attr("disabled", "disabled");
        }

    }

    $(document).ready(function () {
        $("#txt_tanggal").datepicker();
        RefreshPanelGrid();


    })
    $("#btt_clear_pembelian").click(function () {
        listheader = [];
        dataset = [];
        RefreshPanelGrid();
        $(".kolomheaderpo").empty();
        $("#dd_id_supplier").removeAttr("disabled");
        $("#dd_id_cabang").removeAttr("disabled");
        $("#dd_id_gudang").removeAttr("disabled");
        $("#btt_search_po").css("display", "block");
        $("#bodytable").html("<tr><td align=\"center\" colspan=\"6\">No Data Display</td></tr>");
    })
    $("#btt_search_po").click(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/retur/beli/search_penerimaan',
            data: $("#frm_pembelian").serialize() ,
            success: function (data) {
                modalbootstrap(data, "Search PO", "1280px");

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });

    })
    $("#frm_pembelian").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data retur berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                LoadBar.show();
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/retur/beli/retur_beli_manipulate',
                    dataType: 'json',
                    data: $("#frm_pembelian").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/retur/beli";

                        } else
                        {
                            messageerror(data.msg);
                        }
                        LoadBar.hide();

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                        LoadBar.hide();
                    }
                });
            }
        });
        return false;


    })
</script>