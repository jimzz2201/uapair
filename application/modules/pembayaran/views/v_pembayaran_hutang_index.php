
<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Pembayaran Hutang</h2>
                                <p>Pembayaran Hutang | Manage Pembayaran Hutang</p>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-3'>
                        <div class='breadcomb-report'>
                            <?php echo anchor(site_url('pembayaran/hutang/create'), '<i class="fa fa-plus"></i> Create', 'class="btn btn-success"'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification"></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Tanggal', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>

                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "","autocomplete"=>"off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker(date('Y-m-d')), array('required' => 'required')); ?>
                        </div>
                        <?= form_label('s / d', "txt_tanggal_akhir", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "","autocomplete"=>"off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker(date('Y-m-d'))); ?>
                        </div>
                        <?= form_label('Cabang', "txt_tanggal_akhir", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_cabang', 'value' => @$id_cabang, 'class' => 'form-control', 'id' => 'id_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown(@$list_cabang, "", "Cabang")); ?>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Supplier', "dd_id_supplier", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_supplier', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_supplier', 'placeholder' => 'Supplier'), DefaultEmptyDropdown(@$list_supplier, "", "Supplier")); ?>
                        </div>
                        <?= form_label('Status', "txt_status", array("class" => 'col-sm-1 control-label')); ?>    
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'status', 'value' => "", 'class' => 'form-control', 'id' => 'status', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_status, "", "Status")); ?>
                        </div>
                        <?= form_label('Gudang', "txt_tanggal_akhir", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_gudang', 'value' => @$id_gudang, 'class' => 'form-control', 'id' => 'id_gudang', 'placeholder' => 'Gudang'), DefaultEmptyDropdown(@$list_gudang, "", "Gudang")); ?>
                        </div>
                        <div class="col-sm-2">
                            <button id="btt_Search" type="submit" class="btn-small btn-block btn-primary pull-right">Search</button>
                        </div>
                    </div>
                </div>



            </form>
            <form id="frm_extra" method="post" target="_blank" style="display: none;">
                <input type="hidden" name="search" id="extra" value=""/>
            </form>
            <br/>
            <div class="row">
                <div class="col-md-12">
                    <div id="report-view" style="overflow-x: auto;"></div>
                </div>
            </div>
        </div>
    </div>

</section>

<div class="">
    <div class="box box-default form-element-list">


        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">
                       
                    </table>
                </div>
            </div>

        </div>
    </div>

</div>
<script type="text/javascript">
    var table;
    $("form#frm_search").submit(function () {
        table.fnDraw(false);
        pembayarann false;
    })
    
    function viewpembayaran(id_pembayaran_pemhutangan_master)
    {
        $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/pembayaran/hutang/viewpembayaranhutang',
                    data: {
                        id_pembayaran_pemhutangan_master: id_pembayaran_pemhutangan_master
                    },
                    success: function (data) {
                        modaldialog(data)
                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }

                });
    }
    function batal(id_pembayaran_pemhutangan_master) {
        swal({
            title: "Apakah kamu yakin membatalkan pembayaran berikut?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/pembayaran/hutang/batal',
                    dataType: 'json',
                    data: {
                        id_pembayaran_pemhutangan_master: id_pembayaran_pemhutangan_master
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }

                });
            }
        });
        pembayarann false;

    }

    function view(id_pembayaran_pemhutangan_master)
    {
        $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/pembayaran/hutang/view',
                    data: {
                        id_pembayaran_pemhutangan_master: id_pembayaran_pemhutangan_master
                    },
                    success: function (data) {
                        modaldialog(data)
                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }

                });
    }

    function RefreshGrid() {


    }

    $(document).ready(function () {
        $(".datepicker").datepicker();
        $(".select2").select2();
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            pembayarann {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": baseurl+"index.php/pembayaran/hutang/getdatapembayaran", "type": "POST", "data": function (d) {
                    pembayarann $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_pembayaran_pemhutangan_master",
                    title: "Kode",
                    width: "50px",
                    orderable: false
                }
                , {data: "tanggal", orderable: false, title: "Tanggal", "width": "120px",
                    mRender: function (data, type, row) {
                        pembayarann DefaultDateFormat(data == undefined ? 0 : data);
                    }}
                , {data: "kode_pembayaran_pemhutangan", orderable: false, title: "Kode&nbsp;Retur", "width": "100px"}
                , {data: "nama_supplier", orderable: false, title: "Supplier", "width": "250px"}
                , {data: "nomor_master", orderable: false, title: "NO Doc", "width": "100px"}
                , {data: "kode_penerimaan", orderable: false, title: "Penerimaan", "width": "100px"}
                , {data: "nama_cabang", orderable: false, title: "Cabang", "width": "100px"}
                , {data: "nama_gudang", orderable: false, title: "Gudang", "width": "100px"}
                ,

                {
                    "data": "action_view",
                    "orderable": false,
                    "width": "180px",
                    "className": "text-center nopadding",
                    mRender: function (data, type, row) {

                        var action = "";
                        if (row['status'] == "2")
                        {
                            pembayarann row['action_view'] + ' | Batal';
                        } else
                        {
                            pembayarann row['action_view'] + row['action_edit'] + row['action_delete'];
                        }
                    }
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

            },
            initComplete: function () {
                RefreshGrid();
            }
        });

    });
</script>
