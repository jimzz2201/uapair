<ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
    <?php
    foreach ($listmenu as $key=>$menu) {
        if (count($menu->submenu) > 0) {
            ?>
            <li class="<?php echo $menu->kode_form == @$form || ($key==0&&@$form=='')? "active" : "" ?>"><a data-toggle="tab" href="#tab<?php echo $menu->kode_form ?>"><i class="<?php echo $menu->form_icon ?>"></i> <?php echo $menu->form_name ?></a>
            </li>
            <?php
        }
    }
    ?>


</ul>
<div class="tab-content custom-menu-content">
    <?php
    foreach ($listmenu as $key=>$menu) {
        if (count($menu->submenu) > 0) {
            ?>
            <?php if (count($menu->submenu) > 0) { ?>
                <div id="tab<?php echo $menu->kode_form ?>" class="tab-pane <?php echo $menu->kode_form == @$form  || ($key==0&&@$form=='') ? "in active" : "" ?> notika-tab-menu-bg animated flipInX">
                    <ul class="notika-main-menu-dropdown">
                        <?php foreach ($menu->submenu as $key => $submenu) { ?>
                            <li class="<?php echo $submenu->kode_form==@$formsubmenu?"active":"" ?>"><a href="<?php echo base_url() . $submenu->form_url ?>"><?php echo $submenu->form_name ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <?php
        }
    }
    ?>


</div>