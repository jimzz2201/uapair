<div class="mobile-menu-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="mobile-menu">
                    <nav id="dropdown">
                        <ul class="mobile-menu-nav">
                            <?php
                            foreach ($listmenu as $menu) {
                                if (count($menu->submenu) > 0) {
                                    ?>
                                    <li class="treeview <?php echo $menu->urlclass ?>">
                                        <a data-toggle="collapse" data-target="#mob<?= $menu->kode_form ?>" href="<?php echo CheckEmpty($menu->form_url) ? 'javascript:;' : base_url() . $menu->form_url ?>"><?php echo $menu->form_name ?></a>
                                        <?php if (count($menu->submenu) > 0) { ?>
                                            <ul class="collapse dropdown-header-top" id="mob<?= $menu->kode_form ?>">
                                                <?php foreach ($menu->submenu as $key => $submenu) { ?>
                                                    <li class="<?php echo ($key == 0 ? 'litreeview' : '') . $submenu->urlclass ?>"><a href="<?php echo base_url() . $submenu->form_url ?>"><i class="fa fa-circle-o"></i><?php echo $submenu->form_name ?></a></li>
                                                        <?php } ?>
                                            </ul>
                                        <?php } ?>
                                    </li>
                                <?php }
                            }
                            ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
