<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_pegawai extends CI_Model
{

    public $table = 'uap_pegawai';
    public $id = 'id_pegawai';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;
    function __construct()
    {
        parent::__construct();
    }
    function AlreadyUse($id_user,$id_pegawai)
    {
        $this->db->from("uap_pegawai");
        $this->db->where("id_user",$id_user);
        if(!CheckEmpty($id_pegawai))
        {
            $this->db->where("id_pegawai !=",$id_pegawai);
        }
        $row=$this->db->get()->row();
        return $row!=null;
    }
    // datatables
    function GetDatapegawai() {
        $this->load->library('datatables');
        $this->datatables->select('id_pegawai,kode_pegawai,nama_pegawai,uap_pegawai.status,id_user,nama_cabang');
        $this->datatables->join('uap_cabang',"uap_cabang.id_cabang=uap_pegawai.id_cabang","left");
        $this->datatables->from('uap_pegawai');
        //add this line for join
        //$this->datatables->join('table2', 'uap_pegawai.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editpegawai($1);return false;"));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletepegawai($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_pegawai');
        return $this->datatables->generate();
    }

    // get all
    function GetOnePegawai($keyword, $type = 'id_pegawai') {
        $this->db->where($type, $keyword);
        $pegawai = $this->db->get($this->table)->row();
        return $pegawai;
    }

    function PegawaiManipulate($model) {
        try {
            
            $model['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
            $model['status'] = DefaultCurrencyDatabase($model['status']);
            $model['id_user'] = ForeignKeyFromDb($model['id_user']);

            if (CheckEmpty($model['id_pegawai'])) {
                $model['kode_pegawai'] = AutoIncrement($this->table, "S", "kode_pegawai", 4);
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                return array("st" => true, "msg" => "Pegawai successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_pegawai" => $model['id_pegawai']));
                return array("st" => true, "msg" => "Pegawai has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDropDownPegawai() {

        $where = [];
        $where['status'] = 1;
        $listreturn = GetTableData("uap_pegawai", 'id_pegawai', 'nama_pegawai', $where, 'obj');
        return $listreturn;
    }

    function PegawaiDelete($id_pegawai) {
        try {
            $this->db->delete($this->table, array('id_pegawai' => $id_pegawai));
        } catch (Exception $ex) {
            $model['updated_date'] = GetDateNow();
            $model['status'] = 2;
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array('id_pegawai' => $id_pegawai));
        }
        return array("st" => true, "msg" => "Pegawai has been deleted from database");
    }


}