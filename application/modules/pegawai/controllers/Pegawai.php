<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pegawai extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_pegawai');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module="K088";
        $header="K086";
        $title="Pegawai";
        CekModule($module);
        LoadTemplate(array("title"=>$title,"form"=>$header,"formsubmenu"=>$module), "pegawai/v_pegawai_index", $javascript);
        
    } 
    
    public function getdatapegawai() {
        header('Content-Type: application/json');
        echo $this->m_pegawai->GetDatapegawai();
    }
    public function get_one_pegawai() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_pegawai', 'pegawai', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_pegawai->GetOnePegawai($model["id_pegawai"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data barang tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function create_pegawai() 
    {
        $row = (object) array();
        $row->button = 'Add';
        $module = "";
        $header = "";
        CekModule($module);
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->title = 'Create Pegawai';
        $row = json_decode(json_encode($row), true);
        $this->load->model("cabang/m_cabang");
        $this->load->model("user/m_user");
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $row['list_user'] = $this->m_user->GetDropDownUser();
        $javascript = array();
        $this->load->view('pegawai/v_pegawai_manipulate', $row);
    }
    
    public function pegawai_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('nama_pegawai', 'nama pegawai', 'trim|required');
	$model=$this->input->post();
        if(!CheckEmpty($model['id_user'])&&$this->m_pegawai->AlreadyUse($model['id_user'],$model['id_pegawai']))
        {
            $message.="User sudah terassign di pegawai yang lain<br/>";
        }
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_pegawai->PegawaiManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_pegawai($id=0) 
    {
        $row = $this->m_pegawai->GetOnePegawai($id);
        
        if ($row) {
            $row->button='Update';
             $module="";
            $header="";
            CekModule($module);
            $row->form=$header;
            $row->formsubmenu = $module;
            $row->title = 'Update Pegawai';
            $row = json_decode(json_encode($row), true);
            $this->load->model("cabang/m_cabang");
            $this->load->model("user/m_user");
            $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
            $row['list_user'] = $this->m_user->GetDropDownUser();
            $javascript = array();
            $this->load->view('pegawai/v_pegawai_manipulate', $row);
        } else {
            SetMessageSession(0, "Pegawai cannot be found in database");
            redirect(site_url('pegawai'));
        }
    }
    
    public function pegawai_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_pegawai', 'Pegawai', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_pegawai->PegawaiDelete($model['id_pegawai']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Pegawai.php */