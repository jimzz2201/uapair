<div class="modal-header">
    Pegawai <?php echo @$button ?>
</div>
<div class="modal-body">
    <form id="frm_pegawai" class="form-horizontal form-groups-bordered validate" method="post">
        <input type="hidden" name="id_pegawai" value="<?php echo @$id_pegawai; ?>" /> 
        <div class="form-group">
            <?= form_label('Kode Pegawai', "txt_kode_pegawai", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text','readonly'=>'readonly',  'value' => @$kode_pegawai, 'class' => 'form-control', 'id' => 'txt_kode_pegawai', 'placeholder' => 'Kode Pegawai')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Nama Pegawai', "txt_nama_pegawai", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text', 'name' => 'nama_pegawai', 'value' => @$nama_pegawai, 'class' => 'form-control', 'id' => 'txt_nama_pegawai', 'placeholder' => 'Nama Pegawai')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Cabang', "txt_id_cabang", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
               <?= form_dropdown(array("selected" => @$id_cabang, "name" => "id_cabang"), DefaultEmptyDropdown($list_cabang, 'obj','Cabang'), @$id_cabang, array('class' => 'form-control', 'id' => 'dd_id_cabang')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('User', "txt_id_user", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                   <?= form_dropdown(array("selected" => @$id_user, "name" => "id_user"), DefaultEmptyDropdown($list_user, 'obj','User'), @$id_user, array('class' => 'form-control', 'id' => 'dd_id_user')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
            <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
        </div>

    </form>
</div>
<script>
    $("#frm_pegawai").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/pegawai/pegawai_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>