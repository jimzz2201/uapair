<div class="modal-header">
        Merek <?php echo @$button ?>
    </div>
<div class="modal-body">
<form id="frm_merek" class="form-horizontal form-groups-bordered validate" method="post">
	<input type="hidden" name="id_merek" value="<?php echo @$id_merek; ?>" /> 
	<div class="form-group">
		<?= form_label('Kode Merek', "txt_kode_merek", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'kode_merek', 'value' => @$kode_merek, 'class' => 'form-control', 'id' => 'txt_kode_merek', 'placeholder' => 'Kode Merek')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Nama Merek', "txt_nama_merek", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'nama_merek', 'value' => @$nama_merek, 'class' => 'form-control', 'id' => 'txt_nama_merek', 'placeholder' => 'Nama Merek')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Image Merek', "txt_image_merek", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'image_merek', 'value' => @$image_merek, 'class' => 'form-control', 'id' => 'txt_image_merek', 'placeholder' => 'Image Merek')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
		</div>
	</div>
	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
    </div>

</form>
</div>
<script>
$("#frm_merek").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/merek/merek_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>