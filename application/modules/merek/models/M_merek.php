<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_merek extends CI_Model {

    public $table = 'uap_merek';
    public $id = 'id_merek';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDatamerek() {
        $this->load->library('datatables');
        $this->datatables->select('id_merek,kode_merek,nama_merek,image_merek,status');
        $this->datatables->from('uap_merek');
        //add this line for join
        //$this->datatables->join('table2', 'uap_merek.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editmerek($1);return false;"));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletemerek($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_merek');
        return $this->datatables->generate();
    }

    // get all
    function GetOneMerek($keyword, $type = 'id_merek') {
        $this->db->where($type, $keyword);
        $merek = $this->db->get($this->table)->row();
        return $merek;
    }

    function MerekManipulate($model) {
        try {
            $model['status'] = DefaultCurrencyDatabase($model['status']);

            if (CheckEmpty($model['id_merek'])) {
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                return array("st" => true, "msg" => "Merek successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_merek" => $model['id_merek']));
                return array("st" => true, "msg" => "Merek has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    function GetDropDownMerek() {

        $where = [];
        $where['status'] = 1;
        $listreturn = GetTableData("uap_merek", 'id_merek', 'nama_merek', $where, 'obj');
        return $listreturn;
    }

    function MerekDelete($id_merek) {
        try {
            $this->db->delete($this->table, array('id_merek' => $id_merek));
        } catch (Exception $ex) {
            $model['updated_date'] = GetDateNow();
            $model['status'] = 2;
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array('id_merek' => $id_merek));
        }
        return array("st" => true, "msg" => "Merek has been deleted from database");
    }

}
