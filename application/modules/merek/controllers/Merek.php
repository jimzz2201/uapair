<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Merek extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_merek');
    }

    public function index()
    {
         $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K097";
        $header = "K001";
        $title = "Merek";
        CekModule($module);
        LoadTemplate(array("title" => $title, "form" => $header, "formsubmenu" => $module), "merek/v_merek_index", $javascript);
    } 
    
    public function getdatamerek() {
        header('Content-Type: application/json');
        echo $this->m_merek->GetDatamerek();
    }
    
    public function create_merek() 
    {
        $row=(object)array();
        $row->button='Add';
        $row=  json_decode(json_encode($row),true);
        $javascript = array();
        $this->load->view('merek/v_merek_manipulate', $row);       
    }
    
    public function merek_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('kode_merek', 'kode merek', 'trim|required');
	$this->form_validation->set_rules('nama_merek', 'nama merek', 'trim|required');
	  $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_merek->MerekManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_merek($id=0) 
    {
        $row = $this->m_merek->GetOneMerek($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();        $this->load->view('merek/v_merek_manipulate', $row);
        } else {
            SetMessageSession(0, "Merek cannot be found in database");
            redirect(site_url('merek'));
        }
    }
    
    public function merek_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_merek', 'Merek', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_merek->MerekDelete($model['id_merek']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Merek.php */

