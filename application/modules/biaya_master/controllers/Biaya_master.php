<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Biaya_master extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_biaya_master');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module="K030";
        $header="K026";
        $title="Biaya Master";
        CekModule($module);
        LoadTemplate(array("title"=>$title,"form"=>$header,"formsubmenu"=>$module), "biaya_master/v_biaya_master_index", $javascript);
        
    } 
    
    public function getdatabiaya_master() {
        header('Content-Type: application/json');
        echo $this->m_biaya_master->GetDatabiaya_master();
    }
    
    public function create_biaya_master() 
    {
        $row=(object)array();
        $row->button='Add';
        $module="K030";
        $header="K026";
        CekModule($module);
        $row->form=$header;
        $row->formsubmenu=$module;
        $row->default_penerimaan='2';
        $row->default_subject='2';
        $row->listbiayagroup=$this->m_biaya_master->GetDropDownGroup();
        $row->title='Create Biaya Master';
        $row=json_decode(json_encode($row),true);
        $javascript = array();        $this->load->view('biaya_master/v_biaya_master_manipulate', $row);       
    }
    
    public function biaya_master_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('kode_biaya_master', 'kode biaya master', 'trim|required');
	$this->form_validation->set_rules('nama_biaya_master', 'nama biaya master', 'trim|required');
	$model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_biaya_master->Biaya_masterManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_biaya_master($id=0) 
    {
        $row = $this->m_biaya_master->GetOneBiaya_master($id);
        
        if ($row) {
            $row->button='Update';
             $module="K030";
            $header="K026";
            CekModule($module);
            $row->form=$header;
            $row->formsubmenu=$module;
            $row->listbiayagroup=$this->m_biaya_master->GetDropDownGroup();
            $row->title='Update Biaya Master';
            $row = json_decode(json_encode($row), true);
            $javascript = array();        $this->load->view('biaya_master/v_biaya_master_manipulate', $row);
        } else {
            SetMessageSession(0, "Biaya_master cannot be found in database");
            redirect(site_url('biaya_master'));
        }
    }
    public function getdropdownbiayamaster()
    {
        $id_group=$this->input->post("id_biaya_group");
        echo json_encode(array("listbiayamaster"=>$this->m_biaya_master->GetDropDownBiayaMaster($id_group)));
    }
    public function biaya_master_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_biaya_master', 'Biaya_master', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_biaya_master->Biaya_masterDelete($model['id_biaya_master']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Biaya_master.php */