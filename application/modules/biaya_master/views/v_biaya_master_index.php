
<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Biaya master</h2>
                                <p>Biaya master | Data Master</p>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-3'>
                        <div class='breadcomb-report'>
                            <?php echo anchor("", '<i class="fa fa-plus"></i> Create', 'class="btn btn-success" id="btt_create"'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="">
    <div class="box box-default form-element-list">


        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</div>
<script type="text/javascript">
    var table;
    function deletebiaya_master(id_biaya_master) {


        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/biaya_master/biaya_master_delete',
                    dataType: 'json',
                    data: {
                        id_biaya_master: id_biaya_master
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });

    }
    function editbiaya_master(id_biaya_master) {

        $.ajax({url: baseurl + 'index.php/biaya_master/edit_biaya_master/' + id_biaya_master,
            success: function (data) {
                modalbootstrap(data, "Edit Biaya_master");
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }
    $(document).ready(function () {
        $("#btt_create").click(function () {
            $.ajax({url: baseurl + 'index.php/biaya_master/create_biaya_master',
                success: function (data) {
                    modalbootstrap(data);
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
        })
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": "biaya_master/getdatabiaya_master", "type": "POST"},
            columns: [
                {
                    data: "id_biaya_master",
                    title: "Kode",
                    orderable: false
                }
                , {data: "kode_biaya_master", orderable: false, title: "Kode"}
                , {data: "nama_biaya_master", orderable: false, title: "Nama Biaya Master"}
                , {data: "nama_biaya_group", orderable: false, title: "Group", width: "200px"}
                , {data: "default_penerimaan", orderable: false, title: "Posisi",
                    mRender: function (data, type, row) {
                        switch (data) {
                            case '0':
                                return "Pengeluaran";
                                break;
                            case '1':
                                return "Penerimaan";
                                break;
                            default:
                                return "Dua Duanya";

                        }
                    }}
                , {data: "default_subject", orderable: false, title: "Subject",
                    mRender: function (data, type, row) {
                        switch (data) {
                            case '0':
                                return "Kas";
                                break;
                            case '1':
                                return "Bank";
                                break;
                            default:
                                return "Dua Duanya";

                        }
                    }}
                , {data: "status", orderable: false, title: "Status",
                    mRender: function (data, type, row) {
                        return data == 1 ? "Active" : "Not Active";
                    }}
                ,
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
