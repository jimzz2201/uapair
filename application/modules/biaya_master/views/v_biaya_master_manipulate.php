<div class="modal-header">
    Biaya master <?php echo @$button ?>
</div>
<div class="modal-body">
    <form id="frm_biaya_master" class="form-horizontal form-groups-bordered validate" method="post">
        <input type="hidden" name="id_biaya_master" value="<?php echo @$id_biaya_master; ?>" /> 
        <div class="form-group">
            <?= form_label('Kode Biaya Master', "txt_kode_biaya_master", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text', 'name' => 'kode_biaya_master', 'value' => @$kode_biaya_master, 'class' => 'form-control', 'id' => 'txt_kode_biaya_master', 'placeholder' => 'Kode Biaya Master')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Nama Biaya Master', "txt_nama_biaya_master", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text', 'name' => 'nama_biaya_master', 'value' => @$nama_biaya_master, 'class' => 'form-control', 'id' => 'txt_nama_biaya_master', 'placeholder' => 'Nama Biaya Master')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Biaya Group', "txt_id_biaya_group", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array("selected" => @$id_biaya_group, "name" => "id_biaya_group"), DefaultEmptyDropdown($listbiayagroup, '', "Biaya Group"), @$id_biaya_group, array('class' => 'form-control', 'id' => 'id_biaya_group')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Posisi', "txt_default_penerimaan", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array("selected" => @$default_penerimaan, "name" => "default_penerimaan"), array('1' => 'Pengeluaran', '0' => 'Penerimaan', '2' => 'Dua Duanya'), @$default_penerimaan, array('class' => 'form-control', 'id' => 'default_penerimaan')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Subject', "txt_default_subject", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array("selected" => @$default_subject, "name" => "default_subject"), array('1' => 'Bank', '0' => 'Kas', '2' => 'Dua Duanya'), @$default_subject, array('class' => 'form-control', 'id' => 'status')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
            <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
        </div>

    </form>
</div>
<script>
    $("#frm_biaya_master").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/biaya_master/biaya_master_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>