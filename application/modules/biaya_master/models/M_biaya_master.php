<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_biaya_master extends CI_Model {

    public $table = 'uap_biaya_master';
    public $id = 'id_biaya_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDatabiaya_master() {
        $this->load->library('datatables');
        $this->datatables->select('id_biaya_master,kode_biaya_master,nama_biaya_group,nama_biaya_master,default_penerimaan,default_subject,uap_biaya_master.status');
        $this->datatables->from('uap_biaya_master');
        $this->datatables->join('uap_biaya_group', 'uap_biaya_group.id_biaya_group=uap_biaya_master.id_biaya_group', 'left');
        //add this line for join
        //$this->datatables->join('table2', 'uap_biaya_master.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editbiaya_master($1);return false;"));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletebiaya_master($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_biaya_master');
        return $this->datatables->generate();
    }

    // get all
    function GetOneBiaya_master($keyword, $type = 'id_biaya_master') {
        $this->db->where($type, $keyword);
        $biaya_master = $this->db->get($this->table)->row();
        return $biaya_master;
    }
    function GetDropDownGroup() {
        $where = [];
        $where['status'] = 1;
        $listreturn = GetTableData("uap_biaya_group", 'id_biaya_group', 'nama_biaya_group', $where, 'obj');
        return $listreturn;
    }
    function GetDropDownBiayaMaster($id_group=0) {
        $where = [];
        if(!CheckEmpty($id_group))
        {
            $where['id_biaya_group']=$id_group;
        }
        $where['status'] = 1;
        $listreturn = GetTableData("uap_biaya_master", 'id_biaya_master', 'nama_biaya_master', $where, 'obj');
        return $listreturn;
    }

    function Biaya_masterManipulate($model) {
        try {
            $model['nama_biaya_master'] = DefaultCurrencyDatabase($model['nama_biaya_master']);
            $model['id_biaya_group'] = ForeignKeyFromDb($model['id_biaya_group']);
            $model['default_penerimaan'] = DefaultCurrencyDatabase($model['default_penerimaan']);
            $model['default_subject'] = DefaultCurrencyDatabase($model['default_subject']);
            $model['status'] = DefaultCurrencyDatabase($model['status']);

            if (CheckEmpty($model['id_biaya_master'])) {
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                return array("st" => true, "msg" => "Biaya_master successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_biaya_master" => $model['id_biaya_master']));
                return array("st" => true, "msg" => "Biaya_master has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function Biaya_masterDelete($id_biaya_master) {
        try {
            $this->db->delete($this->table, array('id_biaya_master' => $id_biaya_master));
        } catch (Exception $ex) {
            $model['updated_date'] = GetDateNow();
            $model['status'] = 2;
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array('id_biaya_master' => $id_biaya_master));
        }
        return array("st" => true, "msg" => "Biaya_master has been deleted from database");
    }

}
