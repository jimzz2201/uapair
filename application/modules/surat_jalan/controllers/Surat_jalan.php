<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Surat_jalan extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_surat_jalan');
    }
    
    public function searchsurat_jalan()
    {
        $model=$this->input->post();
        $this->load->view("v_surat_jalan_order_search",$model);
    }
    public function surat_jalan_batal(){
        $message = '';
        $this->form_validation->set_rules('id_surat_jalan_master', 'Surat Jalan', 'required');
        $this->form_validation->set_rules('message', 'Message', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_surat_jalan->BatalSuratJalan($model['id_surat_jalan_master'],$model['message']);
        }

        echo json_encode($result);
    }
     public function viewsuratjalan()
    {
        $id=$this->input->post("id_surat_jalan_master");
        $surat_jalan = $this->m_surat_jalan->GetOneSuratJalan($id, true);
        $this->load->model("sales_order/m_sales_order");
        $salesorder=$this->m_sales_order->GetNoSalesOrder($surat_jalan->id_sales_order_master);
        $listheaderso=array(array("tanggal_so"=>$salesorder->tanggal_so,"nomor_so"=>$salesorder->nomor_so));
        $listheaderpenjualan=array(array("tanggal_penjualan"=>$salesorder->tanggal_penjualan,"nomor_penjualan"=>$salesorder->nomor_penjualan));
        $surat_jalan->listheaderso=$listheaderso;
        $surat_jalan->listheaderpenjualan=$listheaderpenjualan;
        $this->load->view("surat_jalan/v_surat_jalan_view",array("row"=>$surat_jalan));
    }
    public function detailsales_order($id) {
        $this->load->model('sales_order/m_sales_order');
        $model = $this->m_sales_order->GetOneSales_Order($id, true,true);

        $this->load->view("v_surat_jalan_order_search_detail", $model);
    }
    public function GetOneSuratJalan()
    {
        $model = $this->input->post();

        $this->form_validation->set_rules('id_surat_jalan_master', 'Surat Jalan', 'trim|required');
        $message = "";
        

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {

            $row = $this->m_surat_jalan->GetOneSuratJalan($model['id_surat_jalan_master'],true,true);
         
            $html=$this->load->view("retur/v_retur_jual_content",$row,true);
            echo json_encode(array("st"=>$row!=null,"obj"=>$row,"html"=>$html));
        }
    }
    public function editsurat_jalan($id,$nomorinvoice)
    {
        $row = (object) array();
        
        $module = "K110";
        $header = "K021";
        CekModule($module);
        $surat_jalan=$this->m_surat_jalan->GetOneSuratJalan($id,true);
        if($surat_jalan==null||$surat_jalan->nomor_master!= str_replace("-", "/", $nomorinvoice))
        {
            SetMessageSession(2, "Anda tidak memiliki akses untuk menuju link tersebut");
            redirect(base_url() . 'index.php/main/message');
        }
        else {
            $row=$surat_jalan;
        }
        $this->load->model("sales_order/m_sales_order");
        $salesorder=$this->m_sales_order->GetNoSalesOrder($surat_jalan->id_sales_order_master);
        $listheaderso=array(array("tanggal_so"=>$salesorder->tanggal_so,"nomor_so"=>$salesorder->nomor_so));
        $listheaderpenjualan=array(array("tanggal_penjualan"=>$salesorder->tanggal_penjualan,"nomor_penjualan"=>$salesorder->nomor_penjualan));
        $row->listheaderso=$listheaderso;
        $row->listheaderpenjualan=$listheaderpenjualan;
        $row->button = 'Edit';
        $this->load->model("customer/m_customer");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = "K022";
        $row->list_customer = $this->m_customer->GetDropDownCustomer();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $jenis_pembayaran=array();
        $jenis_pembayaran[1]="Tunai";
        $jenis_pembayaran[2]="Kredit";
        $row->list_type_pembayaran  =$jenis_pembayaran;
        $list_type_ppn=array();
        $list_type_ppn[1]="Include";
        $list_type_ppn[2]="Exclude";
        $row->list_type_ppn  =$list_type_ppn;
        
        $list_type_pembulatan=array();
        $list_type_pembulatan[1]="Sebelum PPN";
        $list_type_pembulatan[2]="Sesudah PPN";
        $row->list_type_pembulatan  =$list_type_pembulatan;
        $row->title = 'Edit Surat Jalan';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';
        
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'surat_jalan/v_surat_jalan_manipulate', $javascript, $css);
    }
    
    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $module = "K023";
        $header = "K021";
        $title = "Surat Jalan";
        CekModule($module);
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_status']=array("2"=>"Batal","1"=>"Non Batal");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $model['list_gudang']=$this->m_gudang->GetDropDownGudang();
        $this->load->model("customer/m_customer");
        $model['list_customer']=$this->m_customer->GetDropDownCustomer();
        LoadTemplate($model, "surat_jalan/v_surat_jalan_index", $javascript,$css);
    }
    
    public function surat_jalan_manipulate(){
        $model = $this->input->post();
       
        $this->form_validation->set_rules('id_gudang', 'Gudang', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        $listid_sales_order_detail=[];
        $listqty_pengiriman=[];
        $message="";
        if(CheckArray($model, 'id_sales_order_detail'))
        {
            $listid_sales_order_detail=$model['id_sales_order_detail'];
        }
        else
        {
            $message.="Detail Surat Jalan Required<br/>";
        }
        if(CheckArray($model, 'qty_pengiriman'))
        {
            $listqty_pengiriman=$model['qty_pengiriman'];
        }
        else
        {
             $message.="Qty Pengiriman Required<br/>";
        }
        
        if(count($listqty_pengiriman)==0)
        {
             $message.="Detail Pengiriman Required<br/>";
        }
        else if(count($listqty_pengiriman)!=count($listid_sales_order_detail))
        {
            $message.="Something error occured<br/>";
        }
        else{
            $issimpan=false;
            foreach($listqty_pengiriman as $satuanpenerimaan)
            {
                if(!CheckEmpty($satuanpenerimaan))
                {
                    $issimpan=true;
                }
            }
            if(!$issimpan)
            {
                $message.="Quantity pengiriman tidak boleh kosong<br/>";
            }
        }
        
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {

            $data = $this->m_surat_jalan->SuratJalanManipulate($model);
            echo json_encode($data);
        }
        
        
    }
    
    
    
    
    
    public function getdatasurat_jalan() {
         header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_surat_jalan->GetDatasurat_jalan($params);
    }
    
    public function searchsales_order()
    {
        $model=$this->input->post();
        $model['tanggal'] = GetDateNow();
        $model['tanggal_awal']=TambahTanggal($model['tanggal'],"-7 Days");
        $this->load->view("v_surat_jalan_order_search",$model);
    }
    public function create() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K022";
        $header = "K021";
        CekModule($module);
        $this->load->model("customer/m_customer");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->listdetail=[];
        $row->listheaderso=[];
        $row->listheaderpenjualan=[];
        $row->list_customer=$this->m_customer->GetDropDownCustomer();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $jenis_pembayaran=array();
        $jenis_pembayaran[1]="Tunai";
        $jenis_pembayaran[2]="Kredit";
        $row->list_type_pembayaran  =$jenis_pembayaran;
        $list_type_ppn=array();
        $list_type_ppn[1]="Include";
        $list_type_ppn[2]="Exclude";
        $row->list_type_ppn  =$list_type_ppn;
        $list_type_pembulatan=array();
        $list_type_pembulatan[1]="Sebelum PPN";
        $list_type_pembulatan[2]="Sesudah PPN";
        $row->list_type_pembulatan  =$list_type_pembulatan;
        $row->tanggal = GetDateNow();
        $row->jth_tempo = date('Y-m-d', strtotime(GetDateNow() . "+1 months") );
        $row->title = 'Create Surat Jalan';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';
        
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'surat_jalan/v_surat_jalan_manipulate', $javascript, $css);
    }
    
    
    public function get_one_surat_jalan() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_surat_jalan', 'nama surat_jalan', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_surat_jalan->GetOneBarang($model["id_surat_jalan"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data surat_jalan tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    

}

/* End of file Barang.php */