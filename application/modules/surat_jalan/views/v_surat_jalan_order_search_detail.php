<input  type="hidden" name="id_sales_order_master" id="txt_id_sales_order_master_obj" value="<?php echo $id_sales_order_master ?>"   />
<input  type="hidden" name="nomor_master" id="txt_nomor_master_obj" value="<?php echo $nomor_master ?>"  />
<input  type="hidden" name="id_customer" id="txt_id_customer_obj" value="<?php echo $id_customer ?>"  />
<input  type="hidden" name="id_cabang" id="txt_id_cabang_obj" value="<?php echo $id_cabang ?>"  />
<input  type="hidden" name="id_gudang" id="txt_id_gudang_obj" value="<?php echo $id_gudang ?>"  />
<input  type="hidden" name="tanggal" id="txt_tanggal_obj" value="<?php echo $tanggal ?>"  />
<input  type="hidden" name="id_pegawai" id="txt_id_pegawai_obj" value="<?php echo $id_pegawai ?>"  />
<input  type="hidden" name="type_ppn" id="txt_type_ppn_obj" value="<?php echo $type_ppn ?>"  />
<input  type="hidden" name="ppn" id="txt_ppn_obj" value="<?php echo $ppn ?>"  />
<input  type="hidden" name="disc_pembulatan" id="txt_disc_pembulatan_obj" value="<?php echo $disc_pembulatan ?>"  />
<input  type="hidden" name="jumlah_bayar" id="txt_jumlah_bayar_obj" value="<?php echo $jumlah_bayar ?>"  />
<input  type="hidden" name="alamat" id="txt_alamat_obj" value="<?php echo $alamat ?>"  />
<input  type="hidden" name="contact_person" id="txt_contact_person_obj" value="<?php echo $contact_person ?>"  />
<input  type="hidden" name="no_telp" id="txt_no_telp_obj" value="<?php echo $no_telp ?>"  
<input  type="hidden" name="type_pembayaran" id="txt_type_pembayaran_obj" value="<?php echo $type_pembayaran ?>"  />
<table class="table table-striped table-bordered table-hover dataTable no-footer">
    <thead>
        <tr>
            <th><input type="checkbox" checked="" class="i-checks" id="cb_all" /></th>
            <th>SKU Barang</th>
            <th>Nama Barang</th>
            <th width="100px">Stock</th>
            <th width="100px">Qty</th>
            <th width="100px">Dikirim</th>
            <th width="100px">Pengiriman   </th>
            <th>Satuan</th>
        </tr>
    </thead>
    <?php foreach (@$listdetail as $key => $detail) {
        ?>
        <tr>
            <td align="center">
                <input  checked="" type="checkbox" id="cb_det_<?php echo $key ?>" class="i-checks checkdetail"  />
                <?= form_input(array('type' => 'hidden', "id" => "txt_id_sales_order_detail_" . $key, 'value' => $detail->id_sales_order_detail)); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_stock_" . $key, 'value' => DefaultCurrency($detail->stock))); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_dikirim_" . $key, 'value' => DefaultCurrency($detail->qty_dikirim))); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_nama_barang_" . $key, 'value' => $detail->nama_barang)); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_sku_" . $key, 'value' => $detail->sku)); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_id_barang_" . $key, 'value' => $detail->id_barang)); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_hidden_qty_" . $key, 'value' => $detail->qty)); ?>
            </td>
            <td><?= $detail->sku ?></td>
            <td><?= $detail->nama_barang ?></td>
            <td style="width:100px"><?= DefaultCurrency($detail->stock) ?></td>
            <td style="width:100px"><?= DefaultCurrency($detail->qty) ?></td>
            <td style="width:100px"><?= DefaultCurrency($detail->qty_dikirim) ?></td>
            <td style="width:100px"><?= form_input(array('type' => 'text', 'name' => 'qty[]', 'value' => DefaultCurrency($detail->qty), 'class' => 'form-control qty', 'id' => 'txt_qty_' . $key, 'placeholder' => 'Qty', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'style' => "padding:2px 12px")); ?></td>
            <td><?= $detail->nama_satuan ?></td>
        </tr>

    <?php } ?>
</table>

<br/>
<button type="button" class="btn btn-primary btn-block" id="btt_input_detail" >Input Detail</button>
<script>
    $(document).ready(function () {

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    })
    $("#btt_clear_pembelian").click(function () {
        listheader = [];
        dataset = [];
        RefreshGrid();
        $(".kolomheaderpo").empty();
    })
    $("#btt_input_detail").click(function () {
        var isclosed = false;

        var res = alasql('SELECT * FROM ? where id_sales_order_master=\'' + $("#txt_id_sales_order_master").val() + '\'', [listheader]);
        if (res.length > 0)
        {
            modaldialogerror("Data SO Sudah ada dalam list pembelian");
        } else
        {
            $.each($(".qty"), function (key, value) {
                var qtyid = value.id;
                var id = qtyid.replace("txt_qty_", "");
                if (value.value > 0)
                {
                    isclosed = true;
                    var qty = Number($("#txt_qty_" + id).val().replace(/[^0-9\.]+/g, ""));
                    var detail = {};
                    detail.id_sales_order_detail = $("#txt_id_sales_order_detail_" + id).val();
                    detail.id_surat_jalan_detail = $("#txt_id_surat_jalan_detail_" + id).val();
                    detail.id_barang = $("#txt_id_barang_" + id).val();
                    detail.nama_barang = $("#txt_nama_barang_" + id).val();
                    detail.sku = $("#txt_sku_" + id).val();
                    detail.qty_pengiriman = qty;
                    detail.qty = $("#txt_hidden_qty_" + id).val();;
                    detail.stock =  $("#txt_stock_" + id).val();
                    detail.qty_dikirim =  $("#txt_dikirim_" + id).val();
                    dataset.push(detail);
                    RefreshGrid();
                }
            });
            if (isclosed)
            {
                var header = {};
                header.id_sales_order_master = $("#txt_id_sales_order_master_obj").val();
                header.nomor_master = $("#txt_nomor_master").val();
                header.tgl = $("#txt_tgl").val();
                $("#txt_alamat").val($("#txt_alamat_obj").val());
                $("#txt_contact_person").val($("#txt_contact_person_obj").val());
                $("#txt_no_telp").val($("#txt_no_telp_obj").val());
                $("#txt_id_sales_order_master").val($("#txt_id_sales_order_master_obj").val());
                if ($("#txt_id_customer_obj").val() != "")
                {
                    $("#dd_id_customer").val($("#txt_id_customer_obj").val());
                    $("#dd_id_customer").trigger("change");
                    $("#dd_id_customer").attr("disabled", "disabled");
                }
                if ($("#txt_id_cabang_obj").val() != "")
                {
                    $("#dd_id_cabang").val($("#txt_id_cabang_obj").val());
                    $("#dd_id_cabang").trigger("change");
                    $("#dd_id_cabang").attr("disabled", "disabled");
                }
                if ($("#txt_id_gudang_obj").val() != "")
                {
                    $("#dd_id_gudang").val($("#txt_id_gudang_obj").val());
                    $("#dd_id_gudang").trigger("change");
                    
                }
                $("#btt_search_sales_order").css("display", "none");
                
                  $(".kolomheaderpo").append("Nomor SO : " + $("#txt_tanggal_obj").val() + " == " + $("#txt_nomor_master_obj").val() + "<br/>");
                 listheader.push(header);
                closemodalboostrap();
            } else
            {
                modaldialogerror("Quantity detail pembelian harus lebih besar dari 1");
            }
        }

    })
    $("#cb_all").on("ifChanged", function (e) {
        if (e.target.checked)
        {
            $('.checkdetail').iCheck('check');
        } else
        {
            $('.checkdetail').iCheck('uncheck');
        }
    })
    function ChangeDet(checkid) {
        var id = checkid.replace("cb_det_", "");
        if ($("#" + checkid).is(":checked"))
        {
            $("#txt_qty_" + id).removeAttr("readonly", "readonly");
            $("#txt_qty_" + id).val(Comma($("#txt_hidden_qty_" + id).val()));
        } else
        {
            $("#txt_qty_" + id).attr("readonly", "readonly");
            $("#txt_qty_" + id).val(0);
        }
    }
    $(".checkdetail").on("ifChanged", function (e) {
        ChangeDet(e.target.id);
        if (!e.target.checked)
        {
            $('#cb_all').prop('checked', false).iCheck('update');
        }
    })
</script>