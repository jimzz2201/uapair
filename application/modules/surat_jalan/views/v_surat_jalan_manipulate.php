<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Surat Jalan </h2>
                                <p><?= $button ?> Surat Jalan  | Surat Jalan </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                            <div class="form-group" >
                                <div class="row " style="margin-bottom:20px;">
                                    <div class="col-sm-12 kolomheaderpo">
                                        <?php foreach($listheaderso as $headersatuan){
                                               echo "Nomor SO : " . DefaultDatePicker($headersatuan['tanggal_so']) . " == " . $headersatuan['nomor_so'] . "<br/>";
                                        }?>
                                         <?php foreach($listheaderpenjualan as $headersatuan){
                                                if(!CheckEmpty($headersatuan['nomor_penjualan']))
                                                echo "Nomor Penjualan : " . DefaultDatePicker($headersatuan['tanggal_penjualan']) . " == " . $headersatuan['nomor_penjualan'] . "<br/>";
                                        }?>
                                    </div>

                                </div>
                                <?php if (CheckEmpty(@$id_surat_jalan_master)) { ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <button type="button" class="btn btn-primary btn-block" id="btt_search_sales_order" >Cari Sales Order</button>
                                        </div>
                                        <div class="col-sm-6">
                                            <button type="button" class="btn btn-danger btn-block" id="btt_clear_pembelian" >Clear Data</button>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <form id="frm_penjualan" class="form-horizontal form-groups-bordered validate" method="post">

                                <input type="hidden" name="id_surat_jalan_master" value="<?php echo @$id_surat_jalan_master; ?>" /> 
                                <input type="hidden" id="txt_id_sales_order_master" name="id_sales_order_master" value="<?php echo @$id_sales_order_master; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Customer', "txt_customer", array("class" => 'col-sm-2 control-label')); ?>
                                    <?php
                                    $listcustomer = DefaultEmptyDropdown(@$list_customer, "", "Customer");
                                    if (!CheckEmpty(@$id_customer)) {
                                        $textcustomer = @$list_customer[$id_customer];
                                        @$list_customer = [];
                                        @$list_customer[@$id_customer] = $textcustomer;
                                    }
                                    ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_customer"), @$list_customer, @$id_customer, array('class' => 'form-control', 'id' => 'dd_id_customer')); ?>
                                    </div>    

                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <?php
                                    $listcabang = DefaultEmptyDropdown(@$list_cabang, "", "Cabang");
                                    if (!CheckEmpty(@$id_cabang)) {
                                        $textcabang = $listcabang[@$id_cabang];
                                        $listcabang = [];
                                        $listcabang[@$id_cabang] = $textcabang;
                                    }
                                    ?>
                                    <?= form_label('Nama Cabang', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_cabang"), $listcabang, @$id_cabang, array('class' => 'form-control', 'id' => 'dd_id_cabang')); ?>
                                    </div>
                                    <?php
                                    $listgudang = DefaultEmptyDropdown(@$list_gudang, "", "Gudang");
                                   
                                    if (!CheckEmpty(@$id_gudang)) {
                                        $textgudang = $listgudang[@$id_gudang];
                                        $listgudang = [];
                                        $listgudang[$id_gudang] = $textgudang;
                                    }
                                    ?>
                                    <?= form_label('Gudang', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("name" => "id_gudang"), $listgudang, @$id_gudang, array('class' => 'form-control', 'id' => 'dd_id_gudang')); ?>
                                    </div>

                                </div>
                                 <div class="form-group">
                                    <?= form_label('Kepada', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', "rows" => 4, 'value' => @$contact_person, 'name' => 'contact_person', 'class' => 'form-control', 'id' => 'txt_contact_person', 'placeholder' => 'Kepada')); ?>
                                    </div>
                                     <?= form_label('No Telp', "txt_keterangan", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', "rows" => 5, 'value' => @$no_telp, 'name' => 'no_telp', 'class' => 'form-control', 'id' => 'txt_no_telp', 'placeholder' => 'No Telp')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Alamat', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$alamat, 'name' => 'alamat', 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat')); ?>
                                    </div>

                                </div> 
                                
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>

                                </div>  
                                <hr/>
                                
                                <div class="box box-default">


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet-body form">
                                                <table class="table table-striped table-bordered table-hover" id="mytable">

                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <hr/>
                                
                                <div class="form-group" style="margin-top:50px">
                                    <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>


    var table;
    var dataset = <?php echo json_encode(@$listdetail) ?>;
    var listheader = <?php echo json_encode(@$listheaderso) ?>;
    function deleterow(indrow) {
        dataset.splice(indrow, 1);
        RefreshGrid();
    }
    function RefreshGrid()
    {
        table.fnClearTable();
        if (dataset.length > 0)
        {
            table.fnAddData(dataset);
        }

    }
    $(document).ready(function () {
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
       table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            scrollX: false,
            data: dataset,
            columns: [
                {
                    data: "id_sales_order_detail",
                    title: "Kode",
                    width:"50px",
                    orderable: false
                }
                , {data: "sku", orderable: false, title: "SKU"}
                , {data: "nama_barang", orderable: false, title: "Nama","width":"500px"}
                , {data: "stock", orderable: false, title: "Stock" ,
                    mRender: function (data, type, row, iDisplayIndex) {
                        return Comma(data)
                    }
                }
                , {data: "qty", orderable: false, title: "Quantity" ,
                    mRender: function (data, type, row, iDisplayIndex) {
                        return Comma(data)
                    }
                }
                , {data: "qty_dikirim", orderable: false, title: "Dikirim" ,
                    mRender: function (data, type, row, iDisplayIndex) {
                        return Comma(data)
                    }
                }
                , {data: "qty_pengiriman", orderable: false, title: "Pengiriman",
                        mRender: function (data, type, row, iDisplayIndex) {
                        return "<input type='hidden' name='id_sales_order_detail[]' value='"+row['id_sales_order_detail']+"' /><input type='text' onkeyup ='javascript:this.value=Comma(this.value);' style='padding:2px 10px' onkeypress='return isNumberKey(event);' name='qty_pengiriman[]' value='"+Comma(row['qty_pengiriman'])+"' />";
                        }
                    }
                , {data: "id_sales_order_detail",
                    mRender: function (data, type, row, iDisplayIndex) {
                        return "<button onclick='deleterow(" + iDisplayIndex.row + ")' type=\"button\" data-id='" + iDisplayIndex.row + "' class=\"btn-xs btn-danger waves-effect btn-block\" >Delete</button>";
                    }
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

            },
            initComplete: function () {
               
            }
        });



    })
    function RefreshPanelGrid()
    {
        if ($("#dd_id_cabang").val() == "0" || $("#dd_id_gudang").val() == "0")
        {
            $("#frm_penjualan button").attr("disabled", "disabled");
        } 
        else
        {
            $("#frm_penjualan button").removeAttr("disabled");
        }
    }

    $(document).ready(function () {
        $("#txt_tanggal").datepicker();
        $("#dd_id_cabang , #dd_id_gudang ").change(function () {
            RefreshPanelGrid();
        })
        RefreshPanelGrid();

    })
    $("#btt_clear_pembelian").click(function () {
        listheader = [];
        dataset = [];
        RefreshPanelGrid();
        $(".kolomheaderpo").empty();
        $("#dd_id_supplier").removeAttr("disabled");
        $("#dd_id_cabang").removeAttr("disabled");
        $("#dd_id_gudang").removeAttr("disabled");
        $("#btt_search_sales_order").css("display", "block");
        $("#bodytable").html("<tr><td align=\"center\" colspan=\"6\">No Data Display</td></tr>");
    })
    $("#btt_search_sales_order").click(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/surat_jalan/searchsales_order',
            data: $("#frm_penjualan").serialize() + '&' + $.param({"dataset": dataset}),
            success: function (data) {
                modalbootstrap(data, "Search PO", "80%");

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });

    })
    $("#frm_penjualan").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data penjualan berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/surat_jalan/surat_jalan_manipulate',
                    dataType: 'json',
                    data: $("#frm_penjualan").serialize() + '&' + $.param({"dataset": dataset, "listheader": listheader}),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/surat_jalan";
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;


    })
</script>