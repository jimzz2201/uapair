<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_surat_jalan extends CI_Model {

    public $table = 'uap_surat_jalan';
    public $id = 'id_surat_jalan_po_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }
    function BatalSuratJalan($id_surat_jalan_master,$alasan){
        $message="";
        $this->load->model("barang/m_barang");
        $this->db->from("uap_surat_jalan_master");
        $this->db->where(array("id_surat_jalan_master"=>$id_surat_jalan_master));
        $suratjalan=$this->db->get()->row();
        if($suratjalan!=null)
        {
            if($suratjalan->status=="2")
            {
                 $message.="Surat Jalan sudah dibatalkan sebelumnya<br/>";
            }
        }
        else {
             $message.="Surat Jalan tidak terdaftar dalam database<br/>";
        }
        
        
        
        
        if($message=="")
        {
             $this->db->trans_rollback();
            $this->db->trans_begin();
            $this->db->from("uap_surat_jalan_detail");
            $this->db->where(array("id_surat_jalan_master" => $id_surat_jalan_master));
            $listdetailsuratjalan = $this->db->get()->result();

            $barangsync = [];
            foreach ($listdetailsuratjalan as $detailsuratjalan) {
                if (!in_array($detailsuratjalan->id_barang, $barangsync)) {
                    array_push($barangsync, $detailsuratjalan->id_barang);
                }
                $id_barang_history_terakhir = $detailsuratjalan->id_barang_history;
                $baranghistory = $this->m_barang->GetOneHistoryItem($id_barang_history_terakhir);
                $baranghistory->stock_sisa += $detailsuratjalan->qty;
                $this->db->update("uap_barang_history", $baranghistory, array("id_barang_history" => $id_barang_history_terakhir));
                $this->db->update("uap_barang_jurnal", array("debet" => 0, "kredit" => 0, "description" => $suratjalan->keterangan . ' | Batal ' . $alasan), array("reference_id" => $detailsuratjalan->id_surat_jalan_detail, "dokumen" => $suratjalan->nomor_master));
                $this->db->from("uap_barang_jurnal");
                 $this->db->where(array("reference_id" => $detailsuratjalan->id_surat_jalan_detail, "dokumen" => $suratjalan->nomor_master));
                 $row=$this->db->get()->row();
                 
                 
                
            }
            
                
           
            foreach ($barangsync as $bar) {
                $this->m_barang->SyncronizeHargaAverage($bar, $suratjalan->id_gudang, $suratjalan->tanggal);
              
            }
           
            $this->db->update("uap_surat_jalan_master", array("status" => 2, "keterangan" => $suratjalan->keterangan . ' | Batal ' . $alasan), array("id_surat_jalan_master" => $id_surat_jalan_master));
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
               
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data Surat Jalan Sudah di" . (CheckEmpty(@$model['id_surat_jalan_master']) ? "masukkan" : "update") . " ke dalam database");
               
               
            }
        }
        
        
        return array("st"=>$message=="","msg"=>$message==""?"Data berhasil dibatalkan":$message);
        
        
    }

    public function UpdateStatusSalesOrder($id) {
        $this->load->model("sales_order/m_sales_order");
        $listdetail = $this->m_sales_order->GetDetail($id, true);
        $isfull = true;
        foreach ($listdetail as $detail) {

            if ($detail->qtypenerimaan < $detail->qty) {
                $isfull = false;
            }
        }

        if ($isfull) {
            $this->db->from("uap_surat_jalan_master");
            $this->db->where(array("id_sales_order_master" => $id, "status !=" => 2));
            $this->db->order_by("tanggal", "desc");
            $row = $this->db->get()->row();

            if ($row != null) {
                $this->db->update("uap_sales_order_master", array("tgl_pengiriman" => $row->tanggal), array("id_sales_order_master" => $id));
            }
        } else {
            $this->db->update("uap_sales_order_master", array("tgl_pengiriman" => null), array("id_sales_order_master" => $id));
        }
    }

    function GetDetailSuratJalan($id, $id_surat_jalan_master = 0, $id_gudang = 0) {
        if(CheckEmpty($id_gudang))
        {
            $id_gudang=0;
        }
        $this->load->model("retur/m_retur_jual");
        $this->db->from("uap_sales_order_detail");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_sales_order_detail.id_barang");
        $this->db->join("uap_surat_jalan_detail", "uap_surat_jalan_detail.id_sales_order_detail=uap_sales_order_detail.id_sales_order_detail and id_surat_jalan_master=" . $id_surat_jalan_master, "left");
        $this->db->join("uap_barang_gudang", "uap_barang.id_barang=uap_barang_gudang.id_barang and uap_barang_gudang.id_gudang=" . $id_gudang, "left");
        $this->db->select("sku_barang as sku,uap_sales_order_detail.*,sum(uap_surat_jalan_detail.qty) as qty_pengiriman,uap_barang_gudang.stock");
        $this->db->group_by("uap_sales_order_detail.id_sales_order_detail");
        $this->db->where(array("uap_sales_order_detail.id_sales_order_master" => $id));

        $listdetail = $this->db->get()->result();
      
        foreach ($listdetail as $detail) {
            $detail->qty_dikirim = $this->GetJumlahPengiriman($detail->id_sales_order_detail);
            $detail->jumlahdiretur =$this->m_retur_jual->GetJumlahRetur($detail->id_sales_order_detail,"retur");
            if ($detail->qty_pengiriman > 0) {
                $detail->qty_dikirim -= $detail->qty_pengiriman;
                $detail->stock += $detail->qty_pengiriman;
                $detail->qty_retur=0;
            }
        }
        return $listdetail;
    }

    function GetOneSuratJalan($id, $isfull = false) {
        $this->db->from("uap_surat_jalan_master");
        $this->db->where(array("id_surat_jalan_master" => $id));
        if ($isfull) {
            $this->db->join("uap_customer", "uap_customer.id_customer=uap_surat_jalan_master.id_customer", "left");
            $this->db->join("uap_gudang", "uap_gudang.id_gudang=uap_surat_jalan_master.id_gudang", "left");
            $this->db->join("uap_cabang", "uap_cabang.id_cabang=uap_surat_jalan_master.id_cabang", "left");
            $this->db->select("uap_sales_order_master.ppn,uap_sales_order_master.type_pembulatan,uap_sales_order_master.type_ppn,uap_sales_order_master.tanggal as tanggal_so,uap_sales_order_master.nomor_master as nomor_so,uap_penjualan_master.tanggal as tanggal_invoice,uap_penjualan_master.nomor_master as nomor_invoice,uap_surat_jalan_master.*,uap_customer.nama_customer,uap_cabang.nama_cabang,uap_gudang.nama_gudang");
            $this->db->join("uap_sales_order_master","uap_sales_order_master.id_sales_order_master=uap_surat_jalan_master.id_sales_order_master","left");
            $this->db->join("uap_penjualan_master","uap_penjualan_master.id_sales_order_master=uap_sales_order_master.id_sales_order_master","left");
        }
        $row = $this->db->get()->row();
        if ($isfull) {

            $row->listdetail = $this->GetDetailSuratJalan($row->id_sales_order_master, $id, $row->id_gudang);
        }
        return $row;
    }

    function GetJumlahPengiriman($id, $jenis = "sales_order") {
        $this->db->from("uap_surat_jalan_detail");
        $this->db->join("uap_surat_jalan_master", "uap_surat_jalan_detail.id_surat_jalan_master=uap_surat_jalan_master.id_surat_jalan_master", "left");
        if ($jenis == "sales_order")
            $this->db->where(array("uap_surat_jalan_detail.id_sales_order_detail" => $id, "uap_surat_jalan_master.status !=" => 2));
        else
            $this->db->where(array("uap_surat_jalan_detail.id_surat_jalan_detail" => $id, "uap_surat_jalan_master.status !=" => 2));
        $this->db->select("sum(qty) as qtypengiriman");
        $qtyditerima = $this->db->get()->row();
        return CheckEmpty(@$qtyditerima->qtypengiriman) ? 0 : $qtyditerima->qtypengiriman;
    }

    function GetDetailSuratJalanSummary($id) {
        $this->db->from("uap_sales_order_detail");
        $this->db->where(array("id_sales_order_detail" => $id));
        $row = $this->db->get()->row();
        if ($row) {
            $row->qtypengiriman = $this->GetJumlahPengiriman($id);
        }

        return $row;
    }

    function SuratJalanManipulate($model) {
        try {
            $message = "";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            $this->load->model("barang/m_barang");
            $this->load->model("cabang/m_cabang");
            $this->load->model("customer/m_customer");
            $this->load->model("sales_order/m_sales_order");

            $salesorder = $this->m_sales_order->GetOneSales_Order($model['id_sales_order_master']);
            $pengiriman['tanggal'] = DefaultTanggalDatabase($model['tanggal']);
            $pengiriman['id_customer'] = $salesorder->id_customer;
            $pengiriman['keterangan'] = $model['keterangan'];
            $pengiriman['contact_person'] = $model['contact_person'];
            $pengiriman['no_telp'] = $model['no_telp'];
            $pengiriman['alamat'] = $model['alamat'];
            $pengiriman['id_cabang'] = $salesorder->id_cabang;
            $pengiriman['id_gudang'] = $model['id_gudang'];
            $pengiriman['id_sales_order_master'] = $model['id_sales_order_master'];
            $listidsalesorderdetail = [];
            $listqtypengiriman = [];
            $customer = $this->m_customer->GetOneCustomer($salesorder->id_customer);
            $messageerror = "";
            if (CheckArray($model, 'id_sales_order_detail')) {
                $listidsalesorderdetail = $model['id_sales_order_detail'];
            }
            if (CheckArray($model, 'qty_pengiriman')) {
                $listqtypengiriman = $model['qty_pengiriman'];
            }
            $kodepenerimaan = '';
            $id_penerimaan = 0;
            if (!CheckEmpty($model['id_surat_jalan_master'])) {
                $suratjalanfromdb = $this->GetOneSuratJalan($model['id_surat_jalan_master']);
                if ($suratjalanfromdb != null) {
                    $kodesuratjalan = $suratjalanfromdb->nomor_master;
                }
                $pengiriman['updated_date'] = GetDateNow();
                $pengiriman['updated_by'] = $userid;
                $this->db->update("uap_surat_jalan_master", $pengiriman, array("id_surat_jalan_master" => $model['id_surat_jalan_master']));
                $id_surat_jalan_master = $model['id_surat_jalan_master'];
            } else {
                $cabang = $this->m_cabang->GetOneCabang($salesorder->id_cabang);
                $kodesuratjalan = AutoIncrement('uap_surat_jalan_master', $cabang->kode_cabang . '/SJ/' . date("y") . '/', 'nomor_master', 5);
                $pengiriman['nomor_master'] = $kodesuratjalan;
                $pengiriman['created_date'] = GetDateNow();
                $pengiriman['created_by'] = $userid;
                $pengiriman['status'] = 1;
                $this->db->insert("uap_surat_jalan_master", $pengiriman);
                $id_surat_jalan_master = $this->db->insert_id();
            }
            $barangsync = [];

            $ceksalesorder = [];
            foreach ($listidsalesorderdetail as $key => $itemsalesorder) {
                array_push($ceksalesorder, $itemsalesorder);
                $this->db->from("uap_surat_jalan_detail");
                $listqtypengiriman[$key] = DefaultCurrencyDatabase($listqtypengiriman[$key]);
                $this->db->select("sum(uap_surat_jalan_detail.qty) as qty");
                $this->db->where(array("id_sales_order_detail" => $itemsalesorder, "id_surat_jalan_master" => $id_surat_jalan_master));
                $rowitem = $this->db->get()->row();
                $updateitem = [];
                $updateitem['qty'] = $listqtypengiriman[$key];
                $detailpengiriman = $this->GetDetailSuratJalanSummary($itemsalesorder);

                if ($detailpengiriman == null) {
                    $messageerror .= "Ada Detail Sales Order yang tidak terdapat di system<br/>";
                } else {
                    
                    if ($detailpengiriman->qtypengiriman - (CheckEmpty($rowitem) ? 0 : $rowitem->qty) + $listqtypengiriman[$key] > $detailpengiriman->qty) {
                        $messageerror .= "Detail Pengiriman " . $detailpengiriman->nama_barang . " lebih besar daripada qty sales order<br/>";
                        break;
                    }
                    $updateitem['id_barang'] = $detailpengiriman->id_barang;
                    $updateitem['nama_barang'] = $detailpengiriman->nama_barang;
                    if (!in_array($updateitem['id_barang'], $barangsync)) {
                        array_push($barangsync, $updateitem['id_barang']);
                    }


                    $updateitem['id_sales_order_detail'] = $detailpengiriman->id_sales_order_detail;
                    $updateitem['qty'] = $listqtypengiriman[$key];
                    $id_history = 0;

                    $this->db->from("uap_surat_jalan_detail");
                    $this->db->where(array("id_sales_order_detail" => $itemsalesorder, "id_surat_jalan_master" => $id_surat_jalan_master));
                    $listdetailsuratjalan = $this->db->get()->result();

                    $incrementpengiriman = $listqtypengiriman[$key];
                    $id_barang_history_terakhir = 0;
                    
                    foreach ($listdetailsuratjalan as $detailsuratjalan) {
                        if ($incrementpengiriman >= $detailsuratjalan->qty) {
                            $id_barang_history_terakhir = $detailsuratjalan->id_barang_history;
                            $incrementpengiriman -= $detailsuratjalan->qty;
                        } else {
                            $id_barang_history_terakhir = $detailsuratjalan->id_barang_history;
                            $baranghistory = $this->m_barang->GetOneHistoryItem($id_barang_history_terakhir);
                            $baranghistory->stock_sisa += $detailsuratjalan->qty - $incrementpengiriman;
                            $this->db->update("uap_barang_history", $baranghistory, array("id_barang_history" => $id_barang_history_terakhir));
                            $this->db->update("uap_surat_jalan_detail", array("qty" => $incrementpengiriman), array("id_surat_jalan_detail" => $detailsuratjalan->id_surat_jalan_detail));
                            $this->db->update("uap_barang_jurnal", array("debet" => 0, "kredit" => $incrementpengiriman), array("reference_id" => $detailsuratjalan->id_surat_jalan_detail, "dokumen" => $kodesuratjalan));
                            $incrementpengiriman = 0;
                           
                        }
                    }

                    if ($incrementpengiriman > 0) {
                        $this->db->from("uap_barang_history");
                        $this->db->where(array("id_gudang" => $model['id_gudang'], "id_barang" => $detailpengiriman->id_barang, "stock_sisa >" => 0));
                        $this->db->order_by("tanggal", "asc");
                        $listhistoryfromdb = $this->db->get()->result();
                        foreach ($listhistoryfromdb as $histdb) {
                            $qtydatabase = 0;
                            if ($incrementpengiriman > $histdb->stock_sisa) {
                                $qtydatabase = $histdb->stock_sisa;
                                $incrementpengiriman -= $histdb->stock_sisa;
                                $histdb->stock_sisa = 0;
                            } else {
                                $histdb->stock_sisa = $histdb->stock_sisa - $incrementpengiriman;
                                $qtydatabase = $incrementpengiriman;
                                $incrementpengiriman = 0;
                            }

                            $histdb->updated_by = $userid;
                            $histdb->updated_date = GetDateNow();
                            $this->db->update("uap_barang_history", $histdb, array("id_barang_history" => $histdb->id_barang_history));
                            $jurnal = array();
                            $jurnal['id_barang'] = $detailpengiriman->id_barang;
                            
                            $jurnal['tanggal_jurnal'] = $pengiriman['tanggal'];
                            $jurnal['dokumen'] = $kodesuratjalan;
                            $jurnal['description'] = "Pengiriman ke " . @$$customer->nama_customer . " untuk Invoice " . $salesorder->nomor_master;
                            $jurnal['kredit'] = $qtydatabase;
                            $jurnal['debet'] = 0;
                            $jurnal['harga_average'] = 0;
                            $jurnal['harga_beli_akhir'] = 0;
                            $jurnal['harga'] = $histdb->harga_ref;
                            $jurnal['id_gudang'] = $model['id_gudang'];
                            $jurnal['id_barang_history'] = $histdb->id_barang_history;
                            $jurnal['created_date'] = GetDateNow();
                            $jurnal['created_by'] = $userid;
                            $suratjalaninsertdetail = array();
                            $suratjalaninsertdetail['id_surat_jalan_master'] = $id_surat_jalan_master;
                            $suratjalaninsertdetail['id_sales_order_detail'] = $itemsalesorder;
                            $suratjalaninsertdetail['id_barang'] = $detailpengiriman->id_barang;
                            $suratjalaninsertdetail['nama_barang'] = $detailpengiriman->nama_barang;
                            $suratjalaninsertdetail['qty'] = $jurnal['kredit'];
                            $suratjalaninsertdetail['id_satuan'] = $detailpengiriman->id_satuan;
                            $suratjalaninsertdetail['status'] = 1;
                            $suratjalaninsertdetail['id_barang_history'] = $histdb->id_barang_history;
                            $suratjalaninsertdetail['created_date'] = GetDateNow();
                            $suratjalaninsertdetail['created_by'] = $userid;
                            $this->db->insert("uap_surat_jalan_detail", $suratjalaninsertdetail);
                            $id_surat_jalan_detail=$this->db->insert_id();
                            $jurnal['reference_id'] = $id_surat_jalan_detail;
                            $this->db->insert("uap_barang_jurnal", $jurnal);
                            
                            if ($incrementpengiriman == 0) {
                                break;
                            }
                        }
                        if ($incrementpengiriman > 0) {
                            $messageerror .= "Detail Pengiriman " . $detailpengiriman->nama_barang . " tidak ada dalam stock<br/>";
                        }
                    }
                }
            }

            $this->db->from("uap_surat_jalan_detail");
            $this->db->where(array("id_surat_jalan_master" => $id_surat_jalan_master));
            if (count($ceksalesorder) > 0) {
                $this->db->where_not_in("id_sales_order_detail", $ceksalesorder);
            }
            $listnotindb = $this->db->get()->result();
           
            foreach ($listnotindb as $detailsal) {
                if (!in_array($detailsal->id_barang, $barangsync)) {
                    array_push($barangsync, $detailsal->id_barang);
                }

                $id_barang_history_terakhir = $detailsal->id_barang_history;
                $baranghistory = $this->m_barang->GetOneHistoryItem($id_barang_history_terakhir);
                $baranghistory->stock_sisa += $detailsal->qty;
                $this->db->update("uap_barang_history", $baranghistory, array("id_barang_history" => $id_barang_history_terakhir));
                $this->db->update("uap_surat_jalan_detail", array("qty" => 0), array("id_surat_jalan_detail" => $detailsal->id_surat_jalan_detail));
                $this->db->update("uap_barang_jurnal", array("debet" => 0, "kredit" => 0), array("reference_id" => $detailsal->id_surat_jalan_detail, "dokumen" => $kodesuratjalan));
            }
            foreach ($barangsync as $bar) {
                $this->m_barang->SyncronizeHargaAverage($bar, $model['id_gudang'], $pengiriman['tanggal']);
            }

            $this->UpdateStatusSalesOrder($model['id_sales_order_master']);

            $message .= $messageerror;

            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data Surat Jalan Sudah di" . (CheckEmpty(@$model['id_surat_jalan_master']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_penerimaan, $kodepenerimaan, 'penerimaan');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDatasurat_jalan($params) {
        $isedit = CekModule("K110", false);
        $isdelete = CekModule("K111", false);
        $this->load->library('datatables');
        $this->datatables->select('uap_surat_jalan_master.status,id_surat_jalan_master,nama_customer,uap_surat_jalan_master.tanggal,replace(uap_surat_jalan_master.nomor_master,"/","-") as nomor_link,uap_surat_jalan_master.nomor_master,uap_sales_order_master.tanggal as tanggal_so,uap_sales_order_master.nomor_master as no_so,uap_penjualan_master.nomor_master as no_invoice,uap_penjualan_master.tanggal as tanggal_invoice');
        $this->datatables->join("uap_sales_order_master", "uap_sales_order_master.id_sales_order_master=uap_surat_jalan_master.id_sales_order_master", "left");
        $this->datatables->join("uap_penjualan_master", "uap_sales_order_master.id_sales_order_master=uap_penjualan_master.id_sales_order_master", "left");
        $this->datatables->join("uap_customer", "uap_customer.id_customer=uap_surat_jalan_master.id_customer", "left");
        $this->datatables->from('uap_surat_jalan_master');
        $extra = array();
        //add this line for join
        //
        //$this->datatables->join('table2', 'uap_sales_order.field = table2.field');
        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['uap_surat_jalan_master.id_cabang'] = $params['id_cabang'];
        }
        if (isset($params['id_customer']) && !CheckEmpty(@$params['id_customer'])) {
            $params['uap_surat_jalan_master.id_customer'] = $params['id_customer'];
        }
        if (isset($params['id_gudang']) && !CheckEmpty(@$params['id_gudang'])) {
            $params['uap_surat_jalan_master.id_gudang'] = $params['id_gudang'];
        }
        if (isset($params['status']) && !CheckEmpty($params['status'])) {
            $params['uap_surat_jalan_master.status'] = $params['status'];
        }

        unset($params['id_customer']);
        unset($params['id_gudang']);
        unset($params['id_cabang']);
        unset($params['status']);


        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "uap_surat_jalan_master.tanggal BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tanggal'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }


        if (count($params)) {
            $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        $stredit = '';
        $strdelete = '';
        $straction = anchor("", 'view', array('class' => 'btn btn-default btn-xs', "onclick" => "viewsuratjalan($1);return false;"));

        if ($isedit) {
            $stredit .= anchor("surat_jalan/editsurat_jalan/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $strdelete .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batal($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_surat_jalan_master,nomor_master');
        $this->datatables->add_column('action_edit', $stredit, 'id_surat_jalan_master,nomor_link');
        $this->datatables->add_column('action_delete', $strdelete, 'id_surat_jalan_master,nomor_master');
        return $this->datatables->generate();
    }

}
