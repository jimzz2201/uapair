<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pembelian extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_pembelian');
    }

    public function detailpo($id) {
        $model = $this->m_pembelian->GetOnePembelianPo($id, true);

        $this->load->view("v_pembelian_po_search_detail", $model);
    }
    public function GetOnePembelian()
    {
        $model = $this->input->post();

        $this->form_validation->set_rules('id_pembelian_master', 'nama pembelian', 'trim|required');
        $message = "";
        

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {

            $row = $this->m_pembelian->GetOnePembelian($model['id_pembelian_master'],true,true);
            echo json_encode(array("st"=>$row!=null,"obj"=>$row));
        }
    }
    
    

    public function po() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K018";
        $header = "K016";
        $title = "Pembelian PO";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier']=$this->m_supplier->GetDropDownSupplier();
        LoadTemplate($model, "pembelian/v_pembelian_po_index", $javascript, $css);
    }

    public function searchpo() {
        $model = $this->input->post();
        $this->load->view("v_pembelian_po_search", $model);
    }
    public function search() {
        $model = $this->input->post();
        $this->load->view("v_pembelian_search", $model);
    }
    public function editpembelianpo($id, $nomorpo) {
        $row = (object) array();

        $module = "K017";
        $header = "K016";
        CekModule($module);
        $pembelianpo = $this->m_pembelian->GetOnePembelianPo($id, true);
        if ($pembelianpo == null || $pembelianpo->nomor_po_master != str_replace("-", "/", $nomorpo)) {
            SetMessageSession(2, "Anda tidak memiliki akses untuk menuju link tersebut");
            redirect(base_url() . 'index.php/main/message');
        } else {
            $row = $pembelianpo;
        }
        $row->button = 'Edit';
        $this->load->model("supplier/m_supplier");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->list_supplier = $this->m_supplier->GetDropDownSupplier();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $row->tgl_po = GetDateNow();
        $row->title = 'Edit Pembelian PO ' . $pembelianpo->nomor_po_master;
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'pembelian/v_pembelian_po_manipulate', $javascript, $css);
    }
    
    public function penerimaanlist(){
         $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K102";
        $header = "K016";
        $title = "Pembelian PO";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $model['list_status']=array("2"=>"Batal","1"=>"Non Batal");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $model['list_gudang']=$this->m_gudang->GetDropDownGudang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier']=$this->m_supplier->GetDropDownSupplier();
        LoadTemplate($model, "pembelian/v_penerimaan_index", $javascript, $css);
    }
    
    function GetOnePenerimaanPembelian()
    {
        $id_penerimaan=@$this->input->post("id_penerimaan");
        $penerimaan = $this->m_pembelian->GetOnePenerimaanPembelian($id_penerimaan, true,true);
        $html=$this->load->view("retur/v_retur_beli_content",$penerimaan,true);
        echo json_encode(array("st"=>$penerimaan!=null,"obj"=>$penerimaan,"html"=>$html));
    }
    
    function editpenerimaan($id = null, $invoice = "") {
        $row = (object) array();
        
        $module = "K101";
        $header = "K016";
        $penerimaan = $this->m_pembelian->GetOnePenerimaanPembelian($id, true);
        CekModule($module);
       if ($penerimaan == null || $penerimaan->kode_penerimaan != str_replace("-", "/", $invoice)) {
            SetMessageSession(2, "Anda tidak memiliki akses untuk menuju link tersebut");
            redirect(base_url() . 'index.php/main/message');
        } else {
            $row = $penerimaan;
        }
        $row->button = 'Edit';
        $this->load->model("supplier/m_supplier");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = $module;
       
        $pembelian=$this->m_pembelian->GetOnePembelian($penerimaan->id_pembelian_master);
        $listheader=array(array("tanggal"=>$pembelian->tanggal,"nomor_master"=>$pembelian->nomor_master));
        $row->listheader = $listheader;
        $row->list_supplier = $this->m_supplier->GetDropDownSupplier();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $row->title = 'Edit Penerimaan';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';

        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'pembelian/v_penerimaan_manipulate', $javascript, $css);
    }

    public function viewpenerimaan()
    {
        $id=$this->input->post("id_penerimaan");
        $penerimaan = $this->m_pembelian->GetOnePenerimaanPembelian($id, true);
        
        $this->load->view("pembelian/v_penerimaan_view",array("row"=>$penerimaan));
    }
    public function penerimaan() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K101";
        $header = "K016";
        CekModule($module);
        $this->load->model("supplier/m_supplier");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->listdetail = [];
        $row->listheader = [];
        $row->list_supplier = $this->m_supplier->GetDropDownSupplier();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $row->tanggal = GetDateNow();
        $row->title = 'Create Pembelian';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';

        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'pembelian/v_penerimaan_manipulate', $javascript, $css);
    }
    
    public function penerimaan_manipulate()
    {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_pembelian_master', 'Pembelian', 'trim|required');
        $this->form_validation->set_rules('id_gudang', 'Gudang', 'trim|required');
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        $listidpembelian=[];
        $listpenerimaan=[];
        $message="";
        if(CheckArray($model, 'id_pembelian_detail'))
        {
            $listidpembelian=$model['id_pembelian_detail'];
        }
        else
        {
            $message.="Detail Pembelian Required<br/>";
        }
        if(CheckArray($model, 'penerimaan'))
        {
            $listpenerimaan=$model['penerimaan'];
        }
        else
        {
             $message.="Qty Penerimaan Required<br/>";
        }
        
        if(count($listidpembelian)==0)
        {
             $message.="Detail Pembelian Required<br/>";
        }
        else if(count($listidpembelian)!=count($listpenerimaan))
        {
            $message.="Something error occured<br/>";
        }
        else{
            $issimpan=false;
            foreach($listpenerimaan as $satuanpenerimaan)
            {
                if(!CheckEmpty($satuanpenerimaan))
                {
                    $issimpan=true;
                }
            }
            if(!$issimpan)
            {
                $message.="Quantity penerimaan tidak boleh kosong<br/>";
            }
        }
        
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {

            $data = $this->m_pembelian->PenerimaanPembelian($model);
            echo json_encode($data);
        }
    }
    
    
    public function editpembelian($id, $nomorinvoice) {
        $row = (object) array();

        $module = "K019";
        $header = "K016";
        CekModule($module);
        $this->load->model("supplier/m_supplier");
        $pembelian = $this->m_pembelian->GetOnePembelian($id, true);
        $this->m_supplier->KoreksiSaldoHutang($pembelian->id_supplier);
        if ($pembelian == null || $pembelian->nomor_master != str_replace("-", "/", $nomorinvoice)) {
            SetMessageSession(2, "Anda tidak memiliki akses untuk menuju link tersebut");
            redirect(base_url() . 'index.php/main/message');
        } else {
            $row = $pembelian;
        }
        $row->button = 'Edit';
        $this->load->model("supplier/m_supplier");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->list_supplier = $this->m_supplier->GetDropDownSupplier();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $jenis_pembayaran = array();
        $jenis_pembayaran[1] = "Tunai";
        $jenis_pembayaran[2] = "Kredit";
        $row->list_type_pembayaran = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row->list_type_ppn = $list_type_ppn;

        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row->list_type_pembulatan = $list_type_pembulatan;
        $row->title = 'Edit Pembelian';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';

        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'pembelian/v_pembelian_manipulate', $javascript, $css);
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K020";
        $header = "K016";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier']=$this->m_supplier->GetDropDownSupplier();
        LoadTemplate($model, "pembelian/v_pembelian_index", $javascript, $css);
    }

    public function getdatapembelianPO() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_pembelian->GetDatapembelianPO($params);
    }

    public function getdatapembelian() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_pembelian->GetDatapembelian($params);
    }
    public function getdatapenerimaan() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_pembelian->GetDatapenerimaan($params);
    }
    public function getdatapembelianforpenerimaan() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        $params['tgl_pengiriman']=null;
        echo $this->m_pembelian->GetDatapembelian($params);
    }
    
    public function create() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K019";
        $header = "K016";
        CekModule($module);
        $this->load->model("supplier/m_supplier");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->listdetail = [];
        $row->listheader = [];
        $row->list_supplier = $this->m_supplier->GetDropDownSupplier();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $jenis_pembayaran = array();
        $jenis_pembayaran[1] = "Tunai";
        $jenis_pembayaran[2] = "Kredit";
        $row->list_type_pembayaran = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row->list_type_ppn = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row->list_type_pembulatan = $list_type_pembulatan;
        $row->tanggal = GetDateNow();
        $row->jth_tempo = date('Y-m-d', strtotime(GetDateNow() . "+1 months"));
        $row->title = 'Create Pembelian';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';

        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'pembelian/v_pembelian_manipulate', $javascript, $css);
    }

    public function po_create() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K017";
        $header = "K016";
        CekModule($module);
        $this->load->model("supplier/m_supplier");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->listdetail = [];
        $row->list_supplier = $this->m_supplier->GetDropDownSupplier();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $row->tgl_po = GetDateNow();
        $row->title = 'Create Pembelian PO';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'pembelian/v_pembelian_po_manipulate', $javascript, $css);
    }

    public function create_pembelian() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K004";
        $header = "K001";
        CekModule($module);
        $this->load->model("satuan/m_satuan");
        $this->load->model("jenis/m_jenis");
        $this->load->model("merek/m_merek");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->list_merek = $this->m_merek->GetDropDownMerek();
        $row->list_jenis = $this->m_jenis->GetDropDownJenis();
        $row->list_satuan = $this->m_satuan->GetDropDownSatuan();
        $row->title = 'Create Barang';
        $row = json_decode(json_encode($row), true);

        $javascript = array();
        LoadTemplate($row, 'pembelian/v_pembelian_manipulate', $javascript);
    }

    public function mutasi_pembelian_action() {
        $model = $this->input->post();

        $this->form_validation->set_rules('id_pembelian', 'nama pembelian', 'trim|required');
        $this->form_validation->set_rules('id_gudang', 'gudang', 'trim|required');
        $this->form_validation->set_rules('tanggal_jurnal', 'tanggal jurnal', 'trim|required');
        $this->form_validation->set_rules('jenis', 'jenis aliran', 'trim|required');
        $message = "";
        if (array_column($model, 'id_history')) {
            foreach ($model['id_history'] as $key => $history) {
                if ($model['qty'][$key] > $model['stock'][$key] && $history != "0" && $model['jenis'] == "2") {
                    $message .= "Quantity tidak boleh lebih besar dari stock yang tersedia";
                }
            }
        }

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {

            $data = $this->m_pembelian->MutasiBarang($model);
            echo json_encode($data);
        }
    }

    public function pembelian_po_manipulate() {
        $message = '';
        $model = $this->input->post();

        if (CheckEmpty($model['dataset']) || count($model['dataset']) == 0) {
            $message .= "Detail Pembelian is required<br/>";
        }
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');
        $this->form_validation->set_rules('id_gudang', 'Gudang', 'trim|required');
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_pembelian->PembelianPOManipulate($model);
            echo json_encode($status);
        }
    }

    public function pembelian_manipulate() {
        $message = '';
        $model = $this->input->post();

        if (CheckEmpty($model['dataset']) || count($model['dataset']) == 0) {
            $message .= "Detail Pembelian is required<br/>";
        }
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');
        $this->form_validation->set_rules('id_gudang', 'Gudang', 'trim|required');
        $this->form_validation->set_rules('id_supplier', 'Supplier', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        $this->form_validation->set_rules('jth_tempo', 'Jatuh Tempo', 'trim|required');
        $this->form_validation->set_rules('type_pembayaran', 'Pembayaran', 'trim|required');
        if (!CheckEmpty(@$model['type_pembayaran']) && $model['type_pembayaran'] == "1") {
            $this->form_validation->set_rules('jumlah_bayar', 'Jumlah Bayar', 'trim|required');
        }
        
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
       
            $status = $this->m_pembelian->PembelianManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_pembelian($id = 0) {
        $row = $this->m_pembelian->GetOneBarang($id);

        if ($row) {
            $row->button = 'Update';
            $module = "K004";
            $header = "K001";
            CekModule($module);
            $row->form = $header;
            $row->formsubmenu = $module;
            $row->title = 'Update Barang';
            $this->load->model("satuan/m_satuan");
            $this->load->model("jenis/m_jenis");
            $this->load->model("merek/m_merek");
            $row->list_merek = $this->m_merek->GetDropDownMerek();
            $row->list_jenis = $this->m_jenis->GetDropDownJenis();
            $row->list_satuan = $this->m_satuan->GetDropDownSatuan();
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            LoadTemplate($row, 'pembelian/v_pembelian_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Barang cannot be found in database");
            redirect(site_url('pembelian'));
        }
    }

    public function get_one_pembelian() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_pembelian', 'nama pembelian', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_pembelian->GetOneBarang($model["id_pembelian"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data pembelian tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
     public function penerimaan_batal() {
        $message = '';
        $this->form_validation->set_rules('id_penerimaan', 'Barang', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_pembelian->BatalPenerimaan($model['id_penerimaan']);
        }

        echo json_encode($result);
    }
    public function pembelian_delete() {
        $message = '';
        $this->form_validation->set_rules('id_pembelian', 'Barang', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_pembelian->BarangDelete($model['id_pembelian']);
        }

        echo json_encode($result);
    }

}

/* End of file Barang.php */