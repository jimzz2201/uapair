<div style="text-align: left">
    <h3>Penerimaan : <?php echo @$row->kode_penerimaan ?><?php
        if (@$row->status == "2") {
            echo "  <span style='color:red;font-weight:bold'>( BATAL )</span>";
        }
        ?></h3>
    <p>View Penerimaan Pembelian | Penerimaan Pembelian</p>
</div>

<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">

                        <div class="form-group" >
                            <div class="row " style="margin-bottom:20px;">
                                <div class="col-sm-12 kolomheaderpo">
                                    <hr/>
                                    <?php
                                    echo "<b>Nomor : " . DefaultDatePicker(@$row->tanggal) . " == " . @$row->nomor_master . "</b><br/>";
                                    ?>
                                    <hr/>
                                </div>

                            </div>

                        </div>
                        <form id="frm_pembelian" class="form-horizontal form-groups-bordered validate" method="post">
                            <input type="hidden" id="txt_id_penerimaan_pembelian" name="id_penerimaan" value="<?php echo @$id_penerimaan; ?>" /> 
                            <input type="hidden" id="txt_id_pembelian_master" name="id_pembelian_master" value="<?php echo @$id_pembelian_master; ?>" /> 
                            <div class="form-group">
                                <?= form_label('Supplier', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?= form_input(array('type' => 'text', 'value' => @$row->nama_supplier, 'class' => 'form-control', 'placeholder' => 'Supplier', 'disabled' => 'disabled')); ?>
                                </div>    



                            </div>
                            <div class="form-group">
                                <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'value' => DefaultDatePicker(@$row->tanggal_penerimaan), 'class' => 'form-control ', 'placeholder' => 'Tanggal', 'disabled' => 'disabled')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= form_label('Keterangan', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?= form_textarea(array('type' => 'text', 'disabled' => 'disabled', 'value' => @$row->keterangan, 'class' => 'form-control', 'placeholder' => 'Keterangan')); ?>
                                </div>
                            </div>
                            <div class="form-group">

                                <?= form_label('Cabang', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'value' => @$row->nama_cabang, 'class' => 'form-control', 'placeholder' => 'Cabang', 'disabled' => 'disabled')); ?>
                                </div>

                                <?= form_label('Gudang', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                <div class="col-sm-5">
                                    <?= form_input(array('type' => 'text', 'value' => @$row->nama_gudang, 'class' => 'form-control', 'placeholder' => 'Gudang', 'disabled' => 'disabled')); ?>
                                </div>

                            </div>


                            <div class="box box-default">


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet-body form">
                                            <table class="table table-striped table-bordered table-hover" id="mytable" style="width:100%;">
                                                <thead><tr>
                                                        <th>SKU</th>
                                                        <th>Nama Barang</th>
                                                        <th>Qty Penerimaan</th>
                                                    </tr></thead>
                                                <tbody>
                                                    <?php foreach (@$row->listdetail as $detail) {
                                                        if($detail->jumlahditerima>0)
                                                        {
                                                            echo "<tr>";
                                                            echo "<td>".$detail->sku."</td>";
                                                            echo "<td>".$detail->nama_barang."</td>";
                                                            echo "<td>".DefaultCurrency($detail->jumlahditerima)."</td>";
                                                            echo "</tr>";
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <hr/>


                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

</section>