<div class="modal-header">
    Pencarian PO
</div>
<div class="modal-body" style="min-width:100px;">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="portlet-body form">

                    <table class="table table-striped table-bordered table-hover" id="mytablesearch" style="width:100%">

                    </table>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="detailpo">

            </div>
        </div>
    </div>
</div>
<script>
    var tablesearch;
    $(document).ready(function () {
        setTimeout(function () {
            InitialGridSearch();
        }, 500);
    })

    function selectpo(id_po)
    {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/pembelian/detailpo/' + id_po,
            success: function (data) {
                $("#detailpo").html(data);

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }
    function InitialGridSearch()
    {
        tablesearch = $("#mytablesearch").dataTable({

            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": baseurl+"index.php/pembelian/getdatapembelianPO", "type": "POST"},
            columns: [
                {data: "tgl_po", orderable: false, title: "Tanggal",
                    mRender: function (data, type, row) {
                        return DefaultDateFormat(data);
                    }}
                , {data: "nama_supplier", orderable: false, title: "Supplier"}
                , {data: "nomor_po_master", orderable: false, title: "NO PO"}
                , {data: "nominal_po", orderable: false, title: "Jumlah",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                ,
                {
                    "data": "id_pembelian_po_master",
                    "orderable": false,
                    "className": "text-center nopadding",
                    mRender: function (data, type, row) {
                        return "<a href=\"javascript:;\" class=\"btn  btn-success btn-xs\" onclick=\"selectpo(" + data + ");return false;\">Pilih</a>";
                    }
                }
            ],
            order: [[0, 'desc']],
            initComplete: function () {

            }

        });
    }



</script>