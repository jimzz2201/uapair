<input  type="hidden" name="id_pembelian_po_master" id="txt_id_pembelian_po_master" value="<?php echo $id_pembelian_po_master ?>"   />
<input  type="hidden" name="nomor_po_master" id="txt_nomor_po_master" value="<?php echo $nomor_po_master ?>"  />
<input  type="hidden" name="id_supplier" id="txt_id_supplier_obj" value="<?php echo $id_supplier ?>"  />
<input  type="hidden" name="id_cabang" id="txt_id_cabang_obj" value="<?php echo $id_cabang ?>"  />
<input  type="hidden" name="id_gudang" id="txt_id_gudang_obj" value="<?php echo $id_gudang?>"  />
<input  type="hidden" name="tgl_po" id="txt_tgl_po" value="<?php echo $tgl_po ?>"  />
<table class="table table-striped table-bordered table-hover dataTable no-footer">
    <thead>
        <tr>
            <th><input type="checkbox" checked="" class="i-checks" id="cb_all" /></th>
            <th>SKU Barang</th>
            <th>Nama Barang</th>
            <th>Price Barang</th>
            <th width="100px">Qty</th>
            <th width="100px">Quantity Order</th>
            <th>Satuan</th>
        </tr>
    </thead>
    <?php foreach (@$listdetail as $key => $detail) {
        ?>
        <tr>
            <td align="center">
                <input  checked="" type="checkbox" id="cb_det_<?php echo $key ?>" class="i-checks checkdetail"  />
                <?= form_input(array('type' => 'hidden', "id" => "txt_harga_beli_akhir_" . $key, 'value' => DefaultCurrency($detail->harga_beli_akhir))); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_harga_beli_" . $key, 'value' => DefaultCurrency($detail->price_barang))); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_disc1_" . $key, 'value' => DefaultCurrency($detail->disc_1))); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_disc2_" . $key, 'value' => DefaultCurrency($detail->disc_2))); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_nama_barang_" . $key, 'value' => $detail->nama_barang)); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_sku_" . $key, 'value' => $detail->sku)); ?>
                <?= form_input(array('type' => 'hidden', "id" => "txt_id_barang_" . $key, 'value' => $detail->id_barang)); ?>
            </td>
            <td><?= $detail->sku ?></td>
            <td><?= $detail->nama_barang ?></td>
            <td><?= DefaultCurrency($detail->price_barang) ?></td>
            <td style="width:100px"><?= DefaultCurrency($detail->qty) ?></td>
            <td style="width:100px"><?= form_input(array('type' => 'text', 'name' => 'qty[]', 'value' => DefaultCurrency($detail->qty), 'class' => 'form-control qty', 'id' => 'txt_qty_' . $key, 'placeholder' => 'Qty', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'style' => "padding:2px 12px")); ?></td>
            <td><?= $detail->nama_satuan ?></td>
        </tr>

    <?php } ?>
</table>

<br/>
<button type="button" class="btn btn-primary btn-block" id="btt_input_detail" >Input Detail</button>
<script>
    $(document).ready(function () {

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    })
    $("#btt_clear_pembelian").click(function () {
        listheader = [];
        dataset = [];
        RefreshGrid();
        $(".kolomheaderpo").empty();
    })
    $("#btt_input_detail").click(function () {
        var isclosed = false;

        var res = alasql('SELECT * FROM ? where id_pembelian_po_master=\'' + $("#txt_id_pembelian_po_master").val() + '\'', [listheader]);
        if (res.length > 0)
        {
            modaldialogerror("Data PO Sudah ada dalam list pembelian");
        } else
        {
            $.each($(".qty"), function (key, value) {
                var qtyid = value.id;
                var id = qtyid.replace("txt_qty_", "");
                if (value.value > 0)
                {
                    isclosed = true;
                    var pricelist = 0;
                    if ($("#txt_harga_beli_" + id).val() == "0")
                    {
                        pricelist = Number($("#txt_harga_beli_akhir_" + id).val().replace(/[^0-9\.]+/g, ""));
                    } else
                    {
                        pricelist = Number($("#txt_harga_beli_" + id).val().replace(/[^0-9\.]+/g, ""));
                    }

                    var disc1 = Number($("#txt_disc1_" + id).val().replace(/[^0-9\.]+/g, ""));
                    var disc2 = Number($("#txt_disc2_" + id).val().replace(/[^0-9\.]+/g, ""));
                    var qty = Number($("#txt_qty_" + id).val().replace(/[^0-9\.]+/g, ""));
                    var subtotal = pricelist * ((100 - disc1) / 100) * ((100 - disc2) / 100) * qty;
                    var detail = {};
                    detail.id_pembelian_detail = 0;
                    detail.id_barang = $("#txt_id_barang_" + id).val();
                    detail.price_barang = pricelist;
                    detail.nama_barang = $("#txt_nama_barang_" + id).val();
                    detail.sku = $("#txt_sku_" + id).val();
                    detail.qty = qty;
                    detail.disc_1 = disc1;
                    detail.disc_2 = disc2;
                    detail.subtotal = subtotal;
                    dataset.push(detail);
                    RefreshGrid();
                }
            });
            if (isclosed)
            {
                var header = {};
                header.id_pembelian_po_master = $("#txt_id_pembelian_po_master").val();
                header.nomor_po_master = $("#txt_nomor_po_master").val();
                header.tgl_po = $("#txt_tgl_po").val();
                if ($("#txt_id_supplier_obj").val() != "")
                {
                    $("#dd_id_supplier").val($("#txt_id_supplier_obj").val());
                    $("#dd_id_supplier").trigger("change");
                }
                if ($("#txt_id_cabang_obj").val() != "")
                {
                    $("#dd_id_cabang").val($("#txt_id_cabang_obj").val());
                    $("#dd_id_cabang").trigger("change");
                }
                if ($("#txt_id_gudang_obj").val() != "")
                {
                    $("#dd_id_gudang").val($("#txt_id_gudang_obj").val());
                    $("#dd_id_gudang").trigger("change");
                }
                $(".kolomheaderpo").append("Nomor PO : " + $("#txt_tgl_po").val() + " == " + $("#txt_nomor_po_master").val() + "<br/>");
                
                listheader.push(header);
                closemodalboostrap();
            } else
            {
                modaldialogerror("Quantity detail pembelian harus lebih besar dari 1");
            }
        }

    })
    $("#cb_all").on("ifChanged", function (e) {
        if (e.target.checked)
        {
            $('.checkdetail').iCheck('check');
        } else
        {
            $('.checkdetail').iCheck('uncheck');
        }
    })
    function ChangeDet(checkid) {
        var id = checkid.replace("cb_det_", "");
        if ($("#" + checkid).is(":checked"))
        {
            $("#txt_qty_" + id).removeAttr("readonly", "readonly");
            $("#txt_qty_" + id).val(Comma($("#txt_hidden_qty_" + id).val()));
        } else
        {
            $("#txt_qty_" + id).attr("readonly", "readonly");
            $("#txt_qty_" + id).val(0);
        }
    }
    $(".checkdetail").on("ifChanged", function (e) {
        ChangeDet(e.target.id);
        if (!e.target.checked)
        {
            $('#cb_all').prop('checked', false).iCheck('update');
        }
    })
</script>