<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Penerimaan Pembelian</h2>
                                <p><?= $button ?> Penerimaan Pembelian | Penerimaan Pembelian</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                            <div class="form-group" >
                                <div class="row " style="margin-bottom:20px;">
                                    <div class="col-sm-12 kolomheaderpo">
                                        <?php
                                        foreach (@$listheader as $headersatuan) {
                                            echo "Nomor : " . DefaultDatePicker($headersatuan['tanggal']) . " == " . $headersatuan['nomor_master'] . "<br/>";
                                        }
                                        ?>
                                    </div>

                                </div>
                                <?php if (CheckEmpty(@$id_penerimaan)) { ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <button type="button" class="btn btn-primary btn-block" id="btt_search_po" >Cari No Pembelian</button>
                                        </div>
                                        <div class="col-sm-6">
                                            <button type="button" class="btn btn-danger btn-block" id="btt_clear_pembelian" >Clear Data</button>
                                        </div>
                                    </div>
                                <?Php } ?>
                            </div>
                            <form id="frm_pembelian" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" id="txt_id_penerimaan_pembelian" name="id_penerimaan" value="<?php echo @$id_penerimaan; ?>" /> 
                                <input type="hidden" id="txt_id_pembelian_master" name="id_pembelian_master" value="<?php echo @$id_pembelian_master; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Supplier', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        $listsupplier = DefaultEmptyDropdown(@$list_supplier, "", "Supplier");
                                        if (!CheckEmpty(@$id_supplier)) {
                                            $textsupplier=$listsupplier[$id_supplier];
                                            $listsupplier=[];
                                            $listsupplier[@$id_supplier]=$textsupplier;
                                        }
                                        ?>
                                        <?= form_dropdown(array("name" => "id_supplier"), $listsupplier, @$id_supplier, array('class' => 'form-control', 'id' => 'dd_id_supplier')); ?>
                                    </div>    

                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">

                                        <?= form_textarea(array('type' => 'text', 'name' => 'keterangan', 'value' => @$keterangan, 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?php
                                    $listcabang = DefaultEmptyDropdown(@$list_cabang, "", "Cabang");
                                    if (!CheckEmpty(@$id_cabang)) {
                                        $textcabang = $listcabang[@$id_cabang];
                                        $listcabang = [];
                                        $listcabang[@$id_cabang] = $textcabang;
                                    }
                                    ?>
                                    <?= form_label('Nama Cabang', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                    <?= form_dropdown(array("name" => "id_cabang"), $listcabang, @$id_cabang, array('class' => 'form-control', 'id' => 'dd_id_cabang')); ?>
                                    </div>
                                        <?php
                                        $listgudang = DefaultEmptyDropdown(@$list_gudang, "", "Gudang");
                                        if (!CheckEmpty(@$id_gudang)) {
                                            $textgudang =$listgudang[@$id_gudang];
                                            $listgudang = [];
                                            $listgudang[$id_gudang] = $textgudang;
                                        }
                                        ?>
                                    <?= form_label('Gudang', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                    <?= form_dropdown(array("name" => "id_gudang"), $listgudang, @$id_gudang, array('class' => 'form-control', 'id' => 'dd_id_gudang')); ?>
                                    </div>

                                </div>


                                <div class="box box-default">


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet-body form">
                                                <table class="table table-striped table-bordered table-hover" id="mytable" style="width:100%;">

                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <hr/>

                                <div class="form-group" style="margin-top:50px">
                                    <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>


    var table;
    var dataset = <?php echo json_encode(@$listdetail) ?>;
    var listheader = <?php echo json_encode(@$listheader) ?>;
    
    function RefreshGrid()
    {
        table.fnClearTable();
        if (dataset.length > 0)
        {
            table.fnAddData(dataset);
        }

    }
    $(document).ready(function () {
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            oLanguage: {
                sProcessing: "loading..."
            },
            searching: false, paging: false, info: false,
            scrollX: false,
            width: "100%",
            data: dataset,
            columns: [
                {
                    data: "id_pembelian_detail",
                    title: "Kode",
                    orderable: false,
                    width: "20px"
                }
                , {data: "sku", orderable: false, title: "SKU"}
                , {data: "nama_barang", orderable: false, title: "Nama"}
                , {data: "qty", orderable: false, title: "Qty", width: "50px"}
                , {data: "jumlahpenerimaan", orderable: false, title: "Diterima", width: "50px"}
                , {data: "id_pembelian_detail", orderable: false, title: "Penerimaan", width: "50px", mRender: function (data, type, row) {
                        return "<input type='hidden' value=" + data + " name='id_pembelian_detail[]' />\n\
            <input type='text' name='penerimaan[]' value='" + Comma(row['jumlahditerima']==null?0:row['jumlahditerima']) + "' onkeyup='javascript:this.value=Comma(this.value);' onkeypress='return isNumberKey(event);' class='form-control' placeholder='Qty Terima' >";
                    }}],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

            }
        }
        );

        RefreshGrid();

    })
    function RefreshPanelGrid()
    {
        if (dataset.length > 0)
        {
            $("#frm_pembelian button").removeAttr("disabled");
        } else
        {
            $("#frm_pembelian button").attr("disabled", "disabled");
        }

    }

    $(document).ready(function () {
        $("#txt_tanggal").datepicker();
        RefreshPanelGrid();
      

    })
    $("#btt_clear_pembelian").click(function () {
        listheader = [];
        dataset = [];
        RefreshGrid();
        RefreshPanelGrid();
        $(".kolomheaderpo").empty();
        $("#dd_id_supplier").removeAttr("disabled");
        $("#btt_search_po").css("display", "block");
    })
    $("#btt_search_po").click(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/pembelian/search',
            data: $("#frm_pembelian").serialize() + '&' + $.param({"dataset": dataset}),
            success: function (data) {
                modalbootstrap(data, "Search PO", "80%");

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });

    })
    $("#frm_pembelian").submit(function () {
         
        swal({
            title: "Apakah kamu yakin ingin menginput data penerimaan berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                 LoadBar.show();
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/pembelian/penerimaan_manipulate',
                    dataType: 'json',
                    data: $("#frm_pembelian").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
<?php if (CekModule("K102", false)) { ?>
                                window.location.href = "<?php echo base_url() ?>index.php/pembelian/penerimaanlist";
<?php } else {
    ?>
                                window.location.reload();
<?php } ?>

                        } else
                        {
                            messageerror(data.msg);
                        }
                          LoadBar.hide();

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                         LoadBar.hide();
                    }
                });
            }
        });
        return false;


    })
</script>