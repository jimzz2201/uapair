<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Pembelian PO</h2>
                                <p><?= $button ?> Pembelian PO | Pembelian PO</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                            <div class="form-group" >
                                <div class="row " style="margin-bottom:20px;">
                                    <div class="col-sm-12 kolomheaderpo">
                                        <?php foreach(@$listheader as $headersatuan){
                                               echo "Nomor PO : " . DefaultDatePicker($headersatuan['tgl_po']) . " == " . $headersatuan['nomor_po_master'] . "<br/>";
                                            
                                        }?>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <button type="button" class="btn btn-primary btn-block" id="btt_search_po" >Cari No PO</button>
                                    </div>
                                    <div class="col-sm-6">
                                        <button type="button" class="btn btn-danger btn-block" id="btt_clear_pembelian" >Clear Data</button>
                                    </div>
                                </div>
                            </div>
                            <form id="frm_pembelian" class="form-horizontal form-groups-bordered validate" method="post">

                                <input type="hidden" name="id_pembelian_master" value="<?php echo @$id_pembelian_master; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Supplier', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_supplier"), DefaultEmptyDropdown(@$list_supplier, "", "Supplier"), @$id_supplier, array('class' => 'form-control', 'id' => 'dd_id_supplier')); ?>
                                    </div>    

                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Nama Cabang', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_cabang"), DefaultEmptyDropdown(@$list_cabang, "", "Cabang"), @$id_cabang, array('class' => 'form-control', 'id' => 'dd_id_cabang')); ?>
                                    </div>
                                    <?= form_label('Gudang', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("name" => "id_gudang"), DefaultEmptyDropdown(@$list_gudang, "", "Gudang"), @$id_gudang, array('class' => 'form-control', 'id' => 'dd_id_gudang')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Type PPN', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "type_ppn"), DefaultEmptyDropdown(@$list_type_ppn, "", "PPN"), @$type_ppn, array('class' => 'form-control', 'id' => 'dd_type_ppn')); ?>
                                    </div>
                                    <?= form_label('PPN', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-1">
                                        <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$ppn), 'class' => 'form-control', 'id' => 'txt_ppn', 'placeholder' => 'PPN', 'name' => 'ppn', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Pembayaran', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "type_pembayaran"), DefaultEmptyDropdown(@$list_type_pembayaran, "", "Pembayaran"), @$type_pembayaran, array('class' => 'form-control', 'id' => 'dd_type_pembayaran')); ?>
                                    </div>
                                    <?= form_label('Jth Tempo', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'jth_tempo', 'value' => DefaultDatePicker(@$jth_tempo), 'class' => 'form-control datepicker', 'id' => 'txt_jth_tempo', 'placeholder' => 'Jatuh Tempo')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>

                                </div>  
                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Nama Barang', "dd_id_barang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_barang"), DefaultEmptyDropdown(@$list_barang, "", "Barang"), @$id_barang, array('class' => 'form-control', 'id' => 'dd_id_barang')); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'value' => "", 'class' => 'form-control', 'id' => 'txt_kode_barang', 'placeholder' => 'SKU', 'disabled' => "disabled")); ?>
                                    </div>
                                    <?= form_label('Harga Beli', "txt_harga_beli", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'value' => "0", 'class' => 'form-control', 'id' => 'txt_harga_beli', 'placeholder' => 'Harga Beli', 'disabled' => "disabled")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Harga Final', "txt_price_list", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'value' => "", 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'class' => 'form-control', 'id' => 'txt_price_list', 'placeholder' => 'Price List')); ?>
                                    </div>
                                    <?= form_label('Disc 1', "txt_disc_1", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-1">
                                        <?= form_input(array('type' => 'text', 'value' => "0", 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'name' => 'disc_1', 'class' => 'form-control', 'id' => 'txt_disc_1', 'placeholder' => 'Disc 1')); ?>
                                    </div>
                                    <?= form_label('Disc 2', "txt_disc_2", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-1">
                                        <?= form_input(array('type' => 'text', 'value' => '0', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'name' => 'disc_2', 'class' => 'form-control', 'id' => 'txt_disc_2', 'placeholder' => 'Disc 2')); ?>
                                    </div>
                                    <?= form_label('Stock', "txt_harga_beli", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'value' => "0", 'class' => 'form-control', 'id' => 'txt_stock', 'placeholder' => 'Stock', 'disabled' => "disabled")); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Quantity', "txt_quantity", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'value' => "0", 'class' => 'form-control', 'id' => 'txt_quantity', 'placeholder' => 'Quantity', 'name' => 'quantity', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Subtotal', "txt_subtotal", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'value' => "0", 'class' => 'form-control', 'id' => 'txt_subtotal', 'placeholder' => 'Subtotal', 'disabled' => "disabled")); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-success waves-effect btn-block" id="btt_add_child" style="padding:4px 12px;"  >Input Item</button>
                                    </div>

                                </div>

                                <div class="box box-default">


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet-body form">
                                                <table class="table table-striped table-bordered table-hover" id="mytable">

                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Total', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'total', 'value' => DefaultCurrency(@$total), 'class' => 'form-control', 'id' => 'txt_total', 'placeholder' => 'Total', "readonly" => "readonly")); ?>
                                    </div>
                                    <?= form_label('PPN', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'nominal_ppn', 'value' => DefaultCurrency(@$nominal_ppn), 'class' => 'form-control', 'id' => 'txt_nominal_ppn', 'placeholder' => 'PPN', "readonly" => "readonly")); ?>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <?= form_label('Type Pembulatan', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "type_pembulatan"), @$list_type_pembulatan, @$type_pembulatan, array('class' => 'form-control', 'id' => 'dd_type_pembulatan')); ?>
                                    </div>
                                    <?= form_label('Pembulatan', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'disc_pembulatan', 'value' => DefaultCurrency(@$disc_pembulatan), 'class' => 'form-control', 'id' => 'txt_disc_pembulatan', 'placeholder' => 'Disc Pembulatan', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <?= form_label('Grand Total', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'grandtotal', 'value' => DefaultCurrency(@$grandtotal), 'class' => 'form-control', 'id' => 'txt_grandtotal', 'placeholder' => 'Grand Total')); ?>
                                    </div>
                                    <?= form_label('Jumlah&nbsp;Bayar', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'jumlah_bayar', 'value' => DefaultCurrency(@$jumlah_bayar), 'class' => 'form-control', 'id' => 'txt_jumlah_bayar', 'placeholder' => 'Jumlah Bayar', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>


                                </div>
                                <div class="form-group" style="margin-top:50px">
                                    <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>


    var table;
    var dataset = <?php echo json_encode(@$listdetail) ?>;
    var listheader = <?php echo json_encode(@$listheader) ?>;
    $("#dd_type_pembulatan").change(function () {
        HitungSemua();
    })
    $("#btt_add_child").click(function () {
        var message = "";
        if ($("#dd_id_barang").val() == "0")
        {
            message += "Barang Harus Dipilih<br/>";
        }
        if ($("#txt_quantity").val() <= 0)
        {
            message += "Quantity Harus Diisi<br/>";
        }

        if (message == "")
        {
            var pricelist = Number($("#txt_price_list").val().replace(/[^0-9\.]+/g, ""));
            var disc1 = Number($("#txt_disc_1").val().replace(/[^0-9\.]+/g, ""));
            var disc2 = Number($("#txt_disc_2").val().replace(/[^0-9\.]+/g, ""));
            var qty = Number($("#txt_quantity").val().replace(/[^0-9\.]+/g, ""));
            var subtotal = pricelist * ((100 - disc1) / 100) * ((100 - disc2) / 100) * qty;
            var detail = {};
            detail.id_pembelian_po_detail = 0;
            detail.id_pembelian_detail = 0;
            detail.id_barang = $("#dd_id_barang").val();
            detail.price_barang = pricelist;
            detail.nama_barang = $("#dd_id_barang :selected").text();
            detail.sku = $("#txt_kode_barang").val();
            detail.qty = qty;
            detail.disc_1 = disc1;
            detail.disc_2 = disc2;
            detail.subtotal = subtotal;
            dataset.push(detail);
            $("#dd_id_barang").val(0);
            $("#dd_id_barang").trigger('change');
            RefreshGrid();

        } else
        {
            messageerror(message);
        }
    })
    function RefreshGrid()
    {
        table.fnClearTable();
        if (dataset.length > 0)
        {
            table.fnAddData(dataset);
            HitungSemua();
        }

    }
    function RefreshPPN()
    {
        if ($("#dd_type_ppn").val() != 0)
        {
            $("#txt_ppn").removeAttr("readonly");
        } else
        {
            $("#txt_ppn").attr("readonly", "readonly");
        }
    }
    $("#dd_type_ppn").change(function () {
        if ($("#dd_type_ppn").val() != 0)
        {
            $("#txt_ppn").val(10);
        } else
        {
            $("#txt_ppn").val(0);
        }
        RefreshPPN();
        HitungSemua();
    })
    function HitungSemua() {
        var total = 0;
        var nominalppn = 0;
        var ppn = Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""));
        if (dataset.length > 0)
        {
            $.each(dataset, function (key, value) {
                total += parseFloat(value.subtotal);
            });
        }
        grandtotal = total;
        
        if ($("#dd_type_pembulatan").val() == "1")
        {   
             total = total - Number($("#txt_disc_pembulatan").val().replace(/[^0-9\.]+/g, ""));
             grandtotal = grandtotal - Number($("#txt_disc_pembulatan").val().replace(/[^0-9\.]+/g, ""));
        }
       
        
        if ($("#dd_type_ppn").val() == "2")
        {
            nominalppn = ppn / 100 * grandtotal;
            grandtotal = grandtotal + nominalppn;
        } else if ($("#dd_type_ppn").val() == "1")
        {
            nominalppn = ppn / (100 + Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""))) * grandtotal;
            total = total - nominalppn;
        }
       if ($("#dd_type_pembulatan").val() == "2")
        {
            grandtotal = grandtotal - Number($("#txt_disc_pembulatan").val().replace(/[^0-9\.]+/g, ""));
        }
        $("#txt_grandtotal").val(number_format(grandtotal.toFixed(4)));
        $("#txt_nominal_ppn").val(number_format(nominalppn.toFixed(4)));
        $("#txt_total").val(number_format(total.toFixed(4)));
    }
    function deleterow(indrow) {
        dataset.splice(indrow, 1);
        RefreshGrid();
    }
    $(document).ready(function () {
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            scrollX: false,
            data: dataset,
            columns: [
                {
                    data: "id_pembelian_detail",
                    title: "Kode",
                    orderable: false
                }
                , {data: "sku", orderable: false, title: "SKU"}
                , {data: "nama_barang", orderable: false, title: "Nama"}
                , {data: "qty", orderable: false, title: "Qty"}
                , {data: "price_barang", orderable: false, title: "Harga Final",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                , {data: "disc_1", orderable: false, title: "Disc 1"}
                , {data: "disc_2", orderable: false, title: "Disc 2"}
                , {data: "subtotal", orderable: false, title: "Subtotal",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                ,
                {data: "id_pembelian_po_detail",
                    mRender: function (data, type, row, iDisplayIndex) {
                        return "<button onclick='deleterow(" + iDisplayIndex.row + ")' type=\"button\" data-id='" + iDisplayIndex.row + "' class=\"btn-xs btn-danger waves-effect btn-block\" >Delete</button>";
                    }
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

            },
            initComplete: function () {
                HitungSemua();
            }
        });



    })
    function RefreshDetail() {
        var pricelist = Number($("#txt_price_list").val().replace(/[^0-9\.]+/g, ""));
        var disc1 = Number($("#txt_disc_1").val().replace(/[^0-9\.]+/g, ""));
        var disc2 = Number($("#txt_disc_2").val().replace(/[^0-9\.]+/g, ""));
        var qty = Number($("#txt_quantity").val().replace(/[^0-9\.]+/g, ""));
        var subtotal = pricelist * ((100 - disc1) / 100) * ((100 - disc2) / 100) * qty;
        $("#txt_subtotal").val(Comma(subtotal));
    }

    function RefreshPanelGrid()
    {
        $("#dd_id_barang").val(0);
        $("#dd_id_barang").trigger('change');
        if ($("#dd_id_cabang").val() == "0" || $("#dd_id_gudang").val() == "0")
        {
            $("#frm_pembelian button").attr("disabled", "disabled");
            $("#txt_price_list").attr("disabled", "disabled");
            $("#txt_disc_1").attr("disabled", "disabled");
            $("#txt_disc_2").attr("disabled", "disabled");
            $("#txt_quantity").attr("disabled", "disabled");
            $("#txt_keterangan").attr("disabled", "disabled");
            $("#dd_id_barang").attr("disabled", "disabled");
            $("#txt_kepada").attr("disabled", "disabled");
            $("#txt_no_fax").attr("disabled", "disabled");
            $("#txt_pembuat").attr("disabled", "disabled");
        } else
        {
            $("#frm_pembelian button").removeAttr("disabled");
            $("#txt_price_list").removeAttr("disabled");
            $("#txt_disc_1").removeAttr("disabled");
            $("#txt_disc_2").removeAttr("disabled");
            $("#txt_quantity").removeAttr("disabled");
            $("#txt_keterangan").removeAttr("disabled");
            $("#dd_id_barang").removeAttr("disabled");
            $("#txt_kepada").removeAttr("disabled");
            $("#txt_no_fax").removeAttr("disabled");
            $("#txt_pembuat").removeAttr("disabled");
        }
    }

    $(document).ready(function () {
        $("#txt_tanggal").datepicker().on('changeDate', function (e) {
                $("#txt_jth_tempo").val(DefaultDateFormat(e.date.addMonths(1))); //Where e contains date, dates and format
        });
        $("#txt_jth_tempo").datepicker();
        RefreshPPN();
        $("#dd_id_barang , #dd_id_supplier").select2();
        $("#dd_id_cabang , #dd_id_gudang ").change(function () {
            RefreshPanelGrid();
        })
        $("#dd_id_barang").change(function () {
            RefreshStock();
        })
        RefreshPanelGrid();

    })
    $("#txt_price_list , #txt_disc_1 ,#txt_disc_2 , #txt_quantity").change(function () {
        if ($("#dd_id_barang").val() != "0")
            RefreshDetail();
    })
    $("#btt_search_po").click(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/pembelian/searchpo',
            data: $("#frm_pembelian").serialize() + '&' + $.param({"dataset": dataset}),
            success: function (data) {
                modalbootstrap(data, "Search PO", "80%");

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });

    })
    $("#frm_pembelian").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data pembelian berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/pembelian/pembelian_manipulate',
                    dataType: 'json',
                    data: $("#frm_pembelian").serialize() + '&' + $.param({"dataset": dataset,"listheader":listheader}),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href="<?php echo base_url()?>index.php/pembelian";
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;


    })
    function RefreshStock() {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/barang/get_stock_barang',
            dataType: 'json',
            data: $("form#frm_pembelian").serialize(),
            success: function (data) {
                $("#txt_stock").val(Comma(data.stock));
                $("#txt_harga_beli").val(Comma(data.harga_beli_akhir));
                $("#txt_kode_barang").val(data.sku_barang);
                $("#txt_price_list").val(Comma(data.harga_beli_akhir));
                $("#txt_quantity").val("1");
                RefreshDetail();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });


    }
</script>