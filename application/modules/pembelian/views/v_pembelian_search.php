<div class="modal-header">
    Pencarian Pembelian
</div>
<div class="modal-body" style="min-width:100px;">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="portlet-body form">

                    <table class="table table-striped table-bordered table-hover" id="mytablesearch" style="width:100%">

                    </table>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="detailpo">

            </div>
        </div>
    </div>
</div>
<script>
    var tablesearch;
    $(document).ready(function () {
        setTimeout(function () {
            InitialGridSearch();
        }, 500);
    })

    function select(id_pembelian)
    {
        $("#btt_search_po").css("display", "none");
        listheader = [];
       
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/pembelian/GetOnePembelian',
            dataType: 'json',
            data: {
                id_pembelian_master:id_pembelian
            },
            success: function (data) {
                if(data.st)
                {
                    var header = {};
                    header.id_pembelian_master =data.obj.id_pembelian_master;
                    header.nomor_master = data.obj.nomor_master;
                    header.tanggal = data.obj.tanggal;
                    dataset=data.obj.listdetail;
                    listheader.push(header);
                    $("#dd_id_supplier").val(data.obj.id_supplier);
                    if($("#dd_id_supplier").val()!=null)
                    {
                        $("#dd_id_supplier").attr("disabled","disabled");
                    }
                    $("#dd_id_cabang").val(data.obj.id_cabang);
                    if($("#dd_id_cabang").val()==null)
                    {
                        $("#dd_id_cabang").val(0);
                    }
                    $("#dd_id_gudang").val(data.obj.id_gudang);
                    if($("#dd_id_gudang").val()==null)
                    {
                        $("#dd_id_gudang").val(0);
                    }
                    
                    $("#txt_id_pembelian_master").val(data.obj.id_pembelian_master);
                    $(".kolomheaderpo").append("Nomor PO : " + DefaultDateFormat( data.obj.tanggal) + " == " +  data.obj.nomor_master + "<br/>");
                    RefreshGrid();
                    RefreshPanelGrid();
                }
                else
                {
                    modaldialogerror("Pembelian tidak ada dalam database");
                }
             
            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });

        

        
        closemodalboostrap();
    }
    function InitialGridSearch()
    {
        tablesearch = $("#mytablesearch").dataTable({

            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": baseurl + "index.php/pembelian/getdatapembelianforpenerimaan", "type": "Post"},
            columns: [
                {data: "tanggal", orderable: false, title: "Tanggal",
                    mRender: function (data, type, row) {
                        return DefaultDateFormat(data);
                    }}
                , {data: "nama_supplier", orderable: false, title: "Supplier"}
                , {data: "nomor_master", orderable: false, title: "NO Pembelian"}
                , {data: "nomor_po_master", orderable: false, title: "NO PO"},
                {
                    "data": "id_pembelian_master",
                    "orderable": false,
                    "className": "text-center nopadding",
                    mRender: function (data, type, row) {
                        return "<a href=\"javascript:;\" class=\"btn  btn-success btn-xs\" onclick=\"select(" + data + ");return false;\">Pilih</a>";
                    }
                }
            ],
            order: [[0, 'desc']],
            initComplete: function () {

            }

        });
    }



</script>