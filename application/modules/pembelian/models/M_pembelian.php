<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_pembelian extends CI_Model {

    public $table = 'uap_pembelian';
    public $id = 'id_pembelian_po_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function GetDetailPo($id) {
        $this->db->from("uap_pembelian_po_detail");
        $this->db->join("uap_satuan", "uap_pembelian_po_detail.id_satuan=uap_satuan.id_satuan", "left");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_pembelian_po_detail.id_barang", "left");
        $this->db->select("uap_pembelian_po_detail.*,sku_barang as sku,nama_satuan,harga_beli_akhir");
        $this->db->where(array("id_pembelian_po_master" => $id, "uap_pembelian_po_detail.status !=" => 5));
        $listdetail = $this->db->get()->result();
        return $listdetail;
    }
     
    function GetListPenerimaan($id, $id_pembelian = 0,$fromretur=0) {
        $this->db->from("uap_pembelian_detail");
        $this->db->join("uap_penerimaan_pembelian_detail", "uap_pembelian_detail.id_pembelian_detail=uap_penerimaan_pembelian_detail.id_pembelian_detail and uap_penerimaan_pembelian_detail.id_penerimaan=" . $id, "left");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_pembelian_detail.id_barang");
        $this->db->select("uap_pembelian_detail.price_barang,uap_pembelian_detail.ppn_satuan,uap_penerimaan_pembelian_detail.id_penerimaan_detail,uap_barang.sku_barang as sku,uap_pembelian_detail.id_pembelian_detail,uap_pembelian_detail.nama_barang,uap_pembelian_detail.qty,ifnull(uap_penerimaan_pembelian_detail.qty,0) as jumlahditerima");
        $this->db->where(array("uap_pembelian_detail.id_pembelian_master" => $id_pembelian));
        
        if ($fromretur) {
           $this->db->where(array("id_penerimaan_detail !=" =>$id)); 
        }
        $this->load->model("retur/m_retur_beli");
        $listdetail = $this->db->get()->result();
        foreach ($listdetail as $detail) {
            $detail->jumlahpenerimaan = $this->GetJumlahPenerimaan(@$detail->id_pembelian_detail) - $detail->jumlahditerima;
            $detail->jumlahdiretur =$this->m_retur_beli->GetJumlahRetur(@$detail->id_penerimaan_detail);
            
           
        }
        return $listdetail;
    }

    function GetOnePenerimaanPembelian($id, $isfull = false,$fromretur=false) {
        $this->db->from("uap_penerimaan_pembelian");
        if($isfull)
        {   
            
            $this->db->join("uap_pembelian_master", "uap_pembelian_master.id_pembelian_master=uap_penerimaan_pembelian.id_pembelian_master", "left");
            $this->db->join("uap_supplier", "uap_supplier.id_supplier=uap_penerimaan_pembelian.id_supplier", "left");
            $this->db->join("uap_gudang", "uap_gudang.id_gudang=uap_penerimaan_pembelian.id_gudang", "left");
            $this->db->join("uap_cabang", "uap_cabang.id_cabang=uap_penerimaan_pembelian.id_cabang", "left");
            $this->db->select("uap_penerimaan_pembelian.*,uap_pembelian_master.ppn,uap_pembelian_master.type_pembulatan,uap_pembelian_master.type_ppn,uap_pembelian_master.nomor_master,uap_supplier.nama_supplier,uap_cabang.nama_cabang,uap_gudang.nama_gudang,uap_pembelian_master.tanggal");
        }
        $this->db->where(array("id_penerimaan" => $id));
        $row = $this->db->get()->row();
        if ($row && $isfull) {
            $row->listdetail = $this->GetListPenerimaan($id, $row->id_pembelian_master,$fromretur);
        }
        return $row;
    }
    
    function GetJumlahPenerimaan($id,$jenis="pembelian") {
        $this->db->from("uap_penerimaan_pembelian_detail");
        $this->db->join("uap_penerimaan_pembelian", "uap_penerimaan_pembelian.id_penerimaan=uap_penerimaan_pembelian_detail.id_penerimaan", "left");
        if($jenis=="pembelian")
            $this->db->where(array("uap_penerimaan_pembelian_detail.id_pembelian_detail" => $id, "uap_penerimaan_pembelian.status !=" => 2));
        else
            $this->db->where(array("uap_penerimaan_pembelian_detail.id_penerimaan_Detail" => $id, "uap_penerimaan_pembelian.status !=" => 2));
        $this->db->select("sum(qty) as qtyditerima");
        $qtyditerima = $this->db->get()->row();
        return CheckEmpty(@$qtyditerima->qtyditerima) ? 0 : $qtyditerima->qtyditerima;
    }

    public function UpdateStatusPembelian($id) {
        $listdetail = $this->GetDetail($id, true);
        $isfull = true;
        foreach ($listdetail as $detail) {
            if ($detail->jumlahpenerimaan < $detail->qty) {
                $isfull = false;
            }
        }
        if ($isfull) {
            $this->db->from("uap_penerimaan_pembelian");
            $this->db->where(array("id_pembelian_master" => $id, "status !=" => 2));
            $this->db->order_by("tanggal_penerimaan", "desc");
            $row = $this->db->get()->row();
            if ($row != null) {
                $this->db->update("uap_pembelian_master", array("tgl_pengiriman" => $row->tanggal_penerimaan), array("id_pembelian_master" => $id));
            }
        } else {
            $this->db->update("uap_pembelian_master", array("tgl_pengiriman" => null), array("id_pembelian_master" => $id));
        }
    }

    function GetDatapenerimaan($params) {

        $isedit = CekModule("K103", false);
        $isdelete = CekModule("K104", false);
        $this->load->library('datatables');
        $this->datatables->select('uap_penerimaan_pembelian.tanggal_penerimaan,uap_penerimaan_pembelian.id_penerimaan,nama_gudang,nama_cabang,nama_supplier,uap_pembelian_master.tanggal,replace(kode_penerimaan,"/","-") as nomor_master_link,kode_penerimaan,nomor_master,uap_penerimaan_pembelian.status');
        $this->datatables->join("uap_supplier", "uap_supplier.id_supplier=uap_penerimaan_pembelian.id_supplier", "left");
        $this->datatables->join("uap_pembelian_master", "uap_pembelian_master.id_pembelian_master=uap_penerimaan_pembelian.id_pembelian_master", "left");
        $this->datatables->join("uap_cabang", "uap_cabang.id_cabang=uap_penerimaan_pembelian.id_cabang", "left");
        $this->datatables->join("uap_gudang", "uap_gudang.id_gudang=uap_penerimaan_pembelian.id_gudang", "left");
        
        $this->datatables->from('uap_penerimaan_pembelian');
        $straction = '';
        $extra = array();
        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['uap_penerimaan_pembelian.id_cabang'] = $params['id_cabang'];
        }
        if (isset($params['id_supplier']) && !CheckEmpty(@$params['id_supplier'])) {
            $params['uap_penerimaan_pembelian.id_supplier'] = $params['id_supplier'];
        }
        if (isset($params['id_gudang']) && !CheckEmpty(@$params['id_gudang'])) {
            $params['uap_penerimaan_pembelian.id_gudang'] = $params['id_gudang'];
        }
        if(!CheckEmpty($params['status']))
        {
            $params['uap_penerimaan_pembelian.status']=$params['status'];
        }
       
        unset($params['id_supplier']);
        unset($params['id_gudang']);
        unset($params['id_cabang']);
        unset($params['status']);


        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "uap_penerimaan_pembelian.tanggal_penerimaan BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tanggal_penerimaan'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }
       

        if (count($params)) {
            $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        $stredit='';
        $strdelete='';
        $strview = anchor("", 'View', array('class' => 'btn btn-default btn-xs', "onclick" => "view($1);return false;"));
        if ($isedit) {
            $stredit = anchor("pembelian/editpenerimaan/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $strdelete .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batal($1);return false;"));
        }
        $this->datatables->add_column('action_view', $strview, 'id_penerimaan,nomor_master_link');
        $this->datatables->add_column('action_edit', $stredit, 'id_penerimaan,nomor_master_link');
        $this->datatables->add_column('action_delete', $strdelete, 'id_penerimaan,nomor_master_link');
        return $this->datatables->generate();
    }

    function BatalPenerimaan($id_penerimaan) {
        $penerimaan = $this->GetOnePenerimaanPembelian($id_penerimaan);
        $message = "";
        if ($penerimaan != null) {
            if ($penerimaan->status == "2")
                $message .= "Penerimaan sudah dibatalkan sebelumnya<br/>";
        }
        else {
            $message .= "Penerimaan tidak ada di database<br/>";
        }
        if ($message=="") {
            $this->db->trans_rollback();
            $this->db->trans_begin();

            $this->db->from("uap_penerimaan_pembelian_detail");
            $this->db->where(array("id_penerimaan" => $id_penerimaan));
            
            try {
                $listdetail = $this->db->get()->result();
                foreach($listdetail as $detail)
                {
                    $this->db->from("uap_barang_history");
                    $this->db->where(array("id_barang_history"=>$detail->id_barang_history));
                    $rowhistory=$this->db->get()->row();
                    if($rowhistory==null)
                    {
                        $message.="History Untuk barang ".$detail->nama_barang ." tidak ada dalam database<br/>";
                        break;
                    }
                    else if($rowhistory!=null && $rowhistory->stock_tersedia!=$rowhistory->stock_sisa)
                    {
                        $message.="Sudah pernah terjadi transaksi untuk  ".$detail->nama_barang ."<br/>";
                        break;
                    }
                    $this->db->update("uap_barang_history",array("stock_tersedia"=>0,"stock_sisa"=>0),array("id_barang_history"=>$rowhistory->id_barang_history));
                    $this->db->from("uap_barang_jurnal");
                    $this->db->where(array("dokumen" => $penerimaan->kode_penerimaan,"reference_id"=>$detail->id_penerimaan_detail));
                    $rowjurnal = $this->db->get()->row();
                    if ($rowjurnal == null) {
                        $message .= "Jurnal Untuk barang " . $detail->nama_barang . " tidak ada dalam database<br/>";
                        break;
                    } else if ($rowjurnal != null && $rowjurnal->debet ==0 &&  $rowjurnal->kredit==0) {
                        $message .= "Sudah pernah terjadi pembatalan transaksi untuk  " . $detail->nama_barang . "<br/>";
                        break;
                    }
                    $this->db->update("uap_barang_jurnal", array("debet" => 0, "kredit" => 0), array("id_jurnal_barang" => $rowjurnal->id_jurnal_barang));
                }
                if($message=="")
                {
                    $this->db->update("uap_penerimaan_pembelian",array("status"=>2,"updated_date"=> GetDateNow(),"updated_by"=> GetUserId()),array("id_penerimaan"=>$id_penerimaan));
                }
                $this->UpdateStatusPembelian($penerimaan->id_pembelian_master);

                if ($this->db->trans_status() === FALSE || $message != '') {
                    $this->db->trans_rollback();
                    $message = "Some Error Occured<br/>" . $message;
                    return array("st" => false, "msg" => $message);
                } else {
                    $this->db->trans_commit();
                    $arrayreturn["st"] = true;
                    $arrayreturn["msg"] =  "Data Penerimaan ".$penerimaan->kode_penerimaan ." sudah dibatalkan";
                    return $arrayreturn;
                }
            } catch (Exception $ex) {
                $this->db->trans_rollback();
                return array("st" => false, "msg" => $ex->getMessage());
            }
        }
        else{
             return array("st" => false, "msg" => $message);
        }
    }

    function PenerimaanPembelian($model) {
        try {
            $message = "";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            $this->load->model("barang/m_barang");
            $this->load->model("cabang/m_cabang");
            $pembelian = $this->GetOnePembelian($model['id_pembelian_master']);
            $penerimaan['tanggal_penerimaan'] = DefaultTanggalDatabase($model['tanggal']);
            $penerimaan['id_supplier'] = $pembelian->id_supplier;
            $penerimaan['keterangan'] = $model['keterangan'];
            $penerimaan['id_cabang'] = $model['id_cabang'];
            $penerimaan['id_gudang'] = $model['id_gudang'];
            $penerimaan['id_pembelian_master'] = $model['id_pembelian_master'];
            $listidpembelian = [];
            $listpenerimaan = [];
            $messageerror = "";
            if (CheckArray($model, 'id_pembelian_detail')) {
                $listidpembelian = $model['id_pembelian_detail'];
            }
            if (CheckArray($model, 'penerimaan')) {
                $listpenerimaan = $model['penerimaan'];
            }
            $kodepenerimaan = '';
            $id_penerimaan = 0;
            if (!CheckEmpty($model['id_penerimaan'])) {
                $penerimaanfromdb = $this->GetOnePenerimaanPembelian($model['id_penerimaan']);
                if ($penerimaanfromdb != null) {
                    $kodepenerimaan = $penerimaanfromdb->kode_penerimaan;
                }
                $penerimaan['updated_date'] = GetDateNow();
                $penerimaan['updated_by'] = $userid;
                $this->db->update("uap_penerimaan_pembelian", $penerimaan, array("id_penerimaan" => $model['id_penerimaan']));
                $id_penerimaan = $model['id_penerimaan'];
            } else {
                $cabang = $this->m_cabang->GetOneCabang($model['id_cabang']);
                $kodepenerimaan = AutoIncrement('uap_penerimaan_pembelian', $cabang->kode_cabang . '/RPB/' . date("y") . '/', 'kode_penerimaan', 5);
                $penerimaan['kode_penerimaan'] = $kodepenerimaan;
                $penerimaan['created_date'] = GetDateNow();
                $penerimaan['created_by'] = $userid;
                $penerimaan['status'] = 1;
                $this->db->insert("uap_penerimaan_pembelian", $penerimaan);
                $id_penerimaan = $this->db->insert_id();
            }

            foreach ($listidpembelian as $key => $itempembelian) {
                $this->db->from("uap_penerimaan_pembelian_detail");
                $listpenerimaan[$key] = DefaultCurrencyDatabase($listpenerimaan[$key]);
                $this->db->where(array("id_pembelian_detail" => $itempembelian, "id_penerimaan" => $id_penerimaan));
                $rowitem = $this->db->get()->row();
                $updateitem = [];
                $updateitem['qty'] = $listpenerimaan[$key];
                $detailpembelian = $this->GetDetailPembelianSummary($itempembelian);

                if ($detailpembelian == null) {
                    $messageerror .= "Ada Detail Pembelian yang tidak terdapat di system<br/>";
                } else {


                    if ($detailpembelian->qtyditerima - (CheckEmpty($rowitem) ? 0 : $rowitem->qty) + $listpenerimaan[$key] > $detailpembelian->qty) {
                        $messageerror .= "Detail Pembelian " . $detailpembelian->nama_barang . " lebih besar daripada qty pembelian<br/>";
                        break;
                    }

                    $updateitem['nama_barang'] = $detailpembelian->nama_barang;
                    $updateitem['id_barang'] = $detailpembelian->id_barang;
                    $updateitem['id_pembelian_detail'] = $detailpembelian->id_pembelian_detail;
                    $updateitem['qty'] = $listpenerimaan[$key];
                    $id_history = 0;
                    $barang_history = null;
                    if ($rowitem != null)
                        $barang_history = $this->m_barang->GetDetailHistoryBarang($rowitem->id_barang_history);
                    if ($barang_history != null) {
                        $id_history = $barang_history->id_barang_history;
                        if ($barang_history->id_gudang != $model['id_gudang'] && $barang_history->stock_tersedia != $barang_history->stock_sisa) {
                            $messageerror .= $detailpembelian->nama_barang .= " sudah pernah dikeluarkan di gudang sebelumnya>br/>";
                            break;
                        } else if ($barang_history->stock_tersedia - $barang_history->stock_sisa > $listpenerimaan[$key]) {
                            $messageerror .= $detailpembelian->nama_barang . " sudah dikeluarkan dan hanya  memiliki sisa " . DefaultCurrency($barang_history->stock_sisa);
                            break;
                        } else {
                            $historybar = [];
                            $historybar['stock_tersedia'] = $listpenerimaan[$key];
                            $historybar['stock_sisa'] = $barang_history->stock_sisa - ($barang_history->stock_tersedia - $listpenerimaan[$key]);
                            $historybar['updated_date'] = GetDateNow();
                            $historybar['tanggal'] = $penerimaan['tanggal_penerimaan'];
                            $historybar['updated_by'] = $userid;
                            $historybar['harga_ref'] = $detailpembelian->price_barang + $detailpembelian->ppn_satuan;
                            $this->db->update("uap_barang_history", $historybar, array("id_barang_history" => $barang_history->id_barang_history));
                        }
                    } else if ($listpenerimaan[$key] > 0) {
                        $historybar = [];
                        $historybar['stock_tersedia'] = $listpenerimaan[$key];
                        $historybar['stock_sisa'] = $listpenerimaan[$key];
                        $historybar['created_date'] = GetDateNow();
                        $historybar['created_by'] = $userid;
                        $historybar['tanggal'] = $penerimaan['tanggal_penerimaan'];
                        $historybar['dokumen'] = $kodepenerimaan;
                        $historybar['id_gudang'] = $model['id_gudang'];
                        $historybar['id_barang'] = $detailpembelian->id_barang;
                        $historybar['harga_ref'] = $detailpembelian->price_barang + $detailpembelian->ppn_satuan;
                        $this->db->insert("uap_barang_history", $historybar);
                        $id_history = $this->db->insert_id();
                    }


                    $updateitem['id_barang_history'] = $id_history;
                    $id_penerimaanid_detail = 0;
                    if ($rowitem != null) {
                        $updateitem['updated_date'] = GetDateNow();
                        $updateitem['updated_by'] = $userid;
                        $this->db->update("uap_penerimaan_pembelian_detail", $updateitem, array("id_penerimaan_detail" => $rowitem->id_penerimaan_detail));
                        $id_penerimaanid_detail = $rowitem->id_penerimaan_detail;
                    } else if ($listpenerimaan[$key] > 0) {
                        $updateitem['id_penerimaan'] = $id_penerimaan;
                        $updateitem['created_date'] = GetDateNow();
                        $updateitem['created_by'] = $userid;
                        $this->db->insert("uap_penerimaan_pembelian_detail", $updateitem);
                        $id_penerimaanid_detail = $this->db->insert_id();
                    }

                    $this->db->from("uap_barang_jurnal");
                    $this->db->where(array("dokumen" => $kodepenerimaan, "reference_id" => $id_penerimaanid_detail));
                    $jurnal = $this->db->get()->row();
                    $updatejurnal = [];
                    $updatejurnal['id_barang_history'] = $id_history;
                    $updatejurnal['id_barang'] = $detailpembelian->id_barang;
                    $updatejurnal['debet'] = $listpenerimaan[$key];
                    $updatejurnal['kredit'] = 0;
                    $updatejurnal['tanggal_jurnal'] = $penerimaan['tanggal_penerimaan'];
                    $updatejurnal['id_gudang'] = $model['id_gudang'];

                    if ($jurnal != null) {


                        $updatejurnal['updated_date'] = GetDateNow();
                        $updatejurnal['updated_by'] = $userid;
                        $this->db->update("uap_barang_jurnal", $updatejurnal, array("id_jurnal_barang" => $jurnal->id_jurnal_barang));
                    } else if ($listpenerimaan[$key] > 0) {
                        $updatejurnal['reference_id'] = $id_penerimaanid_detail;
                        $updatejurnal['dokumen'] = $kodepenerimaan;
                        $updatejurnal['description'] = "Penerimaan Pembelian " . $pembelian->nomor_master;
                        $updatejurnal['harga_beli_akhir'] = $detailpembelian->price_barang;
                        $updatejurnal['created_date'] = GetDateNow();
                        $updatejurnal['created_by'] = $userid;
                        $updatejurnal['harga'] = $detailpembelian->price_barang;
                        $this->db->insert("uap_barang_jurnal", $updatejurnal);
                    }
                    $this->m_barang->SyncronizeHargaAverage($updatejurnal['id_barang'], $updatejurnal['id_gudang'], $updatejurnal['tanggal_jurnal']);
                }
            }
            $this->UpdateStatusPembelian($model['id_pembelian_master']);

            $message .= $messageerror;

            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();

                SetMessageSession(true, "Data Pembelian PO Sudah di" . (CheckEmpty(@$model['id_pembelian_po_master']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_penerimaan, $kodepenerimaan, 'penerimaan');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDetailPembelianSummary($id) {
        $this->db->from("uap_pembelian_detail");
        $this->db->where(array("id_pembelian_detail" => $id));
        $row = $this->db->get()->row();
        if ($row) {
            $row->qtyditerima = $this->GetJumlahPenerimaan($id);
        }

        return $row;
    }
    
    
    function GetDetailPenerimaanSummary($id) {
        $this->db->from("uap_penerimaan_pembelian_detail");
        $this->db->where(array("id_penerimaan_detail" => $id));
        $row = $this->db->get()->row();
        $this->load->model("retur/m_retur_beli");
        if ($row) {
            $row->qtyretur = $this->m_retur_beli->GetJumlahRetur($id,"penerimaan");
        }

        return $row;
    }

    function GetDetail($id, $terima = false) {
        $this->db->select("uap_pembelian_detail.*,sku_barang as sku,nama_satuan,harga_beli_akhir");
        $this->db->from("uap_pembelian_detail");
        $this->db->join("uap_satuan", "uap_pembelian_detail.id_satuan=uap_satuan.id_satuan", "left");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_pembelian_detail.id_barang", "left");

        $this->db->where(array("id_pembelian_master" => $id, "uap_pembelian_detail.status !=" => 5));

        $listdetail = $this->db->get()->result();
        if ($terima) {
            foreach ($listdetail as $detail) {
                $detail->jumlahpenerimaan = $this->GetJumlahPenerimaan($detail->id_pembelian_detail);
            }
        }

        return $listdetail;
    }

    function GetOnePembelianPo($id, $isfull = false) {
        $this->db->from("uap_pembelian_po_master");
        $this->db->where(array("id_pembelian_po_master" => $id));
        $row = $this->db->get()->row();
        if ($isfull) {
            $row->listdetail = $this->GetDetailPo($id);
        }

        return $row;
    }

    function GetOnePembelian($id, $isfull = false, $terima = false) {
        $this->db->from("uap_pembelian_master");
        $this->db->where(array("id_pembelian_master" => $id));
        $row = $this->db->get()->row();
        if ($isfull) {

            $row->listdetail = $this->GetDetail($id, $terima);
            $this->db->from("uap_pembelian_link");
            $this->db->join("uap_pembelian_po_master", "uap_pembelian_po_master.id_pembelian_po_master=uap_pembelian_link.id_pembelian_po_master");
            $this->db->where(array("uap_pembelian_link.id_pembelian_master" => $id));
            $this->db->select("uap_pembelian_po_master.id_pembelian_po_master,nomor_po_master,tgl_po");
            $row->listheader = $this->db->get()->result();
        }

        return $row;
    }

    function PembelianPOManipulate($model) {
        try {
            $message = "";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            $pembelianpo = array();
            $this->load->model("cabang/m_cabang");
            $this->load->model("barang/m_barang");
            $cabang = $this->m_cabang->GetOneCabang($model['id_cabang']);
            $pembelianpo['kepada'] = $model['kepada'];
            $pembelianpo['no_fax'] = $model['no_fax'];
            $pembelianpo['pembuat'] = $model['pembuat'];

            $pembelianpo['keterangan'] = $model['keterangan'];
            $pembelianpo['id_supplier'] = ForeignKeyFromDb($model['id_supplier']);
            $pembelianpo['id_gudang'] = ForeignKeyFromDb($model['id_gudang']);
            $pembelianpo['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
            $pembelianpo['tgl_po'] = DefaultTanggalDatabase($model['tgl_po']);
            $pembelianpo['nominal_po'] = 0;
            $id_pembelian = 0;
            if (CheckEmpty(@$model['id_pembelian_po_master'])) {
                $pembelianpo['created_date'] = GetDateNow();
                $pembelianpo['nomor_po_master'] = AutoIncrement('uap_pembelian_po_master', $cabang->kode_cabang . '/PO/' . date("y") . '/', 'nomor_po_master', 5);
                $pembelianpo['created_by'] = ForeignKeyFromDb($userid);
                $pembelianpo['status'] = '1';
                $this->db->insert("uap_pembelian_po_master", $pembelianpo);
                $id_pembelian = $this->db->insert_id();
            } else {
                $pembelianpo['updated_date'] = GetDateNow();
                $pembelianpo['updated_by'] = ForeignKeyFromDb($userid);
                $id_pembelian = $model['id_pembelian_po_master'];
                $pembeliandb = $this->GetOnePembelianPo($id_pembelian);
                $pembelianpo['nomor_po_master'] = $pembeliandb->nomor_po_master;
                $this->db->update("uap_pembelian_po_master", $pembelianpo, array("id_pembelian_po_master" => $id_pembelian));
            }
            $arraydetailpembelian = array();
            foreach ($model['dataset'] as $key => $det) {
                $detailpembelian = array();
                $detailpembelian['id_barang'] = $det['id_barang'];
                $detailpembelian['price_barang'] = DefaultCurrencyDatabase($det['price_barang']);
                $detailpembelian['nama_barang'] = $det['nama_barang'];
                $detailpembelian['qty'] = DefaultCurrencyDatabase($det['qty']);
                $detailpembelian['disc_1'] = DefaultCurrencyDatabase($det['disc_1']);
                $detailpembelian['disc_2'] = DefaultCurrencyDatabase($det['disc_2']);
                $detailpembelian['subtotal'] = DefaultCurrencyDatabase($det['subtotal']);

                if (CheckEmpty($det['id_pembelian_po_detail'])) {
                    $barang = $this->m_barang->GetOneBarang($det['id_barang']);
                    $detailpembelian['id_satuan'] = ForeignKeyFromDb(@$barang->id_satuan);
                    $detailpembelian['created_date'] = GetDateNow();
                    $detailpembelian['created_by'] = $userid;
                    $detailpembelian['status'] = 1;
                    $detailpembelian['id_pembelian_po_master'] = $id_pembelian;
                    $this->db->insert("uap_pembelian_po_detail", $detailpembelian);
                    $arraydetailpembelian[] = $this->db->insert_id();
                } else {

                    $detailpembelian['updated_date'] = GetDateNow();
                    $detailpembelian['updated_by'] = $userid;
                    if (!CheckEmpty(@$det['id_satuan']))
                        $detailpembelian['id_satuan'] = ForeignKeyFromDb($det['id_satuan']);
                    $this->db->update("uap_pembelian_po_detail", $detailpembelian, array("id_pembelian_po_detail" => $det['id_pembelian_po_detail']));
                    $arraydetailpembelian[] = $det['id_pembelian_po_detail'];
                }
            }


            if (count($arraydetailpembelian) > 0) {
                $this->db->where_not_in("id_pembelian_po_detail", $arraydetailpembelian);
                $this->db->where(array("id_pembelian_po_master" => $id_pembelian));
                $this->db->update("uap_pembelian_po_detail", array("status" => 5, 'updated_date' => GetDateNow(), 'updated_by' => $userid));
            } else {
                $this->db->update("uap_pembelian_po_detail", array("status" => 5, 'updated_date' => GetDateNow()), array("id_pembelian_po_master" => $pembelianpo));
            }




            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();

                SetMessageSession(true, "Data Pembelian PO Sudah di" . (CheckEmpty(@$model['id_pembelian_po_master']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_pembelian, $pembelianpo['nomor_po_master'], 'pembelianpo');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function PembelianManipulate($model) {


        try {
            $message = "";
            $listheader = CheckArray($model, 'listheader');
            $listpopembelian = array();
            foreach ($listheader as $headersatuan) {
                array_push($listpopembelian, array("id_pembelian_po_master" => $headersatuan['id_pembelian_po_master']));
            }

            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            $pembelian = array();
            $this->load->model("cabang/m_cabang");
            $this->load->model("supplier/m_supplier");
            $this->load->model("barang/m_barang");
            $cabang = $this->m_cabang->GetOneCabang($model['id_cabang']);

            $pembelian['keterangan'] = $model['keterangan'];
            $pembelian['id_supplier'] = ForeignKeyFromDb($model['id_supplier']);
            $pembelian['id_gudang'] = ForeignKeyFromDb($model['id_gudang']);
            $pembelian['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
            $pembelian['tanggal'] = DefaultTanggalDatabase($model['tanggal']);
            $pembelian['jth_tempo'] = DefaultTanggalDatabase($model['jth_tempo']);
            $pembelian['type_ppn'] = $model['type_ppn'];
            $pembelian['type_pembayaran'] = $model['type_pembayaran'];
            $pembelian['type_pembulatan'] = $model['type_pembulatan'];
            $pembelian['disc_pembulatan'] = DefaultCurrencyDatabase($model['disc_pembulatan']);
            $pembelian['nominal_ppn'] = DefaultCurrencyDatabase($model['nominal_ppn']);
            $pembelian['grandtotal'] = DefaultCurrencyDatabase($model['grandtotal']);
            $pembelian['jumlah_bayar'] = DefaultCurrencyDatabase($model['jumlah_bayar']) > $pembelian['grandtotal'] ? $pembelian['grandtotal'] : DefaultCurrencyDatabase($model['jumlah_bayar']);
            $pembelian['total'] = DefaultCurrencyDatabase($model['total']);
            $pembelian['ppn'] = DefaultCurrencyDatabase($model['ppn']);

            $supplier = $this->m_supplier->GetOneSupplier($pembelian['id_supplier']);
            if (CheckEmpty(@$model['id_pembelian_master'])) {
                $pembelian['created_date'] = GetDateNow();
                $pembelian['nomor_master'] = AutoIncrement('uap_pembelian_master', $cabang->kode_cabang . '/PR/' . date("y") . '/', 'nomor_master', 5);
                $pembelian['created_by'] = ForeignKeyFromDb($userid);
                $pembelian['status'] = '1';
                $this->db->insert("uap_pembelian_master", $pembelian);
                $id_pembelian = $this->db->insert_id();
            } else {
                $pembelian['updated_date'] = GetDateNow();
                $pembelian['updated_by'] = ForeignKeyFromDb($userid);
                $id_pembelian = $model['id_pembelian_master'];
                $pembeliandb = $this->GetOnepembelian($id_pembelian);
                $pembelian['nomor_master'] = $pembeliandb->nomor_master;
                $this->db->update("uap_pembelian_master", $pembelian, array("id_pembelian_master" => $id_pembelian));
            }

            $this->db->delete("uap_pembelian_link", array("id_pembelian_master" => $id_pembelian));
            foreach ($listpopembelian as $linksatuan) {
                $linksatuan['id_pembelian_master'] = $id_pembelian;
                $this->db->insert("uap_pembelian_link", $linksatuan);
            }
            $arraydetailpembelian = array();
            $totalpembelianwithoutppn = 0;
            $totalpembelianwithppn = 0;
            foreach ($model['dataset'] as $key => $det) {
                $detailpembelian = array();
                $detailpembelian['id_barang'] = $det['id_barang'];
                $detailpembelian['price_barang'] = DefaultCurrencyDatabase($det['price_barang']);
                $detailpembelian['nama_barang'] = $det['nama_barang'];
                $detailpembelian['qty'] = DefaultCurrencyDatabase($det['qty']);
                $detailpembelian['disc_1'] = DefaultCurrencyDatabase($det['disc_1']);
                $detailpembelian['disc_2'] = DefaultCurrencyDatabase($det['disc_2']);
                $detailpembelian['subtotal'] = DefaultCurrencyDatabase($det['subtotal']);
                if ($pembelian['type_ppn'] == "1") {
                    $detailpembelian['ppn_satuan'] = $detailpembelian['subtotal'] * ((float) $pembelian['ppn'] / ((float) $pembelian['ppn'] + (float) 100));
                    $totalpembelianwithoutppn += $detailpembelian['subtotal'] - $detailpembelian['ppn_satuan'];
                } else if ($pembelian['type_ppn'] == "2") {
                    $detailpembelian['ppn_satuan'] = $detailpembelian['subtotal'] * ((float) $pembelian['ppn'] / (float) 100);
                    $totalpembelianwithoutppn += $detailpembelian['subtotal'];
                } else {
                    $detailpembelian['ppn_satuan'] = 0;
                    $totalpembelianwithoutppn += $detailpembelian['subtotal'];
                }
                $totalpembelianwithppn += $detailpembelian['subtotal'];
                if (CheckEmpty(@$det['id_pembelian_detail'])) {
                    $barang = $this->m_barang->GetOneBarang($det['id_barang']);
                    $detailpembelian['id_satuan'] = ForeignKeyFromDb(@$barang->id_satuan);
                    $detailpembelian['created_date'] = GetDateNow();
                    $detailpembelian['created_by'] = $userid;
                    $detailpembelian['status'] = 1;
                    $detailpembelian['id_pembelian_master'] = $id_pembelian;
                    $this->db->insert("uap_pembelian_detail", $detailpembelian);
                    $arraydetailpembelian[] = $this->db->insert_id();
                } else {

                    $detailpembelian['updated_date'] = GetDateNow();
                    $detailpembelian['updated_by'] = $userid;
                    if (!CheckEmpty(@$det['id_satuan']))
                        $detailpembelian['id_satuan'] = ForeignKeyFromDb($det['id_satuan']);
                    $this->db->update("uap_pembelian_detail", $detailpembelian, array("id_pembelian_detail" => $det['id_pembelian_detail']));
                    $arraydetailpembelian[] = $det['id_pembelian_detail'];
                }
            }
            if ($model['type_pembulatan'] == "1") {
                $totalpembelianwithppn = $totalpembelianwithppn - DefaultCurrencyDatabase($pembelian['disc_pembulatan']);
            }

            if (count($arraydetailpembelian) > 0) {
                $this->db->where_not_in("id_pembelian_detail", $arraydetailpembelian);
                $this->db->where(array("id_pembelian_master" => $id_pembelian));
                $this->db->update("uap_pembelian_detail", array("status" => 5, 'updated_date' => GetDateNow(), 'updated_by' => $userid));
            } else {
                $this->db->update("uap_pembelian_detail", array("status" => 5, 'updated_date' => GetDateNow()), array("id_pembelian_master" => $pembelian));
            }
            if ($model['type_pembulatan'] == "1") {
                $totalpembelianwithoutppn = $totalpembelianwithoutppn - DefaultCurrencyDatabase($pembelian['disc_pembulatan']);
            }

            $this->db->from("uap_pembayaran_utang_detail");
            $this->db->join("uap_pembayaran_utang_master", "uap_pembayaran_utang_master.pembayaran_utang_id=uap_pembayaran_utang_detail.pembayaran_utang_id");
            $this->db->where(array("keterangan" => "Pembayaran Di Muka", "id_pembelian_master" => $id_pembelian));

            $pembayaran_utang_master = $this->db->get()->row();
            if ($pembayaran_utang_master != null && $pembelian['jumlah_bayar'] == 0) {
                $this->db->delete("uap_pembayaran_utang_detail", array("pembayaran_utang_detail_id" => $pembayaran_utang_master->pembayaran_utang_detail_id));
                $this->db->delete("uap_pembayaran_utang_master", array("pembayaran_utang_id" => $pembayaran_utang_master->pembayaran_utang_id));
            } else if ($pembayaran_utang_master != null && $pembayaran_utang_master->jumlah_bayar != $pembelian['jumlah_bayar']) {
                $updatedet = array();
                $updatedet['updated_date'] = GetDateNow();
                $updatedet['updated_by'] = $userid;
                $updatedet['jumlah_bayar'] = $pembelian['jumlah_bayar'];
                $updatemas = array();
                $updatemas['updated_date'] = GetDateNow();
                $updatemas['updated_by'] = $userid;
                $updatemas['total_bayar'] = $pembelian['jumlah_bayar'];
                $this->db->update("uap_pembayaran_utang_detail", $updatedet, array("pembayaran_utang_detail_id" => $pembayaran_utang_master->pembayaran_utang_detail_id));
                $this->db->update("uap_pembayaran_utang_master", $updatemas, array("pembayaran_utang_id" => $pembayaran_utang_master->pembayaran_utang_id));
            } else if ($pembayaran_utang_master == null && $pembelian['jumlah_bayar'] > 0) {
                $dokumenpembayaran = AutoIncrement('uap_pembayaran_utang_master', $cabang->kode_cabang . '/BB/' . date("y") . '/', 'dokumen', 5);
                $pembayaranmaster = array();
                $pembayaranmaster['dokumen'] = $dokumenpembayaran;
                $pembayaranmaster['tanggal_transaksi'] = $pembelian['tanggal'];
                $pembayaranmaster['id_supplier'] = $pembelian['id_supplier'];
                $pembayaranmaster['jenis_pembayaran'] = "cash";
                $pembayaranmaster['total_bayar'] = $pembelian['jumlah_bayar'] > $pembelian['grandtotal'] ? $pembelian['grandtotal'] : $pembelian['jumlah_bayar'];
                $pembayaranmaster['biaya'] = 0;
                $pembayaranmaster['potongan'] = 0;
                $pembayaranmaster['status'] = 1;
                $pembayaranmaster['keterangan'] = "Pembayaran Di Muka";
                $pembayaranmaster['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
                $pembayaranmaster['created_date'] = GetDateNow();
                $pembayaranmaster['created_by'] = $userid;
                $this->db->insert("uap_pembayaran_utang_master", $pembayaranmaster);
                $pembayaran_utang_id = $this->db->insert_id();
                $pembayarandetail = array();
                $pembayarandetail['id_pembelian_master'] = $id_pembelian;
                $pembayarandetail['pembayaran_utang_id'] = $pembayaran_utang_id;
                $pembayarandetail['jumlah_bayar'] = $pembelian['jumlah_bayar'];
                $pembayarandetail['created_date'] = GetDateNow();
                $pembayarandetail['created_by'] = $userid;
                $this->db->insert("uap_pembayaran_utang_detail", $pembayarandetail);
            }

            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "24", "dokumen" => $pembelian['nomor_master']));
            $akun_pembelian = $this->db->get()->row();


            if (CheckEmpty($akun_pembelian)) {
                $akunpembelianinsert = array();
                $akunpembelianinsert['id_gl_account'] = "24";
                $akunpembelianinsert['tanggal_buku'] = $pembelian['tanggal'];
                $akunpembelianinsert['id_cabang'] = $pembelian['id_cabang'];
                $akunpembelianinsert['keterangan'] = "Pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunpembelianinsert['dokumen'] = $pembelian['nomor_master'];
                $akunpembelianinsert['subject_name'] = @$supplier->kode_supplier;
                $akunpembelianinsert['id_subject'] = $pembelian['id_supplier'];
                $akunpembelianinsert['debet'] = DefaultCurrencyDatabase($model['type_pembulatan'] == "1" ? $totalpembelianwithppn * 100 / (100 + $pembelian['ppn']) : $totalpembelianwithoutppn, 0);
                $akunpembelianinsert['kredit'] = 0;
                $akunpembelianinsert['created_date'] = GetDateNow();
                $akunpembelianinsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akunpembelianinsert);
            } else {
                $akunpembelianupdate['tanggal_buku'] = $pembelian['tanggal'];
                $akunpembelianupdate['id_cabang'] = $pembelian['id_cabang'];
                $akunpembelianupdate['keterangan'] = "Pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunpembelianupdate['dokumen'] = $pembelian['nomor_master'];
                $akunpembelianupdate['subject_name'] = @$supplier->kode_supplier;
                $akunpembelianupdate['id_subject'] = $pembelian['id_supplier'];
                $akunpembelianupdate['debet'] = $totalpembelianwithoutppn;
                $akunpembelianupdate['kredit'] = 0;
                $akunpembelianupdate['updated_date'] = GetDateNow();
                $akunpembelianupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akunpembelianupdate, array("id_buku_besar" => $akun_pembelian->id_buku_besar));
            }

            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "33", "dokumen" => $pembelian['nomor_master']));
            $akun_ppn = $this->db->get()->row();

            if (CheckEmpty($akun_ppn) && $pembelian['nominal_ppn'] > 0) {
                $akunppninsert = array();
                $akunppninsert['id_gl_account'] = "33";
                $akunppninsert['tanggal_buku'] = $pembelian['tanggal'];
                $akunppninsert['id_cabang'] = $pembelian['id_cabang'];
                $akunppninsert['keterangan'] = "PPN pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunppninsert['dokumen'] = $pembelian['nomor_master'];
                $akunppninsert['subject_name'] = @$supplier->kode_supplier;
                $akunppninsert['id_subject'] = $pembelian['id_supplier'];
                $akunppninsert['debet'] = $pembelian['nominal_ppn'];
                $akunppninsert['kredit'] = 0;
                $akunppninsert['created_date'] = GetDateNow();
                $akunppninsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akunppninsert);
            } else if (!CheckEmpty($akun_ppn)) {
                $akunppnupdate['tanggal_buku'] = $pembelian['tanggal'];
                $akunppnupdate['id_cabang'] = $pembelian['id_cabang'];
                $akunppnupdate['keterangan'] = "PPN pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunppnupdate['dokumen'] = $pembelian['nomor_master'];
                $akunppnupdate['subject_name'] = @$supplier->kode_supplier;
                $akunppnupdate['id_subject'] = $pembelian['id_supplier'];
                $akunppnupdate['debet'] = $pembelian['nominal_ppn'];
                $akunppnupdate['kredit'] = 0;
                $akunppnupdate['updated_date'] = GetDateNow();
                $akunppnupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akunppnupdate, array("id_buku_besar" => $akun_ppn->id_buku_besar));
            }








            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "11", "dokumen" => $pembelian['nomor_master']));
            $akun_utang = $this->db->get()->row();

            if (CheckEmpty($akun_utang) && $pembelian['jumlah_bayar'] < $pembelian['grandtotal']) {
                $akunutanginsert = array();
                $akunutanginsert['id_gl_account'] = "11";
                $akunutanginsert['tanggal_buku'] = $pembelian['tanggal'];
                $akunutanginsert['id_cabang'] = $pembelian['id_cabang'];
                $akunutanginsert['keterangan'] = "Utang pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunutanginsert['dokumen'] = $pembelian['nomor_master'];
                $akunutanginsert['subject_name'] = @$supplier->kode_supplier;
                $akunutanginsert['id_subject'] = $pembelian['id_supplier'];
                $akunutanginsert['debet'] = 0;
                $akunutanginsert['kredit'] = $pembelian['grandtotal'] - $pembelian['jumlah_bayar'];
                $akunutanginsert['created_date'] = GetDateNow();
                $akunutanginsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akunutanginsert);
            } else if (!CheckEmpty($akun_utang)) {
                $akunutangupdate['tanggal_buku'] = $pembelian['tanggal'];
                $akunutangupdate['id_cabang'] = $pembelian['id_cabang'];
                $akunutangupdate['keterangan'] = "Utang pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunutangupdate['dokumen'] = $pembelian['nomor_master'];
                $akunutangupdate['subject_name'] = @$supplier->kode_supplier;
                $akunutangupdate['id_subject'] = $pembelian['id_supplier'];
                $akunutangupdate['debet'] = 0;
                $akunutangupdate['kredit'] = $pembelian['grandtotal'] - $pembelian['jumlah_bayar'];
                $akunutangupdate['updated_date'] = GetDateNow();
                $akunutangupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akunutangupdate, array("id_buku_besar" => $akun_utang->id_buku_besar));
            }
            $this->m_supplier->KoreksiSaldoHutang(@$supplier->id_supplier);


            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "25", "dokumen" => $pembelian['nomor_master']));
            $akun_potongan = $this->db->get()->row();

            if (CheckEmpty($akun_potongan) && $pembelian['disc_pembulatan'] > 0) {
                $akunpotonganinsert = array();
                $akunpotonganinsert['id_gl_account'] = "25";
                $akunpotonganinsert['tanggal_buku'] = $pembelian['tanggal'];
                $akunpotonganinsert['id_cabang'] = $pembelian['id_cabang'];
                $akunpotonganinsert['keterangan'] = "Potongan pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunpotonganinsert['dokumen'] = $pembelian['nomor_master'];
                $akunpotonganinsert['subject_name'] = @$supplier->kode_supplier;
                $akunpotonganinsert['id_subject'] = $pembelian['id_supplier'];
                $akunpotonganinsert['debet'] = 0;
                $akunpotonganinsert['kredit'] = $pembelian['disc_pembulatan'];
                $akunpotonganinsert['created_date'] = GetDateNow();
                $akunpotonganinsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akunpotonganinsert);
            } else if (!CheckEmpty($akun_potongan)) {
                $akunpotonganupdate['tanggal_buku'] = $pembelian['tanggal'];
                $akunpotonganupdate['id_cabang'] = $pembelian['id_cabang'];
                $akunpotonganupdate['keterangan'] = "Potongan pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunpotonganupdate['dokumen'] = $pembelian['nomor_master'];
                $akunpotonganupdate['subject_name'] = @$supplier->kode_supplier;
                $akunpotonganupdate['id_subject'] = $pembelian['id_supplier'];
                $akunpotonganupdate['debet'] = 0;
                $akunpotonganupdate['kredit'] = $pembelian['disc_pembulatan'];
                $akunpotonganupdate['updated_date'] = GetDateNow();
                $akunpotonganupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akunpotonganupdate, array("id_buku_besar" => $akun_potongan->id_buku_besar));
            }


            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "3", "dokumen" => $pembelian['nomor_master']));
            $akun_kas = $this->db->get()->row();

            if (CheckEmpty($akun_kas) && $pembelian['jumlah_bayar'] > 0) {
                $akunkasinsert = array();
                $akunkasinsert['id_gl_account'] = "3";
                $akunkasinsert['tanggal_buku'] = $pembelian['tanggal'];
                $akunkasinsert['id_cabang'] = $pembelian['id_cabang'];
                $akunkasinsert['keterangan'] = "Pengeluaran kas untuk pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunkasinsert['dokumen'] = $pembelian['nomor_master'];
                $akunkasinsert['subject_name'] = @$supplier->kode_supplier;
                $akunkasinsert['id_subject'] = $pembelian['id_supplier'];
                $akunkasinsert['debet'] = 0;
                $akunkasinsert['kredit'] = $pembelian['jumlah_bayar'];
                $akunkasinsert['created_date'] = GetDateNow();
                $akunkasinsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akunkasinsert);
            } else if (!CheckEmpty($akun_kas)) {
                $akunkasupdate['tanggal_buku'] = $pembelian['tanggal'];
                $akunkasupdate['id_cabang'] = $pembelian['id_cabang'];
                $akunkasupdate['keterangan'] = "Pengeluaran kas untuk Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunkasupdate['dokumen'] = $pembelian['nomor_master'];
                $akunkasupdate['subject_name'] = @$supplier->kode_supplier;
                $akunkasupdate['id_subject'] = $pembelian['id_supplier'];
                $akunkasupdate['debet'] = 0;
                $akunkasupdate['kredit'] = $pembelian['jumlah_bayar'];
                $akunkasupdate['updated_date'] = GetDateNow();
                $akunkasupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akunkasupdate, array("id_buku_besar" => $akun_kas->id_buku_besar));
            }

            $this->db->from("uap_buku_besar");
            $this->db->where(array("dokumen" => $pembelian['nomor_master']));
            $this->db->select("sum(debet)-sum(kredit) as selisih");
            $row = $this->db->get()->row();
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "38", "dokumen" => $pembelian['nomor_master']));
            $akun_penyeimbang = $this->db->get()->row();

            if (CheckEmpty($akun_penyeimbang) && $row && $row->selisih > 0) {
                $akun_penyeimbanginsert = array();
                $akun_penyeimbanginsert['id_gl_account'] = "38";
                $akun_penyeimbanginsert['tanggal_buku'] = $pembelian['tanggal'];
                $akun_penyeimbanginsert['id_cabang'] = $pembelian['id_cabang'];
                $akun_penyeimbanginsert['keterangan'] = "Penyeimbang untuk pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akun_penyeimbanginsert['dokumen'] = $pembelian['nomor_master'];
                $akun_penyeimbanginsert['subject_name'] = @$supplier->kode_supplier;
                $akun_penyeimbanginsert['id_subject'] = $pembelian['id_supplier'];
                $akun_penyeimbanginsert['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['created_date'] = GetDateNow();
                $akun_penyeimbanginsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akun_penyeimbanginsert);
            } else if (!CheckEmpty($akun_penyeimbang)) {
                $akun_penyeimbangupdate['tanggal_buku'] = $pembelian['tanggal'];
                $akun_penyeimbangupdate['id_cabang'] = $pembelian['id_cabang'];
                $akun_penyeimbangupdate['keterangan'] = "Penyeimbang untuk Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akun_penyeimbangupdate['dokumen'] = $pembelian['nomor_master'];
                $akun_penyeimbangupdate['subject_name'] = @$supplier->kode_supplier;
                $akun_penyeimbangupdate['id_subject'] = $pembelian['id_supplier'];
                $akun_penyeimbangupdate['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['updated_date'] = GetDateNow();
                $akun_penyeimbangupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akun_penyeimbangupdate, array("id_buku_besar" => $akun_penyeimbang->id_buku_besar));
            }



            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data Pembelian PO Sudah di" . (CheckEmpty(@$model['id_pembelian_master']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_pembelian, $pembelian['nomor_master'], 'pembelian');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    // datatables
    function GetDatapembelianPO($params) {
        $this->load->library('datatables');
        $this->datatables->select('id_pembelian_po_master,nama_supplier,tgl_po,replace(nomor_po_master,"/","-") as nomor_po_master,nominal_po');
        $this->datatables->join("uap_supplier", "uap_supplier.id_supplier=uap_pembelian_po_master.id_supplier", "left");
        $this->datatables->from('uap_pembelian_po_master');
        //add this line for join
        //$this->datatables->join('table2', 'uap_pembelian.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        $extra = array();
        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['uap_pembelian_po_master.id_cabang'] = $params['id_cabang'];
        }
        if (isset($params['id_supplier']) && !CheckEmpty(@$params['id_supplier'])) {
            $params['uap_pembelian_po_master.id_supplier'] = $params['id_supplier'];
        }
        unset($params['id_supplier']);
        unset($params['id_cabang']);
        unset($params['status']);


        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "tgl_po BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tgl_po'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }

        if (count($params)) {
            $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }

        if ($isedit) {
            $straction .= anchor("pembelian/editpembelianpo/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batalpo($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_pembelian_po_master,nomor_po_master');
        return $this->datatables->generate();
    }

    function GetDatapembelian($params) {
        $this->load->library('datatables');
        $this->datatables->select('uap_pembelian_master.id_pembelian_master,nama_supplier,uap_pembelian_master.tanggal,replace(nomor_master,"/","-") as nomor_master_link,nomor_master,uap_pembelian_master.grandtotal,uap_pembelian_master.jth_tempo,group_concat(nomor_po_master," , ") as nomor_po_master');
        $this->datatables->join("uap_supplier", "uap_supplier.id_supplier=uap_pembelian_master.id_supplier", "left");
        $this->datatables->join("uap_pembelian_link", "uap_pembelian_master.id_pembelian_master=uap_pembelian_link.id_pembelian_master", "left");
        $this->datatables->join("uap_pembelian_po_master", "uap_pembelian_po_master.id_pembelian_po_master=uap_pembelian_link.id_pembelian_po_master", "left");
        $this->datatables->from('uap_pembelian_master');
        $this->datatables->group_by('uap_pembelian_master.id_pembelian_master');

//add this line for join
        //$this->datatables->join('table2', 'uap_pembelian.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        $extra = array();
        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['uap_pembelian_master.id_cabang'] = $params['id_cabang'];
        }
        if (isset($params['id_supplier']) && !CheckEmpty(@$params['id_supplier'])) {
            $params['uap_pembelian_master.id_supplier'] = $params['id_supplier'];
        }
        unset($params['id_supplier']);
        unset($params['id_cabang']);
        unset($params['status']);


        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "uap_pembelian_master.tanggal BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tanggal'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }

        if (count($params)) {
            $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }

        if ($isedit) {
            $straction .= anchor("pembelian/editpembelian/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batal($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_pembelian_master,nomor_master_link');
        return $this->datatables->generate();
    }

    

}
