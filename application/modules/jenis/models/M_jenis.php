<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_jenis extends CI_Model {

    public $table = 'uap_jenis';
    public $id = 'id_jenis';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDatajenis() {
        $this->load->library('datatables');
        $this->datatables->select('id_jenis,kode_jenis,nama_jenis,status');
        $this->datatables->from('uap_jenis');
        //add this line for join
        //$this->datatables->join('table2', 'uap_jenis.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editjenis($1);return false;"));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletejenis($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_jenis');
        return $this->datatables->generate();
    }

    // get all
    function GetOneJenis($keyword, $type = 'id_jenis') {
        $this->db->where($type, $keyword);
        $jenis = $this->db->get($this->table)->row();
        return $jenis;
    }
    function GetDropDownJenis() {

        $where = [];
        $where['status'] = 1;
        $listreturn = GetTableData("uap_jenis", 'id_jenis', 'nama_jenis', $where, 'obj');
        return $listreturn;
    }
    function JenisManipulate($model) {
        try {
            $model['status'] = DefaultCurrencyDatabase($model['status']);

            if (CheckEmpty($model['id_jenis'])) {
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                return array("st" => true, "msg" => "Jenis successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_jenis" => $model['id_jenis']));
                return array("st" => true, "msg" => "Jenis has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function JenisDelete($id_jenis) {
        try {
            $this->db->delete($this->table, array('id_jenis' => $id_jenis));
        } catch (Exception $ex) {
            $model['updated_date'] = GetDateNow();
            $model['status'] = 2;
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array('id_jenis' => $id_jenis));
        }
        return array("st" => true, "msg" => "Jenis has been deleted from database");
    }

}
