<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jenis extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_jenis');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K003";
        $header = "K001";
        $title = "Jenis";
        CekModule($module);
        LoadTemplate(array("title" => $title, "form" => $header, "formsubmenu" => $module), "jenis/v_jenis_index", $javascript);
    }

    public function getdatajenis() {
        header('Content-Type: application/json');
        echo $this->m_jenis->GetDatajenis();
    }

    public function create_jenis() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K003";
        $header = "K001";
        CekModule($module);
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->title = 'Create Jenis';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $this->load->view('jenis/v_jenis_manipulate', $row);
    }

    public function jenis_manipulate() {
        $message = '';
        $model = $this->input->post();
        if (!CheckEmpty($model["id_jenis"])) {
            $this->form_validation->set_rules('kode_jenis', 'kode jenis', 'trim|required');
        }

        $this->form_validation->set_rules('nama_jenis', 'nama jenis', 'trim|required');

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_jenis->JenisManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_jenis($id = 0) {
        $row = $this->m_jenis->GetOneJenis($id);

        if ($row) {
            $row->button = 'Update';
            $module = "K003";
            $header = "K001";
            CekModule($module);
            $row->form = $header;
            $row->formsubmenu = $module;
            $row->title = 'Update Jenis';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $this->load->view('jenis/v_jenis_manipulate', $row);
        } else {
            SetMessageSession(0, "Jenis cannot be found in database");
            redirect(site_url('jenis'));
        }
    }

    public function jenis_delete() {
        $message = '';
        $this->form_validation->set_rules('id_jenis', 'Jenis', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_jenis->JenisDelete($model['id_jenis']);
        }

        echo json_encode($result);
    }

}

/* End of file Jenis.php */