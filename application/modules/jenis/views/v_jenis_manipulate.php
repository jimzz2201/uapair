<div class="modal-header">
    Jenis <?php echo @$button ?>
</div>
<div class="modal-body">
    <form id="frm_jenis" class="form-horizontal form-groups-bordered validate" method="post">
        <input type="hidden" name="id_jenis" value="<?php echo @$id_jenis; ?>" /> 
        <div class="form-group">
            <?= form_label('Kode Jenis', "txt_kode_jenis", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text', 'name' => 'kode_jenis', 'value' => @$kode_jenis, 'class' => 'form-control', 'id' => 'txt_kode_jenis', 'placeholder' => 'Kode Jenis')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Nama Jenis', "txt_nama_jenis", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text', 'name' => 'nama_jenis', 'value' => @$nama_jenis, 'class' => 'form-control', 'id' => 'txt_nama_jenis', 'placeholder' => 'Nama Jenis')); ?>
            </div>
        </div>

        <div class="form-group">
            <?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
            <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
        </div>

    </form>
</div>
<script>
    $("#frm_jenis").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/jenis/jenis_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>