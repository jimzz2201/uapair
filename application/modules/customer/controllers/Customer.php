<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_customer');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K012";
        $header = "K011";
        $title = "Customer";
        CekModule($module);
        LoadTemplate(array("title" => $title, "form" => $header, "formsubmenu" => $module), "customer/v_customer_index", $javascript);
    }

    public function getdatacustomer() {
        header('Content-Type: application/json');
        echo $this->m_customer->GetDatacustomer();
    }
    public function getdatablacklist() {
        header('Content-Type: application/json');
        echo $this->m_customer->GetDataBlacklist();
    }
    
    
    
    public function koreksi_saldo_action() {

        $this->form_validation->set_rules('id_customer', 'nama customer', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $model = $this->input->post();
            if (!CheckKey($model, 'saldo_awal')) {
                $customer = $this->m_customer->GetOneCustomer($model["id_customer"]);
                $model['saldo_awal'] = $customer->saldo_awal;
                $model['plafond'] = $customer->plafond;
            }
            $data = $this->m_customer->KoreksiSaldoPiutang($model["id_customer"], $model['saldo_awal'], $model['plafond']);
            echo json_encode($data);
        }
    }

    public function blacklist() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K014";
        $header = "K011";
        $title = "Blacklist Customer";
        CekModule($module);
        LoadTemplate(array("title" => $title, "form" => $header, "formsubmenu" => $module,"list_customer"=>$this->m_customer->GetDropDownCustomer()), "customer/v_customer_blacklist", $javascript);
    }

    public function get_one_customer() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_customer', 'nama customer', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_customer->GetOneCustomer($model["id_customer"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data customer tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function koreksi_saldo_customer() {
        $row = (object) array();
        $row->button = 'Koreksi Saldo';
        $module = "K015";
        $header = "K011";
        $this->load->model("cabang/m_cabang");
        CekModule($module);
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->list_customer = $this->m_customer->GetDropDownCustomer();
        $row->title = 'Koreksi Saldo Customer';
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        LoadTemplate($row, 'customer/v_customer_koreksi', $javascript);
    }

    public function create_customer() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K013";
        $header = "K011";
        CekModule($module);
        $row->form = $header;
        $row->formsubmenu = $module;
        $this->load->model("cabang/m_cabang");
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->title = 'Create Customer';
        $row = json_decode(json_encode($row), true);

        $javascript = array();
        LoadTemplate($row, 'customer/v_customer_manipulate', $javascript);
    }
    public function blacklist_add() {
        $message = '';

        $this->form_validation->set_rules('id_customer', 'customer', 'trim|required');
        $model = $this->input->post();
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_customer->CustomerBlacklist($model['id_customer']);
            echo json_encode($status);
        }
    }
    public function customer_blacklist_hapus() {
        $message = '';

        $this->form_validation->set_rules('id_customer', 'customer', 'trim|required');
        $model = $this->input->post();
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_customer->CustomerHapusBlacklist($model['id_customer']);
            echo json_encode($status);
        }
    }
    public function customer_manipulate() {
        $message = '';

        $this->form_validation->set_rules('nama_customer', 'nama customer', 'trim|required');
        $this->form_validation->set_rules('no_telp', 'no telp', 'trim|required');
        $model = $this->input->post();
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_customer->CustomerManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_customer($id = 0) {
        $row = $this->m_customer->GetOneCustomer($id);

        if ($row) {
            $row->button = 'Update';
            $module = "K013";
            $header = "K011";
            CekModule($module);
            $row->form = $header;
            $row->formsubmenu = $module;
            $row->title = 'Update Customer';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            LoadTemplate($row, 'customer/v_customer_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Customer cannot be found in database");
            redirect(site_url('customer'));
        }
    }

    public function customer_delete() {
        $message = '';
        $this->form_validation->set_rules('id_customer', 'Customer', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_customer->CustomerDelete($model['id_customer']);
        }

        echo json_encode($result);
    }

}

/* End of file Customer.php */