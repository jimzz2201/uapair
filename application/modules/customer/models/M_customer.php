<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_customer extends CI_Model {

    public $table = 'uap_customer';
    public $id = 'id_customer';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function GetDropDownCustomer() {

        $where = [];
        $where['status'] = 1;
        $listreturn = GetTableData("uap_customer", 'id_customer', 'nama_customer', $where, 'obj');
        return $listreturn;
    }

    // datatables
    function GetDatacustomer() {
        $this->load->library('datatables');
        $this->datatables->select('id_customer,kode_customer,nama_customer,npwp,alamat,no_telp,no_telp_2,fax,email,contact_person,keterangan,piutang,saldo_awal,plafond,status,is_blacklist,id_cabang');
        $this->datatables->from('uap_customer');
        //add this line for join
        //$this->datatables->join('table2', 'uap_customer.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor(site_url('customer/edit_customer/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletecustomer($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_customer');
        return $this->datatables->generate();
    }

    function GetDataBlacklist() {
        $this->load->library('datatables');
        $this->datatables->select('id_customer,kode_customer,nama_customer');
        $this->datatables->from('uap_customer');
        $this->datatables->where(array("is_blacklist" => 1));
        //add this line for join
        //$this->datatables->join('table2', 'uap_customer.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';

        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "hapusblacklist($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_customer');
        return $this->datatables->generate();
    }

    function CustomerBlacklist($id_customer) {
        $this->db->from("uap_customer");
        $this->db->where(array("id_customer" => $id_customer, "is_blacklist" => "1"));
        $row = $this->db->get()->row();
        if ($row != null) {
            return array("st" => false, "msg" => "Data Customer sudah ada dalam list blacklist");
        } else {
            $model["is_blacklist"] = 1;
            $model['updated_date'] = GetDateNow();
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array("id_customer" => $id_customer));
            return array("st" => true, "msg" => "Data Customer berhasil dimasukkan ke dalam daftar blacklist");
        }
    }
    function CustomerHapusBlacklist($id_customer) {
        $this->db->from("uap_customer");
        $this->db->where(array("id_customer" => $id_customer, "is_blacklist" => "1"));
        $row = $this->db->get()->row();
        if ($row == null) {
            return array("st" => false, "msg" => "Data Customer tidak ada dalam list blacklist");
        } else {
            $model["is_blacklist"] = 0;
            $model['updated_date'] = GetDateNow();
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array("id_customer" => $id_customer));
            return array("st" => true, "msg" => "Data Customer berhasil dikeluarkan dari daftar blacklist");
        }
    }
    function KoreksiSaldoPiutang($customer_id, $saldo_awal=null, $plafond=null) {
        try {
            $this->db->trans_begin();
            if($saldo_awal==null)
            {
                $customer=$this->GetOneCustomer($customer_id);
                if($customer)
                {
                    $saldo_awal=$customer->saldo_awal;
                    $plafond=$customer->plafond;
                }
            }
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "7", "id_subject" => $customer_id));
            $this->db->select("sum(debet)-sum(kredit) as totalpiutang");
            $totalpiutang = $this->db->get()->row()->totalpiutang;
            $piutang = DefaultCurrencyDatabase($saldo_awal);
            $piutang += CheckEmpty($totalpiutang)?0:$totalpiutang;
            
            $update = array();
            $update['updated_date'] = GetDateNow();
            $update['updated_by'] = ForeignKeyFromDb(GetUserId());
            $update['piutang'] = $piutang;
            $update['plafond'] = DefaultCurrencyDatabase($plafond);
            $update['saldo_awal'] = DefaultCurrencyDatabase($saldo_awal);
            $message = "";
            $this->db->update("uap_customer", $update, array("id_customer" => $customer_id));
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured";
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                $arrayreturn["msg"] = "Hutang dan Saldo Awal Supplier telah diupdate";
                $arrayreturn["st"] = true;
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    // get all
    function GetOneCustomer($keyword, $type = 'id_customer') {
        $this->db->where($type, $keyword);
        $customer = $this->db->get($this->table)->row();
        return $customer;
    }

    function CustomerManipulate($model) {
        try {


            if (CheckKey($model, 'piutang')) {
                unset($model['piutang']);
            }
            if (CheckKey($model, 'saldo_awal')) {
                unset($model['saldo_awal']);
            }
            if (CheckKey($model, 'plafond')) {
                unset($model['plafond']);
            }
            $model['status'] = $model['status'];
            if ($model['allow_all_cabang'] == 1) {
                $model['id_cabang'] = "all";
            } else {
                if (isset($model['id_cabang']) && count($model['id_cabang'])) {
                    $model['id_cabang'] = implode(",", $model['id_cabang']);
                } else {
                    $model['id_cabang'] = null;
                }
            }
            $model['status'] = DefaultCurrencyDatabase($model['status']);
            $model['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
            unset($model['allow_all_cabang']);
            if (CheckEmpty($model['id_customer'])) {
                $model['kode_customer'] = AutoIncrement('uap_customer', '105', 'kode_customer', 5);
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                SetMessageSession(1, 'Customer successfull added into database');
                return array("st" => true, "msg" => "Customer successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_customer" => $model['id_customer']));
                SetMessageSession(1, 'Customer has been updated');
                return array("st" => true, "msg" => "Customer has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function CustomerDelete($id_customer) {
        try {
            $this->db->delete($this->table, array('id_customer' => $id_customer));
        } catch (Exception $ex) {
            $model['updated_date'] = GetDateNow();
            $model['status'] = 2;
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array('id_customer' => $id_customer));
        }
        return array("st" => true, "msg" => "Customer has been deleted from database");
    }

}
