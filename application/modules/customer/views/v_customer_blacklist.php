
<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Customer</h2>
                                <p>Customer | Blacklist</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="">
    <div class="box box-default" style="padding:10px;background-color: #fff;margin-bottom:20px;">
        <form id="frm_blacklist" class="form-horizontal form-groups-bordered validate" method="post">
            <div class="row">
                <div class="">
                    <div class="col-sm-4">
                        <?= form_dropdown(array("name" => "id_customer"), DefaultEmptyDropdown($list_customer, "", "Customer"), @$id_customer, array('class' => 'form-control', 'id' => 'dd_id_customer')); ?>
                    </div>

                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary btn-xs" id="btt_modal_ok" style="padding:5px 20px;font-size:12px;" >Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="box box-default form-element-list">


        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</div>
<script type="text/javascript">
    var table;
    function hapusblacklist(id_customer) {


        swal({
            title: "Apakah anda yakin?",
            text: "Menghapus data customer berikut dari data blacklist!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, hapus!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/customer/customer_blacklist_hapus',
                    dataType: 'json',
                    data: {
                        id_customer: id_customer
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }

                });
            }
        });
    }
    
    $("#frm_blacklist").submit(function () {
        swal({
            title: "Apakah anda ingin melakukan transaksi berikut?",
            text: "Customer ini akan diberi tanda agar tidak bisa membuat invoice!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Tambah ke dalam Blacklist!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/customer/blacklist_add',
                    dataType: 'json',
                    data: $("form#frm_blacklist").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            $("#dd_id_customer").val(0);
                            table.fnDraw(false);
                        
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });

        return false;

    })
    $(document).ready(function () {
        $("#dd_id_customer").select2();
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": "customer/getdatablacklist", "type": "POST"},
            columns: [
                {
                    data: "id_customer",
                    title: "Kode",
                    orderable: false
                }
                , {data: "kode_customer", orderable: false, title: "Kode Customer"}
                , {data: "nama_customer", orderable: false, title: "Nama Customer"}
                ,
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
