<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Customer</h2>
                                <p><?= $button ?> Customer | Data Customer</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_customer" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="id_customer" value="<?php echo @$id_customer; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Nama', "txt_nama_customer", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-7">
                                        <?= form_dropdown(array("name" => "id_customer"), DefaultEmptyDropdown($list_customer, "", "Customer"), @$id_customer, array('class' => 'form-control', 'id' => 'dd_id_customer')); ?>
                                    </div>
                                    <?= form_label('Kode', "txt_kode_customer", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'name' => 'kode_customer', 'value' => @$kode_customer, 'class' => 'form-control', 'id' => 'txt_kode_customer', 'placeholder' => 'Kode Customer')); ?>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <?= form_label('Alamat', "txt_alamat", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'disabled' => 'disabled', 'rows' => 5, 'name' => 'alamat', 'value' => @$alamat, 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat')); ?>
                                    </div>
                                </div>



                                <?php if (CekModule("K096", false)) {
                                    ?>
                                    <div class="form-group">

                                        <?= form_label('Saldo Awal Sebelumnya', "txt_saldo_awal", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'value' => DefaultCurrency(@$saldo_awal), 'class' => 'form-control', 'id' => 'txt_saldo_awal', 'placeholder' => 'Saldo Awal', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                        <?= form_label('Hutang', "txt_piutang", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'value' => DefaultCurrency(@$piutang), 'class' => 'form-control', 'id' => 'txt_piutang', 'placeholder' => 'Hutang', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?= form_label('Saldo Awal', "txt_saldo_awal", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'name' => 'saldo_awal', 'value' => DefaultCurrency(@$saldo_awal), 'class' => 'form-control', 'id' => 'txt_saldo_semestinya', 'placeholder' => 'Saldo Awal', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                        <?= form_label('Plafond', "txt_plafond", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_input(array('type' => 'text', 'name' => 'plafond', 'value' => DefaultCurrency(@$plafond), 'class' => 'form-control', 'id' => 'txt_plafond', 'placeholder' => 'Plafond', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                    </div>
                                <?php } ?>

                                <?php if (CekModule("K096", false)) { ?>
                                    <div class="form-group buttonarea">
                                        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                    </div>
                                <?php } ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $("#dd_id_customer").select2();
        $("#dd_id_customer").change(function () {
            if ($("#dd_id_customer").val() != "0")
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/customer/get_one_customer',
                    dataType: 'json',
                    data: $("form#frm_customer").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            $("#txt_kode_customer").val(data.obj.kode_customer);
                            $("#txt_kode_customer").val(data.obj.kode_customer);
                            $("#txt_alamat").html(data.obj.alamat);
                            $("#txt_plafond").val(Comma(data.obj.plafond));
                            $("#txt_piutang").val(Comma(data.obj.piutang));
                            $("#txt_saldo_awal").val(Comma(data.obj.saldo_awal));
                            $("#txt_saldo_semestinya").val(Comma(data.obj.saldo_awal));
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            } else
            {
                $("#txt_kode_customer").val("");
                $("#txt_alamat").html("");
                $("#txt_saldo_awal").val("0");
                $("#txt_plafond").val("0");
                $("#txt_piutang").val("0");
                $("#txt_saldo_semestinya").val("0");
            }
        })

    })

    $("#frm_customer").submit(function () {
        swal({
            title: "Apakah anda ingin melakukan transaksi berikut?",
            text: "Transaksi ini akan mengubah saldo dan piutang customer!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Ubah data ini!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/customer/koreksi_saldo_action',
                    dataType: 'json',
                    data: $("form#frm_customer").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            $("#dd_id_customer").val(0);
                            $("#txt_kode_customer").val("");
                            $("#txt_alamat").html("");
                            $("#txt_saldo_awal").val("0");
                            $("#txt_plafond").val("0");
                            $("#txt_piutang").val("0");
                            $("#txt_saldo_semestinya").val("0");
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });

        return false;

    })
</script>