<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Retur Penjualan</h2>
                                <p><?= $button ?> Retur Penjualan | Retur Penjualan</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                            <div class="form-group" >
                                <div class="row " style="margin-bottom:20px;">
                                    <div class="col-sm-12 kolomheaderpo">
                                        <?php
                                        foreach (@$listheader as $headersatuan) {
                                            echo "Nomor : " . DefaultDatePicker($headersatuan['tanggal']) . " == " . $headersatuan['nomor_master'] . "<br/>";
                                        }
                                        ?>
                                    </div>

                                </div>
                                <?php if (CheckEmpty(@$id_surat_jalan_master)) { ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <button type="button" class="btn btn-primary btn-block" id="btt_search_po" >Cari No Surat Jalan</button>
                                        </div>
                                        <div class="col-sm-6">
                                            <button type="button" class="btn btn-danger btn-block" id="btt_clear_penjualan" >Clear Data</button>
                                        </div>
                                    </div>
                                <?Php } ?>
                            </div>
                            <form id="frm_penjualan" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" id="txt_id_surat_jalan_master" name="id_surat_jalan_master" value="<?php echo @$id_surat_jalan_master; ?>" /> 
                                <input type="hidden" id="txt_id_sales_order_master" name="id_sales_order_master" value="<?php echo @$id_sales_order_master; ?>" /> 
                                <input type="hidden" id="txt_id_retur_penjualan_master" name="id_retur_penjualan_master" value="<?php echo @$id_retur_penjualan_master; ?>" /> 
                                <input type="hidden" id="txt_id_customer" name="id_customer" value="<?php echo @$id_customer; ?>" /> 
                                <input type="hidden" id="txt_id_cabang" name="id_cabang" value="<?php echo @$id_cabang; ?>" /> 
                                <input type="hidden" id="txt_id_gudang" name="id_gudang" value="<?php echo @$id_gudang; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Customer', "txt_customer", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        $listcustomer = DefaultEmptyDropdown(@$list_customer, "", "Customer");
                                        if (!CheckEmpty(@$id_customer)) {
                                            $textcustomer = $listcustomer[$id_customer];
                                            $listcustomer = [];
                                            $listcustomer[@$id_customer] = $textcustomer;
                                        }
                                        ?>
                                        <?= form_dropdown(array(), $listcustomer, @$id_customer, array('class' => 'form-control', 'id' => 'dd_id_customer')); ?>
                                    </div>    

                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?php
                                    $listcabang = DefaultEmptyDropdown(@$list_cabang, "", "Cabang");
                                    if (!CheckEmpty(@$id_cabang)) {
                                        $textcabang = $listcabang[@$id_cabang];
                                        $listcabang = [];
                                        $listcabang[@$id_cabang] = $textcabang;
                                    }
                                    ?>
                                    <?= form_label('Nama Cabang', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array(), $listcabang, @$id_cabang, array('class' => 'form-control', 'id' => 'dd_id_cabang')); ?>
                                    </div>
                                    <?php
                                    $listgudang = DefaultEmptyDropdown(@$list_gudang, "", "Gudang");
                                    if (!CheckEmpty(@$id_gudang)) {
                                        $textgudang = $listgudang[@$id_gudang];
                                        $listgudang = [];
                                        $listgudang[$id_gudang] = $textgudang;
                                    }
                                    ?>
                                    <?= form_label('Gudang', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array(), $listgudang, @$id_gudang, array('class' => 'form-control', 'id' => 'dd_id_gudang')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Type PPN', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "type_ppn"), DefaultEmptyDropdown(@$list_type_ppn, "", "PPN"), @$type_ppn, array('class' => 'form-control', 'id' => 'dd_type_ppn')); ?>
                                    </div>
                                    <?= form_label('PPN', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-1">
                                        <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$ppn), 'class' => 'form-control', 'id' => 'txt_ppn', 'placeholder' => 'PPN', 'name' => 'ppn', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Pengembalian', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "jenis_potongan"), DefaultEmptyDropdown(@$list_type_pembayaran, "", "Pembayaran"), @$jenis_potongan, array('class' => 'form-control', 'id' => 'dd_type_pembayaran')); ?>
                                    </div>

                                </div>



                                <div class="box box-default">


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet-body form">
                                                <table class="table table-striped table-bordered table-hover" id="mytable" style="width:100%;">
                                                    <thead>
                                                        <tr>
                                                            <th>Kode</th>
                                                            <th>SKU</th>
                                                            <th>Nama Barang</th>
                                                            <th>Diterima</th>
                                                            <th>DiRetur</th>
                                                            <th>Price</th>
                                                            <th>Qty Retur</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="bodytable">
                                                     <?php include APPPATH . 'modules/retur/views/v_retur_jual_content.php' ?> 
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Type Pembulatan', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "type_pembulatan"), @$list_type_pembulatan, @$type_pembulatan, array('class' => 'form-control', 'id' => 'dd_type_pembulatan')); ?>
                                    </div>
                                    <?= form_label('Pembulatan', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'total_potongan', 'value' => DefaultCurrency(@$total_potongan), 'class' => 'form-control', 'id' => 'txt_disc_pembulatan', 'placeholder' => 'Disc Pembulatan', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <?= form_label('PPN', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'ppn_nominal', 'value' => DefaultCurrency(@$ppn_nominal), 'class' => 'form-control', 'id' => 'txt_ppn_nominal', 'placeholder' => 'PPN', "readonly" => "readonly")); ?>
                                    </div>
                                    <?= form_label('Grand Total', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                       <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'grandtotal', 'value' => DefaultCurrency(@$grandtotal), 'class' => 'form-control', 'id' => 'txt_grandtotal', 'placeholder' => 'Grand Total')); ?>
                                    </div>
                                   


                                </div>
                                <div class="form-group" style="margin-top:50px">
                                    <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>


    var table;
    var dataset = <?php echo json_encode(@$listdetail) ?>;
    var listheader = <?php echo json_encode(@$listheader) ?>;

    
    $(document).ready(function () {


    })
    function HitungSemua(){
        var total = 0;
        var nominalppn = 0;
        var grandtotal=0;
        var ppn = Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""));
        if ($(".returcolumn").length > 0)
        {
            $.each($(".returcolumn"), function (key, value) {
                total += Number($("#txt_price_barang_"+key).val().replace(/[^0-9\.]+/g, ""))*Number($("#txt_qty_retur_"+key).val().replace(/[^0-9\.]+/g, ""));
            });
        }
        grandtotal = total;
        if($("#dd_type_ppn").val()=="1")
        {
            $("#dd_type_pembulatan").val("2");
        }
        if ($("#dd_type_pembulatan").val() == "1")
        {   
             total = total - Number($("#txt_disc_pembulatan").val().replace(/[^0-9\.]+/g, ""));
             grandtotal = grandtotal - Number($("#txt_disc_pembulatan").val().replace(/[^0-9\.]+/g, ""));
        }
       
        
        if ($("#dd_type_ppn").val() == "2")
        {
            nominalppn = ppn / 100 * grandtotal;
            grandtotal = grandtotal + nominalppn;
        } else if ($("#dd_type_ppn").val() == "1")
        {
            nominalppn = ppn / (100 + Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""))) * grandtotal;
            total = total - nominalppn;
        }
       if ($("#dd_type_pembulatan").val() == "2")
        {
            grandtotal = grandtotal - Number($("#txt_disc_pembulatan").val().replace(/[^0-9\.]+/g, ""));
        }
        $("#txt_grandtotal").val(number_format(grandtotal.toFixed(4)));
        $("#txt_ppn_nominal").val(number_format(nominalppn.toFixed(4)));
        
    }
    $("#dd_type_pembulatan,#dd_type_ppn,#txt_ppn").change(function () {
        HitungSemua();
    })
    function RefreshPanelGrid()
    {
        if ($(".returcolumn").length > 0)
        {
            $("#frm_penjualan button").removeAttr("disabled");
        } else
        {
            $("#frm_penjualan button").attr("disabled", "disabled");
        }

    }

    $(document).ready(function () {
        $("#txt_tanggal").datepicker();
        RefreshPanelGrid();


    })
    $("#btt_clear_penjualan").click(function () {
        listheader = [];
        dataset = [];
        RefreshPanelGrid();
        $(".kolomheaderpo").empty();
        $("#dd_id_customer").removeAttr("disabled");
        $("#dd_id_cabang").removeAttr("disabled");
        $("#dd_id_gudang").removeAttr("disabled");
        $("#btt_search_po").css("display", "block");
        $("#bodytable").html("<tr><td align=\"center\" colspan=\"6\">No Data Display</td></tr>");
    })
    $("#btt_search_po").click(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/retur/jual/search_surat_jalan_master',
            data: $("#frm_penjualan").serialize() ,
            success: function (data) {
                modalbootstrap(data, "Search PO", "1280px");

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });

    })
    $("#frm_penjualan").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data retur berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                LoadBar.show();
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/retur/jual/retur_jual_manipulate',
                    dataType: 'json',
                    data: $("#frm_penjualan").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/retur/jual";

                        } else
                        {
                            messageerror(data.msg);
                        }
                        LoadBar.hide();

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                        LoadBar.hide();
                    }
                });
            }
        });
        return false;


    })
</script>