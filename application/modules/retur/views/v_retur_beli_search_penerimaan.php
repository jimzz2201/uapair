<div class="modal-header">
    Pencarian Pembelian
</div>
<div class="modal-body" style="min-width:100px;">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="portlet-body form">
                    <form id="frmhide" >
                        <input name="id_supplier" type="hidden" value="<?php echo @$id_supplier ?>">
                        <input name="id_cabang" type="hidden" value="<?php echo @$id_cabang ?>">
                        <input name="id_gudang" type="hidden" value="<?php echo @$id_gudang ?>">
                        <div class="form-group">
                            <?= form_label('Tanggal', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>

                            <div class="col-sm-2">
                                <?= form_input(array("selected" => "","autocomplete"=>"off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal_searching'), DefaultDatePicker(@$tanggal_awal), array('required' => 'required')); ?>
                            </div>
                            <?= form_label('s / d', "txt_tanggal_akhir", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-2">
                                <?= form_input(array("selected" => "","autocomplete"=>"off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir_searching'), DefaultDatePicker(@$tanggal)); ?>
                            </div>
                            <div class="col-sm-2">
                                <button id="btt_Search" type="submit" class="btn-small btn-block btn-primary pull-right">Search</button>
                            </div>

                        </div>

                    </form>
                    <table class="table table-striped table-bordered table-hover" id="mytablesearch" style="width:100%">

                    </table>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="detailpo">

            </div>
        </div>
    </div>
</div>
<script>
    var tablesearch;
    $(document).ready(function () {
        setTimeout(function () {
            InitialGridSearch();
        }, 500);
        $(".datepicker").datepicker();
    })
    $("form#frmhide").submit(function(){
        tablesearch.fnDraw(false);
        return false;
    })

    function select(id_penerimaan)
    {
        $("#btt_search_po").css("display", "none");
        listheader = [];

        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/pembelian/GetOnePenerimaanPembelian',
            dataType: 'json',
            data: {
                id_penerimaan: id_penerimaan
            },
            success: function (data) {
                if (data.st)
                {
                    var header = {};
                    header.id_pembelian_master = data.obj.id_pembelian_master;
                    header.nomor_master = data.obj.nomor_master;
                    header.tanggal_penerimaan = data.obj.tanggal_penerimaan;
                    header.tanggal = data.obj.tanggal;
                    header.kode_penerimaan = data.obj.kode_penerimaan;
                    $("#bodytable").empty();
                    $("#bodytable").html(data.html);
                    listheader.push(header);
                    $("#dd_id_supplier").val(data.obj.id_supplier);
                    if ($("#dd_id_supplier").val() != null)
                    {
                        $("#dd_id_supplier").attr("disabled", "disabled");
                        $("#txt_id_supplier").val(data.obj.id_supplier);
                    }
                    $("#dd_id_cabang").val(data.obj.id_cabang);
                    $("#dd_type_ppn").val(data.obj.type_ppn);
                    $("#dd_type_pembulatan").val(data.obj.type_pembulatan);
                    $("#txt_ppn").val(Comma(data.obj.ppn));
                    if ($("#dd_id_cabang").val() == null)
                    {
                        $("#dd_id_cabang").val(0);
                    }
                    else
                    {
                        $("#dd_id_cabang").attr("disabled", "disabled");
                        $("#txt_id_cabang").val(data.obj.id_cabang);
                    }
                    $("#dd_id_gudang").val(data.obj.id_gudang);
                    if ($("#dd_id_gudang").val() == null)
                    {
                        $("#dd_id_gudang").val(0);
                    }
                    else
                    {
                        $("#dd_id_gudang").attr("disabled", "disabled");
                        $("#txt_id_gudang").val(data.obj.id_gudang);
                    }
                    $("#txt_id_pembelian_master").val(data.obj.id_pembelian_master);
                    $("#txt_id_penerimaan").val(data.obj.id_penerimaan);
                    $(".kolomheaderpo").append("Nomor PO : " + DefaultDateFormat(data.obj.tanggal) + " == " + data.obj.kode_penerimaan + "<br/>Nomor Penerimaan : "+DefaultDateFormat(data.obj.tanggal_penerimaan) + " == " + data.obj.nomor_master +"<br/>");
                    RefreshPanelGrid();
                } else
                {
                    modaldialogerror("Pembelian tidak ada dalam database");
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });




        closemodalboostrap();
    }
    function InitialGridSearch()
    {
        tablesearch = $("#mytablesearch").dataTable({

            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": baseurl+"index.php/retur/beli/getdatapenerimaan", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frmhide").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_penerimaan",
                    title: "Kode",
                    width: "50px",
                    orderable: false,
                    className: "text-center nopadding",
                    mRender: function (data, type, row) {

                       return row['action_view']
                    }
                }
                
                , {data: "tanggal_penerimaan", orderable: false, title: "Tanggal", "width": "120px",
                    mRender: function (data, type, row) {
                        return DefaultDateFormat(data == undefined ? 0 : data);
                    }}
                , {data: "kode_penerimaan", orderable: false, title: "Kode&nbsp;Penerimaan", "width": "100px"}
                , {data: "nama_supplier", orderable: false, title: "Supplier", "width": "250px"}
                , {data: "nomor_master", orderable: false, title: "NO Doc", "width": "100px"}
                , {data: "nama_cabang", orderable: false, title: "Cabang", "width": "100px"}
                , {data: "nama_gudang", orderable: false, title: "Gudang", "width": "100px"}
               
            ],
            order: [[0, 'desc']],
            initComplete: function () {

            }

        });
    }



</script>