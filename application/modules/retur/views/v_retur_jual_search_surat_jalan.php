<div class="modal-header">
    Pencarian Penjualan
</div>
<div class="modal-body" style="min-width:100px;">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="portlet-body form">
                    <form id="frmhide" >
                        <input name="id_customer" type="hidden" value="<?php echo @$id_customer ?>">
                        <input name="id_cabang" type="hidden" value="<?php echo @$id_cabang ?>">
                        <input name="id_gudang" type="hidden" value="<?php echo @$id_gudang ?>">
                        <div class="form-group">
                            <?= form_label('Tanggal', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>

                            <div class="col-sm-2">
                                <?= form_input(array("selected" => "","autocomplete"=>"off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal_searching'), DefaultDatePicker(@$tanggal_awal), array('required' => 'required')); ?>
                            </div>
                            <?= form_label('s / d', "txt_tanggal_akhir", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-2">
                                <?= form_input(array("selected" => "","autocomplete"=>"off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir_searching'), DefaultDatePicker(@$tanggal)); ?>
                            </div>
                            <div class="col-sm-2">
                                <button id="btt_Search" type="submit" class="btn-small btn-block btn-primary pull-right">Search</button>
                            </div>

                        </div>

                    </form>
                    <table class="table table-striped table-bordered table-hover" id="mytablesearch" style="width:100%">

                    </table>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="detailpo">

            </div>
        </div>
    </div>
</div>
<script>
    var tablesearch;
    $(document).ready(function () {
        setTimeout(function () {
            InitialGridSearch();
        }, 500);
        $(".datepicker").datepicker();
    })
    $("form#frmhide").submit(function(){
        tablesearch.fnDraw(false);
        return false;
    })

    function select(id_surat_jalan_master)
    {
        $("#btt_search_po").css("display", "none");
        listheader = [];

        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/surat_jalan/GetOneSuratJalan',
            dataType: 'json',
            data: {
                id_surat_jalan_master: id_surat_jalan_master
            },
            success: function (data) {
                if (data.st)
                {
                    var header = {};
                    header.id_surat_jalan_master = data.obj.id_surat_jalan_master;
                    
                    $("#bodytable").empty();
                    $("#bodytable").html(data.html);
                    listheader.push(header);
                    $("#dd_id_customer").val(data.obj.id_customer);
                    if ($("#dd_id_customer").val() != null)
                    {
                        $("#dd_id_customer").attr("disabled", "disabled");
                        $("#txt_id_customer").val(data.obj.id_customer);
                    }
                    $("#dd_id_cabang").val(data.obj.id_cabang);
                    $("#dd_type_ppn").val(data.obj.type_ppn);
                    $("#dd_type_pembulatan").val(data.obj.type_pembulatan);
                    $("#txt_ppn").val(Comma(data.obj.ppn));
                    if ($("#dd_id_cabang").val() == null)
                    {
                        $("#dd_id_cabang").val(0);
                    }
                    else
                    {
                        $("#dd_id_cabang").attr("disabled", "disabled");
                        $("#txt_id_cabang").val(data.obj.id_cabang);
                    }
                    $("#dd_id_gudang").val(data.obj.id_gudang);
                    if ($("#dd_id_gudang").val() == null)
                    {
                        $("#dd_id_gudang").val(0);
                    }
                    else
                    {
                        $("#dd_id_gudang").attr("disabled", "disabled");
                        $("#txt_id_gudang").val(data.obj.id_gudang);
                    }
                    $("#txt_id_sales_order_master").val(data.obj.id_sales_order_master);
                    $("#txt_id_surat_jalan_master").val(data.obj.id_surat_jalan_master);
                    var headertulisan="Nomor Surat Jalan : " + DefaultDateFormat(data.obj.tanggal) + " == " + data.obj.nomor_master+"<br/>";
                    if(data.obj.nomor_so!=null&&data.obj.nomor_so!="")
                    {
                        headertulisan+="Nomor SO : " + DefaultDateFormat(data.obj.tanggal_so) + " == " + data.obj.nomor_so+"<br/>";
                    }
                    if(data.obj.nomor_invoice!=null&&data.obj.nomor_invoice!="")
                    {
                        headertulisan+="Nomor Penjualan : " + DefaultDateFormat(data.obj.tanggal_invoice) + " == " + data.obj.nomor_invoice+"<br/>";
                    }
                    $(".kolomheaderpo").html( headertulisan);
                    RefreshPanelGrid();
                } else
                {
                    modaldialogerror("Penjualan tidak ada dalam database");
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });




        closemodalboostrap();
    }
    function InitialGridSearch()
    {
        tablesearch = $("#mytablesearch").dataTable({

            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": baseurl+"index.php/retur/jual/getdatasurat_jalan", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frmhide").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_surat_jalan_master",
                    title: "Kode",
                    width: "50px",
                    orderable: false,
                    className: "text-center nopadding",
                    mRender: function (data, type, row) {

                       return row['action_view']
                    }
                }
                
                , {data: "tanggal", orderable: false, title: "Tanggal", "width": "120px",
                    mRender: function (data, type, row) {
                        return DefaultDateFormat(data == undefined ? 0 : data);
                    }}
                , {data: "nomor_master", orderable: false, title: "Kode&nbsp;Penerimaan", "width": "100px"}
                , {data: "nama_customer", orderable: false, title: "Customer", "width": "250px"}
                , {data: "nomor_invoice", orderable: false, title: "NO Invoice", "width": "100px"}
                , {data: "nama_cabang", orderable: false, title: "Cabang", "width": "100px"}
                , {data: "nama_gudang", orderable: false, title: "Gudang", "width": "100px"}
               
            ],
            order: [[0, 'desc']],
            initComplete: function () {

            }

        });
    }



</script>