
<?php
if (count($listdetail) > 0) {
    $listdetail = json_decode(json_encode($listdetail), true);
    foreach ($listdetail as $key => $detailsatuan) {
        echo "<tr>";
        echo form_input(array('type' => 'hidden', 'name' => 'id_surat_jalan_detail[]', 'value' => @$detailsatuan['id_surat_jalan_detail']));
        echo form_input(array('type' => 'hidden', 'name' => 'id_sales_order_detail[]', 'value' => $detailsatuan['id_sales_order_detail']));
        echo "<td>" . ($key + 1) . "</td>";
        echo "<td>" . $detailsatuan['sku'] . "</td>";
        echo "<td>" . $detailsatuan['nama_barang'] . "</td>";
        echo "<td>" . DefaultCurrency($detailsatuan['qty_pengiriman']) . "</td>";
        echo "<td>" . DefaultCurrency($detailsatuan['jumlahdiretur']- DefaultCurrencyDatabase($detailsatuan['qty_retur'])) . "</td>";
        echo "<td>" . form_input(array('type' => 'text', 'class' => 'form-control', "id" => "txt_price_barang_" . $key, 'name' => 'price_barang[]', 'value' => DefaultCurrency($detailsatuan['price_barang']), 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')) . "</td>";
        echo "<td>" . form_input(array('type' => 'text', 'class' => 'form-control returcolumn', "id" => "txt_qty_retur_" . $key, 'name' => 'qty_retur[]', 'value' => DefaultCurrency($detailsatuan['qty_retur']), 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')) . "</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>Keterangan</td>";
        echo "<td colspan='6'>" . form_input(array('type' => 'text', 'class' => 'form-control', "id" => "txt_keterangan_retur_" . $key, 'name' => 'keterangan_retur[]', 'value' => @$detailsatuan['keterangan_retur'])) . "</td>";
        echo "</tr>";
    }
} else {
    echo '<tr><td align="center" colspan="6">No Data Display</td></tr>';
}
?>