<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_retur_beli extends CI_Model {

    public $table = 'uap_retur_beli_master';
    public $id = 'id_retur_pembelian_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }
    
    
    function BatalRetur($id_retur_beli_master){
        $returbeli = $this->GetOneReturBeli($id_retur_beli_master);
        $this->load->model("supplier/m_supplier");
        $message = "";
        if ($returbeli != null) {
            if ($returbeli->status == "2")
                $message .= "Retur Beli sudah dibatalkan sebelumnya<br/>";
        }
        else {
            $message .= "Retur Beli tidak ada di database<br/>";
        }
        if ($message=="") {
            $this->db->trans_rollback();
            $this->db->trans_begin();

            $this->db->from("uap_retur_beli_detail");
            $this->db->where(array("id_retur_pembelian_master" => $id_retur_beli_master));
            
            try {
                $listdetail = $this->db->get()->result();
                foreach($listdetail as $detail)
                {
                    $this->db->from("uap_barang_history");
                    $this->db->where(array("id_barang_history"=>$detail->id_barang_history));
                    $rowhistory=$this->db->get()->row();
                    if($rowhistory==null)
                    {
                        $message.="History Untuk barang ".$detail->nama_barang ." tidak ada dalam database<br/>";
                        break;
                    }
                    else if($rowhistory!=null && $rowhistory->stock_tersedia < $rowhistory->stock_sisa+$detail->qty_retur)
                    {
                        $message.="Quota untuk  ".$detail->nama_barang ." melebihi batas<br/>";
                        break;
                    }
                    $this->db->update("uap_barang_history",array("stock_sisa"=>$rowhistory->stock_sisa+$detail->qty_retur),array("id_barang_history"=>$rowhistory->id_barang_history));
                    $this->db->from("uap_barang_jurnal");
                    $this->db->where(array("dokumen" => $returbeli->kode_retur_pembelian,"reference_id"=>$detail->id_retur_beli_detail));
                    $rowjurnal = $this->db->get()->row();
                    if ($rowjurnal == null) {
                        $message .= "Jurnal Untuk barang " . $detail->nama_barang . " tidak ada dalam database<br/>";
                        break;
                    } else if ($rowjurnal != null && $rowjurnal->debet ==0 &&  $rowjurnal->kredit==0) {
                        $message .= "Sudah pernah terjadi pembatalan transaksi untuk  " . $detail->nama_barang . "<br/>";
                        break;
                    }
                    $this->db->update("uap_barang_jurnal", array("debet" => 0, "kredit" => 0), array("id_jurnal_barang" => $rowjurnal->id_jurnal_barang));
                }
                if($message=="")
                {
                    $this->db->update("uap_retur_beli_master",array("status"=>2,"updated_date"=> GetDateNow(),"updated_by"=> GetUserId()),array("id_retur_pembelian_master"=>$id_retur_beli_master));
                }
                
                $this->db->from("uap_buku_besar");
                $this->db->where(array("dokumen"=>$returbeli->kode_retur_pembelian));
                $listbukubesar=$this->db->get()->result();
                
                foreach($listbukubesar as $buku)
                {
                     $this->db->update("uap_buku_besar",array("updated_date"=> GetDateNow(),"updated_by"=> GetUserId(),"debet"=>0,"kredit"=>0,"keterangan"=>"Batal oleh ". GetUsername()." | ".$buku->keterangan),array("id_buku_besar"=>$buku->id_buku_besar));
             
                }
                if($returbeli->jenis_potongan=="Potongan Utang")
                {
                    $this->m_supplier->KoreksiSaldoHutang($returbeli->id_supplier);
                }
                
                
                if ($this->db->trans_status() === FALSE || $message != '') {
                    $this->db->trans_rollback();
                    $message = "Some Error Occured<br/>" . $message;
                    return array("st" => false, "msg" => $message);
                } else {
                    $this->db->trans_commit();
                    $arrayreturn["st"] = true;
                    $arrayreturn["msg"] =  "Data Retur ".$returbeli->kode_retur_pembelian ." sudah dibatalkan";
                    return $arrayreturn;
                }
            } catch (Exception $ex) {
                $this->db->trans_rollback();
                return array("st" => false, "msg" => $ex->getMessage());
            }
        }
        else{
             return array("st" => false, "msg" => $message);
        }
    }
    
    
    function GetDetailReturBeli($id) {
        $this->load->model("pembelian/m_pembelian");
        $this->db->from("uap_retur_beli_detail");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_retur_beli_detail.id_barang");
        $this->db->join("uap_penerimaan_pembelian_detail", "uap_penerimaan_pembelian_detail.id_penerimaan_detail=uap_retur_beli_detail.id_penerimaan_detail","left");
        $this->db->select("sku_barang as sku,uap_retur_beli_detail.*,uap_penerimaan_pembelian_detail.qty,ifnull(uap_penerimaan_pembelian_detail.qty,0) as jumlahditerima,uap_retur_beli_detail.price as price_barang");
        $this->db->where(array("uap_retur_beli_detail.id_retur_pembelian_master" => $id));
       
        $listdetail = $this->db->get()->result();
        foreach ($listdetail as $detail) {
            $detail->jumlahdiretur = $this->GetJumlahRetur(@$detail->id_penerimaan_detail,"penerimaan");
        }
        return $listdetail;
    }
    function GetListPenerimaan($id, $id_pembelian = 0,$fromretur=0) {
        $this->db->from("uap_pembelian_detail");
        $this->db->join("uap_penerimaan_pembelian_detail", "uap_pembelian_detail.id_pembelian_detail=uap_penerimaan_pembelian_detail.id_pembelian_detail and uap_penerimaan_pembelian_detail.id_penerimaan=" . $id, "left");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_pembelian_detail.id_barang");
        $this->db->select("uap_pembelian_detail.price_barang,uap_pembelian_detail.ppn_satuan,uap_penerimaan_pembelian_detail.id_penerimaan_detail,uap_barang.sku_barang as sku,uap_pembelian_detail.id_pembelian_detail,uap_pembelian_detail.nama_barang,uap_pembelian_detail.qty,ifnull(uap_penerimaan_pembelian_detail.qty,0) as jumlahditerima");
        $this->db->where(array("uap_pembelian_detail.id_pembelian_master" => $id_pembelian));
        if ($fromretur) {
           $this->db->where(array("id_penerimaan_detail !=" =>$id)); 
        }
        $this->load->model("retur/m_retur_beli");
        $listdetail = $this->db->get()->result();
        foreach ($listdetail as $detail) {
            $detail->jumlahpenerimaan = $this->GetJumlahPenerimaan(@$detail->id_penerimaan_detail,"penerimaan") - $detail->jumlahditerima;
            $detail->jumlahdiretur =$this->m_retur_beli->GetJumlahRetur(@$detail->id_penerimaan_detail,"penerimaan");
           
        }
        return $listdetail;
    }
    function GetOneReturBeli($id_retur,$isfull=false)
    {
        $this->db->from("uap_retur_beli_master");
        $this->db->where(array("id_retur_pembelian_master"=>$id_retur));
        $this->db->join("uap_pembelian_master", "uap_pembelian_master.id_pembelian_master=uap_retur_beli_master.id_pembelian_master", "left");
        $this->db->join("uap_supplier", "uap_supplier.id_supplier=uap_retur_beli_master.id_supplier", "left");
        $this->db->join("uap_gudang", "uap_gudang.id_gudang=uap_retur_beli_master.id_gudang", "left");
        $this->db->join("uap_cabang", "uap_cabang.id_cabang=uap_retur_beli_master.id_cabang", "left");
  
       
        $this->db->select("uap_retur_beli_master.*,nomor_master,nama_cabang,nama_gudang,nama_supplier");
        $row=$this->db->get()->row();
        
        if($isfull)
        {
            $row->listdetail=$this->GetDetailReturBeli($id_retur);
        }
        
        return $row;
    }
    function GetDataReturBeli($params)
    {
        $isedit = CekModule("K105", false);
        $isdelete = CekModule("K106", false);
        $this->load->library('datatables');
        $this->datatables->select('uap_retur_beli_master.tanggal,kode_retur_pembelian,uap_retur_beli_master.id_retur_pembelian_master,nama_gudang,nama_cabang,nama_supplier,replace(kode_retur_pembelian,"/","-") as nomor_master_link,kode_penerimaan,nomor_master,uap_retur_beli_master.status');
        $this->datatables->join("uap_supplier", "uap_supplier.id_supplier=uap_retur_beli_master.id_supplier", "left");
        $this->datatables->join("uap_pembelian_master", "uap_pembelian_master.id_pembelian_master=uap_retur_beli_master.id_pembelian_master", "left");
        $this->datatables->join("uap_penerimaan_pembelian", "uap_penerimaan_pembelian.id_penerimaan=uap_retur_beli_master.id_penerimaan", "left");
        
        $this->datatables->join("uap_cabang", "uap_cabang.id_cabang=uap_retur_beli_master.id_cabang", "left");
        $this->datatables->join("uap_gudang", "uap_gudang.id_gudang=uap_retur_beli_master.id_gudang", "left");

        $this->datatables->from('uap_retur_beli_master');
        $straction = '';
        $extra = array();
        
        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['uap_retur_beli_master.id_cabang'] = $params['id_cabang'];
        }
        if (isset($params['id_supplier']) && !CheckEmpty(@$params['id_supplier'])) {
            $params['uap_retur_beli_master.id_supplier'] = $params['id_supplier'];
        }
        if (isset($params['id_gudang']) && !CheckEmpty(@$params['id_gudang'])) {
            $params['uap_retur_beli_master.id_gudang'] = $params['id_gudang'];
        }
        if(!CheckEmpty($params['status']))
        {
            $params['uap_retur_beli_master.status']=$params['status'];
        }
        unset($params['id_supplier']);
        unset($params['id_gudang']);
        unset($params['id_cabang']);
        unset($params['status']);

       
        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "uap_retur_beli_master.tanggal BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        else if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['uap_retur_beli_master.tanggal'] = DefaultTanggalDatabase($params['start_date']);
             unset($params['start_date'], $params['end_date']);
        }
        else if (isset($params['end_date']) && empty($params['start_date']))
        {
            $params['uap_retur_beli_master.tanggal'] = DefaultTanggalDatabase($params['end_date']);
             unset($params['start_date'], $params['end_date']);
        }
       

        if (count($params)) {
            $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        $stredit='';
        $strdelete='';
        $strview = anchor("", 'Select', array('class' => 'btn btn-default btn-xs', "onclick" => "viewretur($1);return false;"));
        if ($isedit) {
            $stredit = anchor("retur/beli/editreturbeli/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $strdelete .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batal($1);return false;"));
        }
        $this->datatables->add_column('action_view', $strview, 'id_retur_pembelian_master,nomor_master_link');
        $this->datatables->add_column('action_edit', $stredit, 'id_retur_pembelian_master,nomor_master_link');
        $this->datatables->add_column('action_delete', $strdelete, 'id_retur_pembelian_master,nomor_master_link');
        return $this->datatables->generate();
    }
    
    function ReturPembelian($model) {
        try {
            $message = "";
            
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            
            $this->load->model("barang/m_barang");
            $this->load->model("cabang/m_cabang");
            $this->load->model("pembelian/m_pembelian");
            $this->load->model("supplier/m_supplier");
            $retur['tanggal'] = DefaultTanggalDatabase($model['tanggal']);
            $retur['id_supplier'] = $model['id_supplier'];
            $supplier = $this->m_supplier->GetOneSupplier($retur['id_supplier']);
            $retur['id_cabang'] = $model['id_cabang'];
            $retur['jenis_potongan'] = $model['jenis_potongan'];
            $retur['type_ppn'] = $model['type_ppn'];
            $retur['type_pembulatan'] = $model['type_pembulatan'];
            $retur['id_penerimaan'] = $model['id_penerimaan'];
            $retur['total_potongan'] = DefaultCurrencyDatabase($model['total_potongan']);
            $retur['ppn'] = DefaultCurrencyDatabase($model['ppn']);
            $retur['ppn_nominal'] = DefaultCurrencyDatabase($model['ppn_nominal']) ;
            $retur['grandtotal'] = DefaultCurrencyDatabase($model['grandtotal']) ;
            $retur['id_gudang'] = $model['id_gudang'];
            $retur['id_pembelian_master'] = $model['id_pembelian_master'];
            $pembelian = $this->m_pembelian->GetOnePembelian($model['id_pembelian_master']);
            $listidpembelian = [];
            $listpenerimaan = [];
            $listpricebarang = [];
            $listketerangan = [];
            $listqty=[];
            $messageerror = "";
            if (CheckArray($model, 'id_pembelian_detail')) {
                $listidpembelian = $model['id_pembelian_detail'];
            }
            if (CheckArray($model, 'id_penerimaan_detail')) {
                $listpenerimaan = $model['id_penerimaan_detail'];
            }
            if (CheckArray($model, 'price_barang')) {
                $listpricebarang = $model['price_barang'];
            }
            if (CheckArray($model, 'keterangan_retur')) {
                $listketerangan = $model['keterangan_retur'];
            }
            if (CheckArray($model, 'qty_retur')) {
                $listqty = $model['qty_retur'];
            }
            
            $kodepenerimaan = '';
            $id_penerimaan = 0;
            if (!CheckEmpty($model['id_retur_pembelian_master'])) {
                $returfromdb = $this->GetOneReturBeli($model['id_retur_pembelian_master']);
                if ($returfromdb != null) {
                    $koderetur = $returfromdb->kode_retur_pembelian;
                }
                $retur['updated_date'] = GetDateNow();
                $retur['updated_by'] = $userid;
                $this->db->update("uap_retur_beli_master", $retur, array("id_retur_pembelian_master" => $model['id_retur_pembelian_master']));
                $id_retur_pembelian_master = $model['id_retur_pembelian_master'];
            } else {
                $cabang = $this->m_cabang->GetOneCabang($model['id_cabang']);
                $koderetur = AutoIncrement('uap_retur_beli_master', $cabang->kode_cabang . '/RRB/' . date("y") . '/', 'kode_retur_pembelian', 5);
                $retur['kode_retur_pembelian'] = $koderetur;
                $retur['created_date'] = GetDateNow();
                $retur['created_by'] = $userid;
                $retur['status'] = 1;
                $this->db->insert("uap_retur_beli_master", $retur);
                $id_retur_pembelian_master = $this->db->insert_id();
            }
            $totalreturwithoutppn =0;
            $totalreturwithppn =0;
            foreach ($listidpembelian as $key => $itempembelian) {
                $this->db->from("uap_retur_beli_detail");
                $this->db->where(array("id_pembelian_detail" => $itempembelian, "id_retur_pembelian_master" => $id_retur_pembelian_master));
                $rowitem = $this->db->get()->row();
                $updateitem = [];
                $updateitem['qty_retur'] = DefaultCurrencyDatabase($listqty[$key]);
                $updateitem['price'] = DefaultCurrencyDatabase($listpricebarang[$key]) ;
                if ($retur['type_ppn'] == "1") {
                    $updateitem['ppn_satuan'] = ($updateitem['qty_retur']*$updateitem['price']) * ((float) $retur['ppn'] / ((float) $retur['ppn'] + (float) 100));
                    $totalreturwithoutppn += ($updateitem['qty_retur']*$updateitem['price']) - $updateitem['ppn_satuan'];
                } else if ($retur['type_ppn'] == "2") {
                    $updateitem['ppn_satuan'] = ($updateitem['qty_retur']*$updateitem['price']) * ((float) $retur['ppn'] / (float) 100);
                    $totalreturwithoutppn += ($updateitem['qty_retur']*$updateitem['price']);
                } else {
                    $updateitem['ppn_satuan'] = 0;
                    $totalreturwithoutppn += ($updateitem['qty_retur']*$updateitem['price']);
                }
                $totalreturwithppn += ($updateitem['qty_retur']*$updateitem['price']);
                
                $detailpenerimaan = $this->m_pembelian->GetDetailPenerimaanSummary($listpenerimaan[$key]);

                if ($detailpenerimaan == null) {
                    $messageerror .= "Ada Detail Pembelian yang tidak terdapat di penerimaan<br/>";
                } else {
                   
                    
                     if ($detailpenerimaan->qtyretur - (CheckEmpty($rowitem) ? 0 : $rowitem->qty_retur) + $updateitem['qty_retur'] > $detailpenerimaan->qty) {
                        $messageerror .= "Detail Penerimaan " . $detailpenerimaan->nama_barang . " lebih besar daripada qty penerimaan<br/>";
                        break;
                    }

                    $updateitem['nama_barang'] = $detailpenerimaan->nama_barang;
                    $updateitem['id_barang'] = $detailpenerimaan->id_barang;
                    $updateitem['id_pembelian_detail'] = $detailpenerimaan->id_pembelian_detail;
                    $updateitem['id_penerimaan_detail'] = $detailpenerimaan->id_penerimaan_detail;
                    $updateitem['keterangan_retur'] = $listketerangan[$key];
                    $id_history = 0;
                    $barang_history = null;
                   
                    if ($rowitem != null)
                        $barang_history = $this->m_barang->GetDetailHistoryBarang($rowitem->id_barang_history);
                    if ($rowitem == null) {
                        $this->db->from("uap_penerimaan_pembelian_detail");
                        $this->db->where(array("id_pembelian_detail" => $itempembelian, "id_penerimaan" => $model['id_penerimaan']));
                        $rowpenerimaan = $this->db->get()->row();
                       
                        if($rowpenerimaan!=null)
                        {
                            $barang_history = $this->m_barang->GetDetailHistoryBarang($rowpenerimaan->id_barang_history);
                        }
                        
                    }
                     
                    if ($barang_history != null) {
                        $id_history = $barang_history->id_barang_history;
                        if ($barang_history->stock_sisa +(CheckEmpty($rowitem) ? 0 : $rowitem->qty_retur) <  $updateitem['qty_retur']) {
                            $messageerror .= $detailpenerimaan->nama_barang .= " tidak mencukupi untuk dikeluarkan<br/>";
                            break;
                        }  else {
                            $historybar = [];
                            $historybar['stock_sisa'] = $barang_history->stock_sisa + (CheckEmpty($rowitem) ? 0 : $rowitem->qty_retur)- $updateitem['qty_retur'];
                            $historybar['updated_date'] = GetDateNow();
                            $historybar['updated_by'] = $userid;
                            $this->db->update("uap_barang_history", $historybar, array("id_barang_history" => $barang_history->id_barang_history));
                        }
                    } else if ($updateitem['qty_retur'] > 0) {
                         $messageerror .= $detailpenerimaan->nama_barang .= " tidak ada data barang yang bisa untuk dikeluarkan<br/>";
                         break;
                    }


                    $updateitem['id_barang_history'] = $id_history;
                    $id_retur_beli_detail = 0;
                    
                    if ($rowitem != null) {
                        $updateitem['updated_date'] = GetDateNow();
                        $updateitem['updated_by'] = $userid;
                        $this->db->update("uap_retur_beli_detail", $updateitem, array("id_retur_beli_detail" => $rowitem->id_retur_beli_detail));
                        $id_retur_beli_detail = $rowitem->id_retur_beli_detail;
                    } else if ($updateitem['qty_retur'] > 0) {
                        $updateitem['id_retur_pembelian_master'] = $id_retur_pembelian_master;
                        $updateitem['created_date'] = GetDateNow();
                        $updateitem['created_by'] = $userid;
                        $this->db->insert("uap_retur_beli_detail", $updateitem);
                        $id_retur_beli_detail = $this->db->insert_id();
                    }

                    $this->db->from("uap_barang_jurnal");
                    $this->db->where(array("dokumen" => $koderetur, "reference_id" => $id_retur_beli_detail));
                    $jurnal = $this->db->get()->row();
                    $updatejurnal = [];
                    $updatejurnal['id_barang_history'] = $id_history;
                    $updatejurnal['id_barang'] = $detailpenerimaan->id_barang;
                    $updatejurnal['debet'] = 0;
                    $updatejurnal['kredit'] = $updateitem['qty_retur'];
                    $updatejurnal['tanggal_jurnal'] = $retur['tanggal'];
                    $updatejurnal['id_gudang'] = $model['id_gudang'];

                    if ($jurnal != null) {


                        $updatejurnal['updated_date'] = GetDateNow();
                        $updatejurnal['updated_by'] = $userid;
                        $this->db->update("uap_barang_jurnal", $updatejurnal, array("id_jurnal_barang" => $jurnal->id_jurnal_barang));
                    } else if ($updateitem['qty_retur'] > 0) {
                        $updatejurnal['reference_id'] = $id_retur_beli_detail;
                        $updatejurnal['dokumen'] = $koderetur;
                        $updatejurnal['description'] = "Retur Pembelian " . $pembelian->nomor_master;
                        $updatejurnal['harga_beli_akhir'] = 0;
                        $updatejurnal['created_date'] = GetDateNow();
                        $updatejurnal['created_by'] = $userid;
                        $updatejurnal['harga'] = $updateitem['price'];
                        $this->db->insert("uap_barang_jurnal", $updatejurnal);
                    }
                    $this->m_barang->SyncronizeHargaAverage($updatejurnal['id_barang'], $updatejurnal['id_gudang'], $updatejurnal['tanggal_jurnal']);
                }
            }
            
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "3", "dokumen" => $koderetur));
            $akun_kas = $this->db->get()->row();
            
            if (CheckEmpty($akun_kas) && $model['jenis_potongan'] == "Cash") {
                $akunkasinsert = array();
                $akunkasinsert['id_gl_account'] = "3";
                $akunkasinsert['tanggal_buku'] = $retur['tanggal'];
                $akunkasinsert['id_cabang'] = $retur['id_cabang'];
                $akunkasinsert['keterangan'] = "Pengembalian kas untuk retur pembelian dari " . @$supplier->nama_supplier . '  untuk invoice ' . $pembelian->nomor_master;
                $akunkasinsert['dokumen'] = $koderetur;
                $akunkasinsert['subject_name'] = @$supplier->kode_supplier;
                $akunkasinsert['id_subject'] = $retur['id_supplier'];
                $akunkasinsert['debet'] = $retur['grandtotal'];
                $akunkasinsert['kredit'] = 0;
                $akunkasinsert['created_date'] = GetDateNow();
                $akunkasinsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akunkasinsert);
            } else if (!CheckEmpty($akun_kas)) {
                $akunkasupdate['tanggal_buku'] = $retur['tanggal'];
                $akunkasupdate['id_cabang'] = $retur['id_cabang'];
                $akunkasupdate['keterangan'] = "Pengembalian kas untuk retur pembelian dari " . @$supplier->nama_supplier . '  untuk invoice ' . $pembelian->nomor_master;
                $akunkasupdate['dokumen'] = $koderetur;
                $akunkasupdate['subject_name'] = @$supplier->kode_supplier;
                $akunkasupdate['id_subject'] = $retur['id_supplier'];
                $akunkasupdate['debet'] =$retur['grandtotal'];
                $akunkasupdate['kredit'] = 0;
                $akunkasupdate['updated_date'] = GetDateNow();
                $akunkasupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akunkasupdate, array("id_buku_besar" => $akun_kas->id_buku_besar));
            }
            
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "11", "dokumen" => $koderetur));
            $akun_utang = $this->db->get()->row();
            
            if (CheckEmpty($akun_utang) && $model['jenis_potongan'] == "Potongan Utang") {
                $akunutanginsert = array();
                $akunutanginsert['id_gl_account'] = "11";
                $akunutanginsert['tanggal_buku'] = $retur['tanggal'];
                $akunutanginsert['id_cabang'] = $retur['id_cabang'];
                $akunutanginsert['keterangan'] = "Potongan Utang untuk retur pembelian dari " . @$supplier->nama_supplier . '  untuk invoice ' . $pembelian->nomor_master;
                $akunutanginsert['dokumen'] = $koderetur;
                $akunutanginsert['subject_name'] = @$supplier->kode_supplier;
                $akunutanginsert['id_subject'] = $retur['id_supplier'];
                $akunutanginsert['debet'] = $retur['grandtotal'];
                $akunutanginsert['kredit'] = 0;
                $akunutanginsert['created_date'] = GetDateNow();
                $akunutanginsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akunutanginsert);
            } else if (!CheckEmpty($akun_utang)) {
                $akunutangupdate['tanggal_buku'] = $retur['tanggal'];
                $akunutangupdate['id_cabang'] = $retur['id_cabang'];
                $akunutangupdate['keterangan'] = "Potongan Utang untuk retur pembelian dari " . @$supplier->nama_supplier . '  untuk invoice ' . $pembelian->nomor_master;
                $akunutangupdate['dokumen'] = $koderetur;
                $akunutangupdate['subject_name'] = @$supplier->kode_supplier;
                $akunutangupdate['id_subject'] = $retur['id_supplier'];
                $akunutangupdate['debet'] = $retur['grandtotal'];
                $akunutangupdate['kredit'] = 0;
                $akunutangupdate['updated_date'] = GetDateNow();
                $akunutangupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akunutangupdate, array("id_buku_besar" => $akun_utang->id_buku_besar));
            }
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "31", "dokumen" => $koderetur));
            $akun_potongan = $this->db->get()->row();

            if (CheckEmpty($akun_potongan) && $retur['total_potongan'] > 0) {
                $akunpotonganinsert = array();
                $akunpotonganinsert['id_gl_account'] = "31";
                $akunpotonganinsert['tanggal_buku'] = $retur['tanggal'];
                $akunpotonganinsert['id_cabang'] = $retur['id_cabang'];
                $akunpotonganinsert['keterangan'] = "Potongan  untuk retur pembelian dari " . @$supplier->nama_supplier . '  untuk invoice ' . $pembelian->nomor_master;
                $akunpotonganinsert['dokumen'] = $koderetur;
                $akunpotonganinsert['subject_name'] = @$supplier->kode_supplier;
                $akunpotonganinsert['id_subject'] = $retur['id_supplier'];
                $akunpotonganinsert['debet'] = $retur['total_potongan'];
                $akunpotonganinsert['kredit'] = 0;
                $akunpotonganinsert['created_date'] = GetDateNow();
                $akunpotonganinsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akunpotonganinsert);
            } else if (!CheckEmpty($akun_potongan)) {
                $akunpotonganupdate['tanggal_buku'] = $retur['tanggal'];
                $akunpotonganupdate['id_cabang'] = $retur['id_cabang'];
                $akunpotonganupdate['keterangan'] =  "Potongan  untuk retur pembelian dari " . @$supplier->nama_supplier . '  untuk invoice ' . $pembelian->nomor_master;
                $akunpotonganupdate['dokumen'] = $koderetur;
                $akunpotonganupdate['subject_name'] = @$supplier->kode_supplier;
                $akunpotonganupdate['id_subject'] = $retur['id_supplier'];
                $akunpotonganupdate['debet'] = $retur['total_potongan'];
                $akunpotonganupdate['kredit'] = 0;
                $akunpotonganupdate['updated_date'] = GetDateNow();
                $akunpotonganupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akunpotonganupdate, array("id_buku_besar" => $akun_potongan->id_buku_besar));
            }
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "30", "dokumen" => $koderetur));
            $akun_retur = $this->db->get()->row();
            if (CheckEmpty($akun_retur)) {
                $akunreturinsert = array();
                $akunreturinsert['id_gl_account'] = "30";
                $akunreturinsert['tanggal_buku'] = $retur['tanggal'];
                $akunreturinsert['id_cabang'] = $retur['id_cabang'];
                $akunreturinsert['keterangan'] = "Retur beli dari invoice " . @$pembelian->nomor_master .' dari '.@$supplier->nama_supplier;
                $akunreturinsert['dokumen'] = $koderetur;
                $akunreturinsert['subject_name'] = @$supplier->kode_supplier;
                $akunreturinsert['id_subject'] = $retur['id_supplier'];
                $akunreturinsert['debet'] = 0;
                $akunreturinsert['kredit'] = DefaultCurrencyDatabase($model['type_ppn'] == "1" ? $totalreturwithppn * 100 / (100 + $retur['ppn']) : $totalreturwithoutppn, 0);
                $akunreturinsert['created_date'] = GetDateNow();
                $akunreturinsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akunreturinsert);
            } else {
                $akunreturupdate['tanggal_buku'] = $retur['tanggal'];
                $akunreturupdate['id_cabang'] = $retur['id_cabang'];
                $akunreturupdate['keterangan'] = "Retur beli dari invoice " . @$pembelian->nomor_master .' dari '.@$supplier->nama_supplier;
                $akunreturupdate['dokumen'] = $koderetur;
                $akunreturupdate['subject_name'] = @$supplier->kode_supplier;
                $akunreturupdate['id_subject'] = $retur['id_supplier'];
                $akunreturupdate['debet'] = 0;
                $akunreturupdate['kredit'] = DefaultCurrencyDatabase($model['type_ppn'] == "1" ? $totalreturwithppn * 100 / (100 + $retur['ppn']) : $totalreturwithoutppn, 0);
                $akunreturupdate['updated_date'] = GetDateNow();
                $akunreturupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akunreturupdate, array("id_buku_besar" => $akun_retur->id_buku_besar));
            }
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "33", "dokumen" => $koderetur));
            $akun_ppn = $this->db->get()->row();

            if (CheckEmpty($akun_ppn) && $retur['ppn_nominal'] > 0) {
                $akunppninsert = array();
                $akunppninsert['id_gl_account'] = "33";
                $akunppninsert['tanggal_buku'] = $retur['tanggal'];
                $akunppninsert['id_cabang'] = $retur['id_cabang'];
                $akunppninsert['keterangan'] = "Potongan ppn untuk Retur beli dari invoice " . @$pembelian->nomor_master .' dari '.@$supplier->nama_supplier;
                $akunppninsert['dokumen'] = $koderetur;
                $akunppninsert['subject_name'] = @$supplier->kode_supplier;
                $akunppninsert['id_subject'] = $retur['id_supplier'];
                $akunppninsert['debet'] = 0;
                $akunppninsert['kredit'] = $retur['ppn_nominal'];
                $akunppninsert['created_date'] = GetDateNow();
                $akunppninsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akunppninsert);
            } else if (!CheckEmpty($akun_ppn)) {
                $akunppnupdate['tanggal_buku'] = $retur['tanggal'];
                $akunppnupdate['id_cabang'] = $retur['id_cabang'];
                $akunppnupdate['keterangan'] = "Potongan ppn untuk Retur beli dari invoice " . @$pembelian->nomor_master .' dari '.@$supplier->nama_supplier;
                $akunppnupdate['dokumen'] = $koderetur;
                $akunppnupdate['subject_name'] = @$supplier->kode_supplier;
                $akunppnupdate['id_subject'] = $retur['id_supplier'];
                $akunppnupdate['debet'] = 0;
                $akunppnupdate['kredit'] = $retur['ppn_nominal'];
                $akunppnupdate['updated_date'] = GetDateNow();
                $akunppnupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akunppnupdate, array("id_buku_besar" => $akun_ppn->id_buku_besar));
            }
            
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("dokumen" => $koderetur));
            $this->db->select("sum(debet)-sum(kredit) as selisih");
            $row = $this->db->get()->row();
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "38", "dokumen" => $koderetur));
            $akun_penyeimbang = $this->db->get()->row();

            if (CheckEmpty($akun_penyeimbang) && $row && $row->selisih > 0) {
                $akun_penyeimbanginsert = array();
                $akun_penyeimbanginsert['id_gl_account'] = "38";
                $akun_penyeimbanginsert['tanggal_buku'] = $retur['tanggal'];
                $akun_penyeimbanginsert['id_cabang'] = $retur['id_cabang'];
                $akun_penyeimbanginsert['keterangan'] = "Penyeimbang untuk retur pembelian Ke " . @$supplier->nama_supplier . ' untuk dokumen ' . $pembelian->nomor_master;
                $akun_penyeimbanginsert['dokumen'] = $koderetur;
                $akun_penyeimbanginsert['subject_name'] = @$supplier->kode_supplier;
                $akun_penyeimbanginsert['id_subject'] = $retur['id_supplier'];
                $akun_penyeimbanginsert['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['created_date'] = GetDateNow();
                $akun_penyeimbanginsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akun_penyeimbanginsert);
            } else if (!CheckEmpty($akun_penyeimbang)) {
                $akun_penyeimbangupdate['tanggal_buku'] = $retur['tanggal'];
                $akun_penyeimbangupdate['id_cabang'] = $retur['id_cabang'];
                $akun_penyeimbangupdate['keterangan'] = "Penyeimbang untuk retur pembelian Ke " . @$supplier->nama_supplier . ' untuk dokumen ' . $pembelian->nomor_master;
                $akun_penyeimbangupdate['dokumen'] = $koderetur;
                $akun_penyeimbangupdate['subject_name'] = @$supplier->kode_supplier;
                $akun_penyeimbangupdate['id_subject'] = $retur['id_supplier'];
                $akun_penyeimbangupdate['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['updated_date'] = GetDateNow();
                $akun_penyeimbangupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akun_penyeimbangupdate, array("id_buku_besar" => $akun_penyeimbang->id_buku_besar));
            }
            $this->db->from("uap_pembayaran_utang_detail");
            $this->db->join("uap_pembayaran_utang_master", "uap_pembayaran_utang_master.pembayaran_utang_id=uap_pembayaran_utang_detail.pembayaran_utang_id");
            $this->db->where(array("keterangan" => "Potongan Retur ".$koderetur, "id_pembelian_master" => $model['id_pembelian_master']));

            $pembayaran_utang_master = $this->db->get()->row();
            if ($pembayaran_utang_master != null && $retur['jenis_potongan'] == "Cash") {
                $this->db->delete("uap_pembayaran_utang_detail", array("pembayaran_utang_detail_id" => $pembayaran_utang_master->pembayaran_utang_detail_id));
                $this->db->delete("uap_pembayaran_utang_master", array("pembayaran_utang_id" => $pembayaran_utang_master->pembayaran_utang_id));
            } else if ($pembayaran_utang_master != null && $pembayaran_utang_master->jumlah_bayar != $retur['grandtotal']) {
                $updatedet = array();
                $updatedet['updated_date'] = GetDateNow();
                $updatedet['updated_by'] = $userid;
                $updatedet['jumlah_bayar'] = $retur['grandtotal'];
                $updatemas = array();
                $updatemas['updated_date'] = GetDateNow();
                $updatemas['updated_by'] = $userid;
                $updatemas['total_bayar'] = $retur['grandtotal'];
                $this->db->update("uap_pembayaran_utang_detail", $updatedet, array("pembayaran_utang_detail_id" => $pembayaran_utang_master->pembayaran_utang_detail_id));
                $this->db->update("uap_pembayaran_utang_master", $updatemas, array("pembayaran_utang_id" => $pembayaran_utang_master->pembayaran_utang_id));
            } else if ($pembayaran_utang_master == null && $retur['jenis_potongan'] !="Cash") {
                $dokumenpembayaran = AutoIncrement('uap_pembayaran_utang_master', $cabang->kode_cabang . '/BB/' . date("y") . '/', 'dokumen', 5);
                $pembayaranmaster = array();
                $pembayaranmaster['dokumen'] = $dokumenpembayaran;
                $pembayaranmaster['tanggal_transaksi'] = $retur['tanggal'];
                $pembayaranmaster['id_supplier'] = $retur['id_supplier'];
                $pembayaranmaster['jenis_pembayaran'] = "retur";
                $pembayaranmaster['total_bayar'] =  $retur['grandtotal'] ;
                $pembayaranmaster['biaya'] = 0;
                $pembayaranmaster['potongan'] = 0;
                $pembayaranmaster['status'] = 1;
                $pembayaranmaster['keterangan'] = "Potongan Retur ".$koderetur;
                $pembayaranmaster['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
                $pembayaranmaster['created_date'] = GetDateNow();
                $pembayaranmaster['created_by'] = $userid;
                $this->db->insert("uap_pembayaran_utang_master", $pembayaranmaster);
                $pembayaran_utang_id = $this->db->insert_id();
                $pembayarandetail = array();
                $pembayarandetail['id_pembelian_master'] =  $model['id_pembelian_master'];
                $pembayarandetail['pembayaran_utang_id'] = $pembayaran_utang_id;
                $pembayarandetail['jumlah_bayar'] = $retur['grandtotal'];
                $pembayarandetail['created_date'] = GetDateNow();
                $pembayarandetail['created_by'] = $userid;
                $this->db->insert("uap_pembayaran_utang_detail", $pembayarandetail);
            }
            if ($retur['jenis_potongan'] == "Potongan Utang") {
                $this->m_supplier->KoreksiSaldoHutang($retur['id_supplier']);
            }



            $message .= $messageerror;

            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data Pembelian PO Sudah di" . (CheckEmpty(@$model['id_pembelian_po_master']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_penerimaan, $kodepenerimaan, 'penerimaan');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    
    function GetJumlahRetur($id,$jenis="pembelian") {
        $this->db->from("uap_retur_beli_detail");
        $this->db->join("uap_penerimaan_pembelian_detail", "uap_penerimaan_pembelian_detail.id_penerimaan_detail=uap_retur_beli_detail.id_penerimaan_detail", "left");
        $this->db->join("uap_retur_beli_master", "uap_retur_beli_master.id_retur_pembelian_master=uap_retur_beli_detail.id_retur_pembelian_master", "left");
        if($jenis=="pembelian")
            $this->db->where(array("uap_penerimaan_pembelian_detail.id_pembelian_detail" => $id, "uap_retur_beli_master.status !=" => 2));
        else 
            $this->db->where(array("uap_retur_beli_detail.id_penerimaan_detail" => $id, "uap_retur_beli_master.status !=" => 2));
        
        $this->db->select("sum(qty) as qtydiretur");
      
        $qtydiretur = $this->db->get()->row();
         return CheckEmpty(@$qtydiretur->qtydiretur) ? 0 : $qtydiretur->qtydiretur;
    }


    function GetDatapenerimaan($params) {

        $this->load->library('datatables');
        $this->datatables->select('uap_penerimaan_pembelian.tanggal_penerimaan,uap_penerimaan_pembelian.id_penerimaan,nama_gudang,nama_cabang,nama_supplier,uap_pembelian_master.tanggal,replace(kode_penerimaan,"/","-") as nomor_master_link,kode_penerimaan,nomor_master,uap_penerimaan_pembelian.status');
        $this->datatables->join("uap_supplier", "uap_supplier.id_supplier=uap_penerimaan_pembelian.id_supplier", "left");
        $this->datatables->join("uap_pembelian_master", "uap_pembelian_master.id_pembelian_master=uap_penerimaan_pembelian.id_pembelian_master", "left");
        $this->datatables->join("uap_cabang", "uap_cabang.id_cabang=uap_penerimaan_pembelian.id_cabang", "left");
        $this->datatables->join("uap_gudang", "uap_gudang.id_gudang=uap_penerimaan_pembelian.id_gudang", "left");

        $this->datatables->from('uap_penerimaan_pembelian');
        $straction = '';
        $extra = array();
        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['uap_penerimaan_pembelian.id_cabang'] = $params['id_cabang'];
        }
        if (isset($params['id_supplier']) && !CheckEmpty(@$params['id_supplier'])) {
            $params['uap_penerimaan_pembelian.id_supplier'] = $params['id_supplier'];
        }
        if (isset($params['id_gudang']) && !CheckEmpty(@$params['id_gudang'])) {
            $params['uap_penerimaan_pembelian.id_gudang'] = $params['id_gudang'];
        }
        $params['uap_penerimaan_pembelian.status !=']=2;
        unset($params['id_supplier']);
        unset($params['id_gudang']);
        unset($params['id_cabang']);
        unset($params['status']);

       
        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "uap_penerimaan_pembelian.tanggal_penerimaan BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        else if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tanggal_penerimaan'] = DefaultTanggalDatabase($params['start_date']);
             unset($params['start_date'], $params['end_date']);
        }
        else if (isset($params['end_date']) && empty($params['start_date']))
        {
            $params['tanggal_penerimaan'] = DefaultTanggalDatabase($params['end_date']);
             unset($params['start_date'], $params['end_date']);
        }
       

        if (count($params)) {
            $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        $stredit='';
        $strdelete='';
        $strview = anchor("", 'Select', array('class' => 'btn btn-default btn-xs', "onclick" => "select($1);return false;"));
        
        $this->datatables->add_column('action_view', $strview, 'id_penerimaan,nomor_master_link');
        return $this->datatables->generate();
    }


}
