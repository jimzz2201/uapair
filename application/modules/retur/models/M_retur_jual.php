<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_retur_jual extends CI_Model {

    public $table = 'uap_retur_jual_master';
    public $id = 'id_retur_penjualan_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function BatalRetur($id_retur_penjualan_master) {
        $returjual = $this->GetOneReturJual($id_retur_penjualan_master);
        $this->load->model("customer/m_customer");
        $message = "";
        if ($returjual != null) {
            if ($returjual->status == "2")
                $message .= "Retur Jual sudah dibatalkan sebelumnya<br/>";
        }
        else {
            $message .= "Retur Jual tidak ada di database<br/>";
        }
        if ($message == "") {
            $this->db->trans_rollback();
            $this->db->trans_begin();

            $this->db->from("uap_retur_jual_detail");
            $this->db->where(array("id_retur_penjualan_master" => $id_retur_penjualan_master));

            try {
                $listdetail = $this->db->get()->result();
                foreach ($listdetail as $detail) {
                    $this->db->from("uap_barang_history");
                    $this->db->where(array("id_barang_history" => $detail->id_barang_history));
                    $rowhistory = $this->db->get()->row();
                    if ($rowhistory == null) {
                        $message .= "History Untuk barang " . $detail->nama_barang . " tidak ada dalam database<br/>";
                        break;
                    } else if ($rowhistory != null && $rowhistory->stock_tersedia > $rowhistory->stock_sisa + $detail->qty_retur) {
                        $message .= "Quota untuk  " . $detail->nama_barang . " melebihi batas<br/>";
                        break;
                    }
                    $this->db->update("uap_barang_history", array("stock_sisa" => $rowhistory->stock_sisa - $detail->qty_retur), array("id_barang_history" => $rowhistory->id_barang_history));
                    $this->db->from("uap_barang_jurnal");
                    $this->db->where(array("dokumen" => $returjual->kode_retur_penjualan, "reference_id" => $detail->id_retur_jual_detail));
                    $rowjurnal = $this->db->get()->row();
                    if ($rowjurnal == null) {
                        $message .= "Jurnal Untuk barang " . $detail->nama_barang . " tidak ada dalam database<br/>";
                        break;
                    } 
                    $this->db->update("uap_barang_jurnal", array("debet" => 0, "kredit" => 0), array("id_jurnal_barang" => $rowjurnal->id_jurnal_barang));
                }
                if ($message == "") {
                    $this->db->update("uap_retur_jual_master", array("status" => 2, "updated_date" => GetDateNow(), "updated_by" => GetUserId()), array("id_retur_penjualan_master" => $id_retur_penjualan_master));
                }

                $this->db->from("uap_buku_besar");
                $this->db->where(array("dokumen" => $returjual->kode_retur_penjualan));
                $listbukubesar = $this->db->get()->result();

                foreach ($listbukubesar as $buku) {
                    $this->db->update("uap_buku_besar", array("updated_date" => GetDateNow(), "updated_by" => GetUserId(), "debet" => 0, "kredit" => 0, "keterangan" => "Batal oleh " . GetUsername() . " | " . $buku->keterangan), array("id_buku_besar" => $buku->id_buku_besar));
                }
                if ($returjual->jenis_potongan == "Potongan Piutang") {
                    $this->m_customer->KoreksiSaldoPiutang($returjual->id_customer);
                }
                $this->db->from("uap_barang_history");
                $list=$this->db->get()->result();
            
                if ($this->db->trans_status() === FALSE || $message != '') {
                    $this->db->trans_rollback();
                    $message = "Some Error Occured<br/>" . $message;
                    return array("st" => false, "msg" => $message);
                } else {
                    $this->db->trans_commit();
                    $arrayreturn["st"] = true;
                    $arrayreturn["msg"] = "Data Retur " . $returjual->kode_retur_penjualan . " sudah dibatalkan";
                    return $arrayreturn;
                }
            } catch (Exception $ex) {
                $this->db->trans_rollback();
                return array("st" => false, "msg" => $ex->getMessage());
            }
        } else {
            return array("st" => false, "msg" => $message);
        }
    }

    function GetDetailReturJual($id_surat_jalan,$id_retur=0) {
        $this->load->model("penjualan/m_penjualan");
        $this->db->from("uap_surat_jalan_detail");
        $this->db->join("uap_surat_jalan_master", "uap_surat_jalan_master.id_surat_jalan_master=uap_surat_jalan_detail.id_surat_jalan_master and uap_surat_jalan_master.status!=2");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_surat_jalan_detail.id_barang");
        $this->db->select("uap_surat_jalan_detail.*,sku_barang as sku,sum(ifnull(uap_surat_jalan_detail.qty,0)) as qty_pengiriman ");
        $this->db->where(array("uap_surat_jalan_detail.id_surat_jalan_master"=>$id_surat_jalan));
        $this->db->group_by("uap_surat_jalan_detail.id_sales_order_detail");
        $listdetail = $this->db->get()->result();
        foreach ($listdetail as $detail) {
            $rowretur=$this->GetJumlahRetur(@$detail->id_sales_order_detail,$detail->id_surat_jalan_master,$id_retur,true);
            $detail->jumlahdiretur=$this->GetJumlahRetur(@$detail->id_sales_order_detail,$detail->id_surat_jalan_master);
            if($rowretur)
            {
                $detail->qty_retur = $rowretur->qtydiretur;
                $detail->price_barang = $rowretur->price;

            }
            else
            {
                $detail->qty_retur = 0;
                $detail->price_barang = 0; 
            }
    
        }
        return $listdetail;
    }

    function GetListPenerimaan($id, $id_penjualan = 0, $fromretur = 0) {
        $this->db->from("uap_penjualan_detail");
        $this->db->join("uap_surat_jalan_master_detail", "uap_penjualan_detail.id_penjualan_detail=uap_surat_jalan_master_detail.id_penjualan_detail and uap_surat_jalan_master_detail.id_surat_jalan_master=" . $id, "left");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_penjualan_detail.id_barang");
        $this->db->select("uap_penjualan_detail.price_barang,uap_penjualan_detail.ppn_satuan,uap_surat_jalan_master_detail.id_surat_jalan_master_detail,uap_barang.sku_barang as sku,uap_penjualan_detail.id_penjualan_detail,uap_penjualan_detail.nama_barang,uap_penjualan_detail.qty,ifnull(uap_surat_jalan_master_detail.qty,0) as jumlahditerima");
        $this->db->where(array("uap_penjualan_detail.id_penjualan_master" => $id_penjualan));
        if ($fromretur) {
            $this->db->where(array("id_surat_jalan_master_detail !=" => $id));
        }
        $this->load->model("retur/m_retur_jual");
        $listdetail = $this->db->get()->result();
        foreach ($listdetail as $detail) {
            $detail->jumlahsurat_jalan_master = $this->GetJumlahPenerimaan(@$detail->id_surat_jalan_master_detail, "surat_jalan_master") - $detail->jumlahditerima;
            $detail->jumlahdiretur = $this->m_retur_jual->GetJumlahRetur(@$detail->id_surat_jalan_master_detail, "surat_jalan_master");
            
        }
        return $listdetail;
    }

    function GetOneReturJual($id_retur, $isfull = false) {
        $this->db->from("uap_retur_jual_master");
        $this->db->where(array("id_retur_penjualan_master" => $id_retur));
        $this->db->join("uap_sales_order_master", "uap_sales_order_master.id_sales_order_master=uap_retur_jual_master.id_sales_order_master", "left");
        $this->db->join("uap_penjualan_master", "uap_sales_order_master.id_sales_order_master=uap_penjualan_master.id_sales_order_master", "left");
        $this->db->join("uap_customer", "uap_customer.id_customer=uap_retur_jual_master.id_customer", "left");
        $this->db->join("uap_gudang", "uap_gudang.id_gudang=uap_retur_jual_master.id_gudang", "left");
        $this->db->join("uap_cabang", "uap_cabang.id_cabang=uap_retur_jual_master.id_cabang", "left");


        $this->db->select("uap_retur_jual_master.*,uap_penjualan_master.nomor_master as nomor_invoice,nama_cabang,nama_gudang,nama_customer,uap_penjualan_master.id_penjualan_master,uap_sales_order_master.id_sales_order_master,uap_sales_order_master.nomor_master as nomor_so,uap_sales_order_master.tanggal as tanggal_so");
        $row = $this->db->get()->row();

        if ($isfull) {
           $row->listdetail = $this->GetDetailReturJual($row->id_surat_jalan_master);
        }

        return $row;
    }

    function GetDataReturJual($params) {
        $isedit = CekModule("K105", false);
        $isdelete = CekModule("K106", false);
        $this->load->library('datatables');
        $this->datatables->select('uap_retur_jual_master.tanggal,kode_retur_penjualan,uap_retur_jual_master.id_retur_penjualan_master,nama_gudang,nama_cabang,nama_customer,replace(kode_retur_penjualan,"/","-") as nomor_master_link,uap_surat_jalan_master.nomor_master as nomor_surat_jalan,uap_penjualan_master.nomor_master as nomor_invoice,uap_retur_jual_master.status');
        $this->datatables->join("uap_customer", "uap_customer.id_customer=uap_retur_jual_master.id_customer", "left");
        $this->datatables->join("uap_penjualan_master", "uap_penjualan_master.id_sales_order_master=uap_retur_jual_master.id_sales_order_master", "left");
        $this->datatables->join("uap_surat_jalan_master", "uap_surat_jalan_master.id_surat_jalan_master=uap_retur_jual_master.id_surat_jalan_master", "left");

        $this->datatables->join("uap_cabang", "uap_cabang.id_cabang=uap_retur_jual_master.id_cabang", "left");
        $this->datatables->join("uap_gudang", "uap_gudang.id_gudang=uap_retur_jual_master.id_gudang", "left");

        $this->datatables->from('uap_retur_jual_master');
        $straction = '';
        $extra = array();

        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['uap_retur_jual_master.id_cabang'] = $params['id_cabang'];
        }
        if (isset($params['id_customer']) && !CheckEmpty(@$params['id_customer'])) {
            $params['uap_retur_jual_master.id_customer'] = $params['id_customer'];
        }
        if (isset($params['id_gudang']) && !CheckEmpty(@$params['id_gudang'])) {
            $params['uap_retur_jual_master.id_gudang'] = $params['id_gudang'];
        }
        if (isset($params['id_gudang']) && !CheckEmpty($params['status'])) {
            $params['uap_retur_jual_master.status'] = $params['status'];
        }

        unset($params['id_customer']);
        unset($params['id_gudang']);
        unset($params['id_cabang']);
        unset($params['status']);


        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "uap_retur_jual_master.tanggal BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        } else if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['uap_retur_jual_master.tanggal'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date'], $params['end_date']);
        } else if (isset($params['end_date']) && empty($params['start_date'])) {
            $params['uap_retur_jual_master.tanggal'] = DefaultTanggalDatabase($params['end_date']);
            unset($params['start_date'], $params['end_date']);
        }


        if (count($params)) {
            $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        $stredit = '';
        $strdelete = '';
        $strview = anchor("", 'Select', array('class' => 'btn btn-default btn-xs', "onclick" => "viewretur($1);return false;"));
        if ($isedit) {
            $stredit = anchor("retur/jual/editreturjual/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $strdelete .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batal($1);return false;"));
        }
        $this->datatables->add_column('action_view', $strview, 'id_retur_penjualan_master,nomor_master_link');
        $this->datatables->add_column('action_edit', $stredit, 'id_retur_penjualan_master,nomor_master_link');
        $this->datatables->add_column('action_delete', $strdelete, 'id_retur_penjualan_master,nomor_master_link');
        return $this->datatables->generate();
    }

    function ReturPenjualan($model) {
        try {
            $message = "";

            $this->db->from("uap_penjualan_master");
            $this->db->where(array("id_sales_order_master" => @$model['id_sales_order_master'], "status !=" => 2));

            $penjualan = $this->db->get()->row();
            if ($penjualan == null ) {
                return array("st" => false, "msg" => "Sales order belum memiliki faktur penjualan");
            }
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();

            $this->load->model("barang/m_barang");
            $this->load->model("cabang/m_cabang");
            $this->load->model("sales_order/m_sales_order");
            $this->load->model("customer/m_customer");
            $retur['tanggal'] = DefaultTanggalDatabase($model['tanggal']);
            $retur['id_customer'] = $model['id_customer'];
            $customer = $this->m_customer->GetOneCustomer($retur['id_customer']);
            $retur['id_cabang'] = $model['id_cabang'];
            $retur['jenis_potongan'] = $model['jenis_potongan'];
            $retur['type_ppn'] = $model['type_ppn'];
            $retur['type_pembulatan'] = $model['type_pembulatan'];
            $retur['id_surat_jalan_master'] = $model['id_surat_jalan_master'];
            $retur['total_potongan'] = DefaultCurrencyDatabase($model['total_potongan']);
            $retur['ppn'] = DefaultCurrencyDatabase($model['ppn']);
            $retur['ppn_nominal'] = DefaultCurrencyDatabase($model['ppn_nominal']);
            $retur['grandtotal'] = DefaultCurrencyDatabase($model['grandtotal']);
            $retur['id_gudang'] = $model['id_gudang'];
            $retur['id_sales_order_master'] = $model['id_sales_order_master'];
            $salesorder = $this->m_sales_order->GetOneSales_Order($model['id_sales_order_master']);
            $listid_sales_order_detail = [];

            $listpricebarang = [];
            $listketerangan = [];
            $listqty = [];
            $messageerror = "";
            if (CheckArray($model, 'id_sales_order_detail')) {
                $listid_sales_order_detail = $model['id_sales_order_detail'];
            }
            if (CheckArray($model, 'price_barang')) {
                $listpricebarang = $model['price_barang'];
            }
            if (CheckArray($model, 'keterangan_retur')) {
                $listketerangan = $model['keterangan_retur'];
            }
            if (CheckArray($model, 'qty_retur')) {
                $listqty = $model['qty_retur'];
            }



            $kodesurat_jalan_master = '';
            $id_surat_jalan_master = 0;
            if (!CheckEmpty($model['id_retur_penjualan_master'])) {
                $returfromdb = $this->GetOneReturJual($model['id_retur_penjualan_master']);
                if ($returfromdb != null) {
                    $koderetur = $returfromdb->kode_retur_penjualan;
                }
                $retur['updated_date'] = GetDateNow();
                $retur['updated_by'] = $userid;
                $this->db->update("uap_retur_jual_master", $retur, array("id_retur_penjualan_master" => $model['id_retur_penjualan_master']));
                $id_retur_penjualan_master = $model['id_retur_penjualan_master'];
            } else {
                $cabang = $this->m_cabang->GetOneCabang($model['id_cabang']);
                $koderetur = AutoIncrement('uap_retur_jual_master', $cabang->kode_cabang . '/RRJ/' . date("y") . '/', 'kode_retur_penjualan', 5);
                $retur['kode_retur_penjualan'] = $koderetur;
                $retur['created_date'] = GetDateNow();
                $retur['created_by'] = $userid;
                $retur['status'] = 1;
                $this->db->insert("uap_retur_jual_master", $retur);
                $id_retur_penjualan_master = $this->db->insert_id();
            }
            $totalreturwithoutppn = 0;
            $totalreturwithppn = 0;
            $barangsync = [];
            foreach ($listid_sales_order_detail as $key => $itemsales_order) {

                $this->db->from("uap_surat_jalan_detail");
                $this->db->where(array("id_sales_order_detail" => $itemsales_order, "id_surat_jalan_master" => $retur['id_surat_jalan_master']));
                $listsuratjalandetail = $this->db->get()->result();
                $namabarang="";
                $qtyretur = DefaultCurrencyDatabase($listqty[$key]);
                foreach ($listsuratjalandetail as $detailsur) {
                    if (!in_array($detailsur->id_barang, $barangsync)) {
                        array_push($barangsync, $detailsur->id_barang);
                    }
                    $this->db->from("uap_retur_jual_detail");
                    $namabarang=$detailsur->nama_barang;
                    $this->db->where(array("id_surat_jalan_detail" => $detailsur->id_surat_jalan_detail, "id_retur_penjualan_master" => $id_retur_penjualan_master));
                    $rowitem = $this->db->get()->row();
                    $returjualupdate = array();
                    $returjualupdate['id_retur_penjualan_master'] = $id_retur_penjualan_master;
                    $returjualupdate['id_surat_jalan_detail'] = $detailsur->id_surat_jalan_detail;
                    $returjualupdate['id_barang'] = $detailsur->id_barang;
                    $returjualupdate['nama_barang'] = $detailsur->nama_barang;
                    $returjualupdate['id_sales_order_detail'] = $detailsur->id_sales_order_detail;
                    $returjualupdate['id_barang_history'] = $detailsur->id_barang_history;
                    $returjualupdate['keterangan_retur'] = $listketerangan[$key];
                    $returjualupdate['price'] = DefaultCurrencyDatabase($listpricebarang[$key]);
                    if ($rowitem != null) {
                        if ($qtyretur >= $detailsur->qty) {
                            $qtyretur-=$detailsur->qty;
                            $returjualupdate['qty_retur'] = $detailsur->qty;
                        } else  {
                            $returjualupdate['qty_retur'] = $qtyretur;
                            $qtyretur = 0;
                        }
                

                        if ($retur['type_ppn'] == "1") {
                            $returjualupdate['ppn_satuan'] = ($returjualupdate['qty_retur'] * $returjualupdate['price']) * ((float) $retur['ppn'] / ((float) $retur['ppn'] + (float) 100));
                            $totalreturwithoutppn += ($returjualupdate['qty_retur'] * $returjualupdate['price']) - $returjualupdate['ppn_satuan'];
                        } else if ($retur['type_ppn'] == "2") {
                            $returjualupdate['ppn_satuan'] = ($returjualupdate['qty_retur'] * $returjualupdate['price']) * ((float) $retur['ppn'] / (float) 100);
                            $totalreturwithoutppn += ($returjualupdate['qty_retur'] * $returjualupdate['price']);
                        } else {
                            $returjualupdate['ppn_satuan'] = 0;
                            $totalreturwithoutppn += ($returjualupdate['qty_retur'] * $returjualupdate['price']);
                        }
                        $returjualupdate['subtotal'] = $returjualupdate['qty_retur'] * $returjualupdate['price'];
                        $totalreturwithppn += ($returjualupdate['qty_retur'] * $returjualupdate['price']);
                        if ($returjualupdate['ppn_satuan'] != $rowitem->ppn_satuan && $rowitem->subtotal != $returjualupdate['price'] * $returjualupdate['qty_retur']) {
                            $this->db->from("uap_barang_jurnal");
                            $this->db->where(array("reference_id" => $rowitem->id_retur_jual_detail));
                            $barangjurnal = $this->db->get()->row();

                            if ($barangjurnal != null) {
                                $id_barang_history_terakhir = $barangjurnal->id_barang_history;
                                $baranghistory = $this->m_barang->GetOneHistoryItem($id_barang_history_terakhir);
                                $baranghistory->stock_sisa +=   $returjualupdate['qty_retur']-$rowitem->qty_retur;
                                
                                $this->db->update("uap_barang_history", $baranghistory, array("id_barang_history" => $id_barang_history_terakhir));
                                $this->db->update("uap_barang_jurnal", array("debet" => $returjualupdate['qty_retur'], "kredit" => 0), array("id_jurnal_barang" => $barangjurnal->id_jurnal_barang));
                            } else {
                                $updatejurnal = [];
                                $updatejurnal['id_barang_history'] = $detailsur->id_barang_history;
                                $updatejurnal['id_barang'] = $detailsur->id_barang;
                                $updatejurnal['debet'] = $returjualupdate['qty_retur'];
                                $updatejurnal['kredit'] = 0;
                                $updatejurnal['tanggal_jurnal'] = $retur['tanggal'];
                                $updatejurnal['id_gudang'] = $model['id_gudang'];
                                $updatejurnal['reference_id'] = $rowitem->id_retur_jual_detail;
                                $updatejurnal['dokumen'] = $koderetur;
                                $updatejurnal['description'] = "Retur Penjualan " . $salesorder->nomor_master;
                                $updatejurnal['harga_beli_akhir'] = 0;
                                $updatejurnal['created_date'] = GetDateNow();
                                $updatejurnal['created_by'] = $userid;
                                $updatejurnal['harga'] = $returjualupdate['price'];
                                $this->db->insert("uap_barang_jurnal", $updatejurnal);
                            }
                            $this->db->update("uap_retur_jual_detail", $returjualupdate, array("id_retur_jual_detail" => $rowitem->id_retur_jual_detail));
                        }
                    } else {
                        if ($qtyretur != 0) {
                            if ($qtyretur >= $detailsur->qty) {
                                $returjualupdate['qty_retur'] = $detailsur->qty;
                                $qtyretur -= $detailsur->qty;
                            } else if ($qtyretur != 0) {
                                $returjualupdate['qty_retur'] = $qtyretur;
                                $qtyretur = 0;
                            }

                            if ($retur['type_ppn'] == "1") {
                                $returjualupdate['ppn_satuan'] = ($returjualupdate['qty_retur'] * $returjualupdate['price']) * ((float) $retur['ppn'] / ((float) $retur['ppn'] + (float) 100));
                                $totalreturwithoutppn += ($returjualupdate['qty_retur'] * $returjualupdate['price']) - $returjualupdate['ppn_satuan'];
                            } else if ($retur['type_ppn'] == "2") {
                                $returjualupdate['ppn_satuan'] = ($returjualupdate['qty_retur'] * $returjualupdate['price']) * ((float) $retur['ppn'] / (float) 100);
                                $totalreturwithoutppn += ($returjualupdate['qty_retur'] * $returjualupdate['price']);
                            } else {
                                $returjualupdate['ppn_satuan'] = 0;
                                $totalreturwithoutppn += ($returjualupdate['qty_retur'] * $returjualupdate['price']);
                            }
                            $returjualupdate['subtotal'] = $returjualupdate['qty_retur'] * $returjualupdate['price'];
                            $totalreturwithppn += ($returjualupdate['qty_retur'] * $returjualupdate['price']);
                            $this->db->insert("uap_retur_jual_detail", $returjualupdate);
                            $id_retur_jual_detail = $this->db->insert_id();
                            $updatejurnal = [];
                            $updatejurnal['id_barang_history'] = $detailsur->id_barang_history;
                            $updatejurnal['id_barang'] = $detailsur->id_barang;
                            $updatejurnal['debet'] = $returjualupdate['qty_retur'];
                            $updatejurnal['kredit'] = 0;
                            $updatejurnal['tanggal_jurnal'] = $retur['tanggal'];
                            $updatejurnal['id_gudang'] = $model['id_gudang'];
                            $updatejurnal['reference_id'] = $id_retur_jual_detail;
                            $updatejurnal['dokumen'] = $koderetur;
                            $updatejurnal['description'] = "Retur Penjualan " . $salesorder->nomor_master;
                            $updatejurnal['harga_beli_akhir'] = 0;
                            $updatejurnal['created_date'] = GetDateNow();
                            $updatejurnal['created_by'] = $userid;
                            $updatejurnal['harga'] = $returjualupdate['price'];
                            $this->db->insert("uap_barang_jurnal", $updatejurnal);
                            $id_barang_history_terakhir=$detailsur->id_barang_history;
                            $baranghistory = $this->m_barang->GetOneHistoryItem($id_barang_history_terakhir);
                            $baranghistory->stock_sisa += $returjualupdate['qty_retur'];
                            $this->db->update("uap_barang_history", $baranghistory, array("id_barang_history" => $id_barang_history_terakhir));
                        }
                    }
                }
                if ($qtyretur > 0) {
                    $messageerror .= "Qty Retur ".$namabarang." lebih besar daripada list di surat jalan<br/>";
                }
            }


            foreach ($barangsync as $bar) {
                $this->m_barang->SyncronizeHargaAverage($bar, $model['id_gudang'], $retur['tanggal']);
            }

            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "17", "dokumen" => $koderetur));
            $akun_retur = $this->db->get()->row();
            if (CheckEmpty($akun_retur)) {
                $akunreturinsert = array();
                $akunreturinsert['id_gl_account'] = "17";
                $akunreturinsert['tanggal_buku'] = $retur['tanggal'];
                $akunreturinsert['id_cabang'] = $retur['id_cabang'];
                $akunreturinsert['keterangan'] = "Retur jual dari invoice " . @$penjualan->nomor_master . ' dari ' . @$customer->nama_customer;
                $akunreturinsert['dokumen'] = $koderetur;
                $akunreturinsert['subject_name'] = @$customer->kode_customer;
                $akunreturinsert['id_subject'] = $retur['id_customer'];
                $akunreturinsert['debet'] = DefaultCurrencyDatabase($model['type_ppn'] == "1" ? $totalreturwithppn * 100 / (100 + $retur['ppn']) : $totalreturwithoutppn, 0);
                $akunreturinsert['kredit'] = 0;
                $akunreturinsert['created_date'] = GetDateNow();
                $akunreturinsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akunreturinsert);
            } else {
                $akunreturupdate['tanggal_buku'] = $retur['tanggal'];
                $akunreturupdate['id_cabang'] = $retur['id_cabang'];
                $akunreturupdate['keterangan'] = "Retur jual dari invoice " . @$penjualan->nomor_master . ' dari ' . @$customer->nama_customer;
                $akunreturupdate['dokumen'] = $koderetur;
                $akunreturupdate['subject_name'] = @$customer->kode_customer;
                $akunreturupdate['id_subject'] = $retur['id_customer'];
                $akunreturupdate['debet'] = DefaultCurrencyDatabase($model['type_ppn'] == "1" ? $totalreturwithppn * 100 / (100 + $retur['ppn']) : $totalreturwithoutppn, 0);
                $akunreturupdate['kredit'] = 0;
                $akunreturupdate['updated_date'] = GetDateNow();
                $akunreturupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akunreturupdate, array("id_buku_besar" => $akun_retur->id_buku_besar));
            }

            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "20", "dokumen" => $koderetur));
            $akun_ppn = $this->db->get()->row();

            if (CheckEmpty($akun_ppn) && $retur['ppn_nominal'] > 0) {
                $akunppninsert = array();
                $akunppninsert['id_gl_account'] = "20";
                $akunppninsert['tanggal_buku'] = $retur['tanggal'];
                $akunppninsert['id_cabang'] = $retur['id_cabang'];
                $akunppninsert['keterangan'] = "Potongan ppn untuk Retur jual dari invoice " . @$penjualan->nomor_master . ' dari ' . @$customer->nama_customer;
                $akunppninsert['dokumen'] = $koderetur;
                $akunppninsert['subject_name'] = @$customer->kode_customer;
                $akunppninsert['id_subject'] = $retur['id_customer'];
                $akunppninsert['debet'] = $retur['ppn_nominal'];
                $akunppninsert['kredit'] = 0;
                $akunppninsert['created_date'] = GetDateNow();
                $akunppninsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akunppninsert);
            } else if (!CheckEmpty($akun_ppn)) {
                $akunppnupdate['tanggal_buku'] = $retur['tanggal'];
                $akunppnupdate['id_cabang'] = $retur['id_cabang'];
                $akunppnupdate['keterangan'] = "Potongan ppn untuk Retur jual dari invoice " . @$penjualan->nomor_master . ' dari ' . @$customer->nama_customer;
                $akunppnupdate['dokumen'] = $koderetur;
                $akunppnupdate['subject_name'] = @$customer->kode_customer;
                $akunppnupdate['id_subject'] = $retur['id_customer'];
                $akunppnupdate['debet'] = $retur['ppn_nominal'];
                $akunppnupdate['kredit'] = 0;
                $akunppnupdate['updated_date'] = GetDateNow();
                $akunppnupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akunppnupdate, array("id_buku_besar" => $akun_ppn->id_buku_besar));
            }

            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "3", "dokumen" => $koderetur));
            $akun_kas = $this->db->get()->row();

            if (CheckEmpty($akun_kas) && $model['jenis_potongan'] == "Cash") {
                $akunkasinsert = array();
                $akunkasinsert['id_gl_account'] = "3";
                $akunkasinsert['tanggal_buku'] = $retur['tanggal'];
                $akunkasinsert['id_cabang'] = $retur['id_cabang'];
                $akunkasinsert['keterangan'] = "Pengembalian kas untuk retur penjualan dari " . @$customer->nama_customer . '  untuk invoice ' . $penjualan->nomor_master;
                $akunkasinsert['dokumen'] = $koderetur;
                $akunkasinsert['subject_name'] = @$customer->kode_customer;
                $akunkasinsert['id_subject'] = $retur['id_customer'];
                $akunkasinsert['debet'] = 0;
                $akunkasinsert['kredit'] = $retur['grandtotal'];
                $akunkasinsert['created_date'] = GetDateNow();
                $akunkasinsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akunkasinsert);
            } else if (!CheckEmpty($akun_kas)) {
                $akunkasupdate['tanggal_buku'] = $retur['tanggal'];
                $akunkasupdate['id_cabang'] = $retur['id_cabang'];
                $akunkasupdate['keterangan'] = "Pengembalian kas untuk retur penjualan dari " . @$customer->nama_customer . '  untuk invoice ' . $penjualan->nomor_master;
                $akunkasupdate['dokumen'] = $koderetur;
                $akunkasupdate['subject_name'] = @$customer->kode_customer;
                $akunkasupdate['id_subject'] = $retur['id_customer'];
                $akunkasupdate['debet'] = 0;
                $akunkasupdate['kredit'] = $retur['grandtotal'];
                $akunkasupdate['updated_date'] = GetDateNow();
                $akunkasupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akunkasupdate, array("id_buku_besar" => $akun_kas->id_buku_besar));
            }



            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "7", "dokumen" => $koderetur));
            $akun_piutang = $this->db->get()->row();

            if (CheckEmpty($akun_piutang) && $model['jenis_potongan'] == "Potongan Piutang") {
                $akunpiutanginsert = array();
                $akunpiutanginsert['id_gl_account'] = "7";
                $akunpiutanginsert['tanggal_buku'] = $retur['tanggal'];
                $akunpiutanginsert['id_cabang'] = $retur['id_cabang'];
                $akunpiutanginsert['keterangan'] = "Potongan Utang untuk retur penjualan dari " . @$customer->nama_customer . '  untuk invoice ' . $penjualan->nomor_master;
                $akunpiutanginsert['dokumen'] = $koderetur;
                $akunpiutanginsert['subject_name'] = @$customer->kode_customer;
                $akunpiutanginsert['id_subject'] = $retur['id_customer'];
                $akunpiutanginsert['debet'] = 0;
                $akunpiutanginsert['kredit'] = $retur['grandtotal'];
                $akunpiutanginsert['created_date'] = GetDateNow();
                $akunpiutanginsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akunpiutanginsert);
            } else if (!CheckEmpty($akun_piutang)) {
                $akunpiutangupdate['tanggal_buku'] = $retur['tanggal'];
                $akunpiutangupdate['id_cabang'] = $retur['id_cabang'];
                $akunpiutangupdate['keterangan'] = "Potongan Utang untuk retur penjualan dari " . @$customer->nama_customer . '  untuk invoice ' . $penjualan->nomor_master;
                $akunpiutangupdate['dokumen'] = $koderetur;
                $akunpiutangupdate['subject_name'] = @$customer->kode_customer;
                $akunpiutangupdate['id_subject'] = $retur['id_customer'];
                $akunpiutangupdate['debet'] = 0;
                $akunpiutangupdate['kredit'] = $retur['grandtotal'];
                $akunpiutangupdate['updated_date'] = GetDateNow();
                $akunpiutangupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akunpiutangupdate, array("id_buku_besar" => $akun_piutang->id_buku_besar));
            }
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "18", "dokumen" => $koderetur));
            $akun_potongan = $this->db->get()->row();

            if (CheckEmpty($akun_potongan) && $retur['total_potongan'] > 0) {
                $akunpotonganinsert = array();
                $akunpotonganinsert['id_gl_account'] = "18";
                $akunpotonganinsert['tanggal_buku'] = $retur['tanggal'];
                $akunpotonganinsert['id_cabang'] = $retur['id_cabang'];
                $akunpotonganinsert['keterangan'] = "Potongan  untuk retur penjualan dari " . @$customer->nama_customer . '  untuk invoice ' . $penjualan->nomor_master;
                $akunpotonganinsert['dokumen'] = $koderetur;
                $akunpotonganinsert['subject_name'] = @$customer->kode_customer;
                $akunpotonganinsert['id_subject'] = $retur['id_customer'];
                $akunpotonganinsert['debet'] = 0;
                $akunpotonganinsert['kredit'] = $retur['total_potongan'];
                $akunpotonganinsert['created_date'] = GetDateNow();
                $akunpotonganinsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akunpotonganinsert);
            } else if (!CheckEmpty($akun_potongan)) {
                $akunpotonganupdate['tanggal_buku'] = $retur['tanggal'];
                $akunpotonganupdate['id_cabang'] = $retur['id_cabang'];
                $akunpotonganupdate['keterangan'] = "Potongan  untuk retur penjualan dari " . @$customer->nama_customer . '  untuk invoice ' . $penjualan->nomor_master;
                $akunpotonganupdate['dokumen'] = $koderetur;
                $akunpotonganupdate['subject_name'] = @$customer->kode_customer;
                $akunpotonganupdate['id_subject'] = $retur['id_customer'];
                $akunpotonganupdate['debet'] = 0;
                $akunpotonganupdate['kredit'] = $retur['total_potongan'];
                $akunpotonganupdate['updated_date'] = GetDateNow();
                $akunpotonganupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akunpotonganupdate, array("id_buku_besar" => $akun_potongan->id_buku_besar));
            }



            $this->db->from("uap_buku_besar");
            $this->db->where(array("dokumen" => $koderetur));
            $this->db->select("sum(debet)-sum(kredit) as selisih");
            $row = $this->db->get()->row();
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "38", "dokumen" => $koderetur));
            $akun_penyeimbang = $this->db->get()->row();

            if (CheckEmpty($akun_penyeimbang) && $row && $row->selisih > 0) {
                $akun_penyeimbanginsert = array();
                $akun_penyeimbanginsert['id_gl_account'] = "38";
                $akun_penyeimbanginsert['tanggal_buku'] = $retur['tanggal'];
                $akun_penyeimbanginsert['id_cabang'] = $retur['id_cabang'];
                $akun_penyeimbanginsert['keterangan'] = "Penyeimbang untuk retur penjualan Ke " . @$customer->nama_customer . ' untuk dokumen ' . $penjualan->nomor_master;
                $akun_penyeimbanginsert['dokumen'] = $koderetur;
                $akun_penyeimbanginsert['subject_name'] = @$customer->kode_customer;
                $akun_penyeimbanginsert['id_subject'] = $retur['id_customer'];
                $akun_penyeimbanginsert['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['created_date'] = GetDateNow();
                $akun_penyeimbanginsert['created_by'] = $userid;
                $this->db->insert("uap_buku_besar", $akun_penyeimbanginsert);
            } else if (!CheckEmpty($akun_penyeimbang)) {
                $akun_penyeimbangupdate['tanggal_buku'] = $retur['tanggal'];
                $akun_penyeimbangupdate['id_cabang'] = $retur['id_cabang'];
                $akun_penyeimbangupdate['keterangan'] = "Penyeimbang untuk retur penjualan Ke " . @$customer->nama_customer . ' untuk dokumen ' . $penjualan->nomor_master;
                $akun_penyeimbangupdate['dokumen'] = $koderetur;
                $akun_penyeimbangupdate['subject_name'] = @$customer->kode_customer;
                $akun_penyeimbangupdate['id_subject'] = $retur['id_customer'];
                $akun_penyeimbangupdate['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['updated_date'] = GetDateNow();
                $akun_penyeimbangupdate['updated_by'] = $userid;
                $this->db->update("uap_buku_besar", $akun_penyeimbangupdate, array("id_buku_besar" => $akun_penyeimbang->id_buku_besar));
            }
            $this->db->from("uap_pembayaran_piutang_detail");
            $this->db->join("uap_pembayaran_piutang_master", "uap_pembayaran_piutang_master.pembayaran_piutang_id=uap_pembayaran_piutang_detail.pembayaran_piutang_id");
            $this->db->where(array("keterangan" => "Potongan Retur " . $koderetur, "id_penjualan_master" => $penjualan->id_penjualan_master));

            $pembayaran_piutang_master = $this->db->get()->row();
            if ($pembayaran_piutang_master != null && $retur['jenis_potongan'] == "Cash") {
                $this->db->delete("uap_pembayaran_piutang_detail", array("pembayaran_piutang_detail_id" => $pembayaran_piutang_master->pembayaran_piutang_detail_id));
                $this->db->delete("uap_pembayaran_piutang_master", array("pembayaran_piutang_id" => $pembayaran_piutang_master->pembayaran_piutang_id));
            } else if ($pembayaran_piutang_master != null && $pembayaran_piutang_master->jumlah_bayar != $retur['grandtotal']) {
                $updatedet = array();
                $updatedet['updated_date'] = GetDateNow();
                $updatedet['updated_by'] = $userid;
                $updatedet['jumlah_bayar'] = $retur['grandtotal'];
                $updatemas = array();
                $updatemas['updated_date'] = GetDateNow();
                $updatemas['updated_by'] = $userid;
                $updatemas['total_bayar'] = $retur['grandtotal'];
                $this->db->update("uap_pembayaran_piutang_detail", $updatedet, array("pembayaran_piutang_detail_id" => $pembayaran_piutang_master->pembayaran_piutang_detail_id));
                $this->db->update("uap_pembayaran_piutang_master", $updatemas, array("pembayaran_piutang_id" => $pembayaran_piutang_master->pembayaran_piutang_id));
            } else if ($pembayaran_piutang_master == null && $retur['jenis_potongan'] != "Cash") {
                $dokumenpembayaran = AutoIncrement('uap_pembayaran_piutang_master', $cabang->kode_cabang . '/BB/' . date("y") . '/', 'dokumen', 5);
                $pembayaranmaster = array();
                $pembayaranmaster['dokumen'] = $dokumenpembayaran;
                $pembayaranmaster['tanggal_transaksi'] = $retur['tanggal'];
                $pembayaranmaster['id_customer'] = $retur['id_customer'];
                $pembayaranmaster['jenis_pembayaran'] = "retur";
                $pembayaranmaster['total_bayar'] = $retur['grandtotal'];
                $pembayaranmaster['biaya'] = 0;
                $pembayaranmaster['potongan'] = 0;
                $pembayaranmaster['status'] = 1;
                $pembayaranmaster['keterangan'] = "Potongan Retur " . $koderetur;
                $pembayaranmaster['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
                $pembayaranmaster['created_date'] = GetDateNow();
                $pembayaranmaster['created_by'] = $userid;
                $this->db->insert("uap_pembayaran_piutang_master", $pembayaranmaster);
                $pembayaran_piutang_id = $this->db->insert_id();
                $pembayarandetail = array();
                $pembayarandetail['id_penjualan_master'] = $penjualan->id_penjualan_master;
                $pembayarandetail['pembayaran_piutang_id'] = $pembayaran_piutang_id;
                $pembayarandetail['jumlah_bayar'] = $retur['grandtotal'];
                $pembayarandetail['created_date'] = GetDateNow();
                $pembayarandetail['created_by'] = $userid;
                $this->db->insert("uap_pembayaran_piutang_detail", $pembayarandetail);
            }
            if ($retur['jenis_potongan'] == "Potongan Piutang") {
                $this->m_customer->KoreksiSaldoPiutang($retur['id_customer']);
            }


            $this->db->from("uap_barang_history");
            $this->db->where(array("id_barang"=>6));
            $list=$this->db->get()->result();
         
            $message .= $messageerror;
            
            
            $this->db->from("uap_barang_gudang");
            $this->db->where(array("id_barang"=>6));
            $list=$this->db->get()->result();
           
            $this->db->from("uap_barang_history");
            $this->db->where(array("id_barang"=>6));
            $list=$this->db->get()->result();
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data Retur Sudah di" . (CheckEmpty($id_retur_penjualan_master) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_retur_penjualan_master, $koderetur, 'retur_jual');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetJumlahRetur($id, $jenis = "penjualan",$id_surat_jalan_master="",$withprice=false) {
        $this->db->from("uap_retur_jual_detail");
        $this->db->join("uap_surat_jalan_detail", "uap_surat_jalan_detail.id_surat_jalan_detail=uap_retur_jual_detail.id_surat_jalan_detail", "left");
        $this->db->join("uap_retur_jual_master", "uap_retur_jual_master.id_retur_penjualan_master=uap_retur_jual_detail.id_retur_penjualan_master", "left");
        if(!CheckEmpty($id_surat_jalan_master))
        {
             $this->db->where(array("uap_surat_jalan_detail.id_sales_order_detail" => $id,"id_surat_jalan_master"=>$id_surat_jalan_master, "uap_retur_jual_master.status !=" => 2));
        }
        else if ($jenis == "penjualan")
            $this->db->where(array("uap_surat_jalan_detail.id_sales_order_detail" => $id, "uap_retur_jual_master.status !=" => 2));
        else
            $this->db->where(array("uap_retur_jual_detail.id_sales_order_detail" => $id, "uap_retur_jual_master.status !=" => 2));
        if($withprice)
        {
            $this->db->select("sum(qty_retur) as qtydiretur,price");
            $qtydiretur = $this->db->get()->row();
            return $qtydiretur;
        }
        else
        {
            $this->db->select("sum(qty_retur) as qtydiretur");
            $qtydiretur = $this->db->get()->row();
            return CheckEmpty(@$qtydiretur->qtydiretur) ? 0 : $qtydiretur->qtydiretur;
        }
    }

    function GetDatasurat_jalan($params) {

        $this->load->library('datatables');
        $this->datatables->select('uap_surat_jalan_master.tanggal,uap_surat_jalan_master.id_surat_jalan_master,nama_gudang,nama_cabang,nama_customer,uap_penjualan_master.tanggal as tanggal_invoice,replace(uap_surat_jalan_master.nomor_master,"/","-") as nomor_master_link,uap_surat_jalan_master.nomor_master,uap_penjualan_master.nomor_master as nomor_invoice,uap_surat_jalan_master.status');
        $this->datatables->join("uap_customer", "uap_customer.id_customer=uap_surat_jalan_master.id_customer", "left");
        $this->datatables->join("uap_sales_order_master", "uap_sales_order_master.id_sales_order_master=uap_surat_jalan_master.id_sales_order_master", "left");
        $this->datatables->join("uap_penjualan_master", "uap_penjualan_master.id_sales_order_master=uap_sales_order_master.id_sales_order_master", "left");
        $this->datatables->join("uap_cabang", "uap_cabang.id_cabang=uap_surat_jalan_master.id_cabang", "left");
        $this->datatables->join("uap_gudang", "uap_gudang.id_gudang=uap_surat_jalan_master.id_gudang", "left");

        $this->datatables->from('uap_surat_jalan_master');
        $straction = '';
        $extra = array();
        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['uap_surat_jalan_master.id_cabang'] = $params['id_cabang'];
        }
        if (isset($params['id_customer']) && !CheckEmpty(@$params['id_customer'])) {
            $params['uap_surat_jalan_master.id_customer'] = $params['id_customer'];
        }
        if (isset($params['id_gudang']) && !CheckEmpty(@$params['id_gudang'])) {
            $params['uap_surat_jalan_master.id_gudang'] = $params['id_gudang'];
        }
        $params['uap_surat_jalan_master.status !='] = 2;
        unset($params['id_customer']);
        unset($params['id_gudang']);
        unset($params['id_cabang']);
        unset($params['status']);


        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "uap_surat_jalan_master.tanggal BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        } else if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['uap_surat_jalan_master.tanggal'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date'], $params['end_date']);
        } else if (isset($params['end_date']) && empty($params['start_date'])) {
            $params['uap_surat_jalan_master.tanggal'] = DefaultTanggalDatabase($params['end_date']);
            unset($params['start_date'], $params['end_date']);
        }


        if (count($params)) {
            $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        $stredit = '';
        $strdelete = '';
        $strview = anchor("", 'Select', array('class' => 'btn btn-default btn-xs', "onclick" => "select($1);return false;"));

        $this->datatables->add_column('action_view', $strview, 'id_surat_jalan_master,nomor_master_link');
        return $this->datatables->generate();
    }

}
