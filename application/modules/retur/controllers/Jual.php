<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jual extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("retur/m_retur_jual");
       
    }
    function retur_jual_manipulate(){
        $model = $this->input->post();
       
        $this->form_validation->set_rules('id_surat_jalan_master', 'Penerimaan', 'trim|required');
        $this->form_validation->set_rules('id_sales_order_master', 'Sales Order', 'trim|required');
        $this->form_validation->set_rules('id_gudang', 'Gudang', 'trim|required');
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');
        $this->form_validation->set_rules('id_customer', 'Customer', 'trim|required');
        $this->form_validation->set_rules('jenis_potongan', 'Jenis Potongan', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        $listorderdetail=[];
        $listsurat_jalan=[];
        $message="";
        if(CheckArray($model, 'id_sales_order_detail'))
        {
            $listorderdetail=$model['id_sales_order_detail'];
        }
        else
        {
            $message.="Detail Retur Required<br/>";
        }
        if(CheckArray($model, 'qty_retur'))
        {
            $listretur=$model['qty_retur'];
        }
        else
        {
             $message.="Qty Retur Required<br/>";
        }
        
        if(count($listorderdetail)==0)
        {
             $message.="Detail Retur Required<br/>";
        }
        else if(count($listorderdetail)!=count($listretur))
        {
            $message.="Something error occured<br/>";
        }
        else{
            $issimpan=false;
            foreach($listretur as $satuanretur)
            {
                if(!CheckEmpty($satuanretur))
                {
                    $issimpan=true;
                }
            }
            if(!$issimpan)
            {
                $message.="Quantity retur tidak boleh kosong<br/>";
            }
        }
      
        
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data = $this->m_retur_jual->ReturPenjualan($model);
            echo json_encode($data);
        }
    }
    public function search_surat_jalan_master(){
      $model = $this->input->post();
      $model['tanggal']= GetDateNow();
      $model['tanggal_awal']=TambahTanggal($model['tanggal'],"-7 Days");
      $this->load->view("v_retur_jual_search_surat_jalan", $model);
    }
    
    public  function index(){
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K043";
        $header = "K021";
        $title = "Retur Jual";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $model['list_gudang']=$this->m_gudang->GetDropDownGudang();
        $model['list_status']=array("2"=>"Batal","1"=>"Non Batal");
        $this->load->model("customer/m_customer");
        $model['list_customer']=$this->m_customer->GetDropDownCustomer();
        LoadTemplate($model, "retur/v_retur_jual_index", $javascript, $css);
    }
    
    public function editreturjual($id, $nomorinvoice) {
        $row = (object) array();

        $module = "K112";
        $header = "K021";
        CekModule($module);
        $retur = $this->m_retur_jual->GetOneReturJual($id, true);
      
        if ($retur == null || $retur->kode_retur_penjualan != str_replace("-", "/", $nomorinvoice)) {
            SetMessageSession(2, "Anda tidak memiliki akses untuk menuju link tersebut");
            redirect(base_url() . 'index.php/main/message');
        } else {
            $row = $retur;
        }
       
        $this->load->model("penjualan/m_penjualan");
        $penjualan=$this->m_penjualan->GetOnePenjualan($retur->id_penjualan_master);
        $listheader=array(array("tanggal"=>$penjualan->tanggal,"nomor_master"=>$penjualan->nomor_master));
       
        $this->load->model("customer/m_customer");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = "K043";
        $row->button = 'Edit';
        $jenis_pembayaran = array();
        $jenis_pembayaran["Cash"] = "Cash";
        $jenis_pembayaran["Potongan Piutang"] = "Potongan Piutang";
        $row->list_type_pembayaran = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row->list_type_ppn = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row->list_type_pembulatan = $list_type_pembulatan;
        $row->listheader = $listheader;
        $row->list_customer = $this->m_customer->GetDropDownCustomer();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $row->title = 'Update Retur Jual';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';

        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'retur/v_retur_jual_manipulate', $javascript, $css);
    }
    public function viewreturjual()
    {
        $id=$this->input->post("id_retur_penjualan_master");
        $retur = $this->m_retur_jual->GetOneReturJual($id, true);
        
        $this->load->view("retur/v_retur_jual_view",array("row"=>$retur));
    }
    
    public function batal()
    {
        $message = '';
        $this->form_validation->set_rules('id_retur_penjualan_master', 'Doc Retur', 'required');
        
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_retur_jual->BatalRetur($model['id_retur_penjualan_master']);
        }

        echo json_encode($result);
    }
    
    public function getdataretur() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_retur_jual->GetDataReturJual($params);
    }
    public function getdatasurat_jalan()
    {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        $this->load->model("retur/m_retur_jual");
        echo $this->m_retur_jual->GetDatasurat_jalan($params);
    }
    
    public function create() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K043";
        $header = "K021";
        CekModule($module);
        $this->load->model("customer/m_customer");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = $module;
        $jenis_pembayaran = array();
        $jenis_pembayaran["Cash"] = "Cash";
        $jenis_pembayaran["Potongan Piutang"] = "Potongan Piutang";
        $row->list_type_pembayaran = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row->list_type_ppn = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row->list_type_pembulatan = $list_type_pembulatan;
        $row->listdetail = [];
        $row->listheader = [];
        $row->list_customer = $this->m_customer->GetDropDownCustomer();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $row->tanggal = GetDateNow();
        $row->title = 'Create Retur Jual';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';

        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'retur/v_retur_jual_manipulate', $javascript, $css);
    }
    
     
}

/* End of file Barang.php */