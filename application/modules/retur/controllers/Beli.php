<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Beli extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("retur/m_retur_beli");
       
    }
    function retur_beli_manipulate(){
        $model = $this->input->post();
        $this->form_validation->set_rules('id_penerimaan', 'Penerimaan', 'trim|required');
        $this->form_validation->set_rules('id_pembelian_master', 'Pembelian', 'trim|required');
        $this->form_validation->set_rules('id_gudang', 'Gudang', 'trim|required');
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');
        $this->form_validation->set_rules('id_supplier', 'Supplier', 'trim|required');
        $this->form_validation->set_rules('jenis_potongan', 'Jenis Potongan', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        $listidpembelian=[];
        $listpenerimaan=[];
        $message="";
        if(CheckArray($model, 'id_pembelian_detail'))
        {
            $listidpembelian=$model['id_pembelian_detail'];
        }
        else
        {
            $message.="Detail Pembelian Required<br/>";
        }
        if(CheckArray($model, 'qty_retur'))
        {
            $listretur=$model['qty_retur'];
        }
        else
        {
             $message.="Qty Retur Required<br/>";
        }
        
        if(count($listidpembelian)==0)
        {
             $message.="Detail Retur Required<br/>";
        }
        else if(count($listidpembelian)!=count($listretur))
        {
            $message.="Something error occured<br/>";
        }
        else{
            $issimpan=false;
            foreach($listretur as $satuanretur)
            {
                if(!CheckEmpty($satuanretur))
                {
                    $issimpan=true;
                }
            }
            if(!$issimpan)
            {
                $message.="Quantity retur tidak boleh kosong<br/>";
            }
        }
      
        
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {

            $data = $this->m_retur_beli->ReturPembelian($model);
            echo json_encode($data);
        }
    }
    public function search_penerimaan(){
      $model = $this->input->post();
      $model['tanggal']= GetDateNow();
      $model['tanggal_awal']=TambahTanggal($model['tanggal'],"-7 Days");
      $this->load->view("v_retur_beli_search_penerimaan", $model);
    }
    
    public  function index(){
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K042";
        $header = "K016";
        $title = "Retur Beli";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $model['list_gudang']=$this->m_gudang->GetDropDownGudang();
        $model['list_status']=array("2"=>"Batal","1"=>"Non Batal");
        $this->load->model("supplier/m_supplier");
        $model['list_supplier']=$this->m_supplier->GetDropDownSupplier();
        LoadTemplate($model, "retur/v_retur_beli_index", $javascript, $css);
    }
    
    public function editreturbeli($id, $nomorinvoice) {
        $row = (object) array();

        $module = "K105";
        $header = "K016";
        CekModule($module);
        $retur = $this->m_retur_beli->GetOneReturBeli($id, true);
        if ($retur == null || $retur->kode_retur_pembelian != str_replace("-", "/", $nomorinvoice)) {
            SetMessageSession(2, "Anda tidak memiliki akses untuk menuju link tersebut");
            redirect(base_url() . 'index.php/main/message');
        } else {
            $row = $retur;
        }
        $pembelian=$this->m_pembelian->GetOnePembelian($retur->id_pembelian_master);
        $listheader=array(array("tanggal"=>$pembelian->tanggal,"nomor_master"=>$pembelian->nomor_master));
       
        $this->load->model("supplier/m_supplier");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = "K042";
        $row->button = 'Edit';
        $jenis_pembayaran = array();
        $jenis_pembayaran["Cash"] = "Cash";
        $jenis_pembayaran["Potongan Utang"] = "Potongan Utang";
        $row->list_type_pembayaran = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row->list_type_ppn = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row->list_type_pembulatan = $list_type_pembulatan;
        $row->listheader = $listheader;
        $row->list_supplier = $this->m_supplier->GetDropDownSupplier();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $row->title = 'Update Retur Beli';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';

        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'retur/v_retur_beli_manipulate', $javascript, $css);
    }
    public function viewreturbeli()
    {
        $id=$this->input->post("id_retur_pembelian_master");
        $retur = $this->m_retur_beli->GetOneReturBeli($id, true);
        
        $this->load->view("retur/v_retur_beli_view",array("row"=>$retur));
    }
    
    public function batal()
    {
        $message = '';
        $this->form_validation->set_rules('id_retur_pembelian_master', 'Doc Retur', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_retur_beli->BatalRetur($model['id_retur_pembelian_master']);
        }

        echo json_encode($result);
    }
    
    public function getdataretur() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_retur_beli->GetDataReturBeli($params);
    }
    public function getdatapenerimaan()
    {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        $this->load->model("retur/m_retur_beli");
        echo $this->m_retur_beli->GetDatapenerimaan($params);
    }
    
    public function create() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K042";
        $header = "K016";
        CekModule($module);
        $this->load->model("supplier/m_supplier");
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $this->load->model("gudang/m_gudang");
        $row->form = $header;
        $row->formsubmenu = $module;
        $jenis_pembayaran = array();
        $jenis_pembayaran["Cash"] = "Cash";
        $jenis_pembayaran["Potongan Utang"] = "Potongan Utang";
        $row->list_type_pembayaran = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row->list_type_ppn = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row->list_type_pembulatan = $list_type_pembulatan;
        $row->listdetail = [];
        $row->listheader = [];
        $row->list_supplier = $this->m_supplier->GetDropDownSupplier();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->list_gudang = $this->m_gudang->GetDropDownGudang();
        $row->list_barang = $this->m_barang->GetDropDownBarang();
        $row->tanggal = GetDateNow();
        $row->title = 'Create Retur Beli';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = 'assets/js/icheck/icheck.min.js';

        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        LoadTemplate($row, 'retur/v_retur_beli_manipulate', $javascript, $css);
    }
    
     
}

/* End of file Barang.php */