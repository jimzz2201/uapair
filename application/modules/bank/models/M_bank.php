<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_bank extends CI_Model {

    public $table = 'uap_bank';
    public $id = 'id_bank';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDatabank() {
        $this->load->library('datatables');
        $this->datatables->select('id_bank,kode_bank,nama_bank,account_name,account_no,lokasi,status');
        $this->datatables->from('uap_bank');
        //add this line for join
        //$this->datatables->join('table2', 'uap_bank.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editbank($1);return false;"));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletebank($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_bank');
        return $this->datatables->generate();
    }

    // get all
    function GetOneBank($keyword, $type = 'id_bank') {
        $this->db->where($type, $keyword);
        $bank = $this->db->get($this->table)->row();
        return $bank;
    }

    function BankManipulate($model) {
        try {
            $model['status'] = DefaultCurrencyDatabase($model['status']);

            if (CheckEmpty($model['id_bank'])) {
                $model['kode_bank'] = AutoIncrement('uap_bank', 'K', 'kode_bank',3);
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                return array("st" => true, "msg" => "Bank successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_bank" => $model['id_bank']));
                return array("st" => true, "msg" => "Bank has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
     function GetDropDownBank() {
        $where = [];
        $where['status'] = 1;
        $listreturn = GetTableData("uap_bank", 'id_bank', array('nama_bank','account_no'), $where, 'obj');
        return $listreturn;
    }
    function BankDelete($id_bank) {
        try {
            $this->db->delete($this->table, array('id_bank' => $id_bank));
        } catch (Exception $ex) {
            $model['updated_date'] = GetDateNow();
            $model['status'] = 2;
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array('id_bank' => $id_bank));
        }
        return array("st" => true, "msg" => "Bank has been deleted from database");
    }

}
