<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bank extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_bank');
    }

    public function index()
    {
        CekModule('K037');
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        LoadTemplate(array("form"=>"K036","formsubmenu"=>"K037"), "bank/v_bank_index", $javascript);
        
    } 
    
    public function getdatabank() {
        header('Content-Type: application/json');
        echo $this->m_bank->GetDatabank();
    }
    
    public function create_bank() 
    {
        $row=(object)array();
        $row->button='Add';
        $row=  json_decode(json_encode($row),true);
        $javascript = array();
        $this->load->view('bank/v_bank_manipulate', $row);       
    }
    
    public function bank_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('nama_bank', 'nama bank', 'trim|required');
	$this->form_validation->set_rules('account_name', 'account name', 'trim|required');
	$this->form_validation->set_rules('account_no', 'account no', 'trim|required');
	$this->form_validation->set_rules('lokasi', 'lokasi', 'trim|required');
	$model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_bank->BankManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_bank($id=0) 
    {
        $row = $this->m_bank->GetOneBank($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();        $this->load->view('bank/v_bank_manipulate', $row);
        } else {
            SetMessageSession(0, "Bank cannot be found in database");
            redirect(site_url('bank'));
        }
    }
    
    public function bank_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_bank', 'Bank', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_bank->BankDelete($model['id_bank']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Bank.php */