<div class="modal-header">
        Bank <?php echo @$button ?>
    </div>
<div class="modal-body">
<form id="frm_bank" class="form-horizontal form-groups-bordered validate" method="post">
	<input type="hidden" name="id_bank" value="<?php echo @$id_bank; ?>" /> 
	<div class="form-group">
		<?= form_label('Kode Bank', "txt_kode_bank", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text','disabled'=>'disabled', 'name' => 'kode_bank', 'value' => @$kode_bank, 'class' => 'form-control', 'id' => 'txt_kode_bank', 'placeholder' => 'Kode Bank')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Nama Bank', "txt_nama_bank", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'nama_bank', 'value' => @$nama_bank, 'class' => 'form-control', 'id' => 'txt_nama_bank', 'placeholder' => 'Nama Bank')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Account Name', "txt_account_name", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'account_name', 'value' => @$account_name, 'class' => 'form-control', 'id' => 'txt_account_name', 'placeholder' => 'Account Name')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Account No', "txt_account_no", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'account_no', 'value' => @$account_no, 'class' => 'form-control', 'id' => 'txt_account_no', 'placeholder' => 'Account No')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Lokasi', "txt_lokasi", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'lokasi', 'value' => @$lokasi, 'class' => 'form-control', 'id' => 'txt_lokasi', 'placeholder' => 'Lokasi')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
		</div>
	</div>
	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
    </div>

</form>
</div>
<script>
$("#frm_bank").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/bank/bank_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>