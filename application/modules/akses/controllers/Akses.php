<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Akses extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_akses');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $row = array();
        $row['title'] = "Hak Akses";
        $row['formsubmenu'] = "K087";
        $row['form'] = "K086";
        CekModule($row['formsubmenu']);
        $row['openmenu'] = "menusetting";
        LoadTemplate($row, "akses/v_akses_index", $javascript);
    }

    public function getdataakses() {
        header('Content-Type: application/json');
        echo $this->m_akses->GetDataakses();
    }

    public function create_akses() {
        $id = 0;
        $row = (object) array();
        $row->button = 'Add';
        $row = json_decode(json_encode($row), true);
        $row['menuakses'] = $this->m_akses->GetAllAkses();
        $menu = [];
        $parent = $this->m_akses->GetParentMenuAkses($id);
        foreach ($parent as $parentItem) {
            $current = [];
            $current['id_form'] = $parentItem->id_form;
            $current['nama_form'] = $parentItem->form_name;
            $current['kode_form'] = $parentItem->kode_form;
            $current['status'] = $parentItem->status;
            $current['icon'] = $parentItem->form_icon;
            $current['class'] = 'parent-menu';
            $current['id_parent'] = null;
            $current['child'] = [];

            $child = $this->m_akses->GetChildMenuAkses($id, $parentItem->id_form);
            if ($child) {
                foreach ($child as $item) {
                    $currentChild = [];
                    $currentChild['id_form'] = $item->id_form;
                    $currentChild['nama_form'] = $item->form_name;
                    $currentChild['kode_form'] = $item->kode_form;
                    $currentChild['status'] = $item->status;
                    $currentChild['class'] = 'child-menu';
                    $currentChild['id_parent'] = $parentItem->id_form;
//                        array_push($menu, $current);
                    $current['child'][] = $currentChild;
                }
            }
            array_push($menu, $current);
        }

//            dumperror($row['menuakses']); die();
//            dumperror($menu); die();
        $row['treemenu'] = $menu;
        $row['openmenu'] = "menusetting";
        $row['title'] = "Create Hak Akses";
        $row['formsubmenu'] = "K087";
        $row['form'] = "K086";
        $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
        $css[] = 'assets/plugins/iCheck/all.css';
        LoadTemplate($row, 'akses/v_akses_manipulate', $javascript, $css);
    }

    public function akses_manipulate() {
        $message = '';

        $this->form_validation->set_rules('nama_group', 'nama group', 'trim|required');
        $model = $this->input->post();
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_akses->AksesManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_akses($id = 0) {
        $row = $this->m_akses->GetOneAkses($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $row['menuakses'] = $this->m_akses->GetAllMenuAkses($id);

            $menu = [];
            $parent = $this->m_akses->GetParentMenuAkses($id);
            foreach ($parent as $parentItem) {
                $current = [];
                $current['id_form'] = $parentItem->id_form;
                $current['nama_form'] = $parentItem->form_name;
                $current['kode_form'] = $parentItem->kode_form;
                $current['status'] = $parentItem->status;
                $current['icon'] = $parentItem->form_icon;
                $current['class'] = 'parent-menu';
                $current['id_parent'] = null;
                $current['child'] = [];

                $child = $this->m_akses->GetChildMenuAkses($id, $parentItem->id_form);
                if ($child) {
                    foreach ($child as $item) {
                        $currentChild = [];
                        $currentChild['id_form'] = $item->id_form;
                        $currentChild['nama_form'] = $item->form_name;
                        $currentChild['kode_form'] = $item->kode_form;
                        $currentChild['status'] = $item->status;
                        $currentChild['class'] = 'child-menu';
                        $currentChild['id_parent'] = $parentItem->id_form;
//                        array_push($menu, $current);
                        $current['child'][] = $currentChild;
                    }
                }
                array_push($menu, $current);
            }

//            dumperror($row['menuakses']); die();
//            dumperror($menu); die();
            $row['treemenu'] = $menu;
            $row['openmenu'] = "menusetting";
            $row['treemenu'] = $menu;
            $row['openmenu'] = "menusetting";
            $row['title'] = "Create Hak Akses";
            $row['formsubmenu'] = "K087";
            $row['form'] = "K086";
            $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
            $css[] = 'assets/plugins/iCheck/all.css';
            LoadTemplate($row, 'akses/v_akses_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Akses cannot be found in database");
            redirect(site_url('akses'));
        }
    }

    public function akses_delete() {
        $message = '';
        $this->form_validation->set_rules('id_group', 'Akses', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_akses->AksesDelete($model['id_group']);
        }

        echo json_encode($result);
    }

}

/* End of file Akses.php */