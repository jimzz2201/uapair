<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_akses extends CI_Model {

    public $table = 'uap_group';
    public $id = 'id_group';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDataakses() {
        $this->load->library('datatables');
        $this->datatables->select('id_group,kode_group,nama_group,status');
        $this->datatables->from('uap_group');
        //add this line for join
        //$this->datatables->join('table2', 'uap_group.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor(site_url('akses/edit_akses/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deleteakses($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_group');
        return $this->datatables->generate();
    }

    function GetAllAkses($id_akses = 0) {
        $this->db->from("uap_form");
        $this->db->where(array("istampilinaccess" => 1));
        $this->db->order_by("urut");
        $limenu = $this->db->get()->result();
        return $limenu;
    }

    function GetAllMenuAkses($id = 0) {
        $this->db->from("uap_form");
        $this->db->select("uap_form.*, status");
        $this->db->join("uap_akses_group", "uap_akses_group.id_akses=uap_form.id_form  and  id_group=" . $id, "left");
        $this->db->join("uap_group", "uap_group.id_group=uap_akses_group.id_group", "left");
        $this->db->where(array("istampilinaccess" => 1));
        $this->db->order_by('urut');
        $listmenuakses = $this->db->get()->result();
        return $listmenuakses;
    }

    function GetParentMenuAkses($id = 0) {
        $this->db->from("uap_form");
        $this->db->select("uap_form.*, status");
        $this->db->join("uap_akses_group", "uap_akses_group.id_akses=uap_form.id_form and id_group=" . $id, "left");
        $this->db->join("uap_group", "uap_group.id_group=uap_akses_group.id_group", "left");
        $this->db->where(array("istampilinaccess" => 1, "kode_parent" => null));
        $this->db->order_by('urut');
        $listmenuakses = $this->db->get()->result();
        return $listmenuakses;
    }

    function GetChildMenuAkses($id = 0, $parent) {
        $this->db->from("uap_form");
        $this->db->select("uap_form.*, status");
        $this->db->join("uap_akses_group", "uap_akses_group.id_akses=uap_form.id_form and id_group=" . $id, "left");
        $this->db->join("uap_group", "uap_group.id_group=uap_akses_group.id_group", "left");
        $this->db->where(array("istampilinaccess" => 1, "kode_parent" => $parent));
        $this->db->order_by('urut');
        $listmenuakses = $this->db->get()->result();
        return $listmenuakses;
    }

    // get all
    function GetOneAkses($keyword, $type = 'id_group') {
        $this->db->where($type, $keyword);
        $akses = $this->db->get($this->table)->row();
        return $akses;
    }

    function AksesManipulate($model) {

        if (!array_key_exists('aksesakses', $model)) {
            $model['aksesakses'] = array();
        }
        $strsuccessfull = "";
        try {
            $this->db->trans_begin();
            if (CheckEmpty($model['id_group'])) {
                $query = "select ifnull(max(right(kode_group,3)),0) as 'sum' from uap_group where kode_group like '%G%'";
                $lastnumber = intval($this->db->query($query)->row()->sum);
                $aksescode = 'G' . substr('0000000' . ($lastnumber + 1), -3);
                $akses = array();
                $akses['kode_group'] = $aksescode;
                $akses['nama_group'] = $model['nama_group'];
                $akses['status'] = $model['status'];
                $akses['created_date'] = GetDateNow();
                $akses['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $akses);
                $id_group = $this->db->insert_id();
                foreach ($model['aksesakses'] as $akses) {
                    $aksessat = array();
                    $aksessat['id_group'] = $id_group;
                    $aksessat['id_akses'] = $akses;
                    $this->db->insert("uap_akses_group", $aksessat);
                }
                $strsuccessfull = 'Akses has been added into database';
            } else {

                $akses['nama_group'] = $model['nama_group'];
                $akses['status'] = $model['status'];
                $akses['updated_date'] = GetDateNow();
                $akses['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $akses, array("id_group" => $model['id_group']));
                $this->db->delete("uap_akses_group", array("id_group" => $model['id_group']));
                foreach ($model['aksesakses'] as $akses) {
                    $aksessat = array();
                    $aksessat['id_group'] = $model['id_group'];
                    $aksessat['id_akses'] = $akses;
                    $this->db->insert("uap_akses_group", $aksessat);
                }
                $strsuccessfull = 'Akses successfull updated';
            }
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                SetMessageSession(0, 'Some Error Occured');
                return array("st" => false, "Some Error Occured");
            } else {
                $this->db->trans_commit();
                SetMessageSession(1, $strsuccessfull);
                return array("st" => true);
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $e->getMessage());
        }
    }

    function CekAkses($id_user, $kode_menu) {
        $this->db->from("uap_akses_group");
        $this->db->join("uap_form", "uap_akses_group.id_akses=uap_form.id_form", "left");
        $this->db->join("uap_user", "uap_akses_group.id_group=uap_user.id_group");
        $this->db->where(array("kode_form" => $kode_menu, "id_user" => $id_user));
        return $this->db->get()->row() != null;
    }

    function AksesDelete($id_group) {
        try {
            $this->db->delete($this->table, array('id_group' => $id_group));
        } catch (Exception $ex) {
            $model['updated_date'] = GetDateNow();
            $model['status'] = 2;
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array('id_group' => $id_group));
        }
        return array("st" => true, "msg" => "Akses has been deleted from database");
    }

}
