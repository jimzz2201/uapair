<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Akses</h2>
                                <p>Akses | Setting</p>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-3'>
                        <div class='breadcomb-report'>
                            <?php echo anchor(site_url('/akses'), '<i class="fa fa-arrow-left"></i> Cancel', 'class="btn btn-warning"'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
           
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_akses" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="id_group" value="<?php echo @$id_group; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Kode Group', "txt_kode_group", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'kode_group', 'value' => @$kode_group, 'class' => 'form-control', 'id' => 'txt_kode_group', 'placeholder' => 'Kode Group', "disabled" => "disabled")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Nama Group', "txt_nama_group", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'nama_group', 'value' => @$nama_group, 'class' => 'form-control', 'id' => 'txt_nama_group', 'placeholder' => 'Nama Group')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                    </div>
                                </div>
                                <div class="form-group">


                                    <div class="col-sm-6">
                                        <?php
                                        $limit = 7;
                                        ?>
                                        <?php foreach (@$treemenu as $key => $menu) { ?>
                                            <?php if ($key < $limit): ?>
                                                <div class="col-sm-12">
                                                    <label class="classcursor <?= $menu['class'] ?>" >
                                                        <input class="minimal-red" style="margin:10px;" <?php echo!CheckEmpty(@$menu['status']) ? 'checked' : '' ?> type="checkbox" name="aksesakses[]" value="<?php echo $menu['id_form']; ?>" id="menu-<?= $menu['id_form'] ?>" class="parentish" /><span class="spanlabel"><?php echo $menu['nama_form']; ?></span>
                                                    </label>
                                                </div>
                                                <?php foreach ($menu['child'] as $childmenu) { ?>
                                                    <div class="col-sm-12">
                                                        <label class="classcursor child-menu-<?php echo $menu['id_form']; ?> <?= $childmenu['class'] ?>" >
                                                            <input class="minimal-red" style="margin:10px;" <?php echo!CheckEmpty(@$childmenu['status']) ? 'checked' : '' ?> type="checkbox" name="aksesakses[]" value="<?php echo $childmenu['id_form']; ?>" class="childish child-menu-<?= $menu['id_form'] ?>" data-parent="menu-<?= $menu['id_form'] ?>" /><span class="spanlabel"><?php echo $childmenu['nama_form']; ?></span>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            <?php endif; ?>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php foreach (@$treemenu as $key => $menu) { ?>
                                            <?php if ($key >= $limit): ?>
                                                <div class="col-sm-12">
                                                    <label class="classcursor <?= $menu['class'] ?>" >
                                                        <input class="minimal-red" style="margin:10px;" <?php echo!CheckEmpty(@$menu['status']) ? 'checked' : '' ?> type="checkbox" name="aksesakses[]" value="<?php echo $menu['id_form']; ?>" id="menu-<?= $menu['id_form'] ?>" class="parentish" /><span class="spanlabel"><?php echo $menu['nama_form']; ?></span>
                                                    </label>
                                                </div>
                                                <?php foreach ($menu['child'] as $childmenu) { ?>
                                                    <div class="col-sm-12">
                                                        <label class="classcursor child-menu-<?php echo $menu['id_form']; ?>  <?= $childmenu['class'] ?>" >
                                                            <input class="minimal-red" style="margin:10px;" <?php echo!CheckEmpty(@$childmenu['status']) ? 'checked' : '' ?> type="checkbox" name="aksesakses[]" value="<?php echo $childmenu['id_form']; ?>" class="childish child-menu-<?= $menu['id_form'] ?>" data-parent="menu-<?= $menu['id_form'] ?>" /><span class="spanlabel"><?php echo $childmenu['nama_form']; ?></span>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            <?php endif; ?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group buttonarea">
                                    <a href="<?php echo base_url() . 'index.php/akses' ?>" class="btn btn-default"  >Cancel</a>
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .child-menu{
        padding-left:30px;
    }
</style>
<script>
    $(document).ready(function () {
        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });
        $('.parent-menu input').on('ifChanged', function (event) {
            var parentId = $(this).attr('id');
            var check = event.target.checked;

            $(".child-" + parentId + " input").prop('checked', check);
            $(".child-" + parentId + " input").iCheck('update');



        });
    })


    $("#frm_akses").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/akses/akses_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st) {
                    window.location.href = baseurl + 'index.php/akses';
                } else {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;
    });

    $(".childish").click(function () {
        var check = false;
        var parent = $(this).data('parent');
        $(".child-" + parent).each(function () {
            check = check || $(this).is(":checked")
        });

        if (check) {
            $("#" + parent).prop('checked', true);
        } else {
            $("#" + parent).prop('checked', false);
        }
    });


</script>