<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo!CheckEmpty(@$title) ? @$title : "UAP AIR System" ?></title>
        <meta name="description" content="">
        <script>
            var baseurl = "<?php echo base_url() ?>";
        </script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon
                    ============================================ -->
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <!-- Google Fonts
                    ============================================ -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
        <!-- Bootstrap CSS
                    ============================================ -->
        <?php
        foreach (@$css as $item) {
            ?> 
            <link rel="stylesheet" href="<?php echo base_url() . $item ?>">
        <?php }
        ?>
        <!-- modernizr JS
                    ============================================ -->
        <script src="<?php echo base_url() ?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/helper.js"></script>
    </head>


    <body>
        <!--[if lt IE 8]>
                <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
            <![endif]-->
        <!-- Start Header Top Area -->
        <div class="header-top-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="logo-area">
                            <!--<a href="#"><img src="<?= base_url() ?>assets/img/logoadmin.png" alt="" /></a>-->
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="header-top-menu">
                            <ul class="nav navbar-nav notika-top-nav">




                                <!--<?php include APPPATH . 'modules/module/views/messages.php' ?>  -->
                                <!--<?php include APPPATH . 'modules/module/views/notification.php' ?>  -->

                                <li class="dropdown user user-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <img src="<?php echo base_url() . 'assets/images/profile/' . (@$profilpic == '' ? 'default_user.jpg' : @$profilpic) ?>" class="user-image" alt="User Image">
                                        <span class="hidden-xs"><?php echo @$nama_user_login ?></span>
                                    </a>
                                    <ul class="dropdown-menu userprofile">
                                        <!-- User image -->
                                        <li class="user-header">
                                            <img src="<?php echo base_url() . 'assets/images/profile/' . (@$profilpic == '' ? 'default_user.jpg' : @$profilpic) ?>" class="img-circle" alt="User Image">


                                        </li>
                                        <!-- Menu Body -->

                                        <!-- Menu Footer-->
                                        <li class="user-footer">
                                            <div class="pull-left">
                                                <a href="<?php echo base_url() ?>index.php/main/change_password" class="btn btn-default btn-flat">Change Password</a>
                                            </div>
                                            <div class="pull-right">
                                                <a href="<?php echo base_url() ?>index.php/logon/logout" class="btn btn-default btn-flat">Sign out</a>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="clear"></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Top Area -->
        <!-- Mobile Menu start -->
        <?php include APPPATH . 'modules/module/views/mobilebar.php' ?> 
        <!-- Mobile Menu end -->
        <!-- Main Menu area start-->
        <div class="main-menu-area mg-tb-40">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <?php include APPPATH . 'modules/module/views/topbar.php' ?> 
                    </div>
                </div>
            </div>
        </div>
        <div class="notika-status-area">
            <div class="container">
                <div id="notification"></div>