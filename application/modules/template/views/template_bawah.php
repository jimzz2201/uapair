</div>
</div>
<!-- End Realtime sts area-->
<!-- Start Footer area-->
<div class="footer-copyright-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="footer-copy-right">
                    <p>Copyright © 2018 
                        . UAP AIR</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="overlay hide">
    <div class="fillbackground" style="height:100%;width:100%;display:block">
        <img src="<?php echo base_url() ?>assets/images/loader.gif" style="width:10%;margin-top:20%" />
    </div>
</div>
</div>
<div data-remodal-id="mymodal" role="dialog" aria-labelledby="modal2Title" aria-describedby="modal2Desc">
    <div>
        <p id="modaldesc">
        </p>
    </div>
</div>

<div id="modalbootstrap" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<?php
foreach (@$javascript as $item) {
    ?> 
    <script type="text/javascript" src="<?php echo base_url() . $item ?>" ></script>
<?php }
?>
<script>

<?php
if (@$messagestatus != null && @$messagestatus != '5' && @$messagestatus != '' && @$message != null && @$message != '') {
    if (@$messagestatus == '2') {
        ?>
            messageerror('<?php echo $message ?>');
        <?php
    } else {
        ?>
            messagesuccess('<?php echo $message ?>');
        <?php
    }
}
if (!CheckEmpty(@$openmenu)) {
    echo '$(".' . @$openmenu . ' ul").addClass("menu-open");';
    echo '$(".' . @$openmenu . '").addClass("active");';
    echo '$(".' . @$openmenu . '").css("display","block");';
}

if (!CheckEmpty(@$currentmenu)) {

    echo '$(".' . @$currentmenu . '").addClass("active");';
}
?>
</script>
</body>
</html>
