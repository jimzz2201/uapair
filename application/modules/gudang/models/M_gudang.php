<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_gudang extends CI_Model {

    public $table = 'uap_gudang';
    public $id = 'id_gudang';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDatagudang() {
        $this->load->library('datatables');
        $this->datatables->select('id_gudang,kode_gudang,nama_gudang,status');
        $this->datatables->from('uap_gudang');
        //add this line for join
        //$this->datatables->join('table2', 'uap_gudang.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editgudang($1);return false;"));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletegudang($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_gudang');
        return $this->datatables->generate();
    }

    // get all
    function GetOneGudang($keyword, $type = 'id_gudang') {
        $this->db->where($type, $keyword);
        $gudang = $this->db->get($this->table)->row();
        return $gudang;
    }

    function GudangManipulate($model) {
        try {
            $model['status'] = DefaultCurrencyDatabase($model['status']);

            if (CheckEmpty($model['id_gudang'])) {
                $model['kode_gudang']= AutoIncrement($this->table, "G", "kode_gudang",2);
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                return array("st" => true, "msg" => "Gudang successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_gudang" => $model['id_gudang']));
                return array("st" => true, "msg" => "Gudang has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    function GetDropDownGudang() {

        $where = [];
        $where['status'] = 1;
        $listreturn = GetTableData("uap_gudang", 'id_gudang', 'nama_gudang', $where, 'obj');
        return $listreturn;
    }
    function GudangDelete($id_gudang) {
        try {
            $this->db->delete($this->table, array('id_gudang' => $id_gudang));
        } catch (Exception $ex) {
            $model['updated_date'] = GetDateNow();
            $model['status'] = 2;
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array('id_gudang' => $id_gudang));
        }
        return array("st" => true, "msg" => "Gudang has been deleted from database");
    }

}
