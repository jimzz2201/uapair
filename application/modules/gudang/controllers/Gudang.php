<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gudang extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_gudang');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K083";
        $header = "K081";
        $title = "Gudang";
        CekModule($module);
        LoadTemplate(array("title" => $title, "form" => $header, "formsubmenu" => $module), "gudang/v_gudang_index", $javascript);
    }

    public function getdatagudang() {
        header('Content-Type: application/json');
        echo $this->m_gudang->GetDatagudang();
    }

    public function create_gudang() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K083";
        $header = "K081";
        CekModule($module);
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->title = 'Create Gudang';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $this->load->view('gudang/v_gudang_manipulate', $row);
    }

    public function gudang_manipulate() {
        $message = '';

        $this->form_validation->set_rules('nama_gudang', 'nama gudang', 'trim|required');
        $model = $this->input->post();
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_gudang->GudangManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_gudang($id = 0) {
        $row = $this->m_gudang->GetOneGudang($id);

        if ($row) {
            $row->button = 'Update';
            $module = "K083";
            $header = "K081";
            CekModule($module);
            $row->form = $header;
            $row->formsubmenu = $module;
            $row->title = 'Update Gudang';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $this->load->view('gudang/v_gudang_manipulate', $row);
        } else {
            SetMessageSession(0, "Gudang cannot be found in database");
            redirect(site_url('gudang'));
        }
    }

    public function gudang_delete() {
        $message = '';
        $this->form_validation->set_rules('id_gudang', 'Gudang', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_gudang->GudangDelete($model['id_gudang']);
        }

        echo json_encode($result);
    }

}

/* End of file Gudang.php */