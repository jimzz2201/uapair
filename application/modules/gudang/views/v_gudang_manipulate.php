<div class="modal-header">
        Gudang <?php echo @$button ?>
    </div>
<div class="modal-body">
<form id="frm_gudang" class="form-horizontal form-groups-bordered validate" method="post">
	<input type="hidden" name="id_gudang" value="<?php echo @$id_gudang; ?>" /> 
	<div class="form-group">
		<?= form_label('Kode Gudang', "txt_kode_gudang", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text','disabled'=>"disabled", 'name' => 'kode_gudang', 'value' => @$kode_gudang, 'class' => 'form-control', 'id' => 'txt_kode_gudang', 'placeholder' => 'Kode Gudang')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Nama Gudang', "txt_nama_gudang", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'nama_gudang', 'value' => @$nama_gudang, 'class' => 'form-control', 'id' => 'txt_nama_gudang', 'placeholder' => 'Nama Gudang')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
		</div>
	</div>
	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
    </div>

</form>
</div>
<script>
$("#frm_gudang").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/gudang/gudang_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>