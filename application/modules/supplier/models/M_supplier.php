<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_supplier extends CI_Model {

    public $table = 'uap_supplier';
    public $id = 'id_supplier';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDatasupplier() {
        $isedit = CekModule("K009", FALSE);
        $isdelete = CekModule("K009", FALSE);
        $this->load->library('datatables');
        $this->datatables->select('id_supplier,kode_supplier,nama_supplier,no_telp,email,contact_person,hutang,status');
        $this->datatables->from('uap_supplier');
        //add this line for join
        //$this->datatables->join('table2', 'uap_supplier.field = table2.field');

        $straction = '';
        if ($isedit) {
            $straction .= anchor(site_url('supplier/edit_supplier/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletesupplier($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_supplier');
        return $this->datatables->generate();
    }

    function KoreksiSaldoHutang($supplier_id, $saldo_awal=null) {
        try {
            if($saldo_awal==null)
            {
                $supplier=$this->GetOneSupplier($supplier_id);
                if($supplier)
                {
                    $saldo_awal=$supplier->saldo_awal;
                }
            }
           
            $this->db->trans_begin();
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account" => "11", "id_subject" => $supplier_id));
            $this->db->select("sum(kredit)-sum(debet) as totalutang");
            $totalutang = $this->db->get()->row()->totalutang;
            $hutang = DefaultCurrencyDatabase($saldo_awal);
            $hutang += CheckEmpty($totalutang)?0:$totalutang;
            $update = array();
            $update['updated_date'] = GetDateNow();
            $update['updated_by'] = ForeignKeyFromDb(GetUserId());
            $update['hutang'] = $hutang;
            $update['saldo_awal'] = DefaultCurrencyDatabase($saldo_awal);
            $message = "";
            $this->db->update("uap_supplier", $update, array("id_supplier" => $supplier_id));
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured";
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                $arrayreturn["msg"] = "Hutang dan Saldo Awal Supplier telah diupdate";
                $arrayreturn["st"] = true;
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    // get all
    function GetOneSupplier($keyword, $type = 'id_supplier',$withdocument="") {
        $this->db->where($type, $keyword);
        $supplier = $this->db->get($this->table)->row();
        if(!CheckEmpty($withdocument))
        {
           $supplier->list_doc=$this->GetDropDownDocument($supplier->id_supplier);
        }
        return $supplier;
    }
    function GetDropDownDocument($id_supplier) {

        $where = [];
        $where['tanggal_lunas'] = null;
        $where['status !='] =2;
        $where['id_supplier']=$id_supplier;
        $listreturn = GetTableData("uap_pembelian_master", 'id_pembelian_master', 'nomor_master', $where, 'obj');
        return $listreturn;
    }


    function SupplierManipulate($model) {
        try {
            if (CheckKey($model, 'hutang')) {
                unset($model['hutang']);
            }
            if (CheckKey($model, 'saldo_awal')) {
                unset($model['saldo_awal']);
            }
            $model['status'] = DefaultCurrencyDatabase($model['status']);
            if ($model['allow_all_cabang'] == 1) {
                $model['id_cabang'] = "all";
            } else {
                if (isset($model['id_cabang']) && count($model['id_cabang'])) {
                    $model['id_cabang'] = implode(",", $model['id_cabang']);
                } else {
                    $model['id_cabang'] = null;
                }
            }
            unset($model['allow_all_cabang']);
            $model['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);

            if (CheckEmpty($model['id_supplier'])) {
                $model['kode_supplier'] = AutoIncrement('uap_supplier', '501', 'kode_supplier', 5);
                $model['created_date'] = GetDateNow();
                $model['hutang'] = 0;
                $model['saldo_awal'] = 0;
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                SetMessageSession(1, 'Supplier successfull added into database');
                return array("st" => true, "msg" => "Supplier successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_supplier" => $model['id_supplier']));
                SetMessageSession(1, 'Supplier has been updated');
                return array("st" => true, "msg" => "Supplier has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDropDownSupplier() {

        $where = [];
        $where['status'] = 1;
        $listreturn = GetTableData("uap_supplier", 'id_supplier', 'nama_supplier', $where, 'obj');
        return $listreturn;
    }

    function SupplierDelete($id_supplier) {
        try {
            $this->db->delete($this->table, array('id_supplier' => $id_supplier));
        } catch (Exception $ex) {
            $model['updated_date'] = GetDateNow();
            $model['status'] = 2;
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array('id_supplier' => $id_supplier));
        }
        return array("st" => true, "msg" => "Supplier has been deleted from database");
    }

}
