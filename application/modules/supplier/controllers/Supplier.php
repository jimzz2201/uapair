<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Supplier extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_supplier');
    }
    

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K008";
        $header = "K007";
        $title = "Supplier";
        CekModule($module);
        LoadTemplate(array("title" => $title, "form" => $header, "formsubmenu" => $module), "supplier/v_supplier_index", $javascript);
    }

    public function getdatasupplier() {
        header('Content-Type: application/json');
        echo $this->m_supplier->GetDatasupplier();
    }

    public function create_supplier() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K009";
        $header = "K007";
        $this->load->model("cabang/m_cabang");
        CekModule($module);
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->title = 'Create Supplier';
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        LoadTemplate($row, 'supplier/v_supplier_manipulate', $javascript);
    }

    public function supplier_manipulate() {
        $message = '';

        $this->form_validation->set_rules('nama_supplier', 'nama supplier', 'trim|required');
        $this->form_validation->set_rules('no_telp', 'no telp', 'trim|required');
        $model = $this->input->post();
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_supplier->SupplierManipulate($model);
            echo json_encode($status);
        }
    }
    public function get_one_supplier(){
        $model=$this->input->post();
        $this->form_validation->set_rules('id_supplier', 'nama supplier', 'trim|required');
        $message="";
      
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_supplier->GetOneSupplier($model["id_supplier"],'id_supplier',@$model['withdocument']);
            $data['st'] = $data['obj']!=null;
            $data['msg']= !$data['st']?"Data supplier tidak ditemukan dalam database":""; 
            echo json_encode($data);
        }
    }
    
    public function koreksi_saldo_action(){
        
        $this->form_validation->set_rules('id_supplier', 'nama supplier', 'trim|required');
        $message="";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $model=$this->input->post();
            if(!CheckKey($model, 'saldo_awal'))
            {
                $supplier=$this->m_supplier->GetOneSupplier($model["id_supplier"]);
                $model['saldo_awal']=$supplier->saldo_awal;
            }
            $data = $this->m_supplier->KoreksiSaldoHutang($model["id_supplier"],$model['saldo_awal']);
            echo json_encode($data);
        }
        
    }
    
    
    public function koreksi_saldo_supplier() {
        $row = (object) array();
        $row->button = 'Koreksi Saldo';
        $module = "K010";
        $header = "K007";
        $this->load->model("cabang/m_cabang");
        CekModule($module);
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->list_supplier=$this->m_supplier->GetDropDownSupplier();
        $row->title = 'Koreksi Saldo Supplier';
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        LoadTemplate($row, 'supplier/v_supplier_koreksi', $javascript);
    }

    public function edit_supplier($id = 0) {
        $row = $this->m_supplier->GetOneSupplier($id);

        if ($row) {
            $row->button = 'Update';
            $module = "K008";
            $header = "K007";
            CekModule($module);
            $row->form = $header;
            $row->formsubmenu = $module;

            if ($row->id_cabang == "0") {
                $row->allow_all_cabang = 0;
                $row->id_cabang = null;
            } elseif ($row->id_cabang == "all") {
                $row->allow_all_cabang = 1;
                $row->id_cabang = null;
            } elseif (!CheckEmpty($row->id_cabang)) {
                $row->allow_all_cabang = 0;
                $row->id_cabang = explode(",", $row->id_cabang);
            }
            $this->load->model("cabang/m_cabang");
            $row->list_cabang = $this->m_cabang->GetDropDownCabang();
            $row->title = 'Update Supplier';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            LoadTemplate($row, 'supplier/v_supplier_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Supplier cannot be found in database");
            redirect(site_url('supplier'));
        }
    }

    public function supplier_delete() {
        $message = '';
        $this->form_validation->set_rules('id_supplier', 'Supplier', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_supplier->SupplierDelete($model['id_supplier']);
        }

        echo json_encode($result);
    }

}

/* End of file Supplier.php */