<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Supplier</h2>
                                <p><?= $button?> Supplier | Data Supplier</p>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-3'>
                        <div class='breadcomb-report'>
                            <?php echo anchor(site_url('/supplier'), '<i class="fa fa-arrow-left"></i> Cancel', 'class="btn btn-warning"'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_supplier" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="id_supplier" value="<?php echo @$id_supplier; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Kode Supplier', "txt_kode_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'name' => 'kode_supplier', 'value' => @$kode_supplier, 'class' => 'form-control', 'id' => 'txt_kode_supplier', 'placeholder' => 'Kode Supplier')); ?>
                                    </div>
                                    <?= form_label('Npwp', "txt_npwp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'npwp', 'value' => @$npwp, 'class' => 'form-control', 'id' => 'txt_npwp', 'placeholder' => 'Npwp')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Nama Supplier', "txt_nama_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_input(array('type' => 'text', 'name' => 'nama_supplier', 'value' => @$nama_supplier, 'class' => 'form-control', 'id' => 'txt_nama_supplier', 'placeholder' => 'Nama Supplier')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Alamat', "txt_alamat", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'rows' => 5, 'name' => 'alamat', 'value' => @$alamat, 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('No Telp', "txt_no_telp", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'no_telp', 'value' => @$no_telp, 'class' => 'form-control', 'id' => 'txt_no_telp', 'placeholder' => 'No Telp')); ?>
                                    </div>
                                    <?= form_label('No Telp 2', "txt_no_telp_2", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'no_telp_2', 'value' => @$no_telp_2, 'class' => 'form-control', 'id' => 'txt_no_telp_2', 'placeholder' => 'No Telp 2')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Fax', "txt_fax", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'fax', 'value' => @$fax, 'class' => 'form-control', 'id' => 'txt_fax', 'placeholder' => 'Fax')); ?>
                                    </div>
                                    <?= form_label('Email', "txt_email", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'email', 'value' => @$email, 'class' => 'form-control', 'id' => 'txt_email', 'placeholder' => 'Email')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Contact Person', "txt_contact_person", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'contact_person', 'value' => @$contact_person, 'class' => 'form-control', 'id' => 'txt_contact_person', 'placeholder' => 'Contact Person')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'rows' => 5, 'name' => 'keterangan', 'value' => @$keterangan, 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>
                                </div>
                                <?php if (CekModule("K095", false)) {
                                    ?>
                                    <div class="form-group">
                                        <?= form_label('Hutang', "txt_hutang", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'name' => 'hutang', 'value' => DefaultCurrency(@$hutang), 'class' => 'form-control', 'id' => 'txt_hutang', 'placeholder' => 'Hutang', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                        <?= form_label('Saldo Awal', "txt_saldo_awal", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'name' => 'saldo_awal', 'value' => DefaultCurrency(@$saldo_awal), 'class' => 'form-control', 'id' => 'txt_saldo_awal', 'placeholder' => 'Saldo Awal', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="form-group">
                                    <?= form_label('Akses semua Cabang', "dd_allow_all_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "allow_all_cabang"), GetYaTidak(), @$allow_all_cabang, array('class' => 'form-control', 'id' => 'dd_allow_all_cabang')); ?>
                                    </div>
                                    <?= form_label('Status', "txt_status", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                    </div>
                                </div>


                                <div class="form-group" id="list_cabang">
                                    <?= form_label('Cabang', "txt_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_dropdown(array("name" => "id_cabang[]"), $list_cabang, @$id_cabang, array('class' => 'form-control', 'id' => 'dd_id_cabang', 'multiple' => 'multiple')); ?>
                                    </div>
                                </div>

                                <div class="form-group buttonarea">
                                    <a href="<?php echo base_url() . 'index.php/supplier' ?>" class="btn btn-default"  >Cancel</a>
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $("#dd_allow_all_cabang").change(function () {
            var allow_all_cabang = $(this).val();
            if (allow_all_cabang == 0) {
                $("#list_cabang").show();
            } else {
                $("#list_cabang").hide();
            }
        }).change();
        $("#dd_id_cabang").select2();
    })
    $("#frm_supplier").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/supplier/supplier_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href = baseurl + 'index.php/supplier';
                } else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>