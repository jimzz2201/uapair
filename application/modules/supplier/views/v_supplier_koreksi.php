<div class='breadcomb-area'>
    <div class='row'>
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class='breadcomb-list'>
                <div class='row'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                        <div class='breadcomb-wp'>
                            <div class='breadcomb-icon'>
                                <i class='notika-icon notika-house'></i>
                            </div>
                            <div class='breadcomb-ctn'>
                                <h2>Supplier</h2>
                                <p><?= $button ?> Supplier | Data Supplier</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_supplier" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="id_supplier" value="<?php echo @$id_supplier; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Nama', "txt_nama_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-7">
                                        <?= form_dropdown(array("name" => "id_supplier"), DefaultEmptyDropdown($list_supplier, "", "Supplier"), @$id_supplier, array('class' => 'form-control', 'id' => 'dd_id_supplier')); ?>
                                    </div>
                                    <?= form_label('Kode', "txt_kode_supplier", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'name' => 'kode_supplier', 'value' => @$kode_supplier, 'class' => 'form-control', 'id' => 'txt_kode_supplier', 'placeholder' => 'Kode Supplier')); ?>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <?= form_label('Alamat', "txt_alamat", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'disabled' => 'disabled', 'rows' => 5, 'name' => 'alamat', 'value' => @$alamat, 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat')); ?>
                                    </div>
                                </div>



                                <?php if (CekModule("K095", false)) {
                                    ?>
                                    <div class="form-group">

                                        <?= form_label('Saldo Awal Sebelumnya', "txt_saldo_awal", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'value' => DefaultCurrency(@$saldo_awal), 'class' => 'form-control', 'id' => 'txt_saldo_awal', 'placeholder' => 'Saldo Awal', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                        <?= form_label('Hutang', "txt_hutang", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'value' => DefaultCurrency(@$hutang), 'class' => 'form-control', 'id' => 'txt_hutang', 'placeholder' => 'Hutang', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?= form_label('Saldo Awal', "txt_saldo_awal", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'name' => 'saldo_awal', 'value' => DefaultCurrency(@$saldo_awal), 'class' => 'form-control', 'id' => 'txt_saldo_semestinya', 'placeholder' => 'Saldo Awal', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>

                                    </div>
                                <?php } ?>

                                <?php if (CekModule("K095", false)) { ?>
                                    <div class="form-group buttonarea">
                                        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                    </div>
                                <?php } ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {

        $("#dd_id_supplier").select2();
        $("#dd_id_supplier").change(function () {
            if ($("#dd_id_supplier").val() != "0")
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/supplier/get_one_supplier',
                    dataType: 'json',
                    data: $("form#frm_supplier").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            $("#txt_kode_supplier").val(data.obj.kode_supplier);
                            $("#txt_kode_supplier").val(data.obj.kode_supplier);
                            $("#txt_alamat").html(data.obj.alamat);
                            $("#txt_hutang").val(Comma(data.obj.hutang));
                            $("#txt_saldo_awal").val(Comma(data.obj.saldo_awal));
                            $("#txt_saldo_semestinya").val(Comma(data.obj.saldo_awal));
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            } else
            {
                $("#txt_kode_supplier").val("");
                $("#txt_alamat").html("");
                $("#txt_saldo_awal").val("0");
                $("#txt_hutang").val("0");
                $("#txt_saldo_semestinya").val("0");
            }
        })
    })

    $("#frm_supplier").submit(function () {
        swal({
            title: "Apakah anda ingin melakukan transaksi berikut?",
            text: "Transaksi ini akan mengubah saldo dan hutang supplier!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Ubah data ini!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/supplier/koreksi_saldo_action',
                    dataType: 'json',
                    data: $("form#frm_supplier").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            $("#dd_id_supplier").val(0);
                            $("#txt_kode_supplier").val("");
                            $("#txt_alamat").html("");
                            $("#txt_saldo_awal").val("0");
                            $("#txt_hutang").val("0");
                            $("#txt_saldo_semestinya").val("0");
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });

        return false;

    })
</script>