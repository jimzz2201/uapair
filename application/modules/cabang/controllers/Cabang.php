<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cabang extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_cabang');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module="K084";
        $header="K081";
        $title="Cabang";
        CekModule($module);
        LoadTemplate(array("title"=>$title,"form"=>$header,"formsubmenu"=>$module), "cabang/v_cabang_index", $javascript);
        
    } 
    
    public function getdatacabang() {
        header('Content-Type: application/json');
        echo $this->m_cabang->GetDatacabang();
    }
    
    public function create_cabang() 
    {
        $row=(object)array();
        $row->button='Add';
        $module="K084";
        $header="K081";
        CekModule($module);
        $row->form=$header;
        $row->formsubmenu=$module;
        $row->title='Create Cabang';
        $row=json_decode(json_encode($row),true);
        $javascript = array();   
        $row['list_gudang'] = GetTableData('uap_gudang', 'id_gudang', 'nama_gudang', ['status' => 1], 'obj');
        $this->load->view('cabang/v_cabang_manipulate', $row);       
    }
    
    public function cabang_manipulate() 
    {
        $message='';
        $model=$this->input->post();

        if(CheckEmpty($model['id_cabang'])||CheckEmpty($model['kode_cabang']))
        {
            $this->form_validation->set_rules('kode_cabang', 'kode cabang', 'trim|required');
	}
        else {
            $cabang=$this->m_cabang->GetOneCabang($model['kode_cabang'],'kode_cabang');
            if($cabang->id_cabang!=$model['id_cabang'])
            {
                $message.='Kode cabang already registered in database<br/>';
            }
        }
	$this->form_validation->set_rules('nama_cabang', 'nama cabang', 'trim|required');
	
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_cabang->CabangManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_cabang($id=0) 
    {
        $row = $this->m_cabang->GetOneCabang($id);
        
        if ($row) {
            $row->button='Update';
            $module="K084";
            $header="K081";
            CekModule($module);
            if ($row->id_gudang == "0") {
                $row->allow_all_gudang = 0;
                $row->id_gudang = null;
            } elseif ($row->id_gudang == "all") {
                $row->allow_all_gudang = 1;
                $row->id_gudang = null;
            } elseif (!CheckEmpty($row->id_gudang)) {
                $row->allow_all_gudang = 0;
                $row->id_gudang = explode(",", $row->id_gudang);
            }
            $row->list_gudang = GetTableData('uap_gudang', 'id_gudang', 'nama_gudang', ['status' => 1], 'obj');
            $row->form=$header;
            $row->formsubmenu=$module;
            $row->title='Update Cabang';
            $row = json_decode(json_encode($row), true);
            $javascript = array();        
            $this->load->view('cabang/v_cabang_manipulate', $row);
        } else {
            SetMessageSession(0, "Cabang cannot be found in database");
            redirect(site_url('cabang'));
        }
    }
    
    public function cabang_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_cabang->CabangDelete($model['id_cabang']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Cabang.php */