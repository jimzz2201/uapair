<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_cabang extends CI_Model {

    public $table = 'uap_cabang';
    public $id = 'id_cabang';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDatacabang() {
        $this->load->library('datatables');
        $this->datatables->select('id_cabang,kode_cabang,nama_cabang,id_gudang,status');
        $this->datatables->from('uap_cabang');
        //add this line for join
        //$this->datatables->join('table2', 'uap_cabang.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editcabang($1);return false;"));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletecabang($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_cabang');
        return $this->datatables->generate();
    }

    // get all
    function GetOneCabang($keyword, $type = 'id_cabang') {
        $this->db->where($type, $keyword);
        $cabang = $this->db->get($this->table)->row();
        return $cabang;
    }

    function CabangManipulate($model) {
        try {
           
           if ($model['allow_all_gudang'] == 1) {
                $model['id_gudang'] = "all";
            } else {
                if (isset($model['id_gudang']) && count($model['id_gudang'])) {
                    $model['id_gudang'] = implode(",", $model['id_gudang']);
                } else {
                    $model['id_gudang'] = null;
                }
            }
            unset($model['allow_all_gudang']);
            $model['id_gudang'] = ForeignKeyFromDb($model['id_gudang']);
            $model['status'] = DefaultCurrencyDatabase($model['status']);
           
            if (CheckEmpty($model['id_cabang'])) {
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                return array("st" => true, "msg" => "Cabang successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_cabang" => $model['id_cabang']));
                return array("st" => true, "msg" => "Cabang has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    function GetDropDownCabang(){
        
        $where = [];
        $where['status'] = 1;
        $listreturn = GetTableData("uap_cabang", 'id_cabang', 'nama_cabang', $where, 'obj');
        return $listreturn;
    }
    function CabangDelete($id_cabang) {
        try {
            $this->db->delete($this->table, array('id_cabang' => $id_cabang));
        } catch (Exception $ex) {
            $model['updated_date'] = GetDateNow();
            $model['status'] = 2;
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array('id_cabang' => $id_cabang));
        }
        return array("st" => true, "msg" => "Cabang has been deleted from database");
    }

}
