<div class="modal-header">
    Cabang <?php echo @$button ?>
</div>
<div class="modal-body">
    <form id="frm_cabang" class="form-horizontal form-groups-bordered validate" method="post">
        <input type="hidden" name="id_cabang" value="<?php echo @$id_cabang; ?>" /> 
        <div class="form-group">
            <?= form_label('Kode Cabang', "txt_kode_cabang", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text', 'name' => 'kode_cabang', 'value' => @$kode_cabang, 'class' => 'form-control', 'id' => 'txt_kode_cabang', 'placeholder' => 'Kode Cabang')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Nama Cabang', "txt_nama_cabang", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text', 'name' => 'nama_cabang', 'value' => @$nama_cabang, 'class' => 'form-control', 'id' => 'txt_nama_cabang', 'placeholder' => 'Nama Cabang')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Akses semua Gudang', "dd_allow_all_cabang", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array("name" => "allow_all_gudang"), GetYaTidak(), @$allow_all_gudang, array('class' => 'form-control', 'id' => 'dd_allow_all_gudang')); ?>
            </div>
        </div>
        <div class="form-group" id="list_gudang">
            <?= form_label('Gudang', "txt_id_gudang", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
            <?= form_dropdown(array("name" => "id_gudang[]"), $list_gudang,  @$id_gudang, array('class' => 'form-control', 'id' => 'dd_id_gudang', 'multiple' => 'multiple')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
            <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
        </div>

    </form>
</div>
<script>
    $(document).ready(function () {
        $("#dd_allow_all_gudang").change(function () {
            var allow_all_cabang = $(this).val();
            if (allow_all_cabang == 0) {
                $("#list_gudang").show();
            } else {
                $("#list_gudang").hide();
            }
        }).change();
        $("#dd_id_gudang").select2();
    })
    
    
    $("#frm_cabang").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/cabang/cabang_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>