<?php

class M_excel extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('excel');
    }

    function getcolumn($index) {
        $stringarray = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
            "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX");

        return $stringarray[$index];
    }

    function EksportTarif($model) {
        $this->load->model("tarif/m_tarif");
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $listreport = $this->m_tarif->GetObjectTarif($model);
        $this->excel->getActiveSheet()->setTitle('Tarif Upload');
        
        $startcolumn = 1;
        $this->excel->getActiveSheet()->getStyle("A" . $startcolumn . ":AA" . $startcolumn)->getFont()->setBold(true)
                ->setName('Verdana')
                ->setSize(10)
                ->getColor()->setRGB('FFFFFF');
        $this->excel->getActiveSheet()->getStyle("A" . $startcolumn . ":AA" . $startcolumn)->getFill()->applyFromArray(array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
                'argb' => "#000"
            )
        ));
        $this->excel->getActiveSheet()->setCellValue('A' . $startcolumn, 'Kota Asal');
        $this->excel->getActiveSheet()->setCellValue('B' . $startcolumn, 'Pangkalan Asal');
        $this->excel->getActiveSheet()->setCellValue('C' . $startcolumn, 'Kota Tujuan');
        $this->excel->getActiveSheet()->setCellValue('D' . $startcolumn, 'Pangkalan Tujuan');
        $this->excel->getActiveSheet()->setCellValue('E' . $startcolumn, 'Kelas Body');
        $this->excel->getActiveSheet()->setCellValue('F' . $startcolumn, 'Harga');
        $this->excel->getActiveSheet()->setCellValue('G' . $startcolumn, 'Harga Lebaran');
       
        
        $startcolumn += 1;
        for ($index = 0; $index < count($listreport); $index++) {
            $this->excel->getActiveSheet()->setCellValue('A' . $startcolumn, $listreport[$index]->kode_kota_asal);
            $this->excel->getActiveSheet()->setCellValue('B' . $startcolumn, $listreport[$index]->kode_pangkalan_asal);
            $this->excel->getActiveSheet()->setCellValue('C' . $startcolumn, $listreport[$index]->kode_kota_tujuan);
            $this->excel->getActiveSheet()->setCellValue('D' . $startcolumn, $listreport[$index]->kode_pangkalan_tujuan);
            $this->excel->getActiveSheet()->setCellValue('E' . $startcolumn, $listreport[$index]->kode_kelas);
            $this->excel->getActiveSheet()->setCellValue('F' . $startcolumn, $listreport[$index]->harga);
            $this->excel->getActiveSheet()->setCellValue('G' . $startcolumn, $listreport[$index]->harga_lebaran);
            $startcolumn += 1;
        }
        $this->excel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);

        



        $this->excel->getActiveSheet()->getStyle('E1:H' . $startcolumn)->getNumberFormat()->setFormatCode("#,##0");
        $this->excel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("G")->setAutoSize(true);
        $this->excel->getActiveSheet()->getStyle("A" . $startcolumn . ":AA" . $startcolumn)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $filename = date("Ymd") . '_Tarif' . '.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter->save('php://output');
    }
    function EksportTrayek($model) {
        $this->load->model("trayek/m_trayek");
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $listreport = $this->m_trayek->GetObjectTrayek($model);
        
        $this->excel->getActiveSheet()->setTitle('Trayek Upload');
        
        $startcolumn = 1;
        $this->excel->getActiveSheet()->getStyle("A" . $startcolumn . ":AA" . $startcolumn)->getFont()->setBold(true)
                ->setName('Verdana')
                ->setSize(10)
                ->getColor()->setRGB('FFFFFF');
        $this->excel->getActiveSheet()->getStyle("A" . $startcolumn . ":AA" . $startcolumn)->getFill()->applyFromArray(array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
                'argb' => "#000"
            )
        ));
        $this->excel->getActiveSheet()->setCellValue('A' . $startcolumn, 'NO');
        $this->excel->getActiveSheet()->setCellValue('B' . $startcolumn, 'ASAL');
        $this->excel->getActiveSheet()->setCellValue('C' . $startcolumn, 'TUJUAN');
        $this->excel->getActiveSheet()->setCellValue('D' . $startcolumn, 'JENIS');
        $this->excel->getActiveSheet()->setCellValue('E' . $startcolumn, 'KELAS');
        $this->excel->getActiveSheet()->setCellValue('F' . $startcolumn, 'ROUTE');
        $this->excel->getActiveSheet()->setCellValue('G' . $startcolumn, 'JARAK');
        $this->excel->getActiveSheet()->setCellValue('H' . $startcolumn, 'P');
        $this->excel->getActiveSheet()->setCellValue('I' . $startcolumn, 'BO BARAT');
        $this->excel->getActiveSheet()->setCellValue('J' . $startcolumn, 'BO TIMUR');
        $this->excel->getActiveSheet()->setCellValue('K' . $startcolumn, 'TAMB RG');
        $this->excel->getActiveSheet()->setCellValue('L' . $startcolumn, 'HONOR PNG 1');
        $this->excel->getActiveSheet()->setCellValue('M' . $startcolumn, 'HONOR PNG 2');
        $this->excel->getActiveSheet()->setCellValue('N' . $startcolumn, 'HONOR PNG PT');
        $this->excel->getActiveSheet()->setCellValue('O' . $startcolumn, 'HONOR KERNET');
        $this->excel->getActiveSheet()->setCellValue('P' . $startcolumn, 'KPS BARAT 1');
        $this->excel->getActiveSheet()->setCellValue('Q' . $startcolumn, 'KPS BARAT 2');
        $this->excel->getActiveSheet()->setCellValue('R' . $startcolumn, 'KPS TIMUR 1');
        $this->excel->getActiveSheet()->setCellValue('S' . $startcolumn, 'KPS TIMUR 2');
         $this->excel->getActiveSheet()->setCellValue('T' . $startcolumn, 'KPS PT');
        
       
        
        $startcolumn += 1;
        for ($index = 0; $index < count($listreport); $index++) {
            $this->excel->getActiveSheet()->setCellValue('A' . $startcolumn, $index + 1);
            $this->excel->getActiveSheet()->setCellValue('B' . $startcolumn, $listreport[$index]->kode_kota_asal);
            $this->excel->getActiveSheet()->setCellValue('C' . $startcolumn, $listreport[$index]->kode_kota_tujuan);
            $this->excel->getActiveSheet()->setCellValue('D' . $startcolumn, $listreport[$index]->nama_jenis_spj);
            $this->excel->getActiveSheet()->setCellValue('E' . $startcolumn, $listreport[$index]->nama_kelas);
            $this->excel->getActiveSheet()->setCellValue('F' . $startcolumn, $listreport[$index]->route);
            $this->excel->getActiveSheet()->setCellValue('G' . $startcolumn, $listreport[$index]->jarak);
            $this->excel->getActiveSheet()->setCellValue('H' . $startcolumn, $listreport[$index]->is_pengemudi_2?"P2":"P1");
            $this->excel->getActiveSheet()->setCellValue('I' . $startcolumn, $listreport[$index]->bo_barat);
            $this->excel->getActiveSheet()->setCellValue('J' . $startcolumn, $listreport[$index]->bo_timur);
            $this->excel->getActiveSheet()->setCellValue('K' . $startcolumn, $listreport[$index]->tambahan_rg);
            $this->excel->getActiveSheet()->setCellValue('L' . $startcolumn, $listreport[$index]->honor_pengemudi_1);
            $this->excel->getActiveSheet()->setCellValue('M' . $startcolumn, $listreport[$index]->honor_pengemudi_2);
            $this->excel->getActiveSheet()->setCellValue('N' . $startcolumn, $listreport[$index]->honor_pengemudi_pt);
            $this->excel->getActiveSheet()->setCellValue('O' . $startcolumn, $listreport[$index]->honor_kernet);
            $this->excel->getActiveSheet()->setCellValue('P' . $startcolumn, $listreport[$index]->kps_barat_1);
            $this->excel->getActiveSheet()->setCellValue('Q' . $startcolumn, $listreport[$index]->kps_barat_2);
            $this->excel->getActiveSheet()->setCellValue('R' . $startcolumn, $listreport[$index]->kps_timur_1);
            $this->excel->getActiveSheet()->setCellValue('S' . $startcolumn, $listreport[$index]->kps_timur_2);
            $this->excel->getActiveSheet()->setCellValue('T' . $startcolumn, $listreport[$index]->kps_pt);

            $startcolumn += 1;
        }
       
        



        $this->excel->getActiveSheet()->getStyle('I1:S' . $startcolumn)->getNumberFormat()->setFormatCode("#,##0");
        $this->excel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("G")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("H")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("I")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("J")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("K")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("L")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("M")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("N")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("O")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("P")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("Q")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("R")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("S")->setAutoSize(true);
        $this->excel->getActiveSheet()->getStyle("A" . $startcolumn . ":AA" . $startcolumn)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $filename = date("Ymd") . '_Trayek' . '.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter->save('php://output');
    }
    function ExportMember($model) {
        $this->load->model("member/m_member");
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $listreport = $this->m_member->GetDataMember($model);
        $this->excel->getActiveSheet()->setTitle('Member Report');

        $startcolumn = 1;
        $this->excel->getActiveSheet()->setCellValue('A1', 'Member Report');
        $startcolumn += 1;
        if (!CheckEmpty($model['start_date'])) {

            $this->excel->getActiveSheet()->setCellValue('A' . $startcolumn, 'Start Date');
            $this->excel->getActiveSheet()->setCellValue('B' . $startcolumn, ' : ' . DefaultTanggal($model['start_date']));
            $startcolumn += 1;
        }
        if (!CheckEmpty($model['end_date'])) {

            $this->excel->getActiveSheet()->setCellValue('A' . $startcolumn, 'End Date');
            $this->excel->getActiveSheet()->setCellValue('B' . $startcolumn, ' : ' . DefaultTanggal($model['end_date']));
            $startcolumn += 1;
        }
        $startcolumn += 1;
        $this->excel->getActiveSheet()->setCellValue('A' . $startcolumn, 'Username');
        $this->excel->getActiveSheet()->setCellValue('B' . $startcolumn, 'Kode User');
        $this->excel->getActiveSheet()->setCellValue('C' . $startcolumn, 'Nama');
        $this->excel->getActiveSheet()->setCellValue('D' . $startcolumn, 'No HP');
        $this->excel->getActiveSheet()->setCellValue('E' . $startcolumn, 'Email');
        $this->excel->getActiveSheet()->setCellValue('F' . $startcolumn, 'Sponsor');
        $this->excel->getActiveSheet()->setCellValue('G' . $startcolumn, 'Upline');
        $this->excel->getActiveSheet()->setCellValue('H' . $startcolumn, 'Position');
        $this->excel->getActiveSheet()->setCellValue('I' . $startcolumn, 'Package Name');
        $this->excel->getActiveSheet()->setCellValue('J' . $startcolumn, 'Join Date');
        $this->excel->getActiveSheet()->getStyle("A" . $startcolumn . ":AA" . $startcolumn)->getFont()->setBold(true)
                ->setName('Verdana')
                ->setSize(10)
                ->getColor()->setRGB('FFFFFF');
        $this->excel->getActiveSheet()->getStyle("A" . $startcolumn . ":AA" . $startcolumn)->getFill()->applyFromArray(array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
                'argb' => "#000"
            )
        ));
        $startcolumn += 1;
        for ($index = 0; $index < count($listreport); $index++) {
            $this->excel->getActiveSheet()->setCellValue('A' . $startcolumn, $listreport[$index]->username);
            $this->excel->getActiveSheet()->setCellValue('B' . $startcolumn, $listreport[$index]->kode_user);
            $this->excel->getActiveSheet()->setCellValue('C' . $startcolumn, $listreport[$index]->name);
            $this->excel->getActiveSheet()->setCellValue('D' . $startcolumn, $listreport[$index]->telpnum);
            $this->excel->getActiveSheet()->setCellValue('E' . $startcolumn, ($listreport[$index]->email));
            $this->excel->getActiveSheet()->setCellValue('F' . $startcolumn, ($listreport[$index]->sponsor_code));
            $this->excel->getActiveSheet()->setCellValue('G' . $startcolumn, ($listreport[$index]->sponsor_upline));
            $this->excel->getActiveSheet()->setCellValue('H' . $startcolumn, ($listreport[$index]->position));
            $this->excel->getActiveSheet()->setCellValue('I' . $startcolumn, $listreport[$index]->package_name);
            $this->excel->getActiveSheet()->setCellValue('J' . $startcolumn, DefaultTanggal($listreport[$index]->create_date));
            
            $startcolumn += 1;
        }
        $this->excel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);

        $this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true)
                ->setName('Times New Roman')
                ->setSize(20)
                ->getColor()->setRGB('000000');



        $this->excel->getActiveSheet()->getStyle('E1:H' . $startcolumn)->getNumberFormat()->setFormatCode("#,##0");
        $this->excel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("G")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("H")->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension("I")->setAutoSize(true);
        $this->excel->getActiveSheet()->getStyle("A" . $startcolumn . ":AA" . $startcolumn)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $filename = date("Ymd") . '_ReportWP' . '.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter->save('php://output');
    }
 
}

?>