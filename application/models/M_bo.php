<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_bo extends CI_Model {

    public $table = 'tic_bo';
    public $id = 'id_bo';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDataBo() {
        $this->load->library('datatables');
        $select = "tic_bo.*, ";
        $select .= "tic_spj.no_spj, ";
        $select .= "tic_body.no_body, tic_body.no_polisi, ";
        $select .= "tic_shift.nama_shift, ";
        $select .= "tic_user.username AS pembuat, ";
        $this->datatables->select($select);
        $this->datatables->from($this->table);
        $this->datatables->join("tic_spj", "tic_spj.id_spj = tic_bo.id_spj");
        $this->datatables->join("tic_body", "tic_body.id_body = tic_spj.id_body", "left");
        $this->datatables->join("tic_shift", "tic_shift.id_shift = tic_bo.id_shift", "left");
        $this->datatables->join("tic_user", "tic_user.id_user = tic_bo.created_by", "left");
        $str = $this->input->post("extra_search");
        parse_str($str, $model);

        $this->datatables->where(array('jenis_bo' => $model['jenis_bo']));
        $this->datatables->where(array("tic_bo.status <" => 2));

        if(!CheckEmpty($model["no_spj"])){
             $this->datatables->where(array("tic_spj.no_spj" => $model["no_spj"]));
        }elseif(!CheckEmpty($model["no_random"])){
            $this->datatables->where(array("tic_spj.no_random" => $model["no_random"]));
        }elseif(!CheckEmpty($model["no_body"])){
            $this->datatables->where(array("tic_body.no_body" => $model["no_body"]));
        }else{
            $this->datatables->where(array("tic_bo.id_bo" => 0));
        }

        $isedit = false;
        $isdelete = false;
        $straction = '';
        if ($isedit) {
            $straction .= anchor(site_url('bo/edit_bo/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletebo($1);return false;"));
        }

        $straction .= anchor("", '<i class="fa fa-remove"></i> Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batalkanBo($1);return false;"));
        $this->datatables->add_column('action', $straction, 'id_bo');
        return $this->datatables->generate();
    }

    public function GetOneBo($keyword, $field = "tic_bo.id_bo", $isfull = false) {
        if (CheckEmpty($field)) {
            $field = 'tic_bo.id_bo';
        }

        $where = array();
        $where[$field] = $keyword;

        $select = "tic_bo.*, ";
        $select .= "tic_spj.no_spj, ";
        $select .= "tic_berangkat.kode_berangkat, ";
        $select .= "tic_body.no_body, tic_body.no_polisi, ";
        $select .= "tic_shift.nama_shift, ";
        $select .= "tic_user.username AS pembuat, ";
        $this->db->select($select);
        $this->db->from($this->table);
        $this->db->join("tic_spj", "tic_spj.id_spj = tic_bo.id_spj");
        $this->db->join("tic_body", "tic_body.id_body = tic_spj.id_body", "left");
        $this->db->join("tic_shift", "tic_shift.id_shift = tic_bo.id_shift", "left");
        $this->db->join("tic_user", "tic_user.id_user = tic_bo.created_by", "left");
        $row = $this->db->where($where)->get()->row();
      

        return $row;
    }

    function BoManipulate($model) {
        try {
            $model['id_spj'] = ForeignKeyFromDb($model['id_spj']);
            $model['tanggal'] = DefaultTanggalDatabase($model['tanggal']);
            $model['nominal'] = DefaultCurrencyDatabase($model['nominal']);
            $model['keterangan'] = DefaultCurrencyDatabase($model['keterangan']);

            if (!isset($model['id_bo']) || CheckEmpty($model['id_bo'])) {
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $model['status'] = 0;
                $model['is_batal'] = 0;
                $model['id_shift'] = GetShift();
//                dumperror($model); die();
                $this->db->insert($this->table, $model);
                SetMessageSession(1, 'Biaya Operasional berhasil dibuat.');
                return array("st" => true, "msg" => "Biaya Operasional berhasil dibuat.", 'timestamp' => strtotime($model['created_date']), "id_bo" => $this->db->insert_id());
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_bo" => $model['id_bo']));
                SetMessageSession(1, 'Biaya Operasional berhasil diubah');
                return array("st" => true, "msg" => "Biaya Operasional berhasil diubah.");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function BoDelete($id_bo) {
        try {
            $this->db->delete($this->table, array('id_bo' => $id_bo));
        } catch (Exception $ex) {
            $model['updated_date'] = GetDateNow();
            $model['status'] = 2;
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array('id_bo' => $id_bo));
        }
        return array("st" => true, "msg" => "Operasional berhasil dihapus.");
    }

    function BoBatal($id_bo, $jenis_bo) {
        $resp = array(
            'st' => true,
            'msg' => ''
        );
        try {
            $model['updated_date'] = GetDateNow();
            $model['is_batal'] = 1;
            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->update($this->table, $model, array('id_bo' => $id_bo));

            $resp['msg'] = GetJenisBO($jenis_bo)." berhasil dibatalkan.";
        } catch (Exception $ex) {
            $resp['st'] = false;
            $resp['msg'] = $ex;
        }
        return $resp;
    }

    function manipulateBoByCond($update, $where){
        $resp = array(
            'st' => true,
            'msg' => ''
        );
        try {
            $this->db->update($this->table, $update, $where);

            $resp['msg'] = "Operasional berhasil diupdate.";
        } catch (Exception $ex) {
            $resp['st'] = false;
            $resp['msg'] = $ex;
        }
        return $resp;
    }

    function GetAvaBo() {
        $this->load->library('datatables');
        $select = "tic_bo.*, ";
        $select .= "tic_spj.no_spj, ";
        $select .= "tic_berangkat.kode_berangkat, ";
        $select .= "tic_body.no_body, tic_body.no_polisi, ";
        $select .= "tic_shift.nama_shift, ";
        $select .= "tic_pangkalan.nama_pangkalan, ";
        $select .= "tic_pangkalan.kode AS pangkalan, ";
        $select .= "tic_user.username AS pembuat, ";
        $select .= "CONCAT(tp1.kode, ' => ', tp2.kode) AS trayek, ";
        $select .= "CONCAT('bo_row_',id_bo) AS DT_RowId";
        $this->datatables->select($select);
        $this->datatables->from($this->table);
        $this->datatables->join("tic_spj", "tic_spj.id_spj = tic_bo.id_spj");
        $this->datatables->join("tic_body", "tic_body.id_body = tic_spj.id_body", "left");
        $this->datatables->join("tic_shift", "tic_shift.id_shift = tic_bo.id_shift", "left");
        $this->datatables->join("tic_pangkalan", "tic_pangkalan.id_pangkalan = tic_bo.id_pangkalan", "left");
        $this->datatables->join("tic_user", "tic_user.id_user = tic_bo.created_by", "left");
        $str = $this->input->post("extra_search");
        parse_str($str, $model);

        $this->datatables->where(array("tic_bo.status" => 0, "tic_bo.is_batal" => 0));

        if(!CheckEmpty($model["dipilih"])){
            $this->datatables->where("tic_bo.id_bo NOT IN (".$model['dipilih'].")");
        }

        if(!CheckEmpty($model["id_pangkalan"])){
             $this->datatables->where(array("tic_bo.id_pangkalan" => $model["id_pangkalan"]));
        }else{
            $this->datatables->where(array("tic_bo.id_bo" => 0));
        }

        $this->datatables->where(array("tic_bo.status <" => 2));

        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor(site_url('bo/edit_bo/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletebo($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_bo');
        return $this->datatables->generate();
    }

    function getBo($jenis, $id_spj){
        $jumlah = 0;
        $this->db->select("SUM(nominal) AS jumlah");
        $this->db->from($this->table);
        $this->db->where(array('id_spj'=>$id_spj, 'jenis_bo'=>$jenis, 'is_batal' => 0));
        $result = $this->db->get()->row_array();

        if($result){
            $jumlah = $result['jumlah'];
        }

        return $jumlah;
    }

}
