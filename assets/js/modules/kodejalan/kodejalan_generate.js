$(document).ready(function () {
    /*$('.datepicker').datepicker({
     autoclose: true,
     dateFormat: 'dd M yy',
     });*/
    $('.datepicker').daterangepicker({
        locale: {
            format: 'DD MMM YYYY',
            firstDay : 1
        },
        showDropdowns: true,
    });

    $(".timepicker").timepicker({
        showInputs: false,
        showMeridian: false
    });

    $(".timepickerX").timepickerExt({
        showInputs: false,
        showMeridian: false
    });

    $(".select2kelas").select2({});

    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red, #cb-allday, #cb-weekday, #cb-weekend').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red',
        labelHover: false
    });

    $("input.flat-green").iCheck({
        radioClass: 'iradio_flat-green',
        labelHover: false
    });

    $("#frm_generate").submit(function(){
        var jml_hari = parseInt($(".cb-hari:checked").length);
        if(jml_hari){
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/kodejalan/generate_action',
                dataType: 'json',
                data: $(this).serialize(),
                success: function (data) {
                    if (data.st) {
                        window.location.href = baseurl + 'index.php/kodejalan';
                    }
                    else {
                        messageerror(data.msg);
                    }

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
        }else{
            alert("Silahkan pilih hari!");
        }
//            $("#frm_generate input[type='submit']").attr('value','<?//=$button;?>//').prop('disabled', false);
        return false;
    });

    $("#cb-allday").on('ifChecked', function(event){
        changeChecked('allday');
    });
    $("#cb-allday").on('ifUnchecked', function(event){
        changeUnchecked('allday');
    });

    $("#cb-weekday").on('ifChecked', function(event){
        changeChecked('weekday');
    });
    $("#cb-weekday").on('ifUnchecked', function(event){
        changeUnchecked('weekday');
    });

    $("#cb-weekend").on('ifChecked', function(event){
        changeChecked('weekend');
    });
    $("#cb-weekend").on('ifUnchecked', function(event){
        changeUnchecked('weekend');
    });

});

function changeChecked(mode) {
    if(mode == "allday"){
        $("#cb-weekday, #cb-weekend").iCheck('check');
    }else if(mode == "weekday"){
        if($("#cb-weekend").is(":checked")){
            $("#cb-allday").prop('checked', true).iCheck('update');
        }
        $(".cb-hari:lt(5)").iCheck('check');
    }else if(mode == "weekend"){
        if($("#cb-weekday").is(":checked")){
            $("#cb-allday").prop('checked', true).iCheck('update');
        }
        $(".cb-hari:gt(4)").iCheck('check');
    }
}
function changeUnchecked(mode) {
    if(mode == "allday"){
        $("#cb-weekday, #cb-weekend").iCheck('uncheck');
    }else if(mode == "weekday"){
        $(".cb-hari:lt(5)").iCheck('uncheck');
        $("#cb-allday").removeAttr('checked').iCheck('update');
    }else if(mode == "weekend"){
        $(".cb-hari:gt(4)").iCheck('uncheck');
        $("#cb-allday").removeAttr('checked').iCheck('update');
    }
}