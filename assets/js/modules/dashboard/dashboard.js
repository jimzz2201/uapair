function buildGraph(el, key, data, label){
    new Morris.Line({
        element: el,
        resize: true,
        data: data,
        xkey: 'y',
        ykeys: [key],
        labels: [label],
        lineColors: ['#efefef'],
        lineWidth: 2,
        hideHover: 'auto',
        gridTextColor: "#fff",
        gridStrokeWidth: 0.4,
        pointSize: 4,
        pointStrokeColors: ["#efefef"],
        gridLineColor: "#efefef",
        gridTextFamily: "Open Sans",
        gridTextSize: 10
    });
}