var tabungan = 0;
var overblass_barat = 0;
var overblass_timur = 0;
var ajaxUrl = baseurl + 'index.php/strook3/';

$(document).ready(function () {
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd M yyyy',
        weekStart: 1,
        language: "id",
        todayHighlight: true
    });

    $("#id_pangkalan_strook_4").select2();
    
    $("#frm_strook3").submit(function () {
        $.ajax({
            type: 'POST',
            url: ajaxUrl + "strook3_manipulate",
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st){
                    window.location.href = ajaxUrl;
                }else{
                    messageerror(data.msg);
                }
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;
    });

    Checkpengemudi();
    ChangeBarat('');
    ChangeTimur('');
    CekLaka("Cb_laka_pengemudi2", "txt_keterangan_laka_pengemudi2");
    CekLaka("Cb_laka_pengemudi1", "txt_keterangan_laka_pengemudi1");

    // event handler function
    $("#Cb_laka_pengemudi2").change(function () {
        if ($("#Cb_is_pengemudi2").is(":checked")) {
            CekLaka("Cb_laka_pengemudi2", "txt_keterangan_laka_pengemudi2");
        }
        else {
            $("#Cb_laka_pengemudi2").prop("checked", false);
            alert("Pengemudi hanya 1");
        }
        RefreshKPSEkstra();
        CheckKPSelamat();
        RefreshKPSUHT();
        HitungSemua();
    });

    $("#Cb_laka_pengemudi1").change(function () {
        CekLaka("Cb_laka_pengemudi1", "txt_keterangan_laka_pengemudi1");
        RefreshKPSEkstra();
        CheckKPSelamat();
        RefreshKPSUHT();
        HitungSemua();
    });

    $("#Cb_is_kps_ekstra_pengemudi").change(function () {
        var isrefresh = true;
        if ($("#Cb_laka_pengemudi1").is(':checked')) {
            alert("Laka Pengemudi 1, kps nonaktif");
            $("#Cb_laka_pengemudi1").prop("checked", false);
            CekLaka("Cb_laka_pengemudi1", "txt_keterangan_laka_pengemudi1");

        }
        if ($("#Cb_laka_pengemudi2").is(':checked')) {
            alert("Laka Pengemudi 2, kps nonaktif");
            $("#Cb_laka_pengemudi2").prop("checked", false);
            CekLaka("Cb_laka_pengemudi2", "txt_keterangan_laka_pengemudi2");

        }
        RefreshKPSEkstra();
        CheckKPSelamat();
        RefreshKPSUHT();
        HitungSemua();
    });

    $("#txt_kp_prs_barat, #txt_kp_prs_timur").change(function(){
        var total_kp_prs = 0;
        total_kp_prs += Number($("#txt_kp_prs_barat").val().replace(/[^0-9\.]+/g, ""));
        total_kp_prs += Number($("#txt_kp_prs_timur").val().replace(/[^0-9\.]+/g, ""));
        $("#txt_total_kp_prs").val(Comma(total_kp_prs));
    });

    $("#txt_lain_lain").change(function () {
        var nominal_lain2 = Number($("#txt_lain_lain").val().replace(/[^0-9\.]+/g, ""));
        if(nominal_lain2 > 0){
            $("#txt_keterangan_lain_lain").attr("required","required");
        }else{
            $("#txt_keterangan_lain_lain").removeAttr("required");
        }
    });

    $("#Cb_is_kp_selamat_barat").change(function () {
        if ($("#Cb_laka_pengemudi1").is(':checked')) {
            alert("Laka Pengemudi 1, kps nonaktif");
            $("#Cb_laka_pengemudi1").prop("checked", false);
            CekLaka("Cb_laka_pengemudi1", "txt_keterangan_laka_pengemudi1");
        }
        if ($("#Cb_laka_pengemudi2").is(':checked')) {
            alert("Laka Pengemudi 2, kps nonaktif");
            $("#Cb_laka_pengemudi2").prop("checked", false);
            CekLaka("Cb_laka_pengemudi2", "txt_keterangan_laka_pengemudi2");
        }
        RefreshKPSEkstra();
        CheckKPSelamat();
        RefreshKPSUHT();
        HitungSemua();
    });

    $('#Cb_is_kp_selamat_timur').change(function () {

        if ($("#Cb_laka_pengemudi1").is(':checked')) {
            alert("Laka Pengemudi 1, kps nonaktif");
            $("#Cb_laka_pengemudi1").prop("checked", false);
            CekLaka("Cb_laka_pengemudi1", "txt_keterangan_laka_pengemudi1");
        }
        if ($("#Cb_laka_pengemudi2").is(':checked')) {
            alert("Laka Pengemudi 2, kps nonaktif");
            $("#Cb_laka_pengemudi2").prop("checked", false);
            CekLaka("Cb_laka_pengemudi2", "txt_keterangan_laka_pengemudi2");
        }
        RefreshKPSEkstra();
        CheckKPSelamat();
        RefreshKPSUHT();
        HitungSemua();
    });

    $("#Cb_is_overblass_barat").change(function () {
        if ($("#Cb_is_overblass_barat").is(':checked')) {
            overblass_barat = Number($("#Txt_Hidden_OverBlass_Barat").val().replace(/[^0-9\.]+/g, ""));
            $("#txt_overblass_barat").val(number_format(overblass_barat));
            $("#txt_overblass_barat").attr("readonly", "readonly");
        }
        else {
            $("#txt_overblass_barat").val('0');
            $("#txt_overblass_barat").attr("readonly", "readonly");
        }
        HitungSemua();
    });

    $("#Cb_is_overblass_timur").change(function () {
        if ($("#Cb_is_overblass_timur").is(':checked')) {
            overblass_timur = Number($("#Txt_Hidden_OverBlass_Timur").val().replace(/[^0-9\.]+/g, ""))
            $("#txt_overblass_timur").val(number_format(overblass_timur));
             $("#txt_overblass_timur").attr("readonly", "readonly");
        }
        else {
            $("#txt_overblass_timur").val(0);
            $("#txt_overblass_timur").attr("readonly", "readonly");
        }
        HitungSemua();
    });

    $("#txt_tabungan").change(function () {
        HitungSemua();
    });

    $("#Cb_is_uht").change(function () {
        if ($("#Cb_laka_pengemudi1").is(':checked')) {
            alert("Laka Pengemudi 1, kps nonaktif");
            $("#Cb_laka_pengemudi1").prop("checked", false);
            CekLaka("Cb_laka_pengemudi1", "txt_keterangan_laka_pengemudi1");

        }
        if ($("#Cb_laka_pengemudi2").is(':checked')) {
            alert("Laka Pengemudi 2, kps nonaktif");
            $("#Cb_laka_pengemudi2").prop("checked", false);
            CekLaka("Cb_laka_pengemudi2", "txt_keterangan_laka_pengemudi2");

        }
        RefreshKPSEkstra();
        CheckKPSelamat();
        RefreshKPSUHT();
        HitungSemua();
    });

    $("#txt_terima_oper_barat_penumpang, #txt_terima_oper_barat_harga").change(function () {
        HitungSemua();
    });

    $("#txt_dioper_barat_penumpang,#txt_dioper_barat_harga").change(function () {
        var penumpangBarat = parseInt($("#Txt_Copy_JumlahPenumpangBarat").val());
        var dioper = parseInt($("#txt_dioper_barat_penumpang").val());
        var hargaKPSelamatBarat = $("#Txt_Copy_HargaKPSelamatBarat").val().replace(/[^0-9\.]+/g, "");
        var persenKPSelamatBarat = $("#TxtPersenKpSelamatBaratPengemudi1").val();
        var penumpangBaratNew = penumpangBarat - dioper;
        var totalKPSelamatBaratNew = ((penumpangBaratNew * hargaKPSelamatBarat)/100) * persenKPSelamatBarat;

        $("#TxtJumlahPenumpangBarat").val(penumpangBaratNew);
        $("#TxtTotalKPSelamatBarat").val(totalKPSelamatBaratNew);
        ChangeBarat("");
        HitungSemua();
    });

    $("#txt_terima_oper_timur_penumpang, #txt_terima_oper_timur_harga").change(function () {
        HitungSemua();
    });

    $("#txt_dioper_timur_penumpang, #txt_dioper_timur_harga").change(function () {
        var penumpangTimur = parseInt($("#Txt_Copy_JumlahPenumpangTimur").val());
        var dioper = parseInt($("#txt_dioper_timur_penumpang").val());
        var hargaKPSelamatTimur = $("#Txt_Copy_HargaKPSelamatTimur").val().replace(/[^0-9\.]+/g, "");
        var persenKPSelamatTimur = $("#TxtPersenKpSelamatTimurPengemudi1").val();
        var penumpangTimurNew = penumpangTimur - dioper;
        var totalKPSelamatTimurNew = ((penumpangTimurNew * hargaKPSelamatTimur)/100) * persenKPSelamatTimur;

        $("#TxtJumlahPenumpangTimur").val(penumpangTimurNew);
        $("#TxtTotalKPSelamatTimur").val(totalKPSelamatTimurNew);
        ChangeTimur("");
        HitungSemua();
    });

    $("#id_jenis_operan_barat").change(function () {
        var id_jenis_operan = $("#id_jenis_operan_barat").val();
        var harga_dioper_barat = parseInt($("#Txt_Copy_HargaKPSelamatBarat").val());
        var harga_terima_oper_barat = parseInt($("#Txt_Copy_HargaKPSelamatBarat").val());
        if(id_jenis_operan){
            $.post(
                ajaxUrl + "get_harga_operan",
                {id_jenis_operan : id_jenis_operan},
                function(resp){
                    harga_terima_oper_barat = parseInt(resp);
                    harga_dioper_barat = parseInt($("#Txt_Copy_HargaKPSelamatBarat").val()) - parseInt(resp);

                    $("#txt_terima_oper_barat_harga").val(Comma(harga_terima_oper_barat));
                    $("#txt_dioper_barat_harga").val(Comma(harga_dioper_barat));
                    HitungSemua();
                }
            );
        }else{
            $("#txt_terima_oper_barat_harga").val(Comma(harga_terima_oper_barat));
            $("#txt_dioper_barat_harga").val(Comma(harga_dioper_barat));
            HitungSemua();
        }
    });

    $("#id_jenis_operan_timur").change(function () {
        var id_jenis_operan = $("#id_jenis_operan_timur").val();
        var harga_dioper_timur = parseInt($("#Txt_Copy_HargaKPSelamatTimur").val());
        var harga_terima_oper_timur = parseInt($("#Txt_Copy_HargaKPSelamatTimur").val());
        if(id_jenis_operan){
            $.post(
                ajaxUrl + "get_harga_operan",
                {id_jenis_operan : id_jenis_operan},
                function(resp){
                    harga_terima_oper_timur = parseInt(resp);
                    harga_dioper_timur = parseInt($("#Txt_Copy_HargaKPSelamatTimur").val()) - parseInt(resp);

                    $("#txt_terima_oper_timur_harga").val(Comma(harga_terima_oper_timur));
                    $("#txt_dioper_timur_harga").val(Comma(harga_dioper_timur));
                    HitungSemua();
                }
            );
        }else{
            $("#txt_terima_oper_timur_harga").val(Comma(harga_terima_oper_timur));
            $("#txt_dioper_timur_harga").val(Comma(harga_dioper_timur));
            HitungSemua();
        }
    });

    $('#Cb_is_kernet_batangan').change(function () {
        if (this.checked) {
            var full_seat_barat = $("#txt_full_seat_barat").val().replace(/[^0-9\.]+/g, "");
            var full_seat_timur = $("#txt_full_seat_timur").val().replace(/[^0-9\.]+/g, "");
            var honor_kernet_batangan = full_seat_barat;
            if(full_seat_timur < honor_kernet_batangan){
                honor_kernet_batangan = full_seat_timur;
            }
            $('#txt_kernet_batangan').val(number_format(honor_kernet_batangan));
            $('#txt_kernet_batangan').removeAttr("readonly");
        } else {
            $('#txt_kernet_batangan').attr("readonly", "readonly");
            $('#txt_kernet_batangan').val(0);
        }
        HitungSemua();
    });

    $('#Cb_is_tambahan_asal_barat').change(function () {
        if (this.checked) {
            var id_spj = $("#id_spj").val();
            $.post(
                ajaxUrl + "get_tambahan",
                {id_spj : id_spj, type:"asal_barat"},
                function(resp){
                    $('#txt_tambahan_asal_barat').val(number_format(resp));
                    $('#txt_tambahan_asal_barat').removeAttr("readonly");
                }
            ).fail(function () {
                
            });
        } else {
            $('#txt_tambahan_asal_barat').attr("readonly", "readonly");
            $('#txt_tambahan_asal_barat').val(0);
            HitungSemua();
        }
    });

    $('#Cb_is_tambahan_tujuan_barat').change(function () {
        if (this.checked) {
            var id_spj = $("#id_spj").val();
            $.post(
                ajaxUrl + "get_tambahan",
                {id_spj : id_spj, type:"tujuan_barat"},
                function(resp){
                    $('#txt_tambahan_tujuan_barat').val(number_format(resp));
                    $('#txt_tambahan_tujuan_barat').removeAttr("readonly");
                }
            ).fail(function () {

            });
        } else {
            $('#txt_tambahan_tujuan_barat').attr("readonly", "readonly");
            $('#txt_tambahan_tujuan_barat').val(0);
            HitungSemua();
        }
    });

    $('#Cb_is_tambahan_asal_timur').change(function () {
        if (this.checked) {
            var id_spj = $("#id_spj").val();
            $.post(
                ajaxUrl + "get_tambahan",
                {id_spj : id_spj, type:"asal_timur"},
                function(resp){
                    $('#txt_tambahan_asal_timur').val(number_format(resp));
                    $('#txt_tambahan_asal_timur').removeAttr("readonly");
                }
            ).fail(function () {

            });
        } else {
            $('#txt_tambahan_asal_timur').attr("readonly", "readonly");
            $('#txt_tambahan_asal_timur').val(0);
            HitungSemua();
        }
    });

    $('#Cb_is_tambahan_tujuan_timur').change(function () {
        if (this.checked) {
            var id_spj = $("#id_spj").val();
            $.post(
                ajaxUrl + "get_tambahan",
                {id_spj : id_spj, type:"tujuan_timur"},
                function(resp){
                    $('#txt_tambahan_tujuan_timur').val(number_format(resp));
                    $('#txt_tambahan_tujuan_timur').removeAttr("readonly");
                }
            ).fail(function () {

            });
        } else {
            $('#txt_tambahan_tujuan_timur').attr("readonly", "readonly");
            $('#txt_tambahan_tujuan_timur').val(0);
            HitungSemua();
        }
    });

    /*$('#Cb_is_snack').change(function () {
        if (this.checked) {
            var biayaSnack = $("#setting_biayasnack").val();
            $('#txt_snack').val(number_format(biayaSnack));
            $('#txt_snack').removeAttr("readonly");
            HitungSemua();
        } else {
            $('#txt_snack').add("readonly", "readonly");
            $('#txt_snack').val(0);
            HitungSemua();
        }
    });*/

    $('#id_pangkalan_strook_4').change(function () {
        $.post(
            ajaxUrl + "get_strook_4",
            {"id_pangkalan": $('#id_pangkalan_strook_4').val()},
            function(resp){
                $('#txt_tambahan_strook_4').val(number_format(resp));
                HitungSemua();
            }, "json"
        )
            .fail(function () {
                $('#txt_tambahan_strook_4').val(0);
                HitungSemua();
            });
    });

    $('#Cb_is_tambahan_interval').change(function () {
        if (this.checked) {
            var is_pengemudi_2 = $("#Cb_is_pengemudi2").is(":checked");
            var tambahan_interval = parseInt($("#setting_tambahaninterval1pengemudi").val());
            if(is_pengemudi_2){
                tambahan_interval += parseInt($("#setting_tambahaninterval2pengemudi").val());
            }
            $('#txt_tambahan_interval').val(number_format(tambahan_interval));
            $('#txt_tambahan_interval').removeAttr("readonly");
        } else {
            $('#txt_tambahan_interval').attr("readonly", "readonly");
            $('#txt_tambahan_interval').val(0);
        }
        HitungSemua();
    });
});

function CekLaka(checkbox, textbox) {
    if ($("#" + checkbox).is(":checked")) {
        $('#' + textbox).removeAttr("readonly");
    }
    else {
        $('#' + textbox).val("");
        $('#' + textbox).attr("readonly", "readonly");
    }
}

function ChangeDuit(Num) {
    Num = Number(Num.replace(/[^0-9\.]+/g, ""));
    Num += '';

    Num = Num.replace(/,/g, '');

    x = Num.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1))
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    HitungSemua();
    return x1 + x2;
}

function RefreshKPSEkstra() {
    var totalpnpkpsekstrabarat = Number($("#Txt_Penumpang_KPS_Barat_Ekstra").val().replace(/[^0-9\.]+/g, ""));
    var totalpnpkpsekstratimur = Number($("#Txt_Penumpang_KPS_Timur_Ekstra").val().replace(/[^0-9\.]+/g, ""));


    if ($('#Cb_is_kps_ekstra_pengemudi').is(':checked')) {
        var hargakpsekstrabarat = Number($("#TxtHargaKPSelamatBarat").val().replace(/[^0-9\.]+/g, ""));
        var hargakpsekstratimur = Number($("#TxtHargaKPSelamatTimur").val().replace(/[^0-9\.]+/g, ""));
        var totalkpsselamatbarat = totalpnpkpsekstrabarat * hargakpsekstrabarat;
        var totalkpsselamattimur = totalpnpkpsekstratimur * hargakpsekstratimur;
        var transitan = 0;
        transitan = (Number($("#txt_terima_oper_barat_harga").val().replace(/[^0-9\.]+/g, "")) * Number($("#txt_terima_oper_barat_penumpang").val().replace(/[^0-9\.]+/g, "")));
        transitan += (Number($("#txt_dioper_barat_harga").val().replace(/[^0-9\.]+/g, "")) * Number($("#txt_dioper_barat_penumpang").val().replace(/[^0-9\.]+/g, "")));
        transitan += (Number($("#txt_terima_oper_timur_harga").val().replace(/[^0-9\.]+/g, "")) * Number($("#txt_terima_oper_timur_penumpang").val().replace(/[^0-9\.]+/g, "")));
        transitan += (Number($("#txt_dioper_timur_harga").val().replace(/[^0-9\.]+/g, "")) * Number($("#txt_dioper_timur_penumpang").val().replace(/[^0-9\.]+/g, "")));
        transitan = Math.round((transitan - 1) / 1000) * 1000;

        $('#txt_kps_ekstra_pengemudi1').val(number_format((totalkpsselamatbarat + totalkpsselamattimur + transitan) / 2));
    }else{
        $('#txt_kps_ekstra_pengemudi1').val("0");
    }

}
function RefreshKPSUHT() {
    var penumpangbarat = Number($("#TxtJumlahPenumpangBarat").val());
    var penumpangtimur = Number($("#TxtJumlahPenumpangTimur").val());
    var perkepalabarat = Number($("#Txt_Perkepala_Barat").val().replace(/[^0-9\.]+/g, ""));
    var perkepalatimur = Number($("#Txt_Perkepala_Timur").val().replace(/[^0-9\.]+/g, ""));
    if ($('#Cb_is_uht').is(':checked')) {
        $('#txt_uht').val(number_format((penumpangbarat * perkepalabarat) + (penumpangtimur * perkepalatimur)));
    }
    else {
        $('#txt_uht').val("0");
    }
}


function ChangeBarat(Num) {
    Num = Number(Num.replace(/[^0-9\.]+/g, ""));
    Num += '';

    Num = Num.replace(/,/g, '');

    x = Num.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1))
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    var banyakpenumpangbarat = $("#TxtJumlahPenumpangBarat").val();
    var kphargaselamatbarat = $("#TxtHargaKPSelamatBarat").val();
    var kppersenselamatbaratpengemudi1 = $("#TxtPersenKpSelamatBaratPengemudi1").val();
    var kppersenselamatbaratpengemudi2 = $("#TxtPersenKpSelamatBaratPengemudi2").val();
    var totalkpselamatbarat = banyakpenumpangbarat * Number(kphargaselamatbarat.replace(/[^0-9\.]+/g, ""));
    var kpselamatbaratpengemudi1 = totalkpselamatbarat * kppersenselamatbaratpengemudi1 / 100;
    var kpselamatbaratpengemudi2 = totalkpselamatbarat * kppersenselamatbaratpengemudi2 / 100;
    var kpselamattimurpengemudi1 = Number($("#TxtKPSelamatTimurPengemudi1").val().replace(/[^0-9\.]+/g, ""));
    var kpselamattimurpengemudi2 = Number($("#TxtKPSelamatTimurPengemudi2").val().replace(/[^0-9\.]+/g, ""));
    $("#TxtTotalKPSelamatBarat").val(number_format(totalkpselamatbarat));
    $("#TxtKPSelamatBaratPengemudi1").val(number_format(kpselamatbaratpengemudi1));
    $("#TxtKPSelamatBaratPengemudi2").val(number_format(kpselamatbaratpengemudi2));
    $("#TxtGrandTotalPengemudi1").val(number_format(kpselamatbaratpengemudi1 + kpselamattimurpengemudi1));
    $("#TxtGrandTotalPengemudi2").val(number_format(kpselamatbaratpengemudi2 + kpselamattimurpengemudi2));
    RefreshKPSEkstra();
    RefreshKPSUHT();
    HitungSemua();
    return x1 + x2;
}

function ChangeTimur(Num) {
    Num = Number(Num.replace(/[^0-9\.]+/g, ""));
    Num += '';

    Num = Num.replace(/,/g, '');

    x = Num.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1))
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    var banyakpenumpangtimur = $("#TxtJumlahPenumpangTimur").val();
    var kphargaselamattimur = $("#TxtHargaKPSelamatTimur").val();
    var kppersenselamattimurpengemudi1 = $("#TxtPersenKpSelamatTimurPengemudi1").val();
    var kppersenselamattimurpengemudi2 = $("#TxtPersenKpSelamatTimurPengemudi2").val();
    var totalkpselamattimur = banyakpenumpangtimur * Number(kphargaselamattimur.replace(/[^0-9\.]+/g, ""));
    var kpselamattimurpengemudi1 = totalkpselamattimur * kppersenselamattimurpengemudi1 / 100;
    var kpselamattimurpengemudi2 = totalkpselamattimur * kppersenselamattimurpengemudi2 / 100;
    var kpselamatbaratpengemudi1 = Number($("#TxtKPSelamatBaratPengemudi1").val().replace(/[^0-9\.]+/g, ""));
    var kpselamatbaratpengemudi2 = Number($("#TxtKPSelamatBaratPengemudi2").val().replace(/[^0-9\.]+/g, ""));
    $("#TxtTotalKPSelamatTimur").val(number_format(totalkpselamattimur));
    $("#TxtKPSelamatTimurPengemudi1").val(number_format(kpselamattimurpengemudi1));
    $("#TxtKPSelamatTimurPengemudi2").val(number_format(kpselamattimurpengemudi2));
    $("#TxtGrandTotalPengemudi1").val(number_format(kpselamatbaratpengemudi1 + kpselamattimurpengemudi1));
    $("#TxtGrandTotalPengemudi2").val(number_format(kpselamatbaratpengemudi2 + kpselamattimurpengemudi2));
    HitungSemua();
    RefreshKPSEkstra();
    RefreshKPSUHT();
    return x1 + x2;
}
function CheckKPSelamat() {
    HitungSemua();
}
function Checkpengemudi() {
    if ($("#Cb_is_pengemudi2").is(':checked')) {
        $("#TxtPersenKpSelamatBaratPengemudi2").val($("#setting_persentasepengemudi2").val());
        $("#TxtPersenKpSelamatTimurPengemudi2").val($("#setting_persentasepengemudi2").val());
        $('#TxtPersenKpSelamatBaratPengemudi2').attr('readonly', false);
        $('#TxtPersenKpSelamatTimurPengemudi2').attr('readonly', false);
        $('#field-pengemudi-2').show();
    } else {
        $("#TxtPersenKpSelamatBaratPengemudi2").val('0');
        $("#TxtPersenKpSelamatTimurPengemudi2").val('0');
        $('#TxtPersenKpSelamatBaratPengemudi2').attr('readonly', true);
        $('#TxtPersenKpSelamatTimurPengemudi2').attr('readonly', true);
        $('#field-pengemudi-2').hide();
    }
    ChangeBarat('');
    ChangeTimur('');
}

function HitungSemua() {
    var kpselamattimurpengemudi1 = Math.round((Number($("#TxtKPSelamatTimurPengemudi1").val().replace(/[^0-9\.]+/g, "")) - 1) / 1000) * 1000;
    var kpselamattimurpengemudi2 = Math.round((Number($("#TxtKPSelamatTimurPengemudi2").val().replace(/[^0-9\.]+/g, "")) - 1) / 1000) * 1000;
    var kpselamatbaratpengemudi1 = Math.round((Number($("#TxtKPSelamatBaratPengemudi1").val().replace(/[^0-9\.]+/g, "")) - 1) / 1000) * 1000;
    var kpselamatbaratpengemudi2 = Math.round((Number($("#TxtKPSelamatBaratPengemudi2").val().replace(/[^0-9\.]+/g, "")) - 1) / 1000) * 1000;
    var total_biaya_crew = 0;
    var total_setoran = 0;
    var isbagi2 = $("#Cb_is_pengemudi2").is(":checked");
    if ($("#Cb_laka_pengemudi2").is(':checked')) {
        kpselamattimurpengemudi2 = 0;
        kpselamatbaratpengemudi2 = 0;
    }
    if ($("#Cb_laka_pengemudi1").is(':checked')) {
        kpselamattimurpengemudi1 = 0;
        kpselamatbaratpengemudi1 = 0;
    }

    overblass_barat = 0;
    if ($("#Cb_is_overblass_barat").is(':checked')) {
        overblass_barat = Number($("#txt_overblass_barat").val().replace(/[^0-9\.]+/g, ""));
        total_biaya_crew += overblass_barat;
    }

    $("#txt_overblass_barat").val(number_format(overblass_barat));

    overblass_timur = 0;
    if ($("#Cb_is_overblass_timur").is(':checked')) {
        overblass_timur = Number($("#txt_overblass_timur").val().replace(/[^0-9\.]+/g, ""));
        total_biaya_crew += overblass_timur;
    }
    $("#txt_overblass_timur").val(number_format(overblass_timur));

    total_biaya_crew += Number($("#txt_honor_pengemudi").val().replace(/[^0-9\.]+/g, ""));

    if (isbagi2) {
        // asalnya dibagi 2
        var tabungan1 = Number($("#Txt_Copy_Tabungan").val().replace(/[^0-9\.]+/g, ""));
        var tabungan2 = tabungan1;
        if ($("#Cb_laka_pengemudi1").is(':checked')) {
            tabungan1 = 0;
        }
        if ($("#Cb_laka_pengemudi2").is(':checked')) {
            tabungan2 = 0;
        }
        tabungan = tabungan1 + tabungan2;

        total_biaya_crew += Number($("#txt_honor_pengemudi_2").val().replace(/[^0-9\.]+/g, ""));
    }
    else {
        if ($("#Cb_laka_pengemudi1").is(':checked')) {
            tabungan = 0;
        }else {
            tabungan = Number($("#Txt_Copy_Tabungan").val().replace(/[^0-9\.]+/g, ""));
        }
    }

    if ($("#Cb_is_kp_selamat_barat").is(':checked')) {
        $("#Txt_Copy_Selamat_Barat_Pengemudi1").val(kpselamatbaratpengemudi1);
        $("#Txt_Copy_Selamat_Barat_Pengemudi2").val(kpselamatbaratpengemudi2);
        $("#txt_kp_selamat_barat_detail").val(number_format(kpselamatbaratpengemudi1 + kpselamatbaratpengemudi2));
    }
    else {
        $("#Txt_Copy_Selamat_Barat_Pengemudi1").val("0");
        $("#Txt_Copy_Selamat_Barat_Pengemudi2").val("0");
        $("#txt_kp_selamat_barat_detail").val("0");
    }

    if ($("#Cb_is_kp_selamat_timur").is(':checked')) {
        $("#Txt_Copy_Selamat_Timur_Pengemudi1").val(kpselamattimurpengemudi1);
        $("#Txt_Copy_Selamat_Timur_Pengemudi2").val(kpselamattimurpengemudi2);
        $("#txt_kp_selamat_timur_detail").val(number_format(kpselamattimurpengemudi1 + kpselamattimurpengemudi2));
    }
    else {
        $("#Txt_Copy_Selamat_Timur_Pengemudi1").val("0");
        $("#Txt_Copy_Selamat_Timur_Pengemudi2").val("0");
        $("#txt_kp_selamat_timur_detail").val("0");
    }

    total_biaya_crew += Number($("#txt_honor_kernet").val().replace(/[^0-9\.]+/g, ""));
    if ($("#Cb_is_kernet_batangan").is(':checked')) {
        total_biaya_crew += Number($("#txt_kernet_batangan").val().replace(/[^0-9\.]+/g, ""));
    }

    total_biaya_crew += Number($("#txt_uang_makan").val().replace(/[^0-9\.]+/g, ""));
    total_biaya_crew += Number($("#txt_uang_maintenance").val().replace(/[^0-9\.]+/g, ""));
    total_biaya_crew += Number($("#txt_tambahan_interval").val().replace(/[^0-9\.]+/g, ""));
    total_biaya_crew += Number($("#txt_parkir").val().replace(/[^0-9\.]+/g, ""));
    total_biaya_crew += Number($("#txt_cuci").val().replace(/[^0-9\.]+/g, ""));
    total_biaya_crew += Number($("#txt_subsidi_toilet").val().replace(/[^0-9\.]+/g, ""));
    total_biaya_crew += Number($("#txt_full_seat_barat").val().replace(/[^0-9\.]+/g, ""));
    total_biaya_crew += Number($("#txt_full_seat_timur").val().replace(/[^0-9\.]+/g, ""));

    if ($("#Cb_is_overblass_barat").is(':checked')) {
        total_biaya_crew += Number($("#txt_overblass_barat").val().replace(/[^0-9\.]+/g, ""));
    }

    if ($("#Cb_is_overblass_timur").is(':checked')) {
        total_biaya_crew += Number($("#txt_overblass_timur").val().replace(/[^0-9\.]+/g, ""));
    }

    total_biaya_crew += Number($("#txt_lain_lain").val().replace(/[^0-9\.]+/g, ""));

    if ($("#Cb_is_kp_selamat_barat").is(':checked')) {
        total_biaya_crew += Number($("#txt_kp_selamat_barat_detail").val().replace(/[^0-9\.]+/g, ""));
    }

    if ($("#Cb_is_kp_selamat_timur").is(':checked')) {
        total_biaya_crew += Number($("#txt_kp_selamat_timur_detail").val().replace(/[^0-9\.]+/g, ""));
    }

    total_biaya_crew += Number($("#txt_kp_prs_barat").val().replace(/[^0-9\.]+/g, ""));
    total_biaya_crew += Number($("#txt_kp_prs_timur").val().replace(/[^0-9\.]+/g, ""));

    var transitan = 0;
    transitan += (Number($("#txt_terima_oper_barat_harga").val().replace(/[^0-9\.]+/g, "")) * Number($("#txt_terima_oper_barat_penumpang").val().replace(/[^0-9\.]+/g, "")));
    transitan += (Number($("#txt_dioper_barat_harga").val().replace(/[^0-9\.]+/g, "")) * Number($("#txt_dioper_barat_penumpang").val().replace(/[^0-9\.]+/g, "")));
    transitan += (Number($("#txt_terima_oper_timur_harga").val().replace(/[^0-9\.]+/g, "")) * Number($("#txt_terima_oper_timur_penumpang").val().replace(/[^0-9\.]+/g, "")));
    transitan += (Number($("#txt_dioper_timur_harga").val().replace(/[^0-9\.]+/g, "")) * Number($("#txt_dioper_timur_penumpang").val().replace(/[^0-9\.]+/g, "")));
    transitan = Math.round((transitan - 1) / 1000) * 1000;

    total_biaya_crew += transitan;
    total_biaya_crew = total_biaya_crew * -1;

    total_setoran += total_biaya_crew;
    total_setoran -= Number($("#txt_uang_tol").val().replace(/[^0-9\.]+/g, ""));
    total_setoran += tabungan;

    if(total_biaya_crew < 0){
        $("#txt_total_biaya_crew_display").val("("+number_format(Math.abs(total_biaya_crew))+")");
        $("#txt_total_biaya_crew_display").css("color", "red");

        $("#txt_total_biaya_crew").val(total_biaya_crew);
    }else{
        $("#txt_total_biaya_crew_display").val(0);
        $("#txt_total_biaya_crew_display").css("color", "#555");

        $("#txt_total_biaya_crew").val(total_biaya_crew);
    }

    if(total_setoran < 0){
        $("#txt_total_display").val("("+number_format(Math.abs(total_setoran))+")");
        $("#txt_total_display").css("color", "red");

        $("#txt_total").val(total_setoran);
    }else{
        $("#txt_total_display").val(0);
        $("#txt_total_display").css("color", "#555");

        $("#txt_total").val(total_setoran);
    }
    $("#txt_transitan").val(number_format(transitan));
    $("#txt_tabungan").val(number_format(tabungan));
}