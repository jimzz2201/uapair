$(document).ready(function () {
    if($(".select2user").length)
        $(".select2user").select2({data:list_user});

    if($(".select2pangkalan").length)
        $(".select2pangkalan").select2({data:list_pangkalan});

    if($(".select2pengurus").length)
        $(".select2pengurus").select2({data:list_pengurus});

    if($(".select2kelas").length)
        $(".select2kelas").select2({data:list_kelas});

    if($(".select2shift").length)
        $(".select2shift").select2({data:list_shift});

    if($(".select2tipebus").length)
        $(".select2tipebus").select2({data:list_tipebus});

    $(".datepicker").datepicker({
        autoclose: true,
        format: 'dd M yyyy',
        weekStart: 1,
        language: "id",
        todayHighlight: true,
    });

    $("form#frm_search").submit(function (e) {
        if($("#mytable").length){
            table.fnDraw(false);
        }else{
            $("#report-view").empty().html('Loading Data...');
            $.post(
                baseurl + 'index.php/report/'+ report +'/view',
                {search : $("#frm_search").serialize()},
                function(data){
                    $("#report-view").empty().html(data);
                }
            );
        }
        return false;
    });

    $("#btn-excel").click(function () {
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/'+ report +'/export';
        $("#frm_extra #extra").val(search);
        $("#frm_extra").attr('action', url);
        $("#frm_extra").submit();
    });

    $("#newEx").click(function () {
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/'+ report +'/Newexport';
        $("#frm_extra #extra").val(search);
        $("#frm_extra").attr('action', url);
        $("#frm_extra").submit();
    });

    $("#btn-excel-detail").click(function () {
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/'+ report +'/export_detail';
        $("#frm_extra #extra").val(search);
        $("#frm_extra").attr('action', url);
        $("#frm_extra").submit();
    });

    $("#btn-print").click(function () {
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/'+ report +'/print_report';
        $("#frm_extra input#extra").val(search);
        $("#frm_extra").attr('action', url);
        $("#frm_extra").submit();
    });

    $("#btt_search_supir").click(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/supir/supir_search',
            success: function (data) {
                modalbootstrap(data, "Data Supir", "90%");
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        })
    });

    $("#txt_nic_supir").focusout(function (e) {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/supir/GetOneSupir',
            dataType: 'json',
            data: {
                keyword: $("#txt_nic_supir").val(),
                type: "nic"
            },
            success: function (data) {
                if ($("#txt_nic_supir").val() != "") {
                    if (data.st == true) {
                        $("#txt_warning_supir").html("");
                        $("#txt_id_supir").val(data.obj.id_supir);
                        $("#txt_nama_supir").val(data.obj.nama);

                        $("#txt_warning_supir").css("display", "none");

                    }
                    else {
                        $("#txt_warning_supir").html('<i class="fa fa-warning"></i> ' + data.msg);
                        $("#txt_nic_supir").focus();
                        $("#txt_nama_supir").val("");
                        $("#txt_id_supir").val(0);
                        $("#txt_warning_supir").css("display", "block");
                    }
                }
                else {
                    $("#txt_id_supir").val(0);
                    $("#txt_nic_supir").html("");
                    $("#txt_nama_supir").val("");
                    $("#txt_warning_supir").css("display", "none");
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        })
    });

    $("#btt_search_kernet").click(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/kernet/kernet_search',
            success: function (data) {
                modalbootstrap(data, "Data Kernet", "90%");
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        })
    });

    $("#txt_nic_kernet").focusout(function (e) {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/kernet/GetOneKernet',
            dataType: 'json',
            data: {
                keyword: $("#txt_nic_kernet").val(),
                type: "nic"
            },
            success: function (data) {
                if ($("#txt_nic_kernet").val() != "") {
                    if (data.st == true) {
                        $("#txt_warning_kernet").html("");
                        $("#txt_id_kernet").val(data.obj.id_kernet);
                        $("#txt_nama_kernet").val(data.obj.nama);

                        $("#txt_warning_kernet").css("display", "none");

                    }
                    else {
                        $("#txt_warning_supir").html('<i class="fa fa-warning"></i> ' + data.msg);
                        $("#txt_nic_supir").focus();
                        $("#txt_nama_supir").val("");
                        $("#txt_id_supir").val(0);
                        $("#txt_warning_supir").css("display", "block");
                    }
                }
                else {
                    $("#txt_id_kernet").val(0);
                    $("#txt_nic_kernet").html("");
                    $("#txt_nama_kernet").val("");
                    $("#txt_warning_kernet").css("display", "none");
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        })
    });


    if($("#mytable").length && typeof url !== "undefined" && typeof columns !== "undefined"){
        if(typeof pageLength !== "undefined"){
            initTable(url, columns, pageLength);
        }else{
            initTable(url, columns, 25);
        }
    }

});

function initTable(url, columns, pageLength = 25){
    var orderDT = [];
    var sDomDT = 'ltrip';
    if(typeof order !== "undefined"){
        orderDT = order;
    }
    if(typeof sDom !== "undefined"){
        sDomDT = sDom;
    }
    table = $("#mytable").dataTable({
        initComplete: function () {
            var api = this.api();
            $('#mytable_filter input')
                .off('.DT')
                .on('keyup.DT', function (e) {
                    if (e.keyCode == 13) {
                        api.search(this.value).draw();
                    }
                });
        },
        oLanguage: {
            sProcessing: "loading..."
        },
        processing: true,
        pageLength: pageLength,
        sDom:sDomDT,
        serverSide: true,
        ajax: {"url": url, "type": "POST",
            "data": function (d) {
                return $.extend({}, d, {
                    "extra_search": $("form#frm_search").serialize()
                });
            }},
        columns: columns,
        order: orderDT,
        fnDrawCallback: function(oSettings){
            if(typeof initCompVar !== "undefined" && initCompVar.length > 0){
                console.log(initCompVar);
                $.post(
                    baseurl + 'index.php/report/'+ report +'/'+initCompUrl,
                    {id_spj: initCompVar},
                    function(resp){
                        if(resp.st){
                        
                            for (var spj in resp.total) {
                                if (resp.total.hasOwnProperty(spj)) {
                                    total = number_format(resp.total[spj], 0, ',', '.');
                                    $("#row_"+spj+" td").last().html(total);
                                    $("#row_"+spj+" td").eq(-2).html( number_format(resp.strook_pt[spj], 0, ',', '.'));
                                    $("#row_"+spj+" td").eq(-3).html( number_format(resp.strook_3[spj], 0, ',', '.'));
                                    $("#row_"+spj+" td").eq(-4).html( number_format(resp.bo[spj], 0, ',', '.'));
                                    $("#row_"+spj+" td").eq(-5).html( number_format(resp.titipan[spj], 0, ',', '.'));
                                    $("#row_"+spj+" td").eq(-6).html( number_format(resp.terminal[spj], 0, ',', '.'));
                                    $("#row_"+spj+" td").eq(-7).html( number_format(resp.penjualan_timur[spj], 0, ',', '.'));
                                    $("#row_"+spj+" td").eq(-8).html( number_format(resp.penjualan_barat[spj], 0, ',', '.'));
                                }
                            }
                        }
                    }, 'json'
                );
                initCompVar = [];
            }
            if(typeof afterLoad !== "undefined"){
                afterLoad();
            }
        },
        rowCallback: function (row, data, iDisplayIndex) {
            if(typeof counterRow !== "undefined" && counterRow){
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        }

    });
}


function changeTipeTanggal(){
    var tipe = $("#tipe_tanggal").val();
    if(tipe == 1){
        $("div#field-tanggal").show();
        $("div#field-bulan").hide();
    }else if(tipe == 2){
        $("div#field-tanggal").hide();
        $("div#field-bulan").show();
    }else{
        $("div#field-tanggal").hide();
        $("div#field-bulan").hide();
    }
}