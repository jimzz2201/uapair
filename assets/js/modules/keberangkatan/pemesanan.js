var table;
var maketReloader;
var changeSeat = null;
var newSeat = null;
var kursi = [];
var urlPemesanan = baseurl + 'index.php/keberangkatan/pemesanan/';
var urlBerangkat = baseurl + 'index.php/keberangkatan/';
var list_tujuan = [];
var pake_promo = false;
var id_promo = null;
var limitKursi = 2;
var isTambahan = false;

$(document).ready(function () {
    $("#id_pangkalan_asal, #id_pangkalan_tujuan").select2({data: list_pangkalan});
    // $('.minimal-red').iCheck({
    //     checkboxClass: 'icheckbox_minimal-red',
    //     radioClass: 'iradio_minimal-red'
    // });

    $("form#form_search").submit(function (e) {
        resetLah();
        $("#smartwizard ul.step-anchor li:gt(1)").removeClass("done");
        table.api().columns.adjust();
        LoadBar.show();
        table.fnDraw(true);
        return false;
    });

    $("#id_pangkalan_tujuan").change(function () {
        $("#kode_promo").val('');
        $("#promo-notif").empty();
        pake_promo = false;
        id_promo = null;
    });

    $("#harga_tiket").change(function () {
        $("#kode_promo").val('');
        $("#promo-notif").empty();
        pake_promo = false;
        id_promo = null;
    });

    $("#search_id_kelas").change(function () {
        var id_kelas = $("#search_id_kelas").val();
        console.log(id_kelas);
        if (id_kelas) {
            $("#mytable tbody tr").each(function () {
                if ($(this).hasClass("kelas_" + id_kelas)) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        } else {
            $("#mytable tbody tr").show();
        }
    });

    $("#id_pangkalan_asal, #id_pangkalan_tujuan").on("select2:select", function (e) {
        setHarga();
    });

    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    table = $("#mytable").dataTable({
        oLanguage: {
            sProcessing: "loading..."
        },
        paginate: false,
        sDom: 't',
        processing: true,
        serverSide: true,
        ajax: {"url": urlPemesanan + "getdatakeberangkatan", "type": "POST",
            "data": function (d) {
                return $.extend({}, d, {
                    "extra_search": $("form#form_search").serialize()
                });
            }},
        // deferRender:    true,
        // scrollY:        400,
        // scrollCollapse: true,
        // scroller:       true,
        columns: [
            // {data: "kode_berangkat", orderable: false, title: "Kode Berangkat"}
            {
                data: "action",
                orderable: false,
                className: "normal-padding",
                width: "50px",
            }
            // , {data: "asal", orderable: false, title: "Asal", className:"normal-padding"}
            // , {data: "tujuan", orderable: false, title: "Tujuan"}
            , {data: "jurusan", orderable: false, title: "Jurusan", className: "normal-padding"}
            , {data: "terjadwal", orderable: false, title: " ", className: "normal-padding text-center", width: "20px",
                mRender: function (data, type, row) {
                    var terjadwal = "N";
                    if (data == "1") {
                        terjadwal = "S"
                    }
                    return terjadwal;
                }
            }
            , {
                data: "tanggal", orderable: false, title: "Tgl Brkt", className: "normal-padding", width: "70px",
                mRender: function (data, type, row) {
                    return DefaultDateFormat(data);
                }
            }
            , {data: "jam", orderable: false, title: "Jam Brkt", className: "normal-padding", width: "40px",
                mRender: function (data, type, row) {
                    var jam = '';
                    if (data.substr(0, 5) != "00:00") {
                        var value = data.split(":");
                        jam = value[0] + ":" + value[1];
                    }
                    return jam;
                }
            }
            , {data: "no_body", orderable: false, title: "No Body", className: "normal-padding", width: "100px"}
            , {data: "p_terakhir", orderable: false, title: "Lokasi Terakhir", className: "normal-padding", width: "80px"}
            , {data: "kode_kelas", orderable: false, title: "Kelas", className: "normal-padding text-center", width: "50px"}
            , {data: "sisa_kursi", orderable: false, title: "Sisa", className: "normal-padding text-center", width: "40px"}
            , {data: "naik", orderable: false, title: "Naik", className: "normal-padding", width: "100px"}
            , {
                data: "tanggal_naik", orderable: false, title: "Tgl Naik", className: "normal-padding", width: "70px",
                mRender: function (data, type, row) {
                    return DefaultDateFormat(data);
                }
            }
            , {data: "jam_naik", orderable: false, title: "Jam Naik", className: "normal-padding", width: "40px",
                mRender: function (data, type, row) {
                    var jam = '';
                    if (row['jam'].substr(0, 5) != "00:00") {
                        var value = data.split(":");
                        jam = value[0] + ":" + value[1];
                    }
                    return jam;
                }
            }
        ],
        order: [[11, 'asc']],
        // rowCallback: function(row, data, iDisplayIndex) {
        // var info = this.fnPagingInfo();
        // var page = info.iPage;
        // var length = info.iLength;
        // var index = page * length + (iDisplayIndex + 1);
        // index += '<input type="hidden" value="'+data.id_berangkat+'"/>';
        // $('td:eq(0)', row).html(index);
        // }
        rowCallback: function (row, data, iDisplayIndex) {
            $(row).addClass("kelas_" + data.id_kelas);
            // console.log(data);
        },
        drawCallback: function (settings) {
            $(document).ajaxComplete(function () {
                LoadBar.hide();
            });
        }

    });

    $(".btn-step-1").unbind('click').click(function () {
        $("#smartwizard").smartWizard("goToStep", 1);
    });

    $(".btn-step-2").unbind('click').click(function () {
        $("#smartwizard").smartWizard("goToStep", 2);
    });

    $(".btn-step-3").unbind('click').click(function () {
        reloadMaket();
        clearInterval(maketReloader);
        maketReloader = setInterval(reloadMaket, 10000);
        $("#smartwizard").smartWizard("goToStep", 3);
    });


    //redirect ke step 1 klo di refresh
    // if(ticketingLayout == 1){
    //     if($("#layout-maket").attr("data-berangkat") == "" || $("#layout-maket").attr("data-berangkat") == null){
    //         $("#smartwizard").smartWizard("goToStep", 1);
    //     }
    // }

    $("#tiketModal .btn-close").click(function () {
        reloadMaket();
        clearInterval(maketReloader);
        maketReloader = setInterval(reloadMaket, 10000);
    });

    $("#btn-promo").click(function () {
        var id_berangkat = $("#layout-maket").attr('data-berangkat');
        if ($("#promo-notif").html() == "")
        {
            setHarga(id_berangkat, true);
        } else
        {
            alert("Kode Promo sudah digunakan");
        }
    });

    $("#kode_promo").click(function () {
        $(this).select();
    });

    $("#agen").change(function () {
        var username_agen = $(this).val();
        if (username_agen) {
            $.post(
                    baseurl + 'index.php/user/checkUser',
                    {username: username_agen},
                    function (resp) {
                        if (resp.ada == true) {
                            $("#check-agen-notif").html('');
                        } else {
                            $("#check-agen-notif").html('<div class="warninglabel"><i class="fa fa-warning"></i> Agen tidak terdaftar di sistem!</div>');
                        }
                    }, 'json'
                    );
        } else {
            $("#check-agen-notif").html('');
        }
    });

});

function ubahFormPesan() {
    if (kursi.length > 1) {
        console.log($("#rombongan").is(':checked'));
        if ($("#rombongan").is(':checked')) {
            for (x = 2; x <= kursi.length; x++) {
                $("div#sub_pesan_form_" + x + " input").removeAttr('required');
                $("div#sub_pesan_form_" + x).hide();
            }
            $(".kursi_pesanan").hide();
            $(".kursi_pesanan_satuan").hide();
            $(".kursi_pesanan_semua").show();
        } else {
            for (x = 2; x <= kursi.length; x++) {
                $("div#sub_pesan_form_" + x + " input").attr('required', 'required');
                $("div#sub_pesan_form_" + x + " input.frm_pesan-turun").removeAttr('required');
                $("div#sub_pesan_form_" + x).show();
            }
            $(".kursi_pesanan").show();
            $(".kursi_pesanan_satuan").show();
            $(".kursi_pesanan_semua").hide();
        }
    }
    changeTotalPemesanan();
}

function deletekeberangkatan(id_berangkat) {
    swal({
        title: "Are you sure delete this data?",
        text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
    }).then((result) => {
        if (result.value)
        {
            $.ajax({
                type: 'POST',
                url: urlBerangkat + 'keberangkatan_delete',
                dataType: 'json',
                data: {
                    id_berangkat: id_berangkat
                },
                success: function (data) {
                    if (data.st) {
                        messagesuccess(data.msg);
                        table.fnDraw(false);
                    } else {
                        messageerror(data.msg);
                    }

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
        }
    });

}

function selectKeberangkatan(id_berangkat) {
    console.log("keberangkatan: " + id_berangkat);
    LoadBar.show();
    kursi = [];
    hapuskursi = [];
    $("#btn-delete_all").html("Start Delete");
    $("#btn-delete_all").removeClass("bg-black-active");
    isdelete = false;
    if (id_berangkat) {
        if (langsung == 1) {
            window.location.href = baseurl + "index.php/keberangkatan/pemesanan/step3/" + id_berangkat;
        } else {
            $.post(
                    urlPemesanan + 'get_keberangkatan',
                    {id_berangkat: id_berangkat},
                    function (data) {
                        list_tujuan = data.list_tujuan;
                        $("#id_pangkalan_asal").empty().select2({data: data.list_asal});
                        $("#id_pangkalan_tujuan").empty().select2({data: data.list_tujuan});
                        if ($("#search_pangkalan_asal").val() != "") {
                            $("#id_pangkalan_asal").val($("#search_pangkalan_asal").val()).trigger("change");
                        } else {
                            $("#id_pangkalan_asal").val(data.id_pangkalan_asal).trigger("change");
                        }
                        if ($("#search_pangkalan_tujuan").val() != "") {
                            $("#id_pangkalan_tujuan").val($("#search_pangkalan_tujuan").val()).trigger("change");
                        } else {
                            $("#id_pangkalan_tujuan").val(data.id_pangkalan_tujuan).trigger("change");
                        }
                        $("#harga_tiket").val(data.harga);
                        $("#id_kelas").val(data.id_kelas);
                        $("#tipe_bus").val(data.tipe_bus);
                        var teks = '<table>';
                        var teks = '<tr><td width="125">Keberangkatan</td><td>: ' + data.kode_berangkat + '</td></tr>';
                        if (data.no_body) {
                            teks += '<tr><td>No Body</td><td>: <span id="lbl_no_body">' + data.no_body + '</span></td></tr>';
                        }

                        $("#tmb_confirm").html('Pesan Tiket');
                        if (data.no_spj) {
                            teks += '<tr><td>No SPJ</td><td>: <span id="lbl_no_spj">' + data.no_spj + '</span></td></tr>';
                            $("#tmb_confirm").html('Beli Tiket');
                        }

                        $("#tmb_reservasi").hide();
                        $("#btn-pesan-tambahan").show();
                        if (data.id_tipe_bus != "undefined" && data.id_tipe_bus == 7) {
                            $("#tmb_reservasi").show();
                            $("#btn-pesan-tambahan").hide();
                        }

                        teks += '<tr><td colspan="2">' + data.pangkalan_asal + ' - ' + data.pangkalan_tujuan + '  [ ' + data.nama_kelas + ' ]</td></tr>';
                        teks += '<tr><td>' + data.tanggal + '</td>';
                        teks += '<td>';
                        if (data.jam != "00:00") {
                            teks += data.jam;
                        }
                        teks += '</td></tr>';
                        teks += '</table>';

                        $("#jadwal_berangkat").html(teks);
                        $("#btn-manifest, #btn-sp, #btn-bonggol , #btn-delete_all").show();

                        $("#btn-pesan, #btn-pesan-tambahan").show();
                        $("#btn-promo").show();
                        // if(data.status < 2 && data.available == 1){
                        //     $("#btn-pesan").show();
                        //     $("#btn-promo").show();
                        // }else{
                        //     $("#btn-pesan").hide();
                        //     $("#btn-promo").hide();
                        // }

                        $("#layout-maket").attr('data-berangkat', id_berangkat);
                        $("#layout-maket").load(urlPemesanan + "view_maket/" + data.id_berangkat + "/" + $("#id_pangkalan_asal").val(), function () {
                            $("#box-print-tiket").hide();
                            setHarga(id_berangkat);
                            clearInterval(maketReloader);
                            maketReloader = setInterval(reloadMaket, 10000);

                            if ($("#smartwizard").length) {
                                console.log(kemana);
                                if (kemana > 0) {
                                    $('#smartwizard').smartWizard("next");
                                }
                            }

                            LoadBar.hide();
                        });
                    }, "json"
                    );
        }

    }

}

function pesanKursi(el) {
    clearKursiCadangan();
    if (changeSeat == null) {
        if ($(el).hasClass('btn-selected')) {
            var idx = kursi.indexOf($(el).data('kursi'));
            if (idx > -1) {
                kursi.splice(idx, 1);
            }
            $(el).removeClass('btn-selected');
        } else {
            if (kursi.length < limitKursi) {
                $(el).addClass('btn-selected');
                kursi.push($(el).data('kursi'));
            }
        }
        // clearInterval(maketReloader);
        // if(kursi.length == 0){
        //     maketReloader = setInterval(reloadMaket, 10000);
        // }
    } else {
        newSeat = $(el).data('kursi');
        var id_berangkat = $("#layout-maket").attr("data-berangkat");
        if (confirm("Konfirmasi pindah ke Kursi No. " + newSeat + "?")) {
            $.post(
                    urlPemesanan + "change_seat",
                    {data: {id_berangkat: id_berangkat, id_pemesanan: changeSeat, no_kursi: newSeat}},
                    function (data) {
                        if (data.st) {
                            messagesuccess(data.msg);
                            reloadMaket();
                            clearInterval(maketReloader);
                            maketReloader = setInterval(reloadMaket, 10000);
                            printSomething(data.url, null, 'Print Tiket', true, false, 400);
                        } else {
                            modaldialogerror(data.msg);
                        }
                        changeSeat = newSeat = null;
                    }, "json"
                    );
        } else {
            changeSeat = newSeat = null;
            reloadMaket();
            maketReloader = setInterval(reloadMaket, 10000);
        }
    }
}

function clearKursiCadangan() {
    for (var k = 0; k <= kursi.length; k++) {
        if (kursi[k] == "xxx") {
            kursi.splice(k, 1);
        }
    }
    kursi.sort();
}

function detailPesanan(el) {
    var no_kursi = $(el).data('kursi');
    var id_pemesanan = $(el).data('pemesanan');
    if (isdelete == false)
    {
        $.post(
                urlPemesanan + 'detail_pemesanan/' + id_pemesanan,
                {no_kursi: no_kursi},
                function (data) {
                    if ($(window).width() > 800) {
                        modalbootstrap(data, "Detail Pemesanan", "40%");
                    } else {
                        modalbootstrap(data, "Detail Pemesanan", "95%");
                    }
                }
        );
    } else
    {
        if ($(el).hasClass('btn-danger') || $(el).hasClass('btn-info')) {
            if ($(el).hasClass('bg-black-active')) {

                $(el).removeClass('bg-black-active');
                var idx = hapuskursi.indexOf($(el).data('pemesanan'));
                if (idx > -1) {
                    hapuskursi.splice(idx, 1);
                }

            } else
            {
                $(el).addClass('bg-black-active');
                console.log(hapuskursi);
                hapuskursi.push($(el).data('pemesanan'));
            }

        } else {
            alert("Kursi yang diminta Belum diorder")
        }
    }
}

function deletePemesanan(id_pemesanan) {
    swal({
        title: "Anda yakin akan menghapus pemesanan ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya, hapus pemesanan ini!",
        closeOnConfirm: true
    }).then((result) => {
        if (result.value)
        {
            $.post(
                    urlPemesanan + 'pemesanan_delete',
                    {id_pemesanan: id_pemesanan},
                    function (data) {
                        closemodalboostrap();
                        if (data.st) {
                            messagesuccess(data.msg);
                            reloadMaket();
                            clearInterval(maketReloader);
                            maketReloader = setInterval(reloadMaket, 10000);
                        } else {
                            modaldialogerror(data.msg);
                        }
                    }, 'json'
                    );
        }
    });
}

function pesanTiket() {
    isTambahan = false;
    clearKursiCadangan();
    var id_berangkat = $("#layout-maket").data("berangkat");
    var id_berangkat = $("#layout-maket").attr("data-berangkat");
    var id_pangkalan_turun = $("#id_pangkalan_tujuan").val();
    var is_transit = $("#is_transit").is(":checked") ? 1 : 0;

    var harga = Number($("#harga_tiket").val().replace(/,/g, ''));
    $('#frm_pesan').trigger('reset');
    $('div.alert').remove();
    if (id_berangkat) {
        if (kursi.length) {
            if (harga != "" && harga != null && !isNaN(harga)) {
                if (harga > 500000 || harga < 10000) {
                    $("#harga_tiket").val(harga).focus().select();
                    alert("Harga tiket tidak valid!");
                } else {
                    clearInterval(maketReloader);
                    $.post(
                            urlPemesanan + 'form_pemesanan/normal',
                            {jml: kursi.length, kursi: kursi, id_berangkat: id_berangkat, id_pangkalan_turun: id_pangkalan_turun, is_transit: is_transit},
                            function (data) {
                                if (data) {
                                    $("form#frm_pesan").empty().html(data);
                                    if (ticketingLayout) {
                                        $('#smartwizard').smartWizard("next");
                                    } else {
                                        $("#tiketModal").modal('show');
                                    }

                                    if ($("#nihil-kursi").length) {
                                        reloadMaket();
                                        clearInterval(maketReloader);
                                        maketReloader = setInterval(reloadMaket, 10000);
                                    }

                                    $('html, body').animate({scrollTop: 0}, 'slow');
                                    changeTotalPemesanan();
                                }
                            }
                    );
                }
            } else {
                $("#harga_tiket").focus().select();
                alert("Harga belum ditentukan!");
            }
        } else {
            alert("Silahkan pilih kursi!");
        }
    } else {
        alert("Silahkan pilih keberangkatan terlebih dahulu!");
    }
}

function pesanTiketTambahan() {
    isTambahan = true;
    kursi = [];
    $(".btn-kursi").removeClass('btn-selected');
    var id_berangkat = $("#layout-maket").attr("data-berangkat");
    var id_pangkalan_turun = $("#id_pangkalan_tujuan").val();
    var is_transit = $("#is_transit").is(":checked") ? 1 : 0;

    var harga = Number($("#harga_tiket").val().replace(/,/g, ''));
    kursi.push("xxx");
    $('#frm_pesan').trigger('reset');
    $('div.alert').remove();
    if (id_berangkat && kursi.length) {
        if (harga != "" && harga != null && !isNaN(harga)) {
            if (harga > 500000 || harga < 10000) {
                $("#harga_tiket").val(harga).focus().select();
                alert("Harga tiket tidak valid!");
            } else {
                clearInterval(maketReloader);
                $.post(
                        urlPemesanan + 'form_pemesanan/tambahan',
                        {jml: 1, kursi: kursi, id_berangkat: id_berangkat, id_pangkalan_turun: id_pangkalan_turun, is_transit: is_transit},
                        function (data) {
                            if (data) {
                                $("form#frm_pesan").empty().html(data);
                                if (ticketingLayout) {
                                    $('#smartwizard').smartWizard("next");
                                } else {
                                    $("#tiketModal").modal('show');
                                }

                                if ($("#nihil-kursi").length) {
                                    reloadMaket();
                                    clearInterval(maketReloader);
                                    maketReloader = setInterval(reloadMaket, 10000);
                                }

                                $('html, body').animate({scrollTop: 0}, 'slow');
                                changeTotalPemesanan();
                            }
                        }
                );
            }
        } else {
            $("#harga_tiket").focus().select();
            alert("Harga belum ditentukan!");
        }
    } else {
        alert("Silahkan pilih keberangkatan terlebih dahulu!");
    }
}

function confirmTiket(beli = true) {
    var id_berangkat = $("#layout-maket").data("berangkat");
    var id_berangkat = $("#layout-maket").attr("data-berangkat");
    // var seat = kursi.join();
    var asal = $("#id_pangkalan_asal").val();
    var tujuan = $("#id_pangkalan_tujuan").val();
    var harga = $("#harga_tiket").val();
    var harga_normal = $("#harga_normal").val();
    var is_transit = $("#is_transit").is(":checked") ? 1 : 0;
    var timestamp = new Date().getTime();
    var agen = $("#agen").val();
    beli = beli ? 1 : 0;
    var addition = "&asal=" + asal + "&tujuan=" + tujuan + "&harga=" + harga + "&harga_normal=" + harga_normal + "&timestamp=" + timestamp + "&is_transit=" + is_transit + "&beli=" + beli;

    if (agen) {
        addition += "&agen=" + agen;
    }

    if (pake_promo && id_promo) {
        addition += "&id_promo=" + id_promo;
    }

    if (id_berangkat) {
        if (kursi.length > 0 && kursi.length <= limitKursi) {
            console.log("pesan");
            if (checkForm("frm_pesan")) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: urlPemesanan + 'pesan_tiket/' + id_berangkat,
                    data: $("#frm_pesan").serialize() + addition,
                    success: function (data) {
                        if (data.st) {
                            $("#tiketModal").modal("hide");
                            reloadMaket();
                            clearInterval(maketReloader);
                            maketReloader = setInterval(reloadMaket, 10000);
                            messagesuccess(data.msg);

                            $("#print_tiket").empty().html(data.tiket);
                            if ($("#smartwizard").length) {
                                $("#smartwizard").smartWizard("next");
                                $("#smartwizard").smartWizard("disableStep", 4);
                                $("#smartwizard ul li:nth-child(4)").removeClass("done");
                            }

                            table.fnDraw(true);
                            if (data.url) {
                                printSomething(data.url, null, data.title, true, false, 400);
                            }
                            if (isTambahan) {
                                kursi = [];
                                isTambahan = false;
                            }
                            /* var is_printed = printSomething('',data.tiket,data.title);
                             if(!is_printed){
                             $("#print_tiket").print();
                             }*/
                        } else {
                            $('div.alert').remove();
                            if (ticketingLayout == 1) {
                                var el = $("#frm_pesan");
                                addAlert(el, "danger", data.msg);
                            } else {
                                modaldialogerror(data.msg);
                            }
                            reloadMaket();
                            clearInterval(maketReloader);
                            maketReloader = setInterval(reloadMaket, 10000);
                        }
                        setHarga();
                        $("#agen").val(defUsername);
                    },
                    error: function (xhr, status, error) {
                        $('div.alert').remove();
                        if (ticketingLayout == 1) {
                            var el = $("#frm_pesan");
                            addAlert(el, "danger", xhr.responseText);
                        } else {
                            modaldialogerror(xhr.responseText);
                        }
                        reloadMaket();
                        clearInterval(maketReloader);
                        maketReloader = setInterval(reloadMaket, 10000);
                    }
                });
            }
        } else {
            alert("Silahkan pilih kursi!");
        }
    } else {
        alert("Silahkan pilih keberangkatan terlebih dahulu!");
}
}

function opsiManifest() {
    var el = "";
    $.post(
            baseurl + 'index.php/keberangkatan/pemesanan/isTutup',
            {id_berangkat: $("#layout-maket").attr("data-berangkat"),id_pangkalan:$("#id_pangkalan_asal").val()},
            function (resp) {
           
                var id_operasional = 0;
                var def_uang_terminal = 0;
                if (resp.operasional) {
                    id_operasional = resp.operasional;
                }
                if (resp.default_uang_terminal) {
                    def_uang_terminal = resp.default_uang_terminal;
                }
                el = '<a class="btn btn-primary" onclick="cetakManifest(' + resp.is_tutup + ',' + id_operasional + ',' + def_uang_terminal + ')" style="text-decoration: none; width: 100% !important; margin-bottom: 5px;"><i class="fa fa-print"></i> Cetak Manifest</a>';
                el += '<a class="btn btn-info" onclick="previewManifest()" style="text-decoration: none; width: 100% !important;"><i class="fa fa-eye"></i> Tampilkan di Layar</a> ';
                modaldialog(el);
            }, 'json'
            );
}

function previewManifest() {
    var id_berangkat = $("#layout-maket").attr("data-berangkat");
    var id_pangkalan_naik = $("#id_pangkalan_asal").val();
    var url_preview = urlPemesanan + 'preview_manifest/' + id_berangkat + '/' + id_pangkalan_naik;
    closemodaldialog();
    $.post(
            url_preview,
            {},
            function (resp) {
                if (resp.st) {
                    var el = '<div style="padding: 10px;">';
                    el += '<table border="0" cellpadding="2" style="width: 100%; margin-bottom: 10px; font-size: 14px;">';
                    el += '<tr><td colspan="2" style="text-align: center; font-weight: bold;">' + resp.asal_tujuan + '</td></tr>';
                    el += '<tr><td width="100">Kode Brkt</td><td>: ' + resp.kode_brkt + '</td></tr>';
                    el += '<tr><td>No Body</td><td>: ' + resp.no_body + '</td></tr>';
                    el += '<tr><td>Tgl Brkt</td><td>: ' + resp.tgl_brkt + '</td></tr>';
                    el += '<tr><td>Naik</td><td>: ' + resp.naik + '</td></tr>';
                    el += '</table>';

                    el += '<table border="1" cellpadding="2" style="border-collapse: collapse; width: 100%; margin-bottom: 10px; font-size: 14px;">';
                    el += '<tr style="font-size: 15px; text-align: center"><td>No</td><td>Nama</td><td>Turun</td><td width="55">Transit</td></tr>';
                    for (var q = 0; q < resp.tiket.length; q++) {
                        el += '<tr>';
                        el += '<td style="text-align: center">' + resp.tiket[q].no_kursi + '</td>';
                        el += '<td>' + resp.tiket[q].nama_penumpang + '</td>';
                        el += '<td>' + resp.tiket[q].turun + '</td>';
                        el += '<td>';
                        if (resp.tiket[q].is_transit == 1) {
                            el += '&bull;';
                        }
                        el += '</td>';
                        el += '</tr>';
                    }
                    el += '</table>';
                    el += '<a class="btn btn-primary" onclick="closemodalboostrap()">Tutup</a>';
                    el += '</div>';
                    modalbootstrap(el);
                } else {
                    alert(resp.msg);
                }
            }, "json"
            );
}

function cetakManifest(is_tutup = 0, id_operasional = 0, def_uang_terminal = null) {
    closemodaldialog();
    var id_berangkat = $("#layout-maket").attr("data-berangkat");
    var id_pangkalan_naik = $("#id_pangkalan_asal").val();
    var url = urlPemesanan + 'print_manifest/' + id_berangkat + '/' + id_pangkalan_naik;
    var url_summary = urlPemesanan + 'print_manifest/' + id_berangkat + '/' + id_pangkalan_naik + '/1';
    var pesan_tambahan = " & data reservasi yang belum dibayar akan dihapus!";
    
    if (!$("#lbl_no_body").length || $("#lbl_no_body").html() == "") {
        alert("SPJ untuk Keberangkatan ini belum dibuat!");
    } else {
        var swal_option = {
            title: "Cetak Manifest?",
            text: "Isi dengan biaya terminal",
            showCancelButton: true,
            confirmButtonColor: "#00a65a",
            confirmButtonText: "Save",
            closeOnConfirm: false
        };
        if (is_tutup == 0 ) {
            swal_option.input = "select";
            swal_option.inputPlaceholder = "Uang Terminal";
            swal_option.inputOptions = {'0':'0','4000':'4,000','9000':'9,000','20000':'20,000'};
            swal_option.inputValue = def_uang_terminal;
        }
        else
        {
            swal_option.text = "Print copy manifest";
        }
        swal(swal_option).then((result = "") => {
            var inputValue = result.value;
            if (id_operasional == 0 && (inputValue == "" || inputValue == null || isNaN(inputValue))) {
                swal("Error", "Tutup penjualan dibatalkan", "error");
                return false;
            }
            if (inputValue !== false && !isNaN(inputValue) && inputValue != null) {
                $.post(
                        baseurl + 'index.php/keberangkatan/pemesanan/tutup_penjualan',
                        {id_berangkat: id_berangkat, id_pangkalan: id_pangkalan_naik, id_operasional: id_operasional, uang_terminal: parseInt(inputValue)},
                        function (resp) {
                            swal.close();
                            if (resp.st) {
                                if (resp.msg) {
                                    swal("Success!", resp.msg, "success");
                                }
                                if (resp.id_operasional) {
                                    url += "/" + resp.id_operasional;
                                }
                                printSomething(url, null, 'Manifest Penumpang', true, false, 400);
                                $("#btn-pesan, #btn-pesan-tambahan").hide();
                                $("#btn-promo").hide();
                                reloadMaket();
                                clearInterval(maketReloader);
                                maketReloader = setInterval(reloadMaket, 10000);
                                $("#sisa_setoran").val(0);
                            } else {
                                if (swal_option.text == "Print copy manifest")
                                {
                                    if (id_operasional) {
                                        url += "/" + id_operasional;
                                    }
                                    printSomething(url, null, 'Manifest Penumpang', true, false, 400);
                                }
                                else
                                {
                                    swal("Error", resp.msg, "error");
                                }
                            }
                        }, "json"
                        );
        }

        });
}

}

function cetakSP() {
    var id_berangkat = $("#layout-maket").attr("data-berangkat");
    var id_pangkalan = $("#id_pangkalan_asal").val();
    var textTambahan = '';
    var status_penjualan = $("#status_penjualan").val();
    var sisa_setoran = $("#sisa_setoran").val();
    // var url = urlPemesanan + 'cetak_sp/'+id_berangkat+'/'+id_pangkalan;
    // printSomething(url,'','Surat Pengantar', true, false, 350);
    // swal({
    //     title: "Anda yakin akan menutup penjualan",
    //     text: "Dan mencetak Surat Pengantar?",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Ya!",
    //     cancelButtonText: "Batal!",
    //     closeOnConfirm: true
    // },function(){
    if (sisa_setoran > 0) {
        $.post(
                baseurl + 'index.php/keberangkatan/pemesanan/getSisaPenjualanOld',
                {id_berangkat: id_berangkat, id_pangkalan: id_pangkalan},
                function (resp) {
                    $.post(
                            baseurl + 'index.php/keberangkatan/penjualan/form_manifest_new',
                            {
                                pilihan: resp.pilihan,
                                no_spj: resp.no_spj,
                                no_body: resp.no_body,
                                pnp: resp.pnp,
                                pemesanan: resp.pemesanan,
                                id_pangkalan: id_pangkalan,
                                text: textTambahan,
                                tutup: true
                            },
                            function (resp) {
                                if (resp.st) {
                                    $("#id_pangkalan_operasional").val($("#id_pangkalan_asal").val());
                                    $("#dipilih_operasional").val("");
                                    total_operasional = 0;
                                    operasional = [];
                                    modalbootstrap(resp.html, "Buat Setoran" + textTambahan, "90%");
                                } else {
                                    messageerror(resp.msg);
                                }
                            }, 'json'
                            );
                }
        , "json"
                );
    } else {
        $("#sisa_setoran").val(0);
        var url = urlPemesanan + 'print_sp/' + id_berangkat + '/' + id_pangkalan;
        printSomething(url, '', 'Surat Pengantar', true, false, 400);
    }

    // });
}

function tutupPenjualan() {
    var id_berangkat = $("#layout-maket").attr("data-berangkat");
    var id_pangkalan_asal = $("#id_pangkalan_asal").val();
    var url = urlPemesanan + 'print_sp/' + id_berangkat + '/' + id_pangkalan_asal;
    $.post(
            baseurl + 'index.php/keberangkatan/pemesanan/tutup_penjualan',
            {id_berangkat: id_berangkat, id_pangkalan: id_pangkalan_asal},
            function (resp) {
                if (resp.st) {
                    if (resp.msg) {
                        swal("Success!", resp.msg, "success");
                    }
                    printSomething(url, '', 'Surat Pengantar', true, false, 400);
                    $("#btn-pesan, #btn-pesan-tambahan").hide();
                    $("#btn-promo").hide();
                    reloadMaket();
                    clearInterval(maketReloader);
                    maketReloader = setInterval(reloadMaket, 10000);
                    $("#sisa_setoran").val(0);
                } else {
                    swal("Error", resp.msg, "error");
                }
            }, "json"
            );
}

function reloadMaket() {
    // kursi = [];
    // $('#frm_pesan').trigger('reset');
    var id_berangkat = $("#layout-maket").attr("data-berangkat");
    var link_maket = urlPemesanan + "view_maket/" + id_berangkat + "/" + $("#id_pangkalan_asal").val();

    $("#layout-maket").load(link_maket, {kursi: kursi, hapuskursi: hapuskursi}, function () {
        var terjual = $("#terjual").val();
        var status_penjualan = $("#status_penjualan").val();
        if (status_penjualan == "1") {
            $("#btn-pesan, #btn-pesan-tambahan").show();
            $("#btn-promo").show();
        } else {
            // $("#btn-pesan").hide();
            // $("#btn-promo").hide();
        }
        if (terjual) {
            terjual = terjual.split(",");
        }
        for (var s = 0; s < kursi.length; s++) {
            if (!$('.btn-kursi[data-kursi="' + kursi[s] + '"]').hasClass('btn-danger')) {
                $('.btn-kursi[data-kursi="' + kursi[s] + '"]').addClass('btn-selected');
            }
        }
        for (var s = 0; s < hapuskursi.length; s++) {
            if ($('.btn-kursi[data-pemesanan="' + hapuskursi[s] + '"]').hasClass('btn-danger') || $('.btn-kursi[data-pemesanan="' + hapuskursi[s] + '"]').hasClass('btn-info')) {
                $('.btn-kursi[data-pemesanan="' + hapuskursi[s] + '"]').addClass('bg-black-active');
            }
        }
        for (var s = 0; s < terjual.length; s++) {
            var idx = jQuery.inArray(parseInt(terjual[s]), kursi);
            if (idx > -1) {
                $('.btn-kursi[data-kursi="' + terjual[s] + '"]').removeClass('btn-selected');
                kursi.splice(idx, 1);

            }
        }
    });
}

function checkForm(id_form) {
    var isValid = true;
    $('#' + id_form + ' input').filter('[required]').each(function () {
        if ($(this).val() === '') {
            isValid = false;
            $(this).select().focus();
            console.log(this);
            return false;
        }
        console.log(isValid);
    });
    return isValid;
}

function setHarga(id_berangkat = 0, promo = false) {
    if (!id_berangkat) {
        id_berangkat = $("#layout-maket").data('berangkat');
        id_berangkat = $("#layout-maket").attr("data-berangkat");
    }

    var kode_promo = null;
    if (promo && $("#kode_promo").val() != "") {
        kode_promo = $("#kode_promo").val();
    }
    $.post(
            urlPemesanan + 'get_tarif',
            {
                id_pangkalan_asal: $("#id_pangkalan_asal").val(),
                id_pangkalan_tujuan: $("#id_pangkalan_tujuan").val(),
                id_kelas: $("#id_kelas").val(),
                id_berangkat: id_berangkat,
                kode_promo: kode_promo,
                nominal: $("#harga_tiket").val()
            },
            function (data) {
                $("#harga_tiket").val(data.harga);
                $("#harga_normal").val(data.harga_normal);
                if (data.fix) {
                    $("#harga_tiket").prop('disabled', true);
                } else {
                    $("#harga_tiket").prop('disabled', false);
                }

                if (data.promo_notif) {
                    $("#promo-notif").html(data.promo_notif).effect("highlight");
                } else {
                    $("#promo-notif").empty();
                }

                if (data.pake_promo) {
                    pake_promo = true;
                    id_promo = data.id_promo;
                } else {
                    pake_promo = false;
                    id_promo = null;
                }

                if (data.tanggal_naik && data.jam_naik) {
                    var teks = '<p id="detail-naik" style="border-top:solid 1px #000; margin-top:10px; font-size: 16px;">';
                    teks += '<span class="span-detail">Naik</span>: ' + $("#id_pangkalan_asal option:selected").html() + '<br/>';
                    teks += '<span class="span-detail">Tanggal Naik</span>: ' + data.tanggal_naik + '<br/>';

                    if (data.jam_naik != "00:00") {
                        teks += '<span class="span-detail">Jam Naik</span>: ' + data.jam_naik;
                    }
                    teks += '</p>';
                    $("#detail-naik").remove();
                    $("#jadwal_berangkat").append(teks);
                }
            }, "json"
            );
}

function reprintTiket(id_berangkat, id_pemesanan) {
    url = urlPemesanan + 'print_tiket/' + id_berangkat + '/' + id_pemesanan;
    printSomething(url, null, 'Tiket', true, false, 400);
}

function bayarTiket(id_pemesanan) {
    $.post(
            urlPemesanan + 'form_bayar_tiket',
            {id_pemesanan: id_pemesanan},
            function (resp) {
                modalbootstrap(resp, "Bayar Reservasi");
            }
    );
}

function confirmBayar(id_berangkat, id_pemesanan) {
    $.post(
            urlPemesanan + 'bayar_tiket',
            {id_berangkat: id_berangkat, id_pemesanan: id_pemesanan},
            function (resp) {
                closemodalboostrap();
                if (resp.st) {
                    url = urlPemesanan + 'print_tiket/' + id_berangkat + '/' + id_pemesanan;
                    printSomething(url, null, 'Tiket', true, false, 400);
                    reloadMaket();
                    clearInterval(maketReloader);
                    maketReloader = setInterval(reloadMaket, 10000);
                    messagesuccess(resp.msg);
                } else {
                    $('div.alert').remove();
                    if (ticketingLayout == 1) {
                        var el = $("#frm_pesan");
                        addAlert(el, "danger", resp.msg);
                    } else {
                        modaldialogerror(resp.msg);
                    }
                    reloadMaket();
                    clearInterval(maketReloader);
                    maketReloader = setInterval(reloadMaket, 10000);
                }

            }, 'json'
            );
}

function pindahKursi(id_pemesanan) {
    changeSeat = id_pemesanan;
    kursi = [];
    var petunjuk = '<div class="modal-body">Silahkan pilih kursi lainnya yang masih kosong!</div>';
    petunjuk += '<div class="modal-footer"> <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button> </div>';
    if ($(window).width() > 800) {
        modalbootstrap(petunjuk, "", "40%");
    } else {
        modalbootstrap(petunjuk, "", "90%");
    }
    clearInterval(maketReloader);
    $(".btn-kursi").removeClass('btn-selected');
}
function hideFormCancelPemesananAll() {
    closemodalboostrap();
}
function deletekursibanyak() {


    if (isdelete == true)
    {


        if (hapuskursi.length > 0)
        {
            $.post(
                    urlPemesanan + 'batal_ticketall',
                    function (data) {
                        if ($(window).width() > 800) {
                            modalbootstrap(data, "Detail Pembatalan", "40%");
                        } else {
                            modalbootstrap(data, "Detail Pembatalan", "95%");
                        }
                    }
            );
        } else
        {
            $("#btn-delete_all").html("Start Delete");
            $("#btn-delete_all").removeClass("bg-black-active");

        }

    } else
    {
        isdelete = true;
        $("#btn-delete_all").html("Stop Delete");

        $("#btn-delete_all").addClass("bg-black-active");
    }

}
function resetLah() {
    $("#layout-maket").empty();
    $("#btn-pesan, #btn-pesan-tambahan").hide();
    $("#btn-promo").hide();
    $("#btn-manifest, #btn-sp, #btn-bonggol ,#btn-delete_all").hide();
    $("#jadwal_berangkat").html('Keberangkatan');
    $("#promo-notif").empty();
    $("#harga_tiket, #harga_normal").val('');
    $("#kode_promo").val('');
    $("#total_pemesanan").html('');
    $("#agen").val(defUsername);
    $("#id_pangkalan_asal, #id_pangkalan_tujuan").empty().select2({data: []}).val('').trigger('change');
    clearInterval(maketReloader);
    pake_promo = false;
    id_promo = null;
    kursi = [];
}

function ubahTransit() {
    if ($("#is_transit").is(':checked')) {
        $("#id_pangkalan_tujuan").empty().select2({data: list_pangkalan});
    } else {
        $("#id_pangkalan_tujuan").empty().select2({data: list_tujuan});
    }
    $("#id_pangkalan_tujuan").val($("#search_pangkalan_tujuan").val()).trigger("change");
}

function changeTotalPemesanan() {
    var hargaTiket = number_parse($("#harga_tiket").val());
    var hargaTotalPesanan = 0;
    if ($("#rombongan").is(":checked")) {
        var opsi_rombongan = $("#txt_jenis_tiket_1").val();
        if (opsi_rombongan == 1) {
            hargaTotalPesanan = parseInt(hargaTiket) * $(".sub_pesan_form").length;
        } else if (opsi_rombongan == 2) {
            hargaTotalPesanan = 0;
        } else if (opsi_rombongan == 3) {
            hargaTotalPesanan = (parseInt(hargaTiket) / 2) * $(".sub_pesan_form").length;
        }
    } else {
        $(".opsi_jenis_tiket").each(function () {
            var hargaSatuan = 0;
            if ($(this).val() == 1) {
                hargaSatuan = parseInt(hargaTiket);
            } else if ($(this).val() == 2) {
                hargaSatuan = 0;
            } else if ($(this).val() == 3) {
                hargaSatuan = parseInt(hargaTiket) / 2;
            }

            hargaTotalPesanan += parseInt(hargaSatuan);
        });
    }

    $("#total_pemesanan").html("Total: Rp." + number_format(hargaTotalPesanan, 0, ',', '.') + ",-");
}

function cetakBonggol() {
    var urlbonggol = baseurl + 'index.php/keberangkatan/pemesanan/print_bonggol/' + $("#layout-maket").attr("data-berangkat") + '/' + $("#search_pangkalan_asal").val();
    printSomething(urlbonggol, '', 'Print Bonggol', true, false, 400);
}

function transitan() {
    var cadangan = 0;
    var kursi_transit = 0;
    var proses_transit = true;
    var id_berangkat = $("#layout-maket").data("berangkat");
    id_berangkat = (!id_berangkat) ? $("#layout-maket").attr("data-berangkat") : id_berangkat;

    if (kursi.length > 1) {
        proses_transit = false;
        alert("Hanya satu kursi yang dapat dipilih untuk penumpang transitan");
    } else if (kursi.length == 0) {
        cadangan = 1;
    } else {
        kursi_transit = kursi[0];
    }

    if (proses_transit) {
        var modalUrl = urlBerangkat + 'transit/transit_form/' + id_berangkat + '/' + cadangan + '/' + kursi_transit;

        $("#modal_transitan .modal-body").load(modalUrl, {id_berangkat: id_berangkat, cadangan: cadangan, kursi_transit: kursi_transit}, function () {
            if ($("#transitan_tanggal").length) {
                $("#transitan_tanggal").Zebra_DatePicker({
                    format: 'd M Y',
                    direction: [false, 1]
                });
            }
            $("#modal_transitan").modal("show");
        });
    }
}

function confirmTransitan() {
    var transitan_id_berangkat = $("#transitan_id_berangkat").val();
    var transitan_ex_kursi = $("#transitan_ex_kursi").val();
    var transitan_no_kursi = $("#transitan_no_kursi").val();

    if (transitan_ex_kursi != "" && transitan_id_berangkat != "" && transitan_ex_kursi != null && transitan_id_berangkat != null) {
        $.post(
                urlBerangkat + 'transit/confirm',
                {
                    transitan_id_berangkat: transitan_id_berangkat,
                    transitan_ex_kursi: transitan_ex_kursi,
                    transitan_no_kursi: transitan_no_kursi
                },
                function (resp) {
                    if (resp.st) {
                        $("#modal_transitan").modal("hide");
                        $("#modal_transitan .modal-body").empty();
                        reloadMaket();
                        clearInterval(maketReloader);
                        maketReloader = setInterval(reloadMaket, 10000);
                        alert(resp.msg);
                    } else {
                        alert(resp.msg);
                    }
                }, "json"
                );
    } else if (transitan_id_berangkat == "") {
        alert("Silahkan pilih keberangkatan sebelumnya!");
    } else if (transitan_ex_kursi == "") {
        alert("Silahkan pilih penumpang!");
    }
}