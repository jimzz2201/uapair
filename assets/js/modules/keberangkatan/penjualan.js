$(document).ready(function () {
    $(".select2pangkalan").select2({data: list_pangkalan});
    $(".select2pangkalan_beli").select2({data: list_pangkalan_beli}).val(default_pangkalan).trigger('change');

    $(".datepicker").datepicker({
        autoclose: true,
        format: 'dd M yyyy',
        weekStart: 1,
        language: "id",
        todayHighlight: true,
    });

    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    table = $("#mytable").dataTable({
        initComplete: function () {
            var api = this.api();
            $('#mytable_filter input')
                .off('.DT')
                .on('keyup.DT', function (e) {
                    if (e.keyCode == 13) {
                        api.search(this.value).draw();
                    }
                });
        },
        oLanguage: {
            sProcessing: "loading..."
        },
        pageLength: 200,
        lengthMenu: [ 50, 100, 200, 250 ],
        processing: true,
        // sDom:'ltrip',
        serverSide: true,
        ajax: {"url": baseurl + "index.php/keberangkatan/penjualan/getdatapenjualan", "type": "POST",
            "data": function (d) {
                return $.extend({}, d, {
                    "extra_search": $("form#frm_search").serialize()
                });
            }},
        columns: [
            {data: "kode", orderable: false, className:"normal-padding",
                title: "<input  id='check_box_all' type='checkbox' class='minimal-red'   />",
                mRender: function (data, type, row) {
                    return "<input name='pilihan["+data+"]' id='checkbox_pilih_" + data + "' type='checkbox' class='minimal-red cb_detailcheck'  value='" + data + "' />";
                },
                 width:'25px'
            }
            , {data: "id_berangkat", orderable: false, title: "#", width:'35px', className:"normal-padding"}
            , {data: "nama_pangkalan", orderable: false, title: "Agen", width:'75px', className:"normal-padding"}
            , {data: "tanggal_penjualan", orderable: false, title: "Tgl Jual", width:'75px', className:"normal-padding",
                mRender: function (data, type, row) {
                    return DefaultDateFormat(data);
                }
            }
            ,{data: "no_spj", orderable: false, title: "No SPJ", width:'200px', className:"normal-padding"}
            ,{data: "trayek", orderable: false, title: "", width:'85px', className:"normal-padding"}
            ,{data: "no_body", orderable: false, title: "No Body", width:'65px', className:"normal-padding text-center"}
            // ,{data: "kode_berangkat", orderable: false, title: "Kode Berangkat"}
            ,{data: "tanggal", orderable: false, title: "Tgl Brgkt", width:'75px', className:"normal-padding"}
            ,{data: "jam", orderable: false, title: "Jam", width:'50px', className:"normal-padding text-center",
				mRender:function(data, type, row){
					var jam = data;
					if(data == "00:00"){
						jam = '';
					}
					return jam;
				}}
            ,{data: "kode", orderable: false, title: "Kode Order", width:'200px', className:"normal-padding"}
            ,{data: "pnp", orderable: false, title: "PNP", width:'50px', className:"normal-padding text-right"}
            ,{data: "total_penjualan", orderable: false, title: "Penjualan", className: "text-right",
                mRender: function (data, type, row) {
                    var inputan = '<input type="hidden" value="'+data+'" name="nominal['+row['kode']+']" id="nominal_'+row['kode']+'" />';
                    var inputan2 = '<input type="hidden" value="'+row['id_pemesanan']+'" name="id_pemesanan['+row['kode']+']" id="pemesanan_'+row['kode']+'" />';
                    return Comma(data) + inputan + inputan2;
                }
            }
        ],
        order: [],
        rowCallback: function(row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(1)', row).html(index);

            $(document).ajaxComplete(function () {
                // $('.minimal-red').iCheck({
                //     checkboxClass: 'icheckbox_minimal-red',
                //     radioClass: 'iradio_minimal-red'
                // });
                //
                $('#check_box_all').prop('checked', false);

                $('#check_box_all').change(function () {
                    console.log();
                    if ($(this).is(':checked')){
                        $('.cb_detailcheck').prop('checked', true);
                        console.log("checked");
                    } else{
                        $('.cb_detailcheck').prop('checked', false);
                        console.log("unchecked");
                    }
                    // $('.cb_detailcheck').iCheck('update');
                });

                $("#id_pangkalan").val($("#id_pangkalan_beli").val());

                // $("#tanggal_penjualan").val($("#tanggal").val());
            })
        }

    });

    $("form#frm_search").submit(function (e) {
        table.fnDraw(false);
        return false;
    });

    $("#btn-manifest").click(function(){
        var diCheck = [];
        var nominal = [];
        var pemesanan = [];
        var kode = 0;
        var tanggal_penjualan = null;

        LoadBar.show();
        $(".cb_detailcheck").each(function () {
            if($(this).is(':checked')){
                kode = $(this).val();

                if(diCheck[kode] === undefined){
                    diCheck.push(kode);
                    nominal.push($("#nominal_"+kode).val());
                    pemesanan.push($("#pemesanan_"+kode).val());
                }
                // diCheck[kode].push(tanggal_penjualan);
                // diCheck[kode+"_"+tanggal_penjualan] = $("#nominal_"+kode+"_"+tanggal_penjualan).val();
            }
        });

        if(diCheck.length){
            $.post(
                baseurl + 'index.php/keberangkatan/penjualan/form_manifest_new',
                {pilihan:diCheck, nominal:nominal, pemesanan:pemesanan, id_pangkalan:$("#id_pangkalan").val()},
                function(resp){
                    if(resp.st){
                        $("#id_pangkalan_operasional").val($("#id_pangkalan").val());
                        $("#dipilih_operasional").val("");
                        total_operasional = 0;
                        operasional = [];
                        modalbootstrap(resp.html, "Buat Setoran", "90%");
                    }else{
                        messageerror(resp.msg);
                    }
					LoadBar.hide();
                }, 'json'
            );
            /*$( "#modal_tanya" ).dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                    "TRANSFER": function() {
                        $("#jenis_pembayaran").val("TRANSFER");
                        createByAjax(diCheck, nominal);
                        $( this ).dialog( "close" );
                    },
                    "CASH": function() {
                        $("#jenis_pembayaran").val("CASH");
                        createByAjax(diCheck, nominal);
                        $( this ).dialog( "close" );
                    }
                }
            });*/

        }else{
            LoadBar.hide();
            alert("Silahkan pilih data yang akan dimasukan ke setoran!");
        }
    });
    
    /*$("#id_pangkalan_asal").change(function () {
        $("#id_pangkalan").val($(this).val());
        table.fnDraw(false);
    });*/

    /*$("#tanggal").change(function () {
        console.log($(this).val());
        $("#tanggal_penjualan").val($(this).val());
    });*/


    /*$('.cb_detailcheck').click(function () {
        if($(this).is(':checked')){
            var id_berangkat = $(this).data('berangkat');
            $('#nominal_'+id_berangkat).select().focus();
            alert(id_berangkat);
        }
    });*/
});

function createByAjax(pilihan, nominal) {
    $.post(
        baseurl + 'index.php/keberangkatan/penjualan/create_manifest_ajax',
        {pilihan:pilihan, nominal:nominal, jenis_pembayaran:$("#jenis_pembayaran").val(), id_pangkalan:$("#id_pangkalan").val()},
        function(resp){
            if(resp.st){
                table.fnDraw(false);
                messagesuccess(resp.msg);
                printSomething(resp.url, null, resp.title, true, false, 400);
            }else{
                messageerror(resp.msg);
            }
        }, 'json'
    );
}