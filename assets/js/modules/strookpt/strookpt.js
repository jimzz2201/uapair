var ajaxUrl = baseurl + 'index.php/strookpt/';

$(document).ready(function () {
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd M yyyy',
        weekStart: 1,
        language: "id",
        todayHighlight: true,
    });
    
    $("#txt_biaya_lain_lain").change(function () {
        var biaya_lain = ParseNumber($("#txt_biaya_lain_lain").val());
        if(biaya_lain > 0){
            $("#txt_keterangan_lain_lain").attr('required','required');
        }else{
            $("#txt_keterangan_lain_lain").removeAttr('required');
        }
    });

    $("#frm_strookpt").submit(function () {
        $.ajax({
            type: 'POST',
            url: ajaxUrl + "strookpt_manipulate",
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st){
                    window.location.href = ajaxUrl;
                }else{
                    messageerror(data.msg);
                }
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;
    });

    $("#txt_harga_kps_pt").change(function(){
        HitungSemua();
    }).change();

    $("#Cb_is_kernet_batangan").change(function () {
        CheckKernet();
        HitungSemua();
    });

    $("#Cb_is_kp_selamat_timur, #Cb_is_kp_selamat_barat, #txt_kernet_batangan, #txt_tabungan, #txt_full_seat_timur, #txt_full_seat_barat, #txt_honor_kernet, #txt_honor_pengemudi, #txt_tambahan_interval, #txt_biaya_tol, #txt_biaya_lain_lain, #persen_selamat_timur").change(function(){
        HitungSemua();
    });

    $("#Cb_is_uht").change(function () {
        RefreshKPSUHT();
    });

    $("#Cb_is_kps_ekstra_pengemudi").change(function () {
        RefreshKPSEkstra();
    });
});

function HitungSemua() {
    var penumpangBarat = $("#txt_jumlah_penumpang_barat").val();
    var penumpangTimur = ParseNumber($("#TxtJumlahPenumpangTimur").val());
    var hargaKPSelamatTimur = ParseNumber($("#persen_selamat_timur").val());
    var persenSelamatTimur = ParseNumber($("#txt_harga_kps_pt").val());
    var totalTimur = penumpangTimur * hargaKPSelamatTimur;
    var kpSelamatTimur = (totalTimur / 100) * persenSelamatTimur;

    var totalBarat = penumpangBarat * hargaKPSelamatTimur;
    var kpSelamatBarat = (totalBarat / 100) * persenSelamatTimur;

    var kpSelamatBaratRound = Math.round((kpSelamatBarat - 1) / 1000) * 1000;
    var kpSelamatTimurRound = Math.round((kpSelamatTimur - 1) / 1000) * 1000;

    $("#TxtTotalKPSelamatTimur").val(Currency(totalTimur));
    $("#TxtKPSelamatTimurPengemudiAfterPersen").val(Currency(kpSelamatTimur));
    $("#TxtAfterPersen").val(Currency(kpSelamatTimur));

    if ($("#Cb_is_kp_selamat_barat").is(':checked')) {
        $("#txt_kp_selamat_barat").val(Currency(kpSelamatBaratRound));
    }else {
        $("#txt_kp_selamat_barat").val(0);
    }

    if ($("#Cb_is_kp_selamat_timur").is(':checked')) {
        $("#txt_kp_selamat_timur").val(Currency(kpSelamatTimurRound));
    }else {
        $("#txt_kp_selamat_timur").val(0);
    }
    RefreshKPSUHT();
    var bppt = 0;
    bppt += ParseNumber($("#txt_honor_pengemudi").val());
    bppt += ParseNumber($("#txt_honor_kernet").val());
    if($("#Cb_is_kp_selamat_timur").is(':checked')) {
        bppt += ParseNumber($("#txt_kernet_batangan").val());
    }
    if($("#Cb_is_kp_selamat_barat").is(':checked')) {
        bppt += ParseNumber($("#txt_kp_selamat_barat").val());
    }
    if($("#Cb_is_kp_selamat_timur").is(':checked')) {
        bppt += ParseNumber($("#txt_kp_selamat_timur").val());
    }
    bppt += ParseNumber($("#txt_full_seat_barat").val());
    bppt += ParseNumber($("#txt_full_seat_timur").val());
    bppt += ParseNumber($("#txt_biaya_lain_lain").val());
    bppt += ParseNumber($("#txt_tambahan_interval").val());
    bppt = bppt * -1;

    $("#txt_biaya_pengemudi_pt").val(number_format(bppt));
    if(bppt < 0){
        $("#txt_biaya_pengemudi_pt_display").css("color","red").val("("+number_format(Math.abs(bppt))+")");
    }else{
        $("#txt_biaya_pengemudi_pt_display").css("color","#555").val(number_format(bppt));
    }

    var totalpt = bppt;
    totalpt -= ParseNumber($("#txt_biaya_tol").val());
    $("#txt_total_biaya_pengemudi_pt").val(number_format(totalpt));
    if(totalpt < 0){
        $("#txt_total_biaya_pengemudi_pt_display").css("color","red").val("("+number_format(Math.abs(totalpt))+")");
    }else{
        $("#txt_total_biaya_pengemudi_pt_display").css("color","#555").val(number_format(totalpt));
    }

    var total = totalpt + ParseNumber($("#txt_tabungan").val());
    $("#txt_total").val(number_format(total));
    if(total < 0){
        $("#txt_total_display").css("color","red").val("("+number_format(Math.abs(total))+")");
    }else{
        $("#txt_total_display").css("color","#555").val(number_format(total));
    }
}

function RefreshKPSUHT() {
    var penumpangBarat = ParseNumber($("#txt_jumlah_penumpang_barat").val());
    var penumpangTimur = ParseNumber($("#TxtJumlahPenumpangTimur").val());
    var perkepalaBarat = ParseNumber($("#Txt_Perkepala_Barat").val());
    var perkepalaTimur = ParseNumber($("#Txt_Perkepala_Timur").val());
    var uht = (penumpangBarat * perkepalaBarat) + (penumpangTimur * perkepalaTimur);
    if ($('#Cb_is_uht').is(':checked')) {
        $('#txt_uht').val(Currency(uht));
    }else {
        $('#txt_uht').val("0");
    }
}

function RefreshKPSEkstra() {
    var totalpnpkpsekstrabarat = ParseNumber($("#Txt_Penumpang_KPS_Barat_Ekstra").val());
    var totalpnpkpsekstratimur = ParseNumber($("#Txt_Penumpang_KPS_Timur_Ekstra").val());
    if ($('#Cb_is_kps_ekstra_pengemudi').is(':checked')) {
        var hargakpsekstrabarat = ParseNumber($("#txt_harga_kps_pt").val());
        var hargakpsekstratimur = ParseNumber($("#txt_harga_kps_pt").val());
        var totalkpsselamatbarat = totalpnpkpsekstrabarat * hargakpsekstrabarat;
        totalkpsselamatbarat = Math.round((totalkpsselamatbarat - 1) / 1000) * 1000;
        var totalkpsselamattimur = totalpnpkpsekstratimur * hargakpsekstratimur;
        totalkpsselamattimur = Math.round((totalkpsselamattimur - 1) / 1000) * 1000;
        var transitan = 0;
        var kpsExtra = number_format((totalkpsselamatbarat + totalkpsselamattimur + transitan) / 2);

        $('#txt_kps_ekstra_pengemudi').val(Currency(kpsExtra));
    }else {
        $('#txt_kps_ekstra_pengemudi').val("0");
    }
}

function CheckKernet() {
    if ($('#Cb_is_kernet_batangan').is(':checked')) {
        var honor_kernet_batangan = ParseNumber($("#txt_copy_kernet_batangan").val());
        $('#txt_kernet_batangan').val(Currency(honor_kernet_batangan)).focus();
        $('#txt_kernet_batangan').removeAttr("readonly");
    }else{
        $('#txt_kernet_batangan').attr("readonly", "readonly");
        $('#txt_kernet_batangan').val(0);
    }
}

function Currency(Num) {
    var ismin = false;
    if (Num.toString().search("-") > -1)
        ismin = true;
    Num = ParseNumber(Num.toString());
    Num += '';


    if (ismin)
        Num = '-' + Num.toString().replace(/,/g, '');
    else
        Num = Num.toString().replace(/,/g, '');
    x = Num.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1))
        x1 = x1.toString().replace(rgx, '$1' + ',' + '$2');

    return x1 + x2;
}