function messageerror(message)
{
    $('html, body').animate({scrollTop: $('#notification').offset().top}, 'slow');
    $("#notification").empty();
    $("#notification").append("<div class=\"alert alert-danger alert-dismissible\"><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><h4><i class='icon fa fa-ban'></i> Error:</h4>" + message + "</div>");
}


Date.prototype.addDays = function (num) {
    var value = this.valueOf();
    value += 86400000 * num;
    return new Date(value);
}

Date.prototype.addSeconds = function (num) {
    var value = this.valueOf();
    value += 1000 * num;
    return new Date(value);
}

Date.prototype.addMinutes = function (num) {
    var value = this.valueOf();
    value += 60000 * num;
    return new Date(value);
}

Date.prototype.addHours = function (num) {
    var value = this.valueOf();
    value += 3600000 * num;
    return new Date(value);
}

Date.prototype.addMonths = function (num) {
    var value = new Date(this.valueOf());

    var mo = this.getMonth();
    var yr = this.getFullYear();
    mo = (mo + num) % 12;
    if (0 > mo) {
        yr += (this.getMonth() + num - mo - 12) / 12;
        mo += 12;
    }
    else
        yr += ((this.getMonth() + num - mo) / 12);
    
    value.setMonth(mo);
    value.setYear(yr);
    return value;
}
function clearmessage()
{
    $("#notification").empty();
}
function messagesuccess(message)
{
    $('html, body').animate({scrollTop: $('#notification').offset().top}, 'slow');
    $("#notification").empty();
    $("#notification").append("<div class=\"alert alert-success alert-dismissible\"><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><h4><i class='icon fa fa-check'></i> Successfull:</h4>" + message + "</div>");
}
function messageErrorFadeOut(message)
{
    console.log("fadeOut");
    $('html, body').animate({scrollTop: $('#notification').offset().top}, 'slow');
    $("#notification").empty();
    $("#notification").append("<div class=\"alert alert-danger alert-dismissible\"><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><h4><i class='icon fa fa-ban'></i> Error:</h4>" + message + "</div>");
    $("#notification").fadeOut(5000, 'swing', function(){
        $("#notification").empty();
    });
}


function messageSuccessFadeOut(message)
{
    $('html, body').animate({scrollTop: $('#notification').offset().top}, 'slow');
    $("#notification").empty();
    $("#notification").append("<div class=\"alert alert-success alert-dismissible\"><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><h4><i class='icon fa fa-check'></i> Successfull:</h4>" + message + "</div>");
    $("#notification").fadeOut(5000, 'linear', function(){
        $("#notification").empty();
    });
}

function DefaultDateFormat(dateform)
{   
   
    if (dateform == null || dateform < '1970-01-01' || dateform==0)
    {
        return '';
    } else
    {

        // return $.datepicker.formatDate('dd M yy', new Date(dateform));
        
        return moment(dateform).format('DD MMM YYYY');
    }
}

function ParseNumber(Num)
{
   return  Number(Num.toString().replace(/[^0-9\.]+/g, ""));
}

function getParameterByName(name) {
    url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function modaldialog(message)
{
    $("#modaldesc").html(message);
    // $('.remodal-wrapper').unbind('click.remodal');
    $('[data-remodal-id = mymodal]').remodal().open();
}
function openmodaldefault(url, width)
{
    if(width==undefined)
    {
        width=480;
    }
    var left = (screen.width/2)-(480/2);
    window.open(url,'Popup', 'width='+width+', height=600, menubar=yes, scrollbars=yes, resizable=yes,left='+left);
}


function closemodaldialog(){
    $('[data-remodal-id = mymodal]').remodal().close();
}
function modaldialogerror(message)
{
    if ($(".modal-body .alert").length)
    {
        $(".modal-body .alert").remove();
    }
    $(".modal-body").prepend("<div class=\"alert alert-danger alert-dismissible\"><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><h4><i class='icon fa fa-ban'></i> Error:</h4>" + message + "</div>");
    $('#modalbootstrap').animate({scrollTop: 0}, 'slow');
}
function modalbootstrap(message, header, width)
{
    if (width != undefined)
    {
        $("#modalbootstrap .modal-dialog").css("width", width);
    }
    $("#modalbootstrap .modal-content").html(message);
    $("#modalbootstrap").modal("show");
}
function closemodalboostrap() {
    $("#modalbootstrap").modal("hide");
}

function display(jenis)
{
    if (jenis == "list")
    {
        $(".product-grid").addClass("product-list");
        $(".product-grid").removeClass("product-grid");
    } else
    {
        $(".product-list").addClass("product-grid");
        $(".product-list").removeClass("product-list");
    }
}

function addAlert(el, type, msg){
    var title, icon = '';
    switch(type){
        case 'danger' : title = 'Error'; icon = 'fa-ban'; break;
        case 'info' : title = 'Info'; icon = 'fa-info'; break;
        case 'warning' : title = 'Warning'; icon = 'fa-warning'; break;
        case 'success' : title = 'Success'; icon = 'fa-check'; break;
    }

    var html = '<div class="alert alert-'+ type + ' alert-dismissible">';
    html += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
    html += '<h4><i class="icon fa '+ icon +'"></i> '+ title +':</h4>';
    html += msg;
    html += '</div>';
    el.prepend(html);
}

function Comma(Num) {
    console.log(Num);
    if (Num == '0'||Num==""||Num==undefined)
        return 0;
    else
    {
        var ismin = false;
        if (Num.toString().search("-") > -1)
            ismin = true;

        Num = Number(Num.toString().replace(/[^0-9\.]+/g, ""));
        Num += '';
        /* if (Number(Num.toString().replace(/[^0-9\.]+/g, "")) != 0)
         {
         if (ismin)
         Num = '-' + Num.replace(/,/g, '');
         else
         Num = Num.replace(/,/g, '');
         }
         */


        x = Num.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1))
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        
        if (typeof HitungSemua == 'function') {
            HitungSemua();
        }
        return x1 + x2;
    }
}


function CommaMin(Num) {
    var ismin = false;
    if (Num.toString().search("-") > -1)
        ismin = true;
    Num = Number(Num.toString().replace(/[^0-9\.]+/g, ""));
    Num += '';


    if (ismin)
        Num = '-' + Num.toString().replace(/,/g, '');
    else
        Num = Num.toString().replace(/,/g, '');
    x = Num.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1))
        x1 = x1.toString().replace(rgx, '$1' + ',' + '$2');
    if (typeof HitungSemua == 'function') {
        HitungSemua();
    }

    return x1 + x2;

}

function PersenFormat(Num) {
    Num = Number(Num.replace(/[^0-9\.]+/g, ""));
    if (Num > 100)
    {
        return 100;
    } else if (Num < 0)
    {
        return 0;
    } else
        return Num;
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57) && charCode != 45)
        return false;

    return true;
}
function number_parse($val)
{
    return  Number($val.replace(/[^0-9\.]+/g, ""))
}
function number_format(number, decimals, dec_point, thousands_sep) {
    var n = number, prec = decimals;

    var toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec);
        return (Math.round(n * k) / k).toString();
    };

    n = !isFinite(+n) ? 0 : +n;
    prec = !isFinite(+prec) ? 0 : Math.abs(prec);
    var sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep;
    var dec = (typeof dec_point === 'undefined') ? '.' : dec_point;

    var s = (prec > 0) ? toFixedFix(n, prec) : toFixedFix(Math.round(n), prec);
    //fix for IE parseFloat(0.55).toFixed(0) = 0;

    var abs = toFixedFix(Math.abs(n), prec);
    var _, i;

    if (abs >= 1000) {
        _ = abs.split(/\D/);
        i = _[0].length % 3 || 3;

        _[0] = s.slice(0, i + (n < 0)) +
                _[0].slice(i).replace(/(\d{3})/g, sep + '$1');
        s = _.join(dec);
    } else {
        s = s.replace('.', dec);
    }

    var decPos = s.indexOf(dec);
    if (prec >= 1 && decPos !== -1 && (s.length - decPos - 1) < prec) {
        s += new Array(prec - (s.length - decPos - 1)).join(0) + '0';
    } else if (prec >= 1 && decPos === -1) {
        s += dec + new Array(prec).join(0) + '0';
    }
    return s;
}
var LoadBar = {
    show: function () {
        $(".overlay").removeClass("hide");
    },
    hide: function ()
    {
        $(".overlay").addClass("hide");
    }

}

function AnimationShow(id, type)
{
    var currentheight = 0;
    var toheight = 0;
    if (type == 'full')
    {

        $(id).css('height', 'auto');
        toheight = $(id).height();

    } else
    {
        $(id).css('height', 'auto');
        currentheight = $(id).height();
    }

    $(id).height(0).animate(
            {
                height: toheight
            }, 1000
            , function () {
                if (type == 'full')
                {
                    $(id).css("height", "");
                    //  $('html, body').animate({scrollTop: $(id).position().top - 100}, 'slow');

                }
            }
    );

}
function CleanTulisan(text)
{
    return text.replace(/<(?:.|\n)*?>/gm, '');
}
function getstatus(id)
{
    if (id == 0)
    {
        return "Not Active";
    } else
    {
        return "Active";
    }
}


function getstatusApprove(id)
{
    if (id == 0)
    {
        return "Pending";
    } else if(id==1)
    {
        return "Approved";
    }
    else
    {
        return "Rejected";
    }
}
function slug(str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
    var to = "aaaaaeeeeeiiiiooooouuuunc------";
    for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

    return str;
}
function readURL(input, idimage) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#' + idimage)
                    .attr('src', e.target.result)
                    .height(200);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function TimeFormat(dateTime) {
    return dateTime.substring(0, 5);
}

$.fn.serializeObject = function () {
    var o = [];
    var a = this.serializeArray();
    $.each(a, function () {
        o[String(this.name)] = this.value;
    });
    return o;
};
function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

function printSomething(url = '', content = '', title = '', print = true, close = false, width = null, height = null) {
    var windowAttr = 'location=no;resizable=yes,menubar=yes, scrollbars=yes';
   
    title = title + "_" + Math.floor((Math.random() * 1000) + 1).toString()
    if(width)
    {
        var left = (screen.width/2)-(480/2);
        windowAttr += ',width='+width+',left='+left;
    }
    if(height)
        windowAttr += ',height='+height;
    else
        windowAttr += ',height=600';
        
   
  
    var myWindow = window.open(url, title, windowAttr);;
    
    if (myWindow == undefined) {
        return false;
    }else{
        if (content) {
            myWindow.document.write('<html><head><title>' + title + '</title></head><body></body></html>');
            $(myWindow.document.body).html(content);
        }

        // myWindow.document.close();
        myWindow.focus();
        if (print == true) {
            myWindow.print();
        }
        if (close == true) {
            myWindow.close();
        }
    }

    return true;
}

function GetMonth(id=null){
    var month = [];
    month[1] = "Januari";
    month[2] = "Februari";
    month[3] = "Maret";
    month[4] = "April";
    month[5] = "Mei";
    month[6] = "Juni";
    month[7] = "Juli";
    month[8] = "Agustus";
    month[9] = "September";
    month[10] = "Oktober";
    month[11] = "November";
    month[12] = "Desember";

    if(id){
        month = month[id];
    }

    return month;
}

function GetDay(id=null){
    var day = [];
    day[1] = "Senin";
    day[2] = "Selasa";
    day[3] = "Rabu";
    day[4] = "Kamis";
    day[5] = "Jumat";
    day[6] = "Sabtu";
    day[7] = "Minggu";

    if(id){
        day = day[id];
    }

    return day;
}

function GetDayMin(id=null){
    var day = [];
    day[1] = "Sn";
    day[2] = "Sl";
    day[3] = "Rb";
    day[4] = "Kms";
    day[5] = "Jmt";
    day[6] = "Sbt";
    day[7] = "Mg";

    if(id){
        day = day[id];
    }

    return day;
}

function checkMin(el, add='') {
    if(el.hasAttribute('min')){
        console.log(el.getAttribute('min'));
        if(parseInt(el.value) < parseInt(el.getAttribute('min'))){
            el.value = el.getAttribute('min');
            alert("Jumlah tidak bisa kurang dari "+el.getAttribute('min')+"."+add);
        }
    }

}